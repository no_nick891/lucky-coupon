! function(){
	
	function CookieAgreement() {
		
		this.init = function() {
			var accepted = getCookie('cookieAccepted'),
				cookieButton = document.querySelector('.accept-cookie'),
				cookieBar = document.querySelector('.cookie-agreement');
			if (!accepted) {
				cookieBar.style.display = 'flex';
			}
			cookieButton.addEventListener('click', function(e){
				setCookie('cookieAccepted', 1);
				cookieBar.style.display = 'none';
			});
		}
		
	}
	
	var cookie = new CookieAgreement();
	cookie.init();
	
	function setCookie(cookieName, cookieValue, expirationDays) {
		expirationDays = expirationDays ? expirationDays : 365;
		var domain = '.' + window.location.hostname,
			d = new Date();
		d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
		var expires = 'expires='+d.toUTCString();
		document.cookie = cookieName + '=' + cookieValue + ';' + expires + ';' + 'domain=' + domain + ';path=/';
	}
	
	function getCookie(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}
}();