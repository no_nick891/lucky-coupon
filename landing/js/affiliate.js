! function(){
	
	function Affiliate() {
		
		this.init = function() {
			var ref = this.getQueryRef();
			if (ref) {
				setCookie('referral', ref);
			}
		};
		
		this.getQueryRef = function() {
			var queryString = window.location.search.replace('?', ''),
				queryArray = queryString.split('&'),
				paramsObjects = queryArray.map(this.splitNameValue),
				object = this.getParamsAsObject(paramsObjects),
				result = false;
			if (object.hasOwnProperty('ref')) {
				result = object.ref;
			}
			return result;
		};
		
		this.splitNameValue = function(value) {
			var arr = value.split('='),
				obj = {};
			obj[arr[0]] = arr[1];
			return obj;
		};
		
		this.getParamsAsObject = function(paramsObjects) {
			var object = {};
			for(var index in paramsObjects) {
				for (var name in paramsObjects[index]) {
					object[name] = paramsObjects[index][name];
				}
			}
			return object;
		};
	}
	
	var affiliate = new Affiliate();
	affiliate.init();
	
	function setCookie(cookieName, cookieValue, expirationDays) {
		expirationDays = expirationDays ? expirationDays : 365;
		var domain = '.' + window.location.hostname,
			d = new Date();
		d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
		var expires = 'expires='+d.toUTCString();
		document.cookie = cookieName + '=' + cookieValue + ';' + expires + ';' + 'domain=' + domain + ';path=/';
	}
}();