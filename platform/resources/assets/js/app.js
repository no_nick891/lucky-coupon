require('./bootstrap');

import Vue from 'vue';

import App from './App.vue';

import router from './router/index';

import { store } from './store/index';

const app = new Vue({
    el: '#app',
    template: '<app></app>',
    components: { App },
    router,
    store
});