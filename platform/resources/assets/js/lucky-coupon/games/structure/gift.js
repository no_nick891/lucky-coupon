export const giftsObject = {
	
	getArrayWithCoupons: function(coup, coupInd, couponsObj) {
		let couponObj = this.getItem(coup);
		couponsObj.push(couponObj);
		return couponsObj;
	},
	
	getItem: function(coupon) {
		let dataAttributes = this.getDataAttributes(coupon);
		dataAttributes.value = encodeURI(dataAttributes.value);
		return {
			'tagName': 'div',
			'className': 'gift',
			'style': {'opacity': 0},
			'data': dataAttributes,
			'meta': {'id': coupon.id, 'temp_id': coupon.temp_id}
		};
	},
	
	getDataAttributes: function (coupon) {
		let data = {};
		for(let prop in coupon) {
			if (this.couponConstructCondition(coupon, prop)) {
				data[prop] = coupon[prop];
			}
		}
		return data;
	},
	
	couponConstructCondition: function (coup, prop) {
		return typeof coup[prop] !== 'undefined'
			&& prop !== 'code'
			&& prop !== 'updated_at'
			&& prop !== 'created_at';
	},
	
};