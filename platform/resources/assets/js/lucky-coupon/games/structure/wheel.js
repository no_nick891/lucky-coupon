export const wheelObject = {
	getArrayWithCoupons: function (coup, coupInd, couponsObj) {
		let couponObj = this.getItem(coup, coupInd);
		couponsObj.push(couponObj);
		return couponsObj;
	},
	
	getItem: function(coupon, coupInd) {
		let dataAttributes = this.getDataAttributes(coupon);
		if (dataAttributes.value.search('%25|%20') === -1) {
			dataAttributes.value = encodeURI(dataAttributes.value);
		}
		dataAttributes.stop = Math.abs(coupInd - 5) * 60 + 30;
		// (Math.floor(Math.random() * 56) + 3)
		return {
			'tagName': 'div',
			'className': 'slice',
			'data': dataAttributes,
			'meta': {'id': coupon.id, 'temp_id': coupon.temp_id}
		};
	},
	
	getDataAttributes: function (coupon) {
		let data = {};
		for(let prop in coupon) {
			if (this.couponConstructCondition(coupon, prop)) {
				data[prop] = coupon[prop];
			}
		}
		return data;
	},
	
	couponConstructCondition: function (coup, prop) {
		return typeof coup[prop] !== 'undefined'
			&& prop !== 'code'
			&& prop !== 'updated_at'
			&& prop !== 'created_at';
	},
};