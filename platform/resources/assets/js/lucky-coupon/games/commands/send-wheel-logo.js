import {file} from '../../../helpers/file';

export default {
	
	handle(targetFile, context, fileName) {
		let sendFile = this.sendImage.bind(context),
			beforeLoad = this.beforeBackgroundLoad.bind(context),
			fileValidationFailed = this.afterValidationFailed.bind(context),
			fileValidationSuccess = this.afterValidationSuccess.bind(context);
		file.name = fileName ? fileName : file.name;
		file.handle(targetFile, {
			sendFile, beforeLoad,
			fileValidationFailed,
			fileValidationSuccess
		});
	},
	
	beforeBackgroundLoad() {
		this.loadingData = true;
		this.$store.state._settings.blockSave = true;
	},
	
	afterValidationFailed() {
		this.loadingData = false;
	},
	
	afterValidationSuccess(e, newFile) {
		this.image = e.target.result;
		this.$store.state._settings.settings.wheelLogoImage.wheelLogo = newFile;
		this.$store.state._settings.blockSave = false;
		this.newFile = true;
	},
	
	sendImage() {
		let fileData = {
			settingName: 'wheelLogoImage',
			imageSetting: 'wheelLogo',
			opacityName: 'opacityWheelLogo',
			fileName: 'wheel_logo_image',
			methodName: 'updateWheelLogoImage'
		};
		file.sendFile.call(this, fileData);
	}
	
}