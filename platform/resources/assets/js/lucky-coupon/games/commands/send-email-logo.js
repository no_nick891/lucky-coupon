import Flash from '../../../helpers/flash';

import {file} from '../../../helpers/file';

export default {
	
	handle(targetFile, context, fileName) {
		let sendFile = this.sendImage.bind(context),
			beforeLoad = this.beforeBackgroundLoad.bind(context),
			fileValidationFailed = this.afterValidationFailed.bind(context),
			fileValidationSuccess = this.afterValidationSuccess.bind(context);
		file.name = fileName ? fileName : file.name;
		file.handle(targetFile, {
			sendFile, beforeLoad,
			fileValidationFailed,
			fileValidationSuccess
		});
	},
	
	beforeBackgroundLoad() {
		this.loadingData = true;
		this.$store.state._settings.blockSave = true;
	},
	
	afterValidationFailed() {
	
	},
	
	afterValidationSuccess(e, newFile) {
		let _settings = this.$store.state._settings,
			behavior = _settings.settings.behavior;
		this.image = e.target.result;
		behavior.receiveEmailLogoImage = newFile;
		_settings.blockSave = false;
	},
	
	sendImage() {
		let formData = new FormData(), image, imageArray,
			_settings = this.$store.state._settings,
			behavior = _settings.settings.behavior;
		image = behavior.receiveEmailLogoImage;
		imageArray = ['logo_image', image];
		if (typeof image !== 'string') {
			imageArray.push(image.name);
		}
		formData.append(...imageArray);
		formData.append('game_id', this.$store.state._games.game.id);
		this.$store.dispatch('updateLogoImage', formData)
			.then(response => {
				let data = response.data;
				if (data.file) {
					let url = file.getTempUrl(data.file);
					this.$store.state._settings.settings.behavior.receiveEmailLogoImage = url;
					this.image = url;
				}
				this.loadingData = false;
			})
			.catch(Flash.setAjaxError.bind(Flash))
	},
};