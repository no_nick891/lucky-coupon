import gamePreview from '../../../helpers/game-preview.js';

import {defaultGameSettings} from '../../../helpers/templates/default/settings';

import {wheelGameSettings} from '../../../helpers/templates/wheel/settings';

import {slotGameSettings} from '../../../helpers/templates/slot/settings';

import {giftGameSettings} from '../../../helpers/templates/gift/settings';

import {defaultGameCoupons} from '../../../helpers/templates/default/coupons';

import {wheelGameCoupons} from '../../../helpers/templates/wheel/coupons';

import {slotGameCoupons} from '../../../helpers/templates/slot/coupons';

import {giftGameCoupons} from '../../../helpers/templates/gift/coupons';

export default {
	
	handle(game) {
		gamePreview.clearAllGlobalVariables();
		let script = gamePreview.getGameScript(
			this.getSettings(game.type),
			this.getCoupons(game.type),
			game.site_id, game, []
		);
		document.body.appendChild(script);
	},
	
	getSettings(type) {
		switch(type) {
			case 'coupon': return defaultGameSettings;
			case 'wheel': return wheelGameSettings;
			case 'slot': return slotGameSettings;
			case 'gift': return giftGameSettings;
		}
	},
	
	getCoupons(type) {
		switch(type) {
			case 'coupon': return defaultGameCoupons;
			case 'wheel': return wheelGameCoupons;
			case 'slot': return slotGameCoupons;
			case 'gift': return giftGameCoupons;
		}
	}
	
};