export default {
	
	getSchema(settings) {
		return this.getSlicesColors(this.getColors(settings));
	},
	
	getSlicesColors(colors) {
		let slices = [];
		for (let index in colors) {
			let color = '';
			if (!colors.hasOwnProperty(index)) continue;
			if (typeof colors[index] === 'string') {
				color = colors[index];
			} else {
				color = colors[index].hex;
			}
			slices.push({'name': index, 'hex': color});
		}
		return slices;
	},
	
	getColors(settings) {
		let colors = {},
			design = settings.colors.design;
		if (design.colorize.active === '1'
			|| design.custom.active === '1'
		) {
			colors = design.custom.schema;
		} else {
			colors = design.present.schema[parseInt(design.present.active) - 1];
		}
		return colors;
	}
}