import { Converter } from '../../../helpers/converter';

export default {
	
	vue: {},
	
	redirectUrl: undefined,
	
	handle: function(redirectUrl) {
		this.redirectUrl = redirectUrl;
		this.setGameVariables();
		this.gameCreated();
		this.updateGame(true);
		this.redirectGames();
	},
	
	setGameVariables: function() {
		if (this.vue.$store.state._games.justCreated) {
			this.vue.game.active = true;
			this.vue.game.new_game = true;
			this.vue.$store.state._games.justCreated = false;
		}
	},
	
	gameCreated: function() {
		this.vue.game.is_created = 1;
		this.vue.$store.commit('updateGameInfo', this.vue.game);
	},
	
	updateGame: function(modal) {
		let game = this.vue.game;
		this.vue.$store.dispatch('updateGame', game);
		let convertedSettings = Converter.convertSettings(this.vue.$store.state._settings.settings),
			coupons = this.vue.$store.state._coupons.coupons,
			updateCoupons = this.getEscapedCoupons(coupons);
		this.vue.$store.state._settings.changes = false;
		this.vue.$store.dispatch('updateSettings', {settings: convertedSettings, game: game, modal: modal});
		this.vue.$store.dispatch('updateCoupons', {game: game, coupons: updateCoupons});
	},
	
	getEscapedCoupons: function(coupons) {
		let updateCoupons = [];
		for (let coupon of coupons) {
			updateCoupons.push(this.escapeString(coupon));
		}
		return updateCoupons;
	},
	
	escapeString: function(coupon) {
		let updatedCoupon = JSON.parse(JSON.stringify(coupon));
		updatedCoupon.value = encodeURI(updatedCoupon.value);
		return updatedCoupon;
	},
	
	redirectGames() {
		this.vue.$router.push(
			typeof this.redirectUrl === 'undefined' ?
				'/site/' + this.vue.$store.state._sites.site.id + '/games' :
				this.redirectUrl
		);
	},
}