export default {
	settingsText: {},
	
	handle(coupons, settingsText) {
		this.settingsText = settingsText;
		return this.getCouponsTextChanged(coupons);
	},
	
	getCouponsTextChanged: function (coupons) {
		let updCoupons = [];
		for (let coupon of coupons) {
			updCoupons.push(this.getCouponEventData(coupon));
		}
		updCoupons.push(this.getButtonEventData());
		return updCoupons;
	},
	
	getCouponEventData: function (coupon) {
		return {
			chance: coupon.chance,
			game_id: coupon.game_id,
			id: coupon.id,
			temp_id: undefined,
			type: coupon.type,
			value: coupon.value,
			line: this.getLines(coupon.type)
		};
	},
	
	getButtonEventData() {
		return {
			id: 0,
			line: this.getLines('button')
		};
	},
	
	getLines(type) {
		let field = '', line1 = '', line2 = '';
		if (type === 'free shipping') {
			field = 'freeShipping';
			line1 = 'free';
			line2 = 'shipping';
		} else if (type === 'free product') {
			field = 'freeProduct';
			line1 = 'free';
			line2 = 'product';
		} else if (type === 'cash') {
			field = 'cash';
		} else if (type === 'discount') {
			field = 'coupon';
		} else if (type === 'button') {
			field = 'button';
			line1 = 'startLine';
			line2 = 'endLine';
		}
		let object = this.settingsText.startScreen[field],
			result = [];
		if (typeof object === 'string') {
			result = ['', object]
		} else {
			result = [object[line1], object[line2]];
		}
		return result;
	},
};