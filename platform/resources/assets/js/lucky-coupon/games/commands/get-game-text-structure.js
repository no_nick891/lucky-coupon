import {getData} from '../../../helpers/data';

export default {
	
	data: {
		lang: 'en',
		gameType: 'default',
		content: {},
		gameTexts: {},
	},
	
	handle() {
		return {
			startScreen: this.getStartScreen(),
			playScreen: this.getPlayScreen(),
			winScreen: this.getWinScreen(),
			bar: this.getBar(),
			trigger: getData(this.data, 'content.trigger', null)
		};
	},
	
	isGift: function () {
		return this.data.gameType === 'gift';
	},
	
	getStartScreen() {
		let freeShipping = this.data.gameTexts.coupons.freeShipping[this.data.lang].split(' '),
			freeProduct = this.data.gameTexts.coupons.freeProduct[this.data.lang].split(' '),
			description = getData(this.data.content, 'description', null),
			result = {};
		result.title = this.convertString(this.data.content.title);
		if (description !== null) {
			result.description = this.convertString(description);
		}
		result.email = this.convertString(this.data.content.email);
		result.userName = this.convertString('Enter your full name');
		result.button = this.getButtonText();
		if (this.data.gameType === 'default') {
			result.coupon = this.convertString(this.data.gameTexts.main.coupon);
			result.freeShipping = {
				free: this.convertString(freeShipping[0]),
				shipping: this.convertString(freeShipping[1])
			};
			result.cash = this.convertString(this.data.gameTexts.main.cash);
			result.freeProduct = {
				free: this.convertString(freeProduct[0]),
				product: this.convertString(freeProduct[1])
			};
		}
		result.note = this.convertString(this.data.content.note);
		result.gdpr = this.convertString(this.data.content.gpdr);
		return result;
	},
	
	getButtonText() {
		switch (this.data.gameType) {
			case 'gift': return this.convertString(this.data.gameTexts.main.play_gift);
			case 'wheel': return this.convertString(this.data.gameTexts.main.play_wheel);
			case 'slot': return this.convertString(this.data.gameTexts.main.play);
			case 'coupon': default: return {
				startLine: this.convertString(this.data.gameTexts.main.start),
				endLine: this.convertString(this.data.gameTexts.main.game)
			};
		}
	},
	
	getPlayScreen() {
		return {
			waitingForResults: this.convertString(
				this.isGift()
				? this.data.gameTexts.changeable.pickGift[this.data.lang]
				: this.data.gameTexts.main['play-description']
			)
		};
	},
	
	getWinScreen() {
		let result = {};
		if (this.data.gameType !== 'gift') {
			result.congratulations = this.convertString(this.data.gameTexts.main.congratulation);
			result.youGot = this.convertString(this.data.gameTexts.main['you-get']);
		}
		
		if (['wheel', 'gift'].indexOf(this.data.gameType) === -1) {
			result.discount = this.convertString(this.data.gameTexts.main.discount);
		}
		result.yourDiscountCodeIs = this.convertString(this.data.gameTexts.main['discount-code']);
		result.button = this.convertString(this.data.gameTexts.main['continue-use-discount']);
		result.note = this.convertString(this.data.gameTexts.main['guide-to-use']);
		return result;
	},
	
	getBar() {
		return {
			your: this.convertString(this.data.gameTexts.main['bar-your']),
			couponCode: this.convertString(this.data.gameTexts.main['bar-coupon-code']),
			reservedFor: this.convertString(this.data.gameTexts.main['bar-reserved-for']),
			copyCode: this.convertString(this.data.gameTexts.main['bar-copy-code']),
			copied: this.convertString(this.data.gameTexts.bar['copied'][this.data.lang]),
			failedCopy: this.convertString(this.data.gameTexts.bar['failedCopy'][this.data.lang])
		};
	},
	
	convertString(string) {
		return string
			? decodeURIComponent(string.replace(/%27/g, '"').replace(/%22/g, "'"))
			: '';
	}
}