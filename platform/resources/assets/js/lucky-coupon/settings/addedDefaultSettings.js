export const addedDefaultSettings = {
	'giftImage': 'gift1',
	'triggerPosition': 'right',
	'behavior.countDownTimeTimeMin': 15,
	'behavior.showPlayGameTrigger': {'yes': '0', 'no': '1'},
	'behavior.collectEmailFromUsers': {'yes': '1', 'no': '0'},
	'behavior.collectNameFromUsers': {'yes': '0', 'no': '1'},
	'behavior.howDoYouWantYourCustomerToReceiveTheCoupon': {
		'winnigGameScreen': '1',
		'sendTroughEmail': '0',
		'winningGameScreenAndEmail': '0'
	},
	'behavior.receiveEmailLogoImage': '',
	'behavior.makeGPDRCompliance': '0',
	'behavior.startDisplayTheGame.onSpecialPagePlace': '0',
	'behavior.countDownTime': '1',
	'behavior.isPoweredByVisible': '1',
	'behavior.collectEmailWithRecart': '0',
	'text.startScreen.userName': 'Enter your full name',
	'behavior.couponReceiveEmailText': {
		"from": "storename@gmail.com",
		"discountCodeSentToYourEmail": "Discount code sent to your email",
		"couponCode": "Your coupon code from",
		"congratulation": "Congratulations",
		"youWon": "YOU WON A",
		"yourDiscount": "Your Discount Code Is:",
		"dontForgetToApplyYourCouponToTheRelevantFieldDoingTheCheckoutProcess": "Don't forget to apply your coupon code to the relevant field doing the checkout process",
		"goToSite": "Go to site",
		"freeProduct": "free product",
		"freeShipping": "Free Shipping",
		"cash": "Cash",
		"discount": "Discount"
	},
	'behavior.couponReceiveEmailText.storeName': 'Your store name',
	'behavior.couponReceiveEmailText.from': 'storename@gmail.com',
	'behavior.whereShouldTheGameAppear': [],
	'behavior.continueUseDiscount': {
		'value': 0,
		'url': '',
		'target': 1
	},
	'behavior.continueUseDiscount.target': 1
};

var test = {
	"position": "center",
	"triggerPosition": "right",
	"font": "%27Open Sans%27, Helvetica, Arial, sans-serif",
	"animation": "fadeIn",
	"backgroundImage": {"image": "\/img\/game\/default.jpg?v=4", "opacity": "0.5"},
	"colors": {
		"background": "#000",
		"coverText": "#fff",
		"couponText": "#000",
		"button": "#f30927",
		"buttonText": "#fff"
	},
	"giftImage": "gift1",
	"content": {
		"title": "You%22ve been Chosen! For a shot at a BIG discount",
		"description": "Enter your email address to find out if you%22ve the winner",
		"email": "Enter your email address",
		"note": "From time to time, we may send you more special offers. You can unsubscribe at any time.",
		"gpdr": "I agree to subscribe to the mailing list",
		"trigger": "Win A Prize"
	},
	"behavior": {
		"language": "en",
		"showPlayGameTrigger": {"yes": "1", "no": "0"},
		"collectEmailFromUsers": {"yes": "1", "no": "0"},
		"collectEmailWithRecart": "0",
		"collectNameFromUsers": {"yes": "0", "no": "1"},
		"chooseInputFieldType": {"emailField": "1", "facebookMessengerField": "0"},
		"startDisplayTheGame": {
			"atOnce": "0",
			"underTheFollowingConditions": {
				"whenTheUserIsLivingTheWebsite": "1",
				"whenTheUserReachesPercentOfThePage": {"value": "1", "percent": "100"},
				"afterSeconds": {"value": "1", "seconds": "10"}
			},
			"onSpecialPagePlace": "0"
		},
		"frequency": {
			"onEveryPageView": "1",
			"notMoreThanOnceEveryNumberTimePerUser": {
				"value": "0",
				"number": "1",
				"time": {"day": "1", "week": "0", "month": "0"}
			}
		},
		"stopToDisplayTheGame": {
			"never": "0",
			"underTheFollowingConditions": {
				"afterTheUserPerformsTheAction": "1",
				"afterShowingItNumberTimesToTheUser": {"value": "1", "number": "1"}
			}
		},
		"howDoYouWantYourCustomerToReceiveTheCoupon": {
			"winnigGameScreen": "1",
			"sendTroughEmail": "0",
			"winningGameScreenAndEmail": "0"
		},
		"couponReceiveEmailText": {
			"from": "storename@gmail.com",
			"discountCodeSentToYourEmail": "Discount code sent to your email",
			"couponCode": "Your coupon code from",
			"congratulation": "Congratulation",
			"youWon": "YOU WON A",
			"yourDiscount": "Your Discount Code Is:",
			"dontForgetToApplyYourCouponToTheRelevantFieldDoingTheCheckoutProcess": "Don%22t forget to apply your coupon code to the relevant field doing the checkout process",
			"goToSite": "Go to site",
			"freeProduct": "free product",
			"freeShipping": "Free Shipping",
			"cash": "Cash",
			"discount": "Discount",
			"storeName": "Your store name"
		},
		"whereShouldTheGameAppear": [{"type": "1", "url": "*"}, {"type": "0", "url": ""}],
		"countDownTimeTimeMin": "15",
		"countDownTime": "1",
		"makeGPDRCompliance": "0",
		"isPoweredByVisible": "1",
		"receiveEmailLogoImage": "",
		"continueUseDiscount": {"value": [[]], "url": "", "target": "1"}
	},
	"text": {
		"startScreen": {
			"title": "You%22ve been Chosen! For a shot at a BIG discount",
			"description": "Enter your email address to find out if you%22ve the winner",
			"email": "Enter your email address",
			"userName": "Enter your full name",
			"button": {"startLine": "Start", "endLine": "GAME"},
			"coupon": "Coupon",
			"freeShipping": {"free": "Free", "shipping": "Shipping"},
			"cash": "Cash",
			"freeProduct": {"free": "Free", "product": "Product"},
			"note": "From time to time, we may send you more special offers. You can unsubscribe at any time.",
			"gdpr": "I agree to subscribe to the mailing list"
		},
		"playScreen": {"waitingForResults": "Let%22s see what you won..."},
		"winScreen": {
			"congratulations": "Congratulations",
			"youGot": "You got a",
			"discount": "Discount",
			"yourDiscountCodeIs": "Your Discount Code Is:",
			"button": "Continue & Use Discount",
			"note": "In order to use this discount add it to the relevant field in checkout"
		},
		"bar": {
			"your": "Your",
			"couponCode": "coupon code",
			"reservedFor": "is reserved for",
			"copyCode": "Copy Code",
			"copied": "Copied",
			"failedCopy": "Failed to copy"
		},
		"trigger": "Win A Prize"
	}
};