export default {
	handle(isPaidUser) {
		let eventData = isPaidUser ? 'purchaseComplete' : 'registrationComplete',
			event = { 'event': eventData };
		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push(event);
		let isRegistered = eventData === 'registrationComplete';
		let facebookEvent = isRegistered ? 'CompleteRegistration' : 'Purchase';
		try{ fbq('track', facebookEvent); } catch(e) {}
		let google = isRegistered ? 'Free' : 'Paid';
		try {
			ga('send', {
				hitType: 'event',
				eventCategory: 'Registration' + google,
				eventAction: 'Registration',
				eventLabel: google + 'PlanUser'
			});
		} catch(e){}
	}
}