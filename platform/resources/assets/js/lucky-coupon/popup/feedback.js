import {getCookie} from '../../helpers/cookies';

export default {
	vue: {},
	handle() {
		if (getCookie('adminArea') !== '1' && !this.vue.user.is_admin) {
			this.vue.showFeedback = true;
			this.vue.$store.dispatch('saveUserProfileData', {
				id: this.vue.user.id,
				is_first_time: 0,
				name: this.vue.user.name,
				email: this.vue.user.email,
				partly: true
			});
		}
	},
}