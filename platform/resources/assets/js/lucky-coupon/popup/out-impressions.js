import subscriber from '../../helpers/subscriber';

export const outImpressions = {
	
	vueInst: {},
	
	user: {},
	
	plans: {},
	
	popup: {},
	
	shopifySubscriber: {},
	
	init(vueInst, user, popup) {
		this.vueInst = vueInst;
		this.user = user;
		this.popup = popup;
		this.plans = user.plans;
		this.shopifySubscriber = user.shopifySubscription;
	},
	
	showIfOutImpressions() {
		if (this.isUserOutOfImpressions()) {
			this.show();
		}
	},
	
	isUserOutOfImpressions() {
		return subscriber.isPlanRichMaxImpr(this.user, this.plans, this.shopifySubscriber);
	},
	
	show() {
		this.popup.open({
			saveButtonHandler: this.redirectUpgrade.bind(this.vueInst)
		});
	},
	
	redirectUpgrade() {
		this.$router.push({path: '/upgrade'});
	}
	
};