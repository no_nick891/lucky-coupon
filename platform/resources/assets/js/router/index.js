import Vue from 'vue';

import VueRouter from 'vue-router';

import VueClipboard from 'vue-clipboard2';

import SiteList from '../views/Sites/List.vue';

import SiteDetail from '../views/Sites/Detail.vue';

import SiteIntegrations from '../views/Sites/Integrations.vue';

import GamesList from '../views/Games/List/Index.vue';

import GameDetail from '../views/Games/Detail/Index.vue';

import GameAdd from '../views/Games/Add.vue';

import PlansList from '../views/Plans/List.vue';

import SubscriptionForm from '../views/Subscriptions/Form.vue';

import Profile from '../views/Profile/Index.vue';

import SiteGameCreate from '../views/Games/Create.vue';

import Affiliate from '../views/Affiliate/Index.vue'

import Loading from '../views/Loading/Index.vue';

Vue.use(VueRouter);

Vue.use(VueClipboard);

const router = new VueRouter({
    routes: [
    	{path: '/sites', component: SiteList},
    	{path: '/site/:site_id', component: SiteDetail},
    	{path: '/site/:site_id/games', component: GamesList},
		{path: '/site/:site_id/integrations', component: SiteIntegrations},
		{path: '/site/:site_id/game/:game_id', component: GameDetail},
		{path: '/site/game/add', component: GameAdd},
		{path: '/upgrade', component: PlansList},
		{path: '/upgrade/:plan_id', component: SubscriptionForm},
		{path: '/profile', component: Profile},
		{path: '/game/create', component: SiteGameCreate},
		{path: '/affiliate', component: Affiliate},
		{path: '/loading', component: Loading}
    ]
});

export default router;