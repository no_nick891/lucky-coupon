import {getData} from './data';

export default {
	
	trialDays: 14,
	
	hasSiteCreateAccess: function(user, sitesCount) {
		if (user.is_admin) return true;
		if (getData(user, 'isracardSubscription.description') === 'Premium Plus') {
			return true;
		} else if (
			this.hasJvzooSub(user, 'JVZoo Premium Plus')
			&& sitesCount <= 2
		) {
			return true;
		} else if (this.isLifeTimeAccess(user, sitesCount)) {
			return true;
		} else if (sitesCount === 0) {
			return true;
		} else if (this.isTrialActive(user) && sitesCount === 0) {
			return true;
		}
		return false;
	},
	
	hasTrialTextAccess: function (user) {
		if (user.is_admin) return false;
		if (user.app_id) return false;
		if (user.hasOwnProperty('created_at')) {
			return !user.isracardSubscription
				&& !user.otherSubscription
				&& this.getDaysDifference(user.created_at) >= 0;
		}
		return false;
	},
	
	isTrialActive: function (user) {
		return this.getDaysDifference(user.created_at) <= this.trialDays;
	},
	
	hasTrialOrSub: function(user) {
		if (user.is_admin) {
			return true;
		}
		if (user.hasOwnProperty('created_at')) {
			return user.otherSubscription
				|| user.isracardSubscription
				|| this.isTrialActive(user);
		}
		return false;
	},
	
	getDaysTrialsLeft: function (createdAt) {
		let days = this.trialDays - this.getDaysDifference(createdAt);
		if (days < 0) {
			return 0;
		}
		return days;
	},
	
	getIsTrialAccount: function (created) {
		let isTrialAccount = false;
		if (created) {
			let difference = this.getDaysDifference(created);
			if (difference >= this.trialDays) {
				isTrialAccount = true;
			}
		}
		return isTrialAccount;
	},
	
	getDaysDifference: function (created) {
		let dateArray = this.parseDateToArray(created);
		dateArray[1] = dateArray[1] - 1;
		let dateCreateObj = new Date(...dateArray),
			now = new Date(),
			day = 60 * 1000 * 60 * 24,
			daysDiffInMs = Math.abs(now.getTime() - dateCreateObj.getTime());
		return Math.round(daysDiffInMs / (day));
	},
	
	parseDateToArray: function (dateString) {
		return dateString
			.split(/(\d{1,4})-(\d{1,2})-(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})/g)
			.filter(Boolean)
			.map(item => parseInt(item));
	},
	
	hasOtherSubscription(user) {
		return getData(user, 'otherSubscription.active', false);
	},
	
	hasJvzooSub(user, subName) {
		return user.hasOwnProperty('otherSubscription')
			&& this.getCurrentPlanName(user, 'otherSubscription') === subName
	},
	
	getCurrentPlanName(user, subscription) {
		if (!user[subscription]) return '';
		for (let plan of user.plans) {
			if(user[subscription].hasOwnProperty('plan_id') && plan.id === user[subscription].plan_id) {
				return plan.name;
			}
		}
	},
	
	isPlanRichMaxImpr(user, plans, shopifySubscriber) {
		let conditionCounter = parseInt(this.getCurrentPlanFieldValue('conditions', plans, shopifySubscriber)),
			impressions = user && user.shopify_impressions ? parseInt(user.shopify_impressions) : 0;
		return impressions >= conditionCounter;
	},
	
	getCurrentPlanFieldValue: function (fieldName, plans, shopifySubscriber) {
		let result = '';
		for (let plan of plans) {
			if (shopifySubscriber && plan.id === shopifySubscriber.plan_id) {
				return plan[fieldName];
			}
		}
		return result;
	},
	
	isPlanName: function (planName, plan, planId) {
		return plan.id === planId && plan.name === planName;
	},
	
	isNewShopifyFreePlan: function(user) {
		if ('shopifySubscription' in user && user.shopifySubscription) {
			let planId = user.shopifySubscription.plan_id;
			for (let plan of user.plans) {
				if (
					this.isPlanName('Starter', plan, planId)
					|| (
						this.isPlanName('Basic', plan, planId)
						&& plan.description.indexOf('no integrations') > -1
					)
				) {
					return true;
				}
			}
		}
		return false;
	},
	
	isLifeTimeAccess: function(user, sitesCount) {
		if (this.getCurrentPlanName(user, 'otherSubscription') !== 'Life time access') return false;
		let licenseCount = 0;
		if (user.license_keys) {
			for (let license of user.license_keys) {
				if (license.license_key) {
					licenseCount++;
				}
			}
		}
		return sitesCount < (licenseCount * 10);
	},
	
	isPaidUser: function(user) {
		return Boolean(this.getCurrentPlanName(user, 'otherSubscription')) ||
			Boolean(this.getCurrentPlanName(user, 'isracardSubscription')) ||
			(Boolean(this.getCurrentPlanName(user, 'shopifySubscription')) &&
			Boolean(this.getCurrentPlanName(user, 'shopifySubscription') !== 'Starter'));
	}
};