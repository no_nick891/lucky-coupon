export default {

	state: {

		success: null,

		error: null,

		string: false,
		
		timeId: null

	},

	setSuccess(message) {
		
		this.removeSuccess();
		
		this.state.success = message;
		
		this.state.string = typeof(message) !== 'string';
		
		this.timeId = setTimeout(function() {
			
			this.removeSuccess();
			
			this.timeId = null;
			
		}.bind(this), 3000);

	},

	setError(message) {
		
		this.removeError();
		
		this.state.error = message;
		
		this.state.string = typeof(message) !== 'string';
		
		this.timeId = setTimeout(function() {
			
			this.removeError();
			
			this.timeId = null;
			
		}.bind(this), 10000);

	},

	setAjaxError(err) {
		if (err.hasOwnProperty('response') && err.response.hasOwnProperty('data')) {
			this.setError(err.response.data);
		} else {
			console.error(err);
			this.setError('Client code error. Please mail to support about this problem.');
		}
	},
	
	removeSuccess() {

		this.state.success = null;

	},

	removeError() {

		this.state.error = null;

	},
	
	clearMessages() {
		
		this.removeSuccess();
		
		this.removeError();
		
	},
	
	clearTimeout() {
		
		if(this.timeId) {
			
			clearTimeout(this.timeId);
			
			this.timeId = null;
		}
		
	}

}