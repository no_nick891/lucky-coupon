import axios from 'axios';

export function post(url, data, headers) {

	let object = {
		method: 'POST',
		url: url,
		data: data
	};
	
	if(headers)
	{
		object.headers = headers;
	}
	
	return axios(object);

}

export function get(url, data) {

	return axios({
		method: 'GET',
		url: url,
		params: data
	});

}

export function del(url, id) {

	return axios({
		method: 'DELETE',
		url: url,
		data: id
	});
}

export function update(url, data) {

	return axios({
		method: 'PATCH',
		url: url,
		data: data
	});

}

export function add(url, data) {

	return axios({
		method: 'PUT',
		url: url,
		data: data
	});

}