export const slotsObject = {
	
	getArrayWithCoupons: function (coup, coupInd, couponsObj) {
		couponsObj.push(this.getRelItem(coup));
		return couponsObj;
	},
	
	getSlotReels: function (couponsObj) {
		return [{
			tagName: 'div',
			className: 'dotted',
			content: [
				this.getWithWrapper(couponsObj, 0),
				this.getWithWrapper(couponsObj, 1),
				this.getWithWrapper(couponsObj, 2)
			]
		}];
	},
	
	getWithWrapper: function(couponsArray, index) {
		let reelWrapper = this.getReelWrapperObject(index), reel;
		for (let coupon of couponsArray) {
			reelWrapper.content.push(coupon);
		}
		reel = this.getReelObject();
		reel.content.push(reelWrapper);
		return reel;
	},
	
	getReelWrapperObject: function (index) {
		return {
			'tagName': 'div',
			'className': 'reel-wrapper',
			'content': [],
			'style': {
				'top': (-110 * index) + 'px'
			}
		};
	},
	
	getRandomInt: function(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	
	getReelObject: function () {
		return {
			'tagName': 'div',
			'className': 'reel',
			'content': []
		};
	},
	
	getRelItem: function(coup) {
		let className;
		switch (coup.type) {
			case 'free shipping':
				className = 'shipping';break;
			case 'discount':
				className = 'coupon'; break;
			case 'cash':
				className = 'cash'; break;
		}
		return this.getItemObject(coup, className);
	},
	
	getItemObject: function(coup, type) {
		let coupon = {
			"tagName": "div",
			"className": "reel-item " + type,
			"data": this.getDataObject(coup),
			"style": {
				"background-color": coup.hasOwnProperty('color') ? coup.color : null
			}
		};
		if (type === 'shipping') {
			coupon.pure = true;
			coupon.content = '<img src="//'+ window.location.hostname + '/img/game/truck_slot.svg"/>';
		} else {
			let value = parseFloat(coup.value).toString();
			coupon.textNode = (type === 'coupon' ? value + '%' : '$' + value);
		}
		return coupon;
	},
	
	getDataObject: function(data) {
		let result = {};
		for(let index in data) {
			if (this.isExcluded(index)) {
				result[index] = data[index];
			}
		}
		return result;
	},
	
	isExcluded: function (index) {
		return index !== 'code'
			&& index !== 'created_at'
			&& index !== 'updated_at'
			&& index !== 'gravity';
	},

};