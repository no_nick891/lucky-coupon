export const script = {
	getScript(url){
		return ("&lt;script&gt;\
				\n	(function(w,i,d,g,e,t,s){t=i.createElement(g);\
				\n		t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t,s);\
				\n	})(window,document,'_lkda','script','" + url + "');\
				\n&lt;/script&gt;").replace(/(&lt;)/mg, '<').replace(/(&gt;)/mg, '>')
	}
};