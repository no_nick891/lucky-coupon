export default {
	
	makeHeader: function(property, isCapital = true) {
		let temp = property.replace(/([A-Z])/g, ' $1').split(' ');
		for(let i = 0; i < temp.length; i++) {
			temp[i] = temp[i].toLowerCase();
		}
		if (isCapital) {
			temp[0] = this.wordsToFirstUpper(temp[0]);
		}
		return temp.join(' ').trim();
	},
	
	wordsToFirstUpper: function (stringOfWords) {
		return stringOfWords.replace(/^./, function (str) {
			return str.toUpperCase();
		});
	}
	
};