export const clipboard = {
	doCopy(e) {
		let doc = $(document),
			textarea = doc.find('#copy-textarea');
		textarea.select();
		setTimeout(function() {
			this.$copyText(this.script)
				.then(function() {
					document.getSelection().removeAllRanges();
					doc.find('#copy-code').html('Copied');
				}.bind(this))
				.catch(function() {
					doc.find('#copy-code').html('Failed');
				}.bind(this));
		}.bind(this), 10);
	}
};