export const fileHelper = {
	
	bindOnLoad: function(file, handler) {
		let reader = new FileReader();
		reader.onload = handler;
		reader.readAsDataURL(file);
	},
	
	validateFile: function(e, fileName) {
		let file = fileHelper.getFile(e, fileName);
		if (fileHelper.getFileSize(file) > 10) return false;
		return file;
	},
	
	getFile: function(e, fileName) {
		let rawFile = e.target.result.split(','),
			extension = fileHelper.getFileExt(rawFile);
		fileName = fileName ? fileName + '.' + extension : 'background_image' + '.' + extension;
		return new File([rawFile[1]], fileName, {
			type: rawFile[0].replace('data:', ''),
			lastModified: new Date()
		});
	},
	
	getFileExt: function(rawFile){
		let mime = rawFile[0].split('/'),
			type = mime[1].split(';');
		return type[0].replace('+xml', '');
	},
	
	getFileSize: function (file) {
		return file.size / 1024 / 1024;
	}
	
};