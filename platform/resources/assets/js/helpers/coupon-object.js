import {couponTranslation} from './language/coupons';

import {getData} from '../helpers/data';

export const couponsObject = {
	
	text: {},
	
	lang: 'en',
	
	getArrayWithCoupons: function (coup, coupInd, couponsObj) {
		let fillerWrapper = false, result,
			couponObj = this.getCouponObject(coup);
		if (coupInd === '3') {
			fillerWrapper = this.getFillerCoupon();
		}
		result = ( fillerWrapper ? [couponObj, fillerWrapper] : [couponObj] );
		couponsObj.push(...result);
		return couponsObj;
	},
	
	getCouponObject: function (coup) {
		return this.getCouponWrapper(false, coup);
	},
	
	getFillerCoupon: function () {
		let fillerWrapper = this.getCouponWrapper(true);
		fillerWrapper.content = this.getEmptyCouponContent();
		return fillerWrapper;
	},
	
	/**
	 * @todo: think about templates for php and js at one place
	 */
	getCouponWrapper: function (filler = false, coupon) {
		var data = this.getCouponDataAttribute(coupon);
		return {
			'tagName': 'li',
			'meta': filler ? [] : {'id': coupon.id, 'temp_id': coupon.temp_id},
			'className': filler ? 'filler' : 'prize-item coupon',
			'data': filler ? [] : data,
			'content': filler ? [] : this.getCouponContent(coupon),
			'pure': !filler
		};
	},
	
	getCouponDataAttribute: function (coup) {
		var data = {};
		for (let prop in coup) {
			if (this.couponConstructCondition(coup, prop)) {
				data[prop] = coup[prop];
			}
		}
		return data;
	},
	
	getCouponContent: function (coup) {
		let result;
		switch (coup.type) {
			case 'discount':
				result = this.getItem(parseFloat(coup.value) + '%', this.getText('startScreen.coupon', 'coupon'), 'coupon');
				break;
			case 'cash':
				result = this.getItem('$' + parseFloat(coup.value), this.getText('startScreen.cash', 'cash'), coup.type);
				break;
			case 'free product':
				result = this.getFreeCoupon('product');
				break;
			case 'free shipping':
				result = this.getFreeCoupon('shipping');
				break;
		}
		return result;
	},
	
	getFreeCoupon: function (type) {
		let fullType = 'free' + this.ucFirst(type),
			free = this.translation(fullType).split(' ');
		let pathToFree = 'startScreen.free' + this.ucFirst(type);
		return this.getItem(
			getData(this.text, pathToFree + '.free', free[0]),
			getData(this.text, pathToFree + '.' + type, free[1]),
			fullType
		);
	},
	
	ucFirst: function (string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	},
	
	getItem: function (upperText, lowerText, type) {
		return '<div class="right"></div>' +
			'<div class="left"></div>' +
			'<div class="upper-text" data-text="startScreen.' + type + '.0">' + upperText + '</div>' +
			'<div class="lower-text" data-text="startScreen.' + type + '.1">' + lowerText + '</div>';
	},
	
	couponConstructCondition: function (coup, prop) {
		return typeof coup[prop] !== 'undefined'
			&& prop !== 'code'
			&& prop !== 'updated_at'
			&& prop !== 'created_at';
	},
	
	getEmptyCouponContent: function () {
		return [{
			'tagName': 'button',
			'className': 'prize-item btn-push red',
			'content': [
				{
					'tagName': 'div',
					'className': 'start-line',
					'textNode': this.getText('startScreen.button.startLine', 'start'),
					'data': {'text': 'startScreen.button.0'}
				},
				{
					'tagName': 'div',
					'className': 'game',
					'textNode': this.getText('startScreen.button.endLine', 'game').toUpperCase(),
					'data': {'text': 'startScreen.button.1'}
				}
			]
		}];
	},
	
	getText: function (textKey, translationKey) {
		return getData(this.text, textKey, this.translation(translationKey))
	},
	
	translation(translationKey) {
		var translations = couponTranslation;
		if (translations.hasOwnProperty(translationKey)) {
			if (translations[translationKey].hasOwnProperty(this.lang)) {
				return translations[translationKey][this.lang];
			}
		}
		return '';
	}
};