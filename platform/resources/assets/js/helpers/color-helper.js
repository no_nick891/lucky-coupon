export default {
	
	generateColors(hex) {
		let colors = [],
			polygons = [
				'polygon1', 'polygon2', 'polygon3',
				'polygon4', 'polygon5', 'polygon6'
			];
		for (let index in polygons) {
			if (!polygons.hasOwnProperty(index)) continue;
			let color = this.getColorTone(hex, index);
			colors.push({'name': polygons[index], 'hex': color});
		}
		return colors;
	},
	
	getColorTone(hex, index) {
		let indexes = [5, 1, 3, 2, 4, 0],
			percent = 0.1 + (parseInt(indexes[index])+1) * 0.1;
		return this.rgbToHex(...this.colorLuminance(this.hexToRgb(hex.replace('#', '')), percent));
	},
	
	hexToRgb(hex) {
		let bigint = parseInt(hex, 16),
			r = (bigint >> 16) & 255,
			g = (bigint >> 8) & 255,
			b = bigint & 255;
		return 'rgb('+ r + ',' + g + ',' + b + ')';
	},
	
	colorLuminance(color, percent) {
		let f=color.split(','),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=parseInt(f[0].slice(4)),G=parseInt(f[1]),B=parseInt(f[2]);
		return [(Math.round((t-R)*p)+R), (Math.round((t-G)*p)+G), (Math.round((t-B)*p)+B)];
	},
	
	rgbToHex(r, g, b) {
		return '#' + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
	},
	
	componentToHex(c) {
		let hex = c.toString(16);
		return hex.length == 1 ? '0' + hex : hex;
	}
}