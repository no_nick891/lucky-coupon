import wheelColorSchema from '../lucky-coupon/games/commands/get-wheel-color-schema';

export default {
	
	game_id: 0,
	
	vue_state: {},
	
	coupons: {},
	
	updateFrame(e) {
		let gameObject = this.getGameObject();
		if (gameObject) {
			if (['opacity', 'image', 'wheelLogo', 'opacityWheelLogo'].indexOf(e.target) === -1) {
				this.vue_state.$store.state._settings.changes = true;
			}
			gameObject.dispatchUpdate(e);
		}
	},
	
	getGameObject() {
		let game = '__game_' + this.game_id;
		if (window.top.hasOwnProperty(game)) {
			return window.top[game] || false;
		}
		return false;
	},
	
	updateFrameCoupons(coupons) {
		this.waitSvg(
			this.updateCouponsText.bind(
				this,
				JSON.parse(JSON.stringify(coupons))
			)
		);
	},
	
	waitSvg(handler) {
		let svg = $('iframe#screen-preview').contents().find('body svg')[0];
		if (typeof svg === 'undefined') {
			setTimeout(this.waitSvg.bind(this, handler), 80);
		} else if(svg.tagName === 'svg') {
			handler();
		}
	},
	
	updateCouponsText(coupons) {
		this.updateFrame({target: 'svg', 'coupon': {'bunch': coupons}});
	},
	
	updateFrameSchema(settings) {
		let schema = wheelColorSchema.getSchema(settings);
		this.waitSvg(this.updateCouponsSchema.bind(this, schema));
	},
	
	updateCouponsSchema(schema){
		this.updateFrame({target: 'svg', schema: schema});
	}
};