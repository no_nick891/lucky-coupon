import { defaultGameSettings } from './templates/default/settings';
import { wheelGameSettings } from './templates/wheel/settings';

export const Converter = {
	getConvertedSettings(settings, frontEnd = true) {
		let settingsConverted = this.convertSettings(settings, frontEnd);
		return JSON.parse(JSON.stringify(settingsConverted));
	},
	convertSettings(settings, frontEnd = true) {
		return this.iterateElements(settings, this.convertSetting, [frontEnd]);
	},
	iterateElements(elements, handler, params = []) {
		let result = {};
		for (let index in elements) {
			if (elements.hasOwnProperty(index)) {
				result[index] = handler(elements, index, ...params);
			}
		}
		return result;
	},
	convertSetting(elements, index, frontEnd) {
		let element = {};
		if (Converter.isColors(index)) {
			if (frontEnd) {
				element = elements[index].hex ? elements[index].hex : elements[index];
			} else if (! elements[index].hasOwnProperty('hex')) {
				element['hex'] = elements[index];
			} else {
				element = elements[index];
			}
		} else if (typeof elements[index] === 'string' || index === 'backgroundImage') {
			element = elements[index];
		} else if (['schema'].indexOf(index) > -1 || index.indexOf('polygon') > -1) {
			if (elements[index].hasOwnProperty('hex')) {
				element = elements[index].hex;
			} else if (typeof elements[index].length === 'undefined'
				&& !elements[index].hasOwnProperty('0')
				&& !elements[index].hasOwnProperty('hex')
				&& !elements[index].hasOwnProperty('polygon1')
			) {
				element['hex'] = elements[index];
			} else if (elements[index].hasOwnProperty('polygon1')) {
				let midResults = {};
				for (let polygon in elements[index]) {
					if (!elements[index].hasOwnProperty(polygon)) continue;
					if (elements[index][polygon].hasOwnProperty('hex')) {
						midResults[polygon] = elements[index][polygon].hex;
					} else {
						midResults[polygon] = {'hex': elements[index][polygon]};
					}
				}
				element = midResults;
			} else {
				element = elements[index];
			}
		} else {
			element = Converter.convertSettings(elements[index], frontEnd);
		}
		return element;
	},
	isColors(property) {
		for(let color in defaultGameSettings.colors) {
			if(property === color) {
				return true;
			}
		}
		return false;
	}
};