import {Converter} from './converter';

import {defaultModalContent, defaultModalWrapper} from './templates/default/template';

import {defaultModalStyle} from './templates/default/style';

import {defaultGameSettings} from './templates/default/settings';

import {couponsObject} from './coupon-object';

import {slotModalContent, slotModalWrapper} from './templates/slot/template';

import {slotModalStyle} from './templates/slot/style';

import {slotGameSettings} from './templates/slot/settings';

import {slotsObject} from './slot-object';

import {giftModalStyle} from './templates/gift/style';

import {giftModalContent, giftModalWrapper} from './templates/gift/template';

import {giftGameSettings} from './templates/gift/settings';

import {giftsObject} from '../lucky-coupon/games/structure/gift';

import {wheelModalStyle} from './templates/wheel/style';

import {wheelModalContent, wheelModalWrapper} from './templates/wheel/template';

import {wheelGameSettings} from './templates/wheel/settings';

import {wheelObject} from '../lucky-coupon/games/structure/wheel';

import {scratchModalStyle} from './templates/scratch/style';

import {scratchModalContent, scratchModalWrapper} from './templates/scratch/template';

import {scratchGameSettings} from './templates/scratch/settings';

import {scratchObject} from '../lucky-coupon/games/structure/scratch';

import {de} from './language/game/de';

import {en} from './language/game/en';

import {es} from './language/game/es';

import {fr} from './language/game/fr';

import {ru} from './language/game/ru';

import {he} from './language/game/he';

import {changeable} from './language/changeable';

import {couponTranslation} from './language/coupons';

import runtimeVersion from '!raw-loader!../lucky-coupon/runtime-version.txt';

import {getData} from '../helpers/data';

export default {
	
	siteId: 0,
	
	settings: {},
	
	coupons: {},
	
	game: {},
	
	integrations: {},
	
	translation: en,
	
	getGameScript(settings, coupons, siteId, gameObject) {
		let name = '';
		this.game = gameObject;
		if (gameObject.hasOwnProperty('id')) {
			name = gameObject.id;
		} else {
			name = 'preview';
		}
		if (settings.backgroundImage.opacity > 0.99999) {
			settings.backgroundImage.opacity = settings.backgroundImage.opacity / 100;
		}
		this.settings = settings;
		this.coupons = coupons;
		this.siteId = siteId;
		this.translation = this.getTranslation();
		let scriptPath = '//' + window.location.host + this.getScriptPath() + '?v' + runtimeVersion,
			scriptInit = $(document.body).find('script[src="' + scriptPath + '"]');
		if (scriptInit.length === 0) {
			let script = document.createElement('script'),
				objectKey = 'game_' + name,
				gameData = this.getGameData(name, scriptPath);
			if (this.isEmptyObject(window.top['_lkda'])) {
				window.top['_lkda'] = {};
			}
			window.top['_lkda'][objectKey] = gameData;
			script.type = 'text/javascript';
			script.src = scriptPath;
			return script;
		}
		return false;
	},
	
	getScriptPath: function () {
		return window.location.host === 'app.lucky-coupon.test'
			? '/js/runtime.dev.js'
			: '/js/runtime.js';
	},
	
	getTranslation: function () {
		switch (this.settings.behavior.language) {
			case 'de':
				return de;
			case 'en':
				return en;
			case 'es':
				return es;
			case 'fr':
				return fr;
			case 'ru':
				return ru;
            case 'he':
                return he;
		}
	},
	
	getGameData: function (gameId, scriptPath) {
		let styleModal = this.modalStyle(),
			gameData = {};
		gameData.modal = {
			modalWrapper: this.copyObject(this.modalWrapper(gameId)),
			modalContent: this.copyObject(this.modalContent()),
			modalStyle: this.copyObject(styleModal),
			modalBehavior: this.copyObject(this.modalBehavior())
		};
		gameData.__scriptPath = scriptPath;
		gameData.__adminPanel = true;
		gameData.id = this.siteId;
		gameData.modal.modalStyle.style = styleModal.style;
		return gameData;
	},
	
	isEmptyObject(obj) {
		if (typeof obj !== 'object') return true;
		for (let prop in obj) {
			if (obj.hasOwnProperty(prop)) return false;
		}
		return JSON.stringify(obj) === JSON.stringify({});
	},
	
	modalWrapper(gameId) {
		let tempObject = this.copyObject(this.getWrapperType());
		tempObject.meta = typeof tempObject.meta !== 'undefined' ? tempObject.meta : {};
		var id = this.siteId; // this var have to stay kind of global
		tempObject.id = eval(decodeURI(tempObject.id));
		tempObject.className = 'game-' + gameId;
		tempObject.meta.gameId = gameId;
		tempObject.meta.language = this.settings.behavior.language;
		tempObject.meta.text = this.settings.text;
		tempObject.meta.translations = {
			coupons: couponTranslation,
			changeable: changeable
		};
		return tempObject;
	},
	
	getWrapperType() {
		let wrapper;
		switch(this.game.type) {
			case 'coupon': wrapper = defaultModalWrapper; break;
			case 'slot': wrapper = slotModalWrapper; break;
			case 'gift': wrapper = giftModalWrapper; break;
			case 'wheel': wrapper = wheelModalWrapper; break;
			case 'scratch': wrapper = scratchModalWrapper; break;
		}
		return wrapper;
	},
	
	modalContent() {
		let modalContent = this.copyObject(this.getContentType());
		modalContent.content = this.getModalContent(modalContent.content);
		return modalContent;
	},
	
	getContentType() {
		let template;
		switch(this.game.type) {
			case 'coupon': template = defaultModalContent; break;
			case 'slot': template = slotModalContent; break;
			case 'gift': template = giftModalContent; break;
			case 'wheel': template = wheelModalContent; break;
			case 'scratch': template = scratchModalContent; break;
		}
		return template;
	},
	
	modalStyle() {
		let style = this.getStyleType();
		style.assetUrl = '//' + window.location.host + '/img/game/';
		style.hostUrl = '//' + window.location.host;
		style.gameId = getData(this.game, 'id', 'preview');
		let settings = this.copyObject(this.settings),
			converted = Converter.getConvertedSettings(settings);
		return this.getModalStyle(converted, style);
	},
	
	getStyleType() {
		let style;
		switch(this.game.type) {
			case 'coupon': style = defaultModalStyle; break;
			case 'slot': style = slotModalStyle; break;
			case 'gift': style = giftModalStyle; break;
			case 'wheel': style = wheelModalStyle; break;
			case 'scratch': style = scratchModalStyle; break;
		}
		return style;
	},
	
	modalBehavior() {
		let gameSettings = this.getSettingsType();
		return this.copyObject(gameSettings.behavior);
	},
	
	getSettingsType() {
		let gameSettings;
		switch(this.game.type) {
			case 'coupon': gameSettings = this.copyObject(defaultGameSettings); break;
			case 'slot': gameSettings = this.copyObject(slotGameSettings); break;
			case 'gift': gameSettings = this.copyObject(giftGameSettings); break;
			case 'wheel': gameSettings = this.copyObject(wheelGameSettings); break;
			case 'scratch': gameSettings = this.copyObject(scratchGameSettings); break;
		}
		this.getUserBehavior(
			gameSettings.behavior,
			this.settings.behavior
		);
		return gameSettings;
	},
	
	getUserBehavior: function (defaultBehavior, userBehavior) {
		for (var index in defaultBehavior) {
			if (
				getData(defaultBehavior, index, false)
				&& getData(userBehavior, index, false)
			) {
				if (typeof defaultBehavior[index] === 'string') {
					defaultBehavior[index] = userBehavior[index];
				} else {
					this.getUserBehavior(defaultBehavior[index], userBehavior[index]);
				}
			}
		}
	},
	
	getModalStyle: function (settings, style) {
		for (let index in settings) {
			if (settings.hasOwnProperty(index)) {
				if (typeof settings[index] === 'string' || typeof settings[index] === 'number') {
					if (style.hasOwnProperty(index)) {
						let result;
						switch (index) {
							case 'button': settings[index].indexOf('#') !== -1 ? result = settings[index] : false; break;
							default: result = settings[index]; break;
						}
						if (typeof result !== 'undefined') {
							style[index] = result;
						}
					}
				} else {
					style = this.getModalStyle(settings[index], style);
				}
			}
		}
		return style;
	},
	
	getModalContent: function (elements) {
		return Converter.iterateElements(elements, this.applyGameProperty.bind(this));
	},
	
	applyGameProperty: function (elements, index) {
		let element = elements[index],
			meta = element['meta'],
			newText = this.getNewText(element),
			textContainer = this.getTextContainerName(meta);
		if (meta === 'coupons') {
			element.content = this.getCoupons();
		} else if (newText !== false) {
			element[textContainer] = newText;
		} else if (typeof meta !== 'undefined') {
			element = this.getElementTranslation(meta, element, textContainer);
		} else if (element.content && elements[index].tagName !== 'meta') {
			element.content = this.getModalContent(elements[index].content);
		}
		return element;
	},
	
	getNewText(element) {
		let dataTextAttr = getData(element, 'data.text', false);
		return getData(this.settings, 'text.' + dataTextAttr, false);
	},
	
	getTextContainerName(meta) {
		return (['email', 'userName'].indexOf(meta) < 0) ? 'textNode' : 'placeholder';
	},
	
	getElementTranslation(meta, element, attribute) {
		let contentText = getData(this.settings, 'content.' + meta, false);
		if (contentText !== false) {
			element[attribute] = contentText;
		}
		let translatedText = getData(this.translation, meta, false);
		if (translatedText !== false) {
			if (element.meta === meta && translatedText) {
				if (this.getCustomFieldNames().indexOf(meta) === -1) {
					if (meta === 'title') {
						element[attribute] = translatedText[this.getTitleType()];
					} else {
						element[attribute] = translatedText;
					}
				}
			}
		}
		return element;
	},
	
	getCustomFieldNames: function () {
		return ['title', 'note', 'description', 'email', 'userName', 'trigger', 'gpdr'];
	},
	
	getTitleType: function () {
		return this.game.type === 'gift' ? this.game.type : 'other';
	},
	
	getRandomString: function () {
		return new Date().valueOf()
			+ Math.random().toString(36).substring(2, 15)
			+ Math.random().toString(36).substring(2, 15);
	},
	
	getCoupons: function () {
		let handler = this.getGameHandler(),
			couponsObj = this.getArrayCoupons(handler);
		if (this.game.type === 'slot') {
			couponsObj.push(couponsObj[0]);
			couponsObj = handler.getSlotReels(couponsObj);
		}
		return couponsObj;
	},
	
	getGameHandler: function() {
		let handler = {};
		couponsObject.lang = this.settings.behavior.language;
		couponsObject.text = this.settings.text;
		switch (this.game.type) {
			case 'coupon': handler = couponsObject;break;
			case 'slot': handler = slotsObject; break;
			case 'gift': handler = giftsObject; break;
			case 'wheel': handler = wheelObject; break;
			case 'scratch': handler = scratchObject; break;
		}
		return handler;
	},
	
	getArrayCoupons: function(handler) {
		let couponsObj = [];
		for (let coupInd in this.coupons) {
			let coup = this.coupons[coupInd];
			couponsObj = handler.getArrayWithCoupons(coup, coupInd, couponsObj);
		}
		return couponsObj;
	},
	
	insertScript: function (frame, script, id, isDetail) {
		let frameBody = this.getFrameBody($(frame)),
			variableScript = document.createElement('script'),
			scriptContent = 'var FRAME_ID = ' + id + ';';
		if(isDetail) {
			scriptContent = 'var FRAME_ID = ' + id + ', DETAIL_PAGE = true;';
		}
		variableScript.innerHTML = scriptContent;
		this.insert(variableScript, frameBody);
		this.insert(script, frameBody);
	},
	
	getFrameBody(frame) {
		return frame.contents().find('body')[0];
	},
	
	insert: function (script, body) {
		if (typeof script === 'object') {
			body.appendChild(script);
			body.style.margin = 0;
			body.style.padding = 0;
		}
	},
	
	clearGlobalVariables(games) {
		if (games.length) {
			for (let game of games) {
				let variable = 'game_' + game.id;
				this.clearObject(variable);
			}
		}
		this.clearObject('game_preview');
	},
	
	clearAllGlobalVariables() {
		if (window.top.hasOwnProperty('_lkda')) {
			for (let index in window.top._lkda) {
				if (window.top._lkda.hasOwnProperty(index)) {
					this.clearObject(index);
				}
			}
		}
	},
	
	clearObject (variable) {
		if (window.top.hasOwnProperty('_lkda')) {
			if (window.top._lkda.hasOwnProperty(variable)) {
				delete window.top._lkda[variable];
			}
		}
	},
	
	copyObject (object) {
		return JSON.parse(JSON.stringify(object));
	}
};