import Flash from './flash.js'

import { fileHelper } from './file-helper.js';

import hook from '../helpers/game-hook';

export const file = {
	
	name: 'background_image',
	
	events: {
		sendFile: function() {},
		beforeLoad: function() {},
		fileValidationFailed: function() {},
		fileValidationSuccess: function() {}
	},
	
	handle: function (targetFile, events) {
		this.events = events ? events : this.events;
		this.events.beforeLoad.call();
		fileHelper.bindOnLoad(targetFile, this.backgroundLoader.bind(this));
	},
	
	backgroundLoader: function (e) {
		let newFile = fileHelper.validateFile(e, this.name);
		if (newFile) {
			this.events.fileValidationSuccess.call(null, e, newFile);
			this.events.sendFile.call();
		} else {
			this.events.fileValidationFailed.call();
			Flash.setError('File size too large. File size should be less than 10Mb.');
		}
	},
	
	sendFile(fileData) {
		let formData = new FormData(), image, imageArray;
		if (this.settings.hasOwnProperty(fileData.settingName)) {
			image = this.settings[fileData.settingName][fileData.imageSetting];
		} else {
			image = this.$store.state._settings.defaultSettings[fileData.settingName][fileData.imageSetting];
		}
		imageArray = [fileData.fileName, image];
		this.loadingData = true;
		if (typeof image !== 'string') {
			imageArray.push(image.name);
		}
		formData.append(...imageArray);
		formData.append('opacity', this.settings[fileData.settingName][fileData.opacityName]);
		formData.append('game_id', this.$store.state._games.game.id);
		this.$store.dispatch(fileData.methodName, formData)
			.then(response => {
				let data = response.data;
				if (data.file) {
					let url = file.getTempUrl(data.file);
					this.$store.state._settings.settings[fileData.settingName][fileData.imageSetting] = url;
					this.image = url;
					if (this.newFile) {
						hook.game_id = this.$store.state._games.game.id;
						hook.updateFrame({target: fileData.imageSetting, file: url});
						this.newFile = false;
						window.dispatchEvent(new Event('resize'));
					}
				} else {
					Flash.setError('File not updated.');
				}
				this.loadingData = false;
			})
			.catch(err => {
				if (err.response && err.response.status !== 200) {
					Flash.setError('Can\'t update ' + fileData.settingName + '.');
				}
			});
	},
	
	getTempUrl(file) {
		return file + '?' + new Date().getTime()
	},
};