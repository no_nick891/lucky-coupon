export const bar = {
	"copied": {
		"en": "Copied",
		"de": "Kopiert",
		"ru": "Cкопировано",
		"fr": "Copié",
		"es": "Copiado",
        "he": "הועתק"
	},
	"failedCopy": {
		"en": "Failed to copy",
		"de": "Kopieren fehlgeschlagen",
		"ru": "Неудалось скопировать",
		"fr": "Échec de la copie",
		"es": "Falló la copia",
        "he": "נכשל בהעתקה"
	}
};