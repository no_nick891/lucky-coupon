export const changeable = {
	"title": {
		"other": {
			"en": "You%22ve been Chosen! For a shot at a BIG discount",
			"de": "Du wurdest ausgewählt! Für einen Schuss mit einem GROßEN Rabatt",
			"ru": "Мы вас избрали! Для возможности получить БОЛЬШУЮ скидку",
			"fr": "Vous avez été sélectionné pour tenter votre chance d%22obtenir une ENORME promotion",
			"es": "¡Has sido Seleccionado! Para obtener un GRAN descuento",
			"he": "שחקו במשחק ותוכלו לזכות בהנחה לקנייה באתר"
		},
		"gift": {
			"en": "Choose your mystery gift to reveal your prize",
			"de": "Wähle dein rätselhaftes Geschenk, um deinen Preis zu entdecken",
			"ru": "Выбирете ваш тайный подарок, чтобы раскрыть ваш приз",
			"fr": "Choisis un cadeau mystère pour découvrir ton prix",
			"es": "Elige tu regalo misterioso para revelar tu premio",
			"he": "בחרו מתנה כדי לגלות במה זכיתם"
		}
	},
	"description": {
		"en": "Enter your email address to find out if you%22ve the winner",
		"de": "Geben Sie Ihre eMail Adresse ein um herauszufinden ob Sie der Gewinner sind",
		"ru": "Введите свой адрес электронной почты, чтобы узнать, стали ли вы победителем",
		"fr": "Entrez votre adresse email pour découvrir si vous êtes le gagnant",
		"es": "Ingresa tu dirección de correo electrónico para averiguar si ganaste",
        "he": "הכניסו את כתובת המייל שלכם וגלו במה זכיתם"
	},
	"note": {
		"en": "From time to time, we may send you more special offers. You can unsubscribe at any time.",
		"de": "Ab und zu senden wir dir spezielle Angebote. Du kannst dich jederzeit abmelden.",
		"ru": "Время от времени мы можем высылать вам спец предложения. Вы можете отписаться от них в любое время.",
		"fr": "Nous allons vous envoyer de temps en temps des offres spéciales. Vous pouvez vous désabonner quand vous le souhaitez.",
		"es": "De vez en cuando, podemos enviarle más ofertas especiales. Puede darse de baja en cualquier momento.",
        "he": "מפעם לפעם אנחנו נשלח לך מיילים על מבצעים נוספים,אפשר לבטל את קבלת המיילים בכל שלב"
	},
	"gpdr": {
		"en": "I agree to subscribe to the mailing list",
		"de": "Ich stimme zu, unsere Mailingliste zu abonnieren",
		"ru": "Я согласен с подпиской на почтовую рассылку",
		"fr": "J%22accepte de m%22abonner à notre liste de diffusion",
		"es": "Acepto suscribirme a nuestra lista de correo",
		"he": "אני מאשר הרשמה לרשימת התפוצה"
	},
	"email": {
		"en": "Enter your email address",
		"de": "eMail Adresse",
		"ru": "Адрес электронной почты",
		"fr": "Adresse email",
		"es": "Dirección de correo electrónico",
		"he": "כתובת מייל"
	},
	"trigger": {
		"en": "Win A Prize",
		"de": "Gewinne einen Preis",
		"ru": "Выиграть приз",
		"fr": "Gagner un prix",
		"es": "Ganar Un Premio",
		"he": "שחקו וזכו"
	},
	"pickGift": {
		"en": "Pick a gift to see what you won",
		"de": "Wählen Sie ein Geschenk, um zu sehen, was Sie gewonnen haben",
		"ru": "Выбери что бы увидеть что вы выиграли",
		"fr": "Choisissez un cadeau pour voir ce que vous avez gagné",
		"es": "Elija un regalo para ver lo que ganó",
		"he": "בחרו מתנה כדי לראות במה זכיתם"
	}
};