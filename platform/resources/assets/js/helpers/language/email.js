export const codeMailTranslation = {
	"couponCode": {
		"en": "Your coupon code from",
		"de": "",
		"ru": "Ваш код купона от сайта",
		"fr": "",
		"es": "",
		"he": ""
	},
	"congratulation": {
		"en": "Congratulation",
		"de": "Glückwünsche",
		"ru": "Поздравляем",
		"fr": "Félicitation",
		"es": "Felicitaciones",
		"he": "מזל טוב"
	},
	"youWon": {
		"en": "YOU WON A",
		"de": "Sie erhalten",
		"ru": "Вы выиграли",
		"fr": "Vous recevez",
		"es": "Recibirás",
		"he": "זכית ב"
	},
	"yourDiscount": {
		"en": "Your <span style='font-size: 14px; line-height: 16px;'>Discount</span> Code Is:",
		"de": "Ihr <span style='font-size: 14px; line-height: 16px;'>Couponcode</span>",
		"ru": "Выш <span style='font-size: 14px; line-height: 16px;'>Скидочный</span> Код:",
		"fr": "Votre code de <span style='font-size: 14px; line-height: 16px;'>réduction</span> est",
		"es": "Su código de <span style='font-size: 14px; line-height: 16px;'>descuento</span> es",
		"he": "קוד הקופון שלך הוא"
	},
	"goToSite": {
		"en": "Go to site",
		"de": "Gehe zur Website zum Einlösen",
		"ru": "Перейти на сайт и использовать",
		"fr": "Aller sur le site pour racheter",
		"es": "Ir al Sitio para Canjear",
		"he": "מעבר לאתר לשימוש בקופון"
	},
	"freeProduct": {
		"en": "free product",
		"de": "Kostenloses Geschenk",
		"ru": "Бесплатный товар",
		"fr": "Cadeau gratuit",
		"es": "Regalo Gratis",
		"he": "הנחה"
	},
	"freeShipping": {
		"en": "Free Shipping",
		"de": "Kostenloser Versand",
		"ru": "Бесплатная доставка",
		"fr": "Livraison gratuite",
		"es": "Envío Gratis",
		"he": "כסף לרכישה"
	},
	"cash": {
		"en": "Cash",
		"de": "Bargeld",
		"ru": "Наличные",
		"fr": "Argent",
		"es": "Efectivo",
		"he": "משלוח חינם"
	},
	"discount": {
		"en": "Discount",
		"de": "Rabatt",
		"ru": "Скидка",
		"fr": "remise",
		"es": "descuento",
		"he": "מוצר מתנה"
	},
	"advice": {
		"en": "Don't forget to apply your coupon code to the relevant field doing the checkout process",
		"de": "",
		"ru": "Не забудьте ввести полученный код в поле для кода при оформлении заказа",
		"fr": "",
		"es": "",
		"he": ""
	}
};