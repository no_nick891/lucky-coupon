export const couponTranslation = {
	"discount": {
		"en": "Discount",
		"de": "Rabatt",
		"ru": "Скидка",
		"fr": "Remise",
		"es": "Descuento",
        "he": "הנחה"
	},
	"coupon": {
		"en": "Coupon",
		"de": "Coupon",
		"ru": "Купон",
		"fr": "Coupon",
		"es": "Cupón",
        "he": "הנחה"
	},
	"cash": {
		"en": "Cash",
		"de": "Bargeld",
		"ru": "Наличные",
		"fr": "Argent",
		"es": "Efectivo",
        "he": "קאש"
	},
	"freeShipping": {
		"en": "FREE Shipping",
		"de": "KOSTENLOSER Versand",
		"ru": "БЕСПЛАТНАЯ доставка",
		"fr": "LIVRAISON gratuite",
		"es": "ENVÍO Gratis",
        "he": "משלוח חינם"
	},
	"freeProduct": {
		"en": "FREE Product",
		"de": "PRODUKT Gratis",
		"ru": "БЕСПЛАТНЫЙ Товар",
		"fr": "PRODUIT Gratuit",
		"es": "PRODUCTO Gratis",
		"he": "מוצר חינם"
	},
	"start": {
		"en": "Start",
		"de": "Spiel",
		"ru": "Начать",
		"fr": "Démarrer",
		"es": "Comenzar",
        "he": "התחילו"
	},
	"game": {
		"en": "Game",
		"de": "starten",
		"ru": "игру",
		"fr": "le jeu",
		"es": "Juego",
		"he": "לשחק"
	}
};