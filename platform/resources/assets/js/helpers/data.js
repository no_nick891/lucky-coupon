export function getData(object, path, defaultValue){
	defaultValue = typeof defaultValue !== 'undefined' ? defaultValue : null;
	var splitPath = path.split('.');
	return splitPath.reduce((xs, x) => (xs && typeof xs[x] !== 'undefined') ? xs[x] : defaultValue, object)
}

export function decodeRecursive(object) {
	for (let index in object) {
		if (!object.hasOwnProperty(index)) return false;
		if (typeof object[index] === 'string') {
			let content = object[index].replace(/%27/g, '"').replace(/%22/g, "'");
			try {
				object[index] = decodeURIComponent(content);
			} catch(e) {
				object[index] = content;
			}
		} else {
			object[index] = decodeRecursive(object[index]);
		}
	}
	return object;
}

export function ucFirst (string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

export function lcFirst (string) {
	return string.charAt(0).toLowerCase() + string.slice(1);
}