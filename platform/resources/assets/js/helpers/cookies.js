export function setCookie(cookieName, cookieValue, expirationDays) {
	expirationDays = expirationDays ? expirationDays : 365;
	let domain = window.location.hostname,
		d = new Date();
	d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
	let expires = 'expires='+d.toUTCString();
	document.cookie = cookieName + '=' + cookieValue + ';' + expires + ';' + 'domain=' + domain + ';path=/';
}

export function getCookie(cookieName) {
	let name = cookieName + '=',
		decodedCookie = decodeURIComponentX(document.cookie),
		ca = decodedCookie.split(';');
	for(let i = 0; i <ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
}

export function deleteCookie(name) {
	document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT;domain='+window.location.hostname+';path=/;';
}

function decodeURIComponentX(str) {
	var out = '', arr, i = 0, l, x;
	arr = str.split(/(%(?:D0|D1)%.{2})/);
	for (l = arr.length; i < l; i++) {
		try {
			x = decodeURIComponent(arr[i]);
		} catch (e) {
			x = arr[i];
		}
		out += x;
	}
	return out
}