export const giftGameCoupons = [{
	"id": null,
	"temp_id": 1,
	"type": "type",
	"value": "25%25%20OFF",
	"code": "",
	"chance": "20.000",
	"gravity": 100
}, {
	"id": null,
	"temp_id": 2,
	"type": "type",
	"value": "50%25%20OFF",
	"code": "",
	"chance": "20.000",
	"gravity": 100
}, {
	"id": null,
	"temp_id": 3,
	"type": "type",
	"value": "60%25%20OFF",
	"code": "",
	"chance": "20.000",
	"gravity": 100
}, {
	"id": null,
	"temp_id": 4,
	"type": "type",
	"value": "5%25%20OFF",
	"code": "",
	"chance": "20.000",
	"gravity": 100
}, {
	"id": null,
	"temp_id": 5,
	"type": "type",
	"value": "Free shipping",
	"code": "",
	"chance": "20.000",
	"gravity": 100
}
];