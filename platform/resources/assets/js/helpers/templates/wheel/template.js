export const wheelModalWrapper = {
	"tagName": "div",
	"id": "%22lucky-coupon-%22+id",
	"meta": {
		"closeItem": {
			"className": "close"
		},
		"type": "wheel",
		"language": "en"
	},
	"style": {
		"display": "none",
		"position": "fixed",
		"top": 0,
		"bottom": 0,
		"z-index": 2147483647,
		"width": "100%",
		"height": "100%",
		"background-color": "rgba(0,0,0,0.5)"
	},
	"content": []
};

export const wheelModalContent = {
	"content": [
		{
			"tagName": "meta",
			"name": "viewport",
			"content": "width=device-width, initial-scale=1.0, user-scalable=no"
		},
		{
			"tagName": "div",
			"className": "lucky-coupon-center wheel-window",
			"style": {
				"display": "none"
			},
			"content": [
				{
					"tagName": "div",
					"className": "lucky-coupon-popup",
					"content": [
						
						{
							"tagName": "div",
							"className": "rename-test"
						},
						
						{
							"tagName": "div",
							"className": "close",
							"content": [{
								"tagName": "div",
								"className": "inner",
								"textNode": " "
							}]
						},
						
						{
							"tagName": "div",
							"className": "lucky-coupon-popup-inner background start-text",
							"content": [
								{
									"tagName": "div",
									"className": "custom-image"
								},
								{
									"tagName": "div",
									"className": "big-text",
									"meta": "title",
									"data": {
										"name": "title",
										"text": "startScreen.title"
									},
									"textNode": "You%22ve been Chosen! For a shot at a BIG discount"
								},
								{
									"tagName": "div",
									"className": "small-text",
									"meta": "description",
									"data": {
										"name": "description",
										"text": "startScreen.description"
									},
									"textNode": "Enter your email address to find out if you%22re the winner"
								},
								{
									"tagName": "form",
									"className": "input-wrapper",
									"id": "WooHoo",
									"content": [
										{
											"tagName": "input",
											"type": "text",
											"maxlength": "500",
											"title": "Name",
											"placeholder": "Enter your full name",
											"meta": "userName",
											"name": "userName",
											"className": "username-input",
											"data": {
												"name": "userName",
												"text": "startScreen.userName"
											}
										},
										{
											"tagName": "input",
											"type": "email",
											"maxlength": "500",
											"title": "Email",
											"placeholder": "Enter your email address",
											"meta": "email",
											"name": "email",
											"className": "email-input",
											"data": {
												"name": "email",
												"text": "startScreen.email"
											}
										},
										{
											"tagName": "div",
											"className": "recart-messenger-widget",
											"style": {"display": "none"},
											"data": {"source": "woohoo"},
											"content": [{
												"tagName": "div",
												"className": "fake-fecebook-form",
												"style": {"display": "none"}
											}]
										},
										{
											"tagName": "span",
											"style": {
												"display": "none",
												"position": "relative"
											},
											"content": [
												{
													"tagName": "span",
													"className": "exclamation",
													"textNode": "!",
													"content": [
														{
															"tagName": "span",
															"className": "arrow-box-wrapper",
															"content": [
																{
																	"tagName": "span",
																	"className": "arrow-box"
																}
															]
														}
													]
												}
											]
										}
									]
								}
							]
						},
						
						{
							"tagName": "div",
							"className": "lucky-coupon-popup-inner background middle hide",
							"content": [
								{
									"tagName": "div",
									"className": "custom-image"
								},
								{
									"tagName": "div",
									"className": "big-text middle-center",
									"textNode": "Let%22s see what you%22re won...",
									"meta": "play-description",
									"data": {
										"text": "playScreen.waitingForResults"
									}
								}
							]
						},
						
						{
							"tagName": "div",
							"className": "lucky-coupon-popup-inner coupon-wrapper start-body",
							"content": [
								{
									"tagName": "div",
									"content": [
										{
											"tagName": "button",
											"className": "btn-push red gift-button",
											"textNode": "SPIN",
											"meta": "play_wheel",
											"data": {
												"text": "startScreen.button"
											}
										},
										{
											"tagName": "div",
											"className": "arrow-down"
										}
									]
								},
								{
									"tagName": "div",
									"className": "wheel-wrapper",
									"content": [
										{
											"tagName": "div",
											"className": "wheel",
											"content": [
												{
													"tagName": "img",
													"className": "wheel-image"
												}
											]
										},
										{
											"tagName": "div",
											"className": "slices",
											"meta": "coupons",
											"style": {"display":"none"},
											"content": []
										}
									]
								},
								{
									"tagName": "div",
									"className": "text small-message",
									"content": [
										{
											"tagName": "div",
											"className": "checkbox-wrapper",
											"style": { "display": "none" },
											"content": [
												{
													"tagName": "label",
													"className": "checkbox-container",
													"content": [
														{
															"id": "coupon-check",
															"tagName": "input",
															"type": "checkbox"
														},
														{
															"tagName": "span",
															"className": "checkmark"
														}
													]
												},
												{
													"tagName": "label",
													"className": "agree-wrapper",
													"attributes": {
														"for": "coupon-check"
													},
													"textNode": "I Agree to subscribe to the newsletter",
													"meta": "gpdr",
													"data": {
														"name": "gpdr",
														"text": "startScreen.gdpr"
													}
												}
											]
										},
										{
											"tagName": "div",
											"meta": "note",
											"data": {
												"name": "note",
												"text": "startScreen.note"
											},
											"style": {"display": "none"},
											"textNode": "From time to time, we may send you more special offers. You can unsubscribe at any time."
										}
									]
								}
							]
						},
						
						{
							"tagName": "div",
							"className": "lucky-coupon-popup-inner background finish-text hide",
							"content": [
								{
									"tagName": "div",
									"className": "custom-image"
								},
								{
									"tagName": "div",
									"className": "big-text congrats",
									"textNode": "Congratulations",
									"meta": "congratulation",
									"data": {"text": "winScreen.congratulations"}
								},
								{
									"tagName": "div",
									"className": "small-text push-bottom",
									"textNode": "You get",
									"meta": "you-get",
									"data": {"text": "winScreen.youGot"}
								}
							]
						},
						
						{
							"tagName": "div",
							"className": "lucky-coupon-popup-inner coupon-wrapper position finish-body hide",
							"content": [
								{
									"tagName": "div",
									"className": "coupon-center",
									"content": [
										{
											"tagName": "div",
											"className": "coupon",
											"content": [
												{
													"tagName": "div",
													"className": "right"
												},
												{
													"tagName": "div",
													"className": "left"
												},
												{
													"tagName": "div",
													"className": "upper-text",
													"content": [
														{
															"tagName": "div",
															"textNode": "10$ cash"
														},
														{
															"tagName": "div",
															"textNode": ""
														}
													]
												},
												{
													"tagName": "div",
													"className": "lower-text",
													"content": [
														{
															"tagName": "div",
															"textNode": "Your Discount Code Is:",
															"meta": "discount-code",
															"data": {"text": "winScreen.yourDiscountCodeIs"}
														},
														{
															"tagName": "div",
															"textNode": ""
														}
													]
												}
											]
										}
									]
								},
								{
									"tagName": "button",
									"className": "continue-btn",
									"textNode": "Continue & Use Discount",
									"meta": "continue-use-discount",
									"data": {"text": "winScreen.button"}
								},
								{
									"tagName": "div",
									"className": "text small-message final-message",
									"textNode": "In order to use this discount add it to the relevant field in checkout",
									"meta": "guide-to-use",
									"data": {"text": "winScreen.note"}
								}
							]
						}

					]
				},

                {
                    "tagName": "div",
                    "className": "powered-by",
                    "content": [
                        {
                            "tagName": "div",
                            "className": "powered-by-wrapper",
                            "content": [
                                {
                                    "tagName": "a",
									"href" : "https://getwoohoo.com",
									"target": "_blank",
                                    "className": "powered-by-image"
                                }
                            ]
                        }
                    ]
                }
			]
		},
		{
			"tagName": "div",
			"className": "lucky-coupon-bar",
			"style": {
				"display": "none"
			},
			"content": [
				{
					"tagName": "span",
					"className": "coupon-code-text",
					"textNode": "Your ",
					"meta": "bar-your",
					"data": {"text": "bar.your"}
				},
				{
					"tagName": "strong",
					"className": "coupon-code-win",
					"textNode": ""
				},
				{
					"tagName": "span",
					"className": "coupon-code-text",
					"textNode": "coupon code",
					"meta": "bar-coupon-code",
					"data": {"text": "bar.couponCode"},
					"style": { "margin-right": "5px" }
				},
				{
					"tagName": "strong",
					"className": "coupon-code-code",
					"content": [{
						"tagName": "input",
						"type": "text",
						"id": "copy-coupon",
						"value": ""
					}]
				},
				{
					"tagName": "span",
					"className": "coupon-code-reserved",
					"style": {"padding": "0 5px"},
					"textNode": "is reserved for",
					"data": {"text": "bar.reservedFor"},
					"meta": "bar-reserved-for"
				},
				{
					"tagName": "strong",
					"className": "coupon-code-time",
					"content": [
						{
							"tagName": "span",
							"textNode": "15m:"
						},
						{
							"tagName": "span",
							"textNode": "00s"
						}
					]
				},
				{
					"tagName": "span",
					"className": "fa fa-times-thin fa-2x",
					"aria-hidden":  "true"
				},
				{
					"tagName": "button",
					"className": "copy-code",
					"textNode": "Copy Code",
					"meta": "bar-copy-code",
					"data": {
						"text": "bar.copyCode",
						"clipboardTarget": "#copy-coupon"
					}
				}
			]
		},
		
		{
			"tagName": "div",
			"className": "lucky-coupon-trigger",
			"style": {"display": "none"},
			"content": [
				{
					"tagName": "div",
					"className": "close",
					"style": {"display": "none"},
					"content": [{
						"tagName": "div",
						"className": "inner",
						"textNode": " "
					}]
				},
				{
					"tagName": "div",
					"meta": "trigger",
					"data": {
						"name": "trigger",
						"text": "trigger"
					},
					"className": "trigger-text",
					"textNode": "Win A Prize"
				},
				{
					"tagName": "div",
					"className": "gift-image"
				}
			]
		}
	]
};