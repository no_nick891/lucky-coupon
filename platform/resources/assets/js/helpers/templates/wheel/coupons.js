export const wheelGameCoupons = [{
	"id": null,
	"temp_id": 1,
	"type": "type",
	"value": "10$ cash",
	"code": "10CASH",
	"chance": "25",
	"gravity": 10
}, {
	"id": null,
	"temp_id": 2,
	"type": "type",
	"value": "No Luck :(",
	"code": "",
	"chance": "0",
	"gravity": 0
}, {
	"id": null,
	"temp_id": 3,
	"type": "type",
	"value": "15%25 Discount",
	"code": "15OFF",
	"chance": "25",
	"gravity": 10
}, {
	"id": null,
	"temp_id": 4,
	"type": "type",
	"value": "Almost...",
	"code": "",
	"chance": "0",
	"gravity": 0
}, {
	"id": null,
	"temp_id": 5,
	"type": "type",
	"value": "Free Shipping",
	"code": "Free",
	"chance": "25",
	"gravity": 10
}, {
	"id": null,
	"temp_id": 6,
	"type": "type",
	"value": "25%25 Discount Today",
	"code": "25OFF",
	"chance": "25",
	"gravity": 10
}];