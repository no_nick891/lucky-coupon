export const wheelColors = {
	hex: '#fff',
	hsl: {
		h: 255,
		s: 255,
		l: 255,
		a: 1
	},
	hsv: {
		h: 255,
		s: 255,
		v: 255,
		a: 1
	},
	rgba: {
		r: 255,
		g: 255,
		b: 255,
		a: 1
	},
	a: 1
};

//@todo: make one common file and make it extendable for all games
//@todo: keep in mind to make the same on backend
export const wheelGameSettings = {
	"position": "center",
	"triggerPosition": "right",
	"font": "%27Open Sans%27, Helvetica, Arial, sans-serif",
	"animation": "fadeIn",
	"wheelLogoImage": {
		"wheelLogo": "",
		"opacityWheelLogo": "1.0"
	},
	"backgroundImage": {
        "image": "/img/game/default.jpg?v=5",
        "opacity": "0.5"
	},
	"colors": {
		"background": "#000",
		"coverText": "#fff",
		"couponText": "#000",
		"button": "#f30927",
		"buttonText": "#fff",
		"design": {
			"present": {
				"active": 1,
				"schema": [
					{
						"polygon1": "#fcc124",
						"polygon2": "#a5deef",
						"polygon3": "#b89ee3",
						"polygon4": "#ffbed9",
						"polygon5": "#ffffd3",
						"polygon6": "#9ff087"
					},
					{
						"polygon1": "#F2727F",
						"polygon2": "#C06C86",
						"polygon3": "#6D5C7E",
						"polygon4": "#325D7F",
						"polygon5": "#0B558F",
						"polygon6": "#F9B294"
					},
					{
						"polygon1": "#FFC65D",
						"polygon2": "#7BC8A4",
						"polygon3": "#4CC3D9",
						"polygon4": "#93648D",
						"polygon5": "#404040",
						"polygon6": "#F16745"
					},
					{
						"polygon1": "#F37022",
						"polygon2": "#CD004D",
						"polygon3": "#6460AA",
						"polygon4": "#0089D1",
						"polygon5": "#0CB14B",
						"polygon6": "#FCB712"
					},
					{
						"polygon1": "#F4E6CC",
						"polygon2": "#FCF5EB",
						"polygon3": "#2F8CAB",
						"polygon4": "#C9E2E9",
						"polygon5": "#FE4902",
						"polygon6": "#A25F08"
					}
				]
			},
			"custom": {
				"active": 0,
				"schema": {
					"polygon1": "#f1b3bb",
					"polygon2": "#de4e5f",
					"polygon3": "#e8818d",
					"polygon4": "#e36776",
					"polygon5": "#ec9aa4",
					"polygon6": "#d93549"
				}
			},
			"colorize": {
				"active": 1,
				"schema": "#D0021B"
			}
		}
	},
	"giftImage": "gift1",
	"content": {
		"title": "You%22ve been Chosen! For a shot at a BIG discount",
		"description": "Enter your email address to find out if you%22ve the winner",
		"email": "Enter your email address",
		"note": "From time to time, we may send you more special offers. You can unsubscribe at any time.",
		"gpdr": "I agree to subscribe to the mailing list",
		"trigger": "Win A Prize"
	},
	"behavior": {
		"language": "en",
		"showPlayGameTrigger": {
			"yes": "1",
			"no": "0"
		},
		"collectEmailFromUsers": {
			"yes": "1",
			"no": "0"
		},
		"collectEmailWithRecart": "0",
		"collectNameFromUsers": {
			"yes": "0",
			"no": "1"
		},
		"chooseInputFieldType": {
			"emailField": "1",
			"facebookMessengerField": "0"
		},
		"startDisplayTheGame": {
			"atOnce": "0",
			"underTheFollowingConditions": {
				"whenTheUserIsLivingTheWebsite": "1",
				"whenTheUserReachesPercentOfThePage": {
					"value": "1",
					"percent": "100"
				},
				"afterSeconds": {
					"value": "1",
					"seconds": "10"
				}
			},
			"onSpecialPagePlace": "0"
		},
		"frequency": {
			"onEveryPageView": "1",
			"notMoreThanOnceEveryNumberTimePerUser": {
				"value": "0",
				"number": "1",
				"time": {
					"day": "1",
					"week": "0",
					"month": "0"
				}
			}
		},
		"stopToDisplayTheGame": {
			"never": "0",
			"underTheFollowingConditions": {
				"afterTheUserPerformsTheAction": "1",
				"afterShowingItNumberTimesToTheUser": {
					"value": "1",
					"number": "1"
				}
			}
		},
		"howDoYouWantYourCustomerToReceiveTheCoupon": {
			"winnigGameScreen": "1",
			"sendTroughEmail": "0",
			"winningGameScreenAndEmail": "0"
		},
		"couponReceiveEmailText": {
			"from": "storename@gmail.com",
			"storeName": "Your store name",
			"discountCodeSentToYourEmail": "Discount code sent to your email",
			"couponCode": "Your coupon code from",
			"congratulation": "Congratulations",
			"youWon": "YOU WON A",
			"yourDiscount": "Your Discount Code Is:",
			"dontForgetToApplyYourCouponToTheRelevantFieldDoingTheCheckoutProcess": "Don't forget to apply your coupon code to the relevant field doing the checkout process",
			"goToSite": "Go to site",
			"freeProduct": "free product",
			"freeShipping": "Free Shipping",
			"cash": "Cash",
			"discount": "Discount"
		},
		"continueUseDiscount": {
			"value": 0,
			"url": "",
			"target": 1
		},
		"whereShouldTheGameAppear": [{
			"type": "1",
			"url": "*"
		}],
		"countDownTimeTimeMin": "15",
		"countDownTime": "1",
		"makeGPDRCompliance": "0",
		"isPoweredByVisible": "1",
		"receiveEmailLogoImage": ""
	}
};