export const wheelText = {
	"startScreen": {
		"title": "Here's your chance to win a BIG prize!",
		"description": "Enter your email address to find out if you%22re the winner",
		"email": "Enter your email address",
		"userName": "Enter your full name",
		"button": "SPIN",
		"note": "From time to time, we may send you more special offers. You can unsubscribe at any time.",
		"gdpr": "I agree to subscribe to the mailing list"
	},
	"playScreen": {
		"waitingForResults": "Let%22s see what you%22re won..."
	},
	"winScreen": {
		"congratulations": "Congratulations",
		"youGot": "You got a",
		"yourDiscountCodeIs": "Your Discount Code Is:",
		"button": "Continue & Use Discount",
		"note": "In order to use this discount add it to the relevant field in checkout"
	},
	"bar": {
		"your": "Your ",
		"couponCode": "coupon code",
		"reservedFor": "is reserved for",
		"copyCode": "Copy Code",
		"copied": "Copied",
		"failedCopy": "Failed to copy"
	},
	"trigger": "Win A Prize"
};