export const scratchModalWrapper = {
	"tagName": "div",
	"id": "%22lucky-coupon-%22+id",
	"meta": {
		"closeItem": {
			"className": "close"
		},
		"type": "coupon",
		"language": "en"
	},
	"style": {
		"display": "none",
		"position": "fixed",
		"top": 0,
		"bottom": 0,
		"z-index": 2147483647,
		"width": "100%",
		"height": "100%",
		"background-color": "rgba(0,0,0,0.5)"
	},
	"content": []
};

export const scratchModalContent = {
	"content": [
		{
			"tagName": "meta",
			"name": "viewport",
			"content": "width=device-width, initial-scale=1.0, user-scalable=no"
		},
		{
			"tagName": "div",
			"className": "lucky-coupon-center",
			"style": {
				"display": "none"
			},
			"content": [
				{
					"tagName": "div",
					"className": "lucky-coupon-popup",
					"content": [
						
						{
							"tagName": "div",
							"className": "close",
							"content": [{
								"tagName": "div",
								"className": "inner",
								"textNode": " "
							}]
						},
						
						{
							"tagName": "div",
							"className": "header",
							"content": [{
								"tagName": "div",
								"className": "left-text",
								"data": { "text": "startScreen.title" }
							}]
						},

						{
						
							"tagName": "div",
							"className": "lucky-coupon-popup-inner",
							"content": [
								{
									"tagName": "ul",
									"meta": "coupons",
									"className": "prize-list",
									"content": []
								},
								{
									"tagName": "div",
									"className": "footer",
									"content": [
										{
											"tagName": "div",
											"className": "left-text small-middle",
											"data": {"text": "winScreen.note"}
										}
									]
								}
							]
						
						},

						{
							"tagName": "div",
							"className": "emailForm",
							"content": [
								{
									"tagName": "div",
									"className": "inner-form",
									"content": [
										{
											"tagName": "div",
											"className": "email-logo-wrapper",
											"content": [
												{
													"tagName": "div",
													"className": "email-logo"
												},
												{
													"tagName": "div",
													"className": "email-text"
												}
											]
										},
										{
											"tagName": "div",
											"className": "description",
											"data": {"text": "startScreen.description"}
										},
										{
											"tagName": "div",
											"className": "input-wrapper",
											"id": "WooHoo",
											"content": [
												{
													"tagName": "input",
													"className": "username-input",
													"type": "text",
													"placeholder": "Enter your full name",
													"name": "userName",
													"data": {"text": "startScreen.userName"},
													"style": {"visibility": "hidden"}
												}
											]
										},
										{
											"tagName": "input",
											"type": "email",
											"maxlength": "500",
											"title": "Email",
											"placeholder": "Enter your email address",
											"meta": "email",
											"name": "email",
											"className": "email-input",
											"data": {"text": "startScreen.email"}
										},
										{
											"tagName": "div",
											"className": "recart-messenger-widget",
											"style": {"display": "none"},
											"data": {"source": "woohoo"},
											"content": [{
												"tagName": "div",
												"className": "fake-fecebook-form",
												"style": {"display": "none"}
											}]
										},
										{
											"tagName": "span",
											"style": {
												"display": "none",
												"position": "relative"
											},
											"content": [
												{
													"tagName": "span",
													"className": "exclamation",
													"textNode": "!",
													"content": [
														{
															"tagName": "span",
															"className": "arrow-box-wrapper",
															"content": [
																{
																	"tagName": "span",
																	"className": "arrow-box"
																}
															]
														}
													]
												}
											]
										}
									]
								},
								{
									"tagName": "div",
									"className": "bottom-text",
									"content": [
										{
											"tagName": "div",
											"className": "checkbox-wrapper",
											"style": {"display": "none"},
											"content": [
												{
													"tagName": "label",
													"className": "checkbox-container",
													"content": [
														{
															"id": "coupon-check",
															"tagName": "input",
															"type": "checkbox"
														},
														{
															"tagName": "span",
															"className": "checkmark"
														}
													]
												},
												{
													"tagName": "label",
													"className": "agree-wrapper",
													"attributes": {"for": "coupon-check"},
													"textNode": "I Agree to subscribe to the newsletter",
													"meta": "gpdr",
													"data": {
														"name": "gpdr",
														"text": "startScreen.gdpr"
													}
												}
											]
										},
										
										{
											"className": "div",
											"tagName": "note",
											"data": {"text": "startScreen.note"},
											"style": {"display": "block"}
										}
									]
								}
							]
						},
						
						{
							"tagName": "div",
							"className": "lucky-coupon-popup-inner",
							"content": [
							
								{
									"tagName": "div",
									"className": "final-screen-wrapper",
									"content": [
										{
											"tagName": "div",
											"className": "text-bold scratch-congratulation",
											"data": {"text": "winScreen.congratulations"},
											"textNode": "Congratulations"
										},
										
										{
											"tagName": "div",
											"className": "text-bold scratch-you-got",
											"data": {"text": "winScreen.youGot"},
											"textNode": "You got a"
										},
										
										{
											"tagName": "div",
											"className": "upper-text",
											"content": [
												{"tagName": "div"},
												{"tagName": "div"}
											]
										},
										
										{
											"tagName": "div",
											"className": "lower-text",
											"content": [
												{
													"tagName": "div",
													"className": "scratch-discount-is",
													"data": {"text": "winScreen.yourDiscountCodeIs"},
													"textNode": "Your Discount Code Is:"
												},
												{"tagName": "div"}
											]
										},
										
										{
											"tagName": "button",
											"className": "continue-btn",
											"data": {"text": "winScreen.button"},
											"textNode": "Continue & Use Discount"
										}
									]
								}
							]
						}
					]
				},

                {
                    "tagName": "div",
                    "className": "powered-by",
                    "content": [
                        {
                            "tagName": "div",
                            "className": "powered-by-wrapper",
                            "content": [
                                {
                                    "tagName": "a",
									"href" : "https://apps.shopify.com/woohoo",
									"target": "_blank",
                                    "className": "powered-by-image"
                                }
                            ]
                        }
                    ]
                }
			]
		},
		{
			"tagName": "div",
			"className": "lucky-coupon-bar",
			"style": {
				"display": "none"
			},
			"content": [
				{
					"tagName": "span",
					"className": "coupon-code-text",
					"textNode": "Your ",
					"meta": "bar-your",
					"data": {
						"text": "bar.your"
					}
				},
				{
					"tagName": "strong",
					"className": "coupon-code-win",
					"textNode": ""
				},
				{
					"tagName": "span",
					"className": "coupon-code-text",
					"textNode": "coupon code",
					"meta": "bar-coupon-code",
					"data": {
						"text": "bar.couponCode"
					},
					"style": { "margin-right": "5px" }
				},
				{
					"tagName": "strong",
					"className": "coupon-code-code",
					"content": [{
						"tagName": "input",
						"type": "text",
						"id": "copy-coupon",
						"value": ""
					}]
				},
				{
					"tagName": "span",
					"className": "coupon-code-reserved",
					"style": {"padding": "0 5px"},
					"textNode": "is reserved for",
					"data": {
						"text": "bar.reservedFor"
					},
					"meta": "bar-reserved-for"
				},
				{
					"tagName": "strong",
					"className": "coupon-code-time",
					"content": [
						{
							"tagName": "span",
							"textNode": "15m:"
						},
						{
							"tagName": "span",
							"textNode": "00s"
						}
					]
				},
				{
					"tagName": "span",
					"className": "fa fa-times-thin fa-2x",
					"aria-hidden":  "true"
				},
				{
					"tagName": "button",
					"className": "copy-code",
					"textNode": "Copy Code",
					"meta": "bar-copy-code",
					"data": {
						"text": "bar.copyCode",
						"clipboardTarget": "#copy-coupon"
					}
				}
			]
		},
		
		{
			"tagName": "div",
			"className": "lucky-coupon-trigger",
			"style": {"display": "none"},
			"content": [
				{
					"tagName": "div",
					"className": "close",
					"style": {"display": "none"},
					"content": [{
						"tagName": "div",
						"className": "inner",
						"textNode": " "
					}]
				},
				{
					"tagName": "div",
					"meta": "trigger",
					"data": {
						"name":"trigger",
						"text": "trigger"
					},
					"className": "trigger-text",
					"textNode": "Win A Prize"
				},
				{
					"tagName": "div",
					"className": "gift-image"
				}
			]
		}
	]
};
