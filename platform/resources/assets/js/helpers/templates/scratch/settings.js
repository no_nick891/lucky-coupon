export const scratchColors = {
	hex: '#fff',
	hsl: {
		h: 255,
		s: 255,
		l: 255,
		a: 1
	},
	hsv: {
		h: 255,
		s: 255,
		v: 255,
		a: 1
	},
	rgba: {
		r: 255,
		g: 255,
		b: 255,
		a: 1
	},
	a: 1
};
//@todo: make one common file and make it expandable for all games
//@todo: keep in mind to make the same on backend
export const scratchGameSettings = {
	"position": "center",
	"triggerPosition": "right",
	"font": "%27Open Sans%27, Helvetica, Arial, sans-serif",
	"animation": "fadeIn",
	"colors": {
		"background": "#000",
		"coverText": "#fff",
		"couponText": "#000",
		"button": "#f30927",
		"buttonText": "#fff"
	},
	"giftImage": "gift1",
	"behavior": {
		"language": "en",
		"showPlayGameTrigger": {
			"yes": "1",
			"no": "0"
		},
		"collectEmailFromUsers": {
			"yes": "1",
			"no": "0"
		},
		"collectEmailWithRecart": "0",
		"collectNameFromUsers": {
			"yes": "0",
			"no": "1"
		},
		"chooseInputFieldType": {
			"emailField": "1",
			"facebookMessengerField": "0"
		},
		"startDisplayTheGame": {
			"atOnce": "0",
			"underTheFollowingConditions": {
				"whenTheUserIsLivingTheWebsite": "1",
				"whenTheUserReachesPercentOfThePage": {
					"value": "1",
					"percent": "100"
				},
				"afterSeconds": {
					"value": "1",
					"seconds": "10"
				}
			},
			"onSpecialPagePlace": "0"
		},
		"frequency": {
			"onEveryPageView": "1",
			"notMoreThanOnceEveryNumberTimePerUser": {
				"value": "0",
				"number": "1",
				"time": {
					"day": "1",
					"week": "0",
					"month": "0"
				}
			}
		},
		"stopToDisplayTheGame": {
			"never": "0",
			"underTheFollowingConditions": {
				"afterTheUserPerformsTheAction": "1",
				"afterShowingItNumberTimesToTheUser": {
					"value": "1",
					"number": "1"
				}
			}
		},
		"howDoYouWantYourCustomerToReceiveTheCoupon": {
			"winnigGameScreen": "1",
			"sendTroughEmail": "0",
			"winningGameScreenAndEmail": "0"
		},
		"couponReceiveEmailText": {
			"from": "storename@gmail.com",
			"discountCodeSentToYourEmail": "Discount code sent to your email",
			"couponCode": "Your coupon code from",
			"congratulation": "Congratulations",
			"youWon": "YOU WON A",
			"yourDiscount": "Your Discount Code Is:",
			"dontForgetToApplyYourCouponToTheRelevantFieldDoingTheCheckoutProcess": "Don't forget to apply your coupon code to the relevant field doing the checkout process",
			"goToSite": "Go to site",
			"freeProduct": "free product",
			"freeShipping": "Free Shipping",
			"cash": "Cash",
			"discount": "Discount"
		},
		"continueUseDiscount": {
			"value": 0,
			"url": "",
			"target": 1
		},
		"whereShouldTheGameAppear": [{
			"type": "1",
			"url": "*"
		}],
		"countDownTimeTimeMin": "15",
		"countDownTime": "1",
		"makeGPDRCompliance": "0",
		"isPoweredByVisible": "1",
		"receiveEmailLogoImage": ""
	}
};