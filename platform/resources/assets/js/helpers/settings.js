import { addedDefaultSettings } from '../lucky-coupon/settings/addedDefaultSettings';

export function changeBehaviorSetting(behavior, topSetting, prop, value) {
	if(behavior[topSetting].hasOwnProperty(prop)){
		if(behavior[topSetting][prop].hasOwnProperty('value')) {
			behavior[topSetting][prop].value = value;
		} else if(typeof behavior[topSetting][prop] === 'string') {
			behavior[topSetting][prop] = value;
		} else {
			for(let subProp in behavior[topSetting][prop]) {
				changeBehaviorSetting(behavior[topSetting], prop, subProp, value);
			}
		}
	}
};

export const settingsSetter = {
	setDefaultData(settings) {
		for (let index in addedDefaultSettings) {
			this.setData(settings, index, addedDefaultSettings[index]);
		}
		try {
			if (typeof settings.colors.design !== 'undefined') {
				this.setData(settings, "wheelLogoImage", {
					"wheelLogo": "",
					"opacityWheelLogo": "1.0"
				});
			}
		} catch(exception) {}
	},

	setData(object, key, data) {
		let splicedKeys = key.split('.'),
			shiftedKey = splicedKeys.shift(),
			joinedKeys = splicedKeys.join('.');
		if (splicedKeys.length > 0 && object.hasOwnProperty(shiftedKey)) {
			this.setData(object[shiftedKey], joinedKeys, data);
		} else if (!object.hasOwnProperty(shiftedKey) && shiftedKey !== 'text') {
			object[shiftedKey] = data;
		}
	}
};