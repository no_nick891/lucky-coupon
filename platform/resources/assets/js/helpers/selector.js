export function getLink(target) {
	return (target.tagName == 'LI') ? 
		target.querySelector('a') : target;
}

export function getLI(target) {
	return (target.tagName == 'UL') ? 
		target.querySelector('li') : target;
}

export function getParent(target, className) {
	if (target.className.indexOf(className) !== -1) return target;
	return getParent(target.parentNode, className);
}

export function getParentByTag(target, tagName) {
	if (target.tagName === tagName) return target;
	return getParent(target.parentNode, tagName);
}