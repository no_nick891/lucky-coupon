import Flash from './flash.js';

export default {
	
	getAllSitesSuccess(response) {
		if (response.data.sites.length > 0) {
			this.$store.commit('addSites', response.data.sites);
			this.$store.commit('currentSite', this.selectedSiteId);
		}
		this.$store.commit('sitesLoaded');
	},
	
	getAllSitesError(err) {
		if (err.response.hasOwnProperty('data')) {
			Flash.setError(err.response.data);
			if (err.response.data === 'Subscription expired.') {
				this.$router.push({path: '/upgrade'});
			}
		}
		// this.$store.commit('sitesLoaded');
	}
};