import {post} from './api';

export const frameDataLoader = {
	
	vue: {},
	
	handle: function (context, gamesId, handler) {
		handler = handler || function(){};
		this.vue = context;
		this.loadData(gamesId, handler);
	},
	
	loadData: function (gamesId, handler) {
		post('/api/v1/setting/all/list', {game_id: gamesId})
			.then(response => {
				let data = response.data;
				this.vue.$store.state._settings.frameData = data.settings;
				this.vue.$store.state._coupons.frameData = data.coupons;
				this.vue.$store.state._integrations.frameData = data.integrations;
				handler();
			});
	}
};