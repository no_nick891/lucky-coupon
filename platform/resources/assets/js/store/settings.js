import { post, update, del } from '../helpers/api';

import { defaultGameSettings } from '../helpers/templates/default/settings';

import { changeBehaviorSetting, settingsSetter} from '../helpers/settings';

import Flash from '../helpers/flash';

const state = {
	settings: {},
	preLoaded: 0,
	loaded: 0,
	updated: 0,
	defaultSettings: JSON.parse(JSON.stringify(defaultGameSettings)),
	blockSave: false,
	newFile: false,
	newWheelFile: false,
	frameData: [],
	desktop: true,
	changes: false,
	textSectionName: '',
	modules: {
		loadLogo: {},
		wheelLoadLogo: {}
	}
};

const getters = {};

const actions = {
	getSettings(context, gameId) {
		if(context.state.preLoaded === 0) {
			context.state.preLoaded = 1;
			post('/api/v1/setting/all', {game_id: gameId})
				.then(response => {
					if(response.data.setting) {
						context.commit('setSettings', response.data.setting);
					}
					context.state.loaded = context.state.loaded + 1;
					context.state.preLoaded = 0;
				})
		}
	},
	updateSettings(context, settings) {
		context.state.updated = 0;
		update('/api/v1/setting/update', settings)
			.then(response => {
				let modal = settings.hasOwnProperty('modal') ? settings.modal : true;
				if(response.data.settings) {
					if (modal) {
						Flash.setSuccess('Game settings updated.');
					}
					context.state.updated = 1;
				}
			})
			.catch(err => {
				if(err.response.status !== 200) {
					Flash.setError('Can\'t update game data.');
				}
			});
	},
	updateBackgroundImage(context, formData) {
		return post('/api/v1/setting/update/file', formData, { 'Content-Type': 'multipart/form-data' });
	},
	deleteBackgroundImage(context, gameId) {
		del('/api/v1/setting/delete/file', {game_id: gameId})
			.then(response => {
				let data = response.data;
				if(data.file === 0) {
					this.$store.state._settings.settings.backgroundImage.image = '';
					Flash.setError('File not deleted.');
				}
			})
			.catch(Flash.setAjaxError.bind(Flash));
	},
	updateLogoImage(context, data) {
		return post('/api/v1/setting/update/email-logo', data, {'Content-Type': 'multipart/form-data'});
	},
	deleteLogoImage(context, gameId) {
		del('/api/v1/setting/delete/email-logo', {game_id: gameId})
			.then(response => {
				let data = response.data;
				if (data.file === 0) {
					this.$store.state._settings.settings.behavior.receiveEmailLogoImage = '';
					Flash.setError('File not deleted.');
				}
			})
			.catch(Flash.setAjaxError.bind(Flash))
	},
	updateWheelLogoImage(context, data) {
		return post('/api/v1/setting/update/wheel-file-logo', data, {'Content-Type': 'multipart/form-data'});
	},
	deleteWheelLogoImage(context, gameId) {
		del('/api/v1/setting/delete/wheel-file-logo', {game_id: gameId})
			.then(response => {
				let data = response.data;
				if(data.file === 0) {
					this.$store.state._settings.settings.wheelLogoImage.wheelLogo = '';
					Flash.setError('File not deleted.');
				}
			})
			.catch(Flash.setAjaxError.bind(Flash));
	},
};

function modifyColors(colors){
	for(let color in colors) {
		var colorValue = colors[color];
		if(! colorValue.hasOwnProperty('hex')
			&& color !== 'design'
		) {
			colors[color] = {hex: colorValue};
		}
		if (color === 'design') {
			modifyColors(colors.design.custom.schema);
			if (!colors.design.colorize.schema.hasOwnProperty('hex')) {
				colors.design.colorize.schema = {hex: colors.design.colorize.schema};
			}
		}
	}
}

const mutations = {
	setSettings(state, settings) {
		settingsSetter.setDefaultData(settings);
		state.settings = settings;
	},
	clearSettings(state) {
		state.settings = {};
	},
	modifyColors(state) {
		modifyColors(state.settings.colors);
	},
	changeBehaviorSettings(state, toChange) {
		let topSetting = toChange.top,
			behavior = state.settings.behavior;
		for(let prop in behavior[topSetting]) {
			changeBehaviorSetting(behavior, topSetting, prop, '0');
		}
		changeBehaviorSetting(behavior, topSetting, toChange.current, '1');
	},
	changeBehaviorTimeSettings(state, change) {
		let time = state.settings.behavior.frequency.notMoreThanOnceEveryNumberTimePerUser.time;
		for(let timeName in time) {
			time[timeName] = '0';
		}
		time[change.timeName] = '1';
	}
};

export default {
	state,
	actions,
	mutations,
	getters
};