const state = {
	popup: {
		messengers: {},
		messengersData: {},
		tshirt: {},
		editEmailTemplate: {}
	}
};

const getters = {

};

const actions = {

};

const mutations = {
	
};

export default {
	state,
	actions,
	mutations,
	getters
};