import {get, post, update} from '../helpers/api';

import Flash from '../helpers/flash';

const state = {
	mailchimp: [],
	klaviyo: [],
	chatchamp: [],
	activecampaign: [],
	omnisend: [],
	mailchimpCurrentSite: {},
	klaviyoCurrentSite: {},
	chatchampCurrentSite: {},
	omnisendCurrentSite: {},
	activecampaignCurrentSite: {},
	servicesStateCounter: 0,
	frameData: [],
	integrations: [],
	loaded: 0
};

const getters = {};

const actions = {
	
	getServicesData(context) {
		return get('/api/v1/integrations/get');
	},
	
	getServiceInformation(context, serviceData) {
		return get('/api/v1/integrations/get/service', serviceData);
	},
	
	setServiceData(context, serviceData) {
		return post('/api/v1/integrations/add', serviceData);
	},
	
	updateService(context, serviceData) {
		return update('/api/v1/integrations/update', serviceData);
	},
	
	getIntegrations(context, gameId) {
		context.state.loaded = 0;
		get('/api/v1/integrations/get/input-type', {game_id: gameId})
			.then(function (response) {
				let data = response.data;
				if (data) {
					context.state.integrations = data;
				}
				context.state.loaded = 1;
			})
			.catch(Flash.setAjaxError.bind(Flash));
	}
	
};

const mutations = {
	
	currentIntegrations(state, siteId) {
		state.mailchimpCurrentSite = state.mailchimp[siteId] || undefined;
		state.klaviyoCurrentSite = state.klaviyo[siteId] || undefined;
        state.activecampaignCurrentSite = state.activecampaign[siteId] || undefined;
		state.omnisendCurrentSite = state.omnisend[siteId] || undefined;
	},
	
	setToZeroServiceCounter(state) {
		state.servicesStateCounter = 0;
	}
	
};

export default {
	state,
	actions,
	mutations,
	getters
};