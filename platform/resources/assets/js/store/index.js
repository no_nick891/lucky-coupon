import Vue from 'vue';

import Vuex from 'vuex';

import _sites from './sites';

import _users from './users';

import _games from './games';

import _coupons from './coupons';

import _settings from './settings.js';

import _shopifyApps from './shopify_apps';

import _plans from './plans';

import _subscriptions from './subscriptions';

import _integrations from './integrations';

import _affiliates from './affiliates';

import _app from './app';

import _shopifyAppOrders from './shopify_app_orders';

import _licenseKeys from './license_keys';

Vue.use(Vuex);

export const store = new Vuex.Store({
	modules: {
		_sites,
		_users,
		_games,
		_coupons,
		_settings,
		_shopifyApps,
		_plans,
		_subscriptions,
		_integrations,
		_affiliates,
		_app,
		_shopifyAppOrders,
		_licenseKeys
	}
});