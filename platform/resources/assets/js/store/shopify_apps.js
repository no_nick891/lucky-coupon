import { get, post } from '../helpers/api';

const state = {
	shopifyApp: {}
};

const getters = {};

const mutations = {};

const actions = {
	
	getShopifyApp(context, appId) {
		post('/api/v1/shopify-app/app', {id: appId})
			.then((response) => {
				if(response.data.hasOwnProperty('shopify_app')) {
					context.state.shopifyApp = response.data.shopify_app;
				}
			});
	},
	
	updateModalFlag(context, data) {
		post('/api/v1/shopify-app/app/update-modal-flag', data);
	}
};

export default {
	state,
	actions,
	mutations,
	getters
};