import { add, post, update, del } from '../helpers/api';

import Flash from '../helpers/flash';

const state = {
	formErrors: []
};

const getters = {

};

const actions = {
	
	addLicenseKey(context, licenseKeyData) {
		return add('/api/v1/license-key/add', licenseKeyData);
	}
	
};

const mutations = {

};

export default {
	state,
	actions,
	mutations,
	getters
};