import { get, post, update } from '../helpers/api';

import Flash from '../helpers/flash';

const state = {
	user: {},
	userLoaded: false,
	subPageAttended: false,
	profileUpdated: true
};

const getters = {

};

const actions = {

	getUser(context) {
		return get('/api/v1/user')
			.then(response => {
				let user = response.data.user;
				if (typeof user['id'] != 'undefined') {
					context.state.userLoaded = true;
					context.commit('setUser', user);
				}
			})
			.catch(err => {
				Flash.setError(err.response.data);
			});
	},
	
	saveUserProfileData(context, data) {
		context.state.profileUpdated = false;
		let partly = false,
			handlerEnd = function(){};
		if (data.hasOwnProperty('partly')) {
			partly = data.partly;
			delete data['partly'];
		}
		if (data.hasOwnProperty('handler')) {
			handlerEnd = data.handler;
			delete data['partly'];
		}
		update('/api/v1/user/update-profile', data)
			.then(response => {
				if (response) {
					context.commit('updateUserProfile', data);
					context.state.profileUpdated = true;
					if (!partly) {
						Flash.setSuccess('Profile updated');
					}
					handlerEnd();
				}
			})
			.catch(err => {
				Flash.setError(err.response.data);
				context.state.profileUpdated = true;
			});
	}
};

const mutations = {

	setUser(state, user) {
		state.user = user;
	},
	
	updateUserProfile(state, dataProfile){
		state.user.name = dataProfile.name;
		state.user.email = dataProfile.email;
		if (dataProfile.hasOwnProperty('is_first_time')) {
			state.user.is_first_time = dataProfile.is_first_time;
		}
	}

};

export default {
	state,
	actions,
	mutations,
	getters
};