import {get} from '../helpers/api';

import Flash from '../helpers/flash';

const state = {
	orders: [],
	loaded: true
};

const getters = {

};

const actions = {
	
	getShopifyOrders(context, params) {
		context.state.loaded = false;
		get('/api/v1/shopify-app/orders', params)
			.then((response) => {
				let data = response.data;
				if (data && data.orders) {
					context.state.orders = data.orders;
				}
				context.state.loaded = true;
			})
			.catch(Flash.setAjaxError.bind(Flash))
	}
	
};

const mutations = {

};

export default {
	state,
	actions,
	mutations,
	getters
};