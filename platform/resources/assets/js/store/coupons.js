import { post, update } from '../helpers/api';

import { defaultGameCoupons } from '../helpers/templates/default/coupons';

import Flash from '../helpers/flash';

const state = {
	coupons: [],
	preLoaded: 0,
	loaded: 0,
	updated: 0,
	defaultCoupons: defaultGameCoupons,
	frameData: []
};

const getters = {
	getCouponById: (state, getters) => (id) => {
		return state.coupons.find(coupon => {
			if (coupon.id === id || coupon.temp_id === id) {
				return coupon;
			}
			return false;
		});
	},
	calculateGravity: (state, getters) => (coupon) => {
		let gravitySum = 0;
		for (let coup in state.coupons) {
			gravitySum = gravitySum + parseInt(state.coupons[coup].gravity);
		}
		return gravitySum;
	},
	getModalCouponsObj:(state, getters) => (couponPattern) => {
		return state.coupons;
	}
};

const mutations = {
	//@todo: refactor this method
	setCoupon(state, coupon) {
		for(let i in state.coupons) {
			if(typeof coupon.id !== 'undefined' && ! isNaN(coupon.id)) {
				if(state.coupons[i].id === coupon.id) {
					for(let prop in coupon) {
						if(coupon.hasOwnProperty(prop)) {
							state.coupons[i][prop] = coupon[prop];
						}
					}
					return true;
				}
			} else {
				if(state.coupons[i].temp_id === coupon.temp_id) {
					for(let prop in coupon) {
						if(coupon.hasOwnProperty(prop)) {
							state.coupons[i][prop] = coupon[prop];
						}
					}
					return true;
				}
			}
		}
	},
	updateCoupons(state, coupons) {
		let i = 0;
		for(let index in coupons) {
			if(coupons.hasOwnProperty(index) && coupons[index] == 1) {
				if( ! isNumeric(state.coupons[i].id)) {
					state.coupons[i].id = parseInt(index);
				}
			}
			i++;
		}
		
		function isNumeric(n) {
			return !isNaN(parseFloat(n)) && isFinite(n);
		}
	},
	setCoupons(state, coupons) {
		for(var index in coupons) {
			let value = coupons[index].value;
			if(/^[0-9,\.]*$/gm.test(value)) {
				coupons[index].value = parseFloat(value);
			} else {
				try {
					coupons[index].value = decodeURI(value);
				} catch(e) {
					coupons[index].value = value;
				}
			}
		}
		state.coupons = coupons;
	}
};

const actions = {
	
	getCoupons(context, gameId) {
		if(context.state.preLoaded === 0){
			context.state.preLoaded = 1;
			post('/api/v1/coupon/all', {game_id: parseInt(gameId)})
				.then(request => {
					if (request.data.coupons.length > 0) {
						context.commit('setCoupons', request.data.coupons);
					}
					context.state.loaded = context.state.loaded + 1;
					context.state.preLoaded = 0
				})
				.catch(err => {
					Flash.setError('Can\'t get coupons. Something goes wrong.');
					context.state.preLoaded = 0
				});
		}
	},
	
	updateCoupon(context, coupon) {
		return update('/api/v1/coupon/update', coupon)
			.then(response => {
				if(response.data.coupon) {
					context.commit('setCoupon', coupon);
					if(typeof coupon.chance !== 'undefined') {
						context.dispatch('reCalculateChance', coupon);
					}
					Flash.setSuccess('Coupon updated.');
				} else {
					Flash.setError('Can\'t update coupons. Something goes wrong');
				}
			})
	},
	
	updateCoupons(context, coupons) {
		context.state.updated = 0;
		update('/api/v1/coupon/update', coupons)
			.then(response => {
				if(response.data.coupons) {
					context.commit('updateCoupons', response.data.coupons);
					context.state.updated = 1;
				}
			})
			.catch(err => {});
	},
	
	reCalculateChance(context, coupon) {
		let gravitySum = context.getters.calculateGravity(coupon);
		for(let coup of state.coupons) {
			coup.gravity = parseInt(coup.gravity);
			if (gravitySum === 0) {
				coup.chance = 0;
			} else {
				coup.chance = parseFloat((coup.gravity / gravitySum * 100).toFixed(1));
			}
		}
	}
};

export default {
	state,
	actions,
	mutations,
	getters
};