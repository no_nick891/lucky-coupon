import {get, post, update} from '../helpers/api';

import Flash from '../helpers/flash';

const state = {
	affiliates: []
};

const getters = {};

const actions = {
	getAffiliatesData(context) {
		return get('/api/v1/affiliates/all')
			.then(response => {
				let data = response.data;
				if (data.affiliates) {
					context.state.affiliates = data.affiliates;
				}
			}).catch(err => {
				Flash.setAjaxError(err)
			});
	},
	
	savePaypalEmail(context, email) {
		return update('/api/v1/affiliates/owner/update', {paypal_email: email})
			.then(response => {
				let data = response.data;
				if (data.affiliate_owner) {
					Flash.setSuccess('Affiliate information updated.')
				}
			})
			.catch(Flash.setAjaxError.bind(Flash));
	}
};

const mutations = {};

export default {
	state,
	actions,
	mutations,
	getters
};