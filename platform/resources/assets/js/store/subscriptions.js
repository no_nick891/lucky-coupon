import Flash from '../helpers/flash';

import {post} from '../helpers/api';

const state = {
	subscription: false
};

const getters = {};

const actions = {
	
	generateSubscription(context, plan) {
		post('/api/v1/subscription/generate-isracard-subscription', plan)
			.then(response => {
				let data = response.data;
				if (data.subscription !== false) {
					context.commit('saveSubscriptionData', data);
				}
			})
			.catch(err => {
				if (err.response.hasOwnProperty('data')) {
					Flash.setError(err.response.data);
				}
				Flash.setError(err.response.data);
			});
	}
	
};

const mutations = {
	
	saveSubscriptionData(state, data) {
		if (data.hasOwnProperty('subscription')) {
			state.subscription = data.subscription;
		}
	}
	
};

export default {
	state,
	actions,
	mutations,
	getters
};