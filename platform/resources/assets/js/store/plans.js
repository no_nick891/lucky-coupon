import { get } from '../helpers/api';

import Flash from '../helpers/flash';

const state = {
	plans: []
};

const getters = {

};

const actions = {

	getPlans(context, appId) {
		get('/api/v1/plans/all', {app_id: appId ? appId : 0})
			.then(response => {
				let data = response.data;
				if(data.plans.length > 0) {
					context.commit('addPlans', data.plans);
				}
			})
			.catch(err => {
				if(err.response.hasOwnProperty('data')) {
					Flash.setError(err.response.data);
				}
			});
	},
	
	getNewPlanLink(context, data) {
		return get('/api/v1/plans/upgrade', data);
	}
	
};

const mutations = {
	addPlans(state, plans) {
		state.plans = plans
	}
};

export default {
	state,
	actions,
	mutations,
	getters
};