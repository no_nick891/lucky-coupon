import { add, post, update, del } from '../helpers/api';

import Flash from '../helpers/flash';

const state = {
	siteId: null,
	site: {},
	sites: [],
	loaded: 0,
	formErrors: {}
};

const getters = {

	getSiteById: (state, getters) => (id) => {
		return state.sites.find(site => site.id === id);
	},

	isSameActive: (state, getters) => (data) => {
		let site = state.sites.find(
			site => { return site.id === data.id; }
		);
		return data.active === site.active;
	}

};

const actions = {

	getAllSites(context) {
		context.loaded = 0;
		return post('/api/v1/site/all');
	},

	createSite(context, data) {
		return add('/api/v1/site/add', data)
			.then(response => {
				if(response.data.add !== false) {
					data['id'] = response.data.site;
					context.commit('addSite', data);
					Flash.setSuccess('Site added successfully.');
					$('.close').click();
				}
			});
	},

	updateSite(context, data) {
		return update('/api/v1/site/update', data)
			.then(response => {
				if(response.data.update === 1) {
					context.commit('updateSiteInfo', data);
				} else {
					Flash.setError('Can\'t update site. Something goes wrong.');
				}
			});
	},

	deleteSite(context, data) {
		let site = context.state.sites.find(site => site.id === data.id);
		if(site !== false) {
			return del('/api/v1/site/delete', data)
				.then(response => {
					if(response.data.delete == 1) {
						context.commit('deleteSite', site.id);
					} else {
						Flash.setError('Can\'t delete site. Something goes wrong.');
					}
				})
				.catch(err => {
					Flash.setError(err.response.data);
				});
		}
		return false;
	}

};

const mutations = {
	
	setDefaultSite(state) {
		state.site = {};
	},
	
	setSiteId(state, siteId) {
		state.siteId = siteId;
	},

	addSite(state, site) {
		state.sites.push(site);
	},

	addSites(state, sites) {
		state.sites = sites;
	},

	sitesLoaded(state) {
		state.loaded = 1;
	},
	
	deleteSite(state, id) {
		state.sites = state.sites.filter(site => site.id !== id);
		if (state.sites.length === 0) {
			state.site = {};
		}
	},

	currentSite(state, siteId) {
		let currentSite = state.sites.find( site => {
			if (siteId === 0) return true;
			if (typeof site['id'] !== 'undefined') {
				return site.id === parseInt(siteId);
			}
		} );
		if (typeof currentSite !== 'undefined') {
			state.siteId = currentSite.id;
			state.site = currentSite;
		}
	},

	updateSite(state, data) {
		state.site = data;
	},

	updateSiteInfo(state, data) {
		for(let i in state.sites) {
			if(state.sites[i].id == data.id) {
				for(let prop in data) {
					state.sites[i][prop] = data[prop];
				}
				return true;
			}
		}
	},

	setFormErrors(state, data) {
		state.formErrors = data;
	},
	
};

export default {
	state,
	actions,
	mutations,
	getters
};