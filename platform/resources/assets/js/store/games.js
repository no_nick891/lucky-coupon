import { add, post, update, del } from '../helpers/api';

import { defaultGameSettings } from '../helpers/templates/default/settings';

import Flash from '../helpers/flash';

const state = {
	gameId: null,
	game: {},
	defaultGame: defaultGameSettings,
	games: [],
	subscribersCount: 0,
	formErrors: {},
	types: [
		{
			name: 'Lucky Coupon',
			type: 'coupon'
		}
	],
	loaded: 0,
	deleteGame: 0,
	justCreated: false,
	newGameType: false,
	frameDataLoaded: 0,
	impressionsMaxPopup: null,
	showFeedback: false,
	entities: {
		totalImpressions: '',
		totalCapturedEmails: '',
		totalCtr: '',
		revenueFromCoupons: ''
	},
	entitiesName: ['totalImpressions', 'totalCapturedEmails', 'totalCtr', 'revenueFromCoupons']
};

const getters = {
	getGameById: (state, getters) => (id) => {
		return state.games.find(game => game.id === id);
	}
};

const actions = {
	createGame(context, game) {
		return add('/api/v1/game/add', game);
	},
	
	createGameWithSite(context, gameType) {
		return add('/api/v1/game/create-with-site', gameType);
	},
	
	getGames(context, id) {
		context.commit('gamesReset');
		post('/api/v1/game/all', {site_id: parseInt(id)})
			.then(response => {
				if (response.data.games !== false) {
					context.commit('addGames', response.data.games);
				} else {
					Flash.setError('Can\'t get games.');
				}
				context.commit('gamesLoaded');
			})
			.catch(Flash.setAjaxError.bind(Flash));
	},

	updateGame(context, game) {
		update('/api/v1/game/update', game)
			.then(response => {
				if(response.data.game === 1) {
					context.commit('updateGameInfo', game);
				}
			})
			.catch(err => {
				if(err.response.status !== 200) {
					Flash.setError('Can\'t update game data.');
				}
			});
	},
	
	deleteGame(context, info) {
		del('/api/v1/game/delete', info)
			.then(response => {
				if(response.data.game === 1) {
					context.commit('deleteGame', info.id);
					context.state.deleteGame = context.state.deleteGame + 1;
					if (info.is_created) {
						Flash.setSuccess('Game deleted.');
					}
				}
			})
			.catch(err => {
				if(err.response.status !== 200) {
					Flash.setError('Can\'t delete game data.');
				}
			});
	},
	
	currentGame(context, id) {
		context.commit('currentGame', id);
	},
	
	getSubscribers(context, id) {
		return post('/api/v1/game/subscribers', {id: id});
	},
	getSubscribersCount(context) {
		var ids = [];
		for(let game of context.state.games) {
			ids.push(game.id);
		}
		post('/api/v1/game/subscribers/count', {ids: ids})
			.then(response => {
				if(response.data.count > 0) {
					context.state.subscribersCount = parseInt(response.data.count);
				}
			});
	}
};

const mutations = {
	clearGame(state) {
		state.game = {};
	},
	
	clearGames(state) {
		state.games = {};
	},
	
	clearGamesDataCounter(state) {
		state.frameDataLoaded = 0;
	},
	
	addGames(state, games) {
		state.games = games;
	},

	gamesLoaded(state) {
		state.loaded = 1;
	},
	
	gamesReset(state) {
		state.loaded = 0;
	},
	
	currentGame(state, gameId) {
		let currentGame = state.games.find( game => {
			if (gameId === 0) return true;
			if (typeof game['id'] !== 'undefined') {
				return game.id === gameId;
			}
		} );
		if (typeof currentGame !== 'undefined') {
			state.gameId = currentGame.id;
			state.game = currentGame;
		}
	},

	updateGameInfo(state, data) {
		for(let i in state.games) {
			if(state.games[i].id == data.id) {
				for(let prop in data) {
					state.games[i][prop] = data[prop];
				}
				return true;
			}
		}
	},
	
	deleteGame(state, id) {
		for(let i in state.games) {
			if(state.games[i].id === id) {
				state.games.splice(parseInt(i), 1);
			}
		}
	},
	
	updateGamesSort(state, games) {
		state.games = games;
	}

};

export default {
	state,
	actions,
	mutations,
	getters
};