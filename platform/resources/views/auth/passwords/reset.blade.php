@extends('layouts.auth')

@section('content')
<div class="container login-register-pages">
    <h3 class="text-center padding-header icon-White_Logo1"></h3>
    <div class="col-md-5 auth">
        <form class="form-signin" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-input text-center">
                <p class="form-signin-heading">Reset Password</p>
            </div>
            <div class="form-input{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="inputEmail">Email</label>
                <input type="email" id="inputEmail" class="form-control" name="email" value="{{ $email or old('email') }}" required="" autofocus="">
                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-input{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="inputPassword">Password</label>
                <input type="password" id="inputPassword" class="form-control" name="password" required="">
                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-input{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="inputPassword">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-input text-center">
                <button class="btn btn-lg btn-primary btn-lk" type="submit">Reset Password</button>
            </div>
        </form>
    </div>
    <p class="text-center">Back to <a href="/login">Sign in</a></p>
</div>
@endsection
