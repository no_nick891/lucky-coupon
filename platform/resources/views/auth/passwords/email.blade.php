@extends('layouts.auth')

@section('content')
<div class="container login-register-pages ">
    <div class="col-md-5 auth form-signin">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="form-signin" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="form-input text-center">
                <p class="form-signin-heading">Forgot your password?</p>
            </div>
            <div class="form-input">
                <p class="form-signin-heading reset-title">Enter your email address and we will send you password reset instructions.</p>
            </div>
            <div class="form-input {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="inputEmail">Email</label>
                <input type="email" id="inputEmail" class="form-control" name="email" value="{{ old('email') }}" required="" autofocus="">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-input text-center">
                <button class="btn btn-lg btn-primary btn-lk password-reset" type="submit">Send password reset link</button>
            </div>
        </form>
    </div>
    <p class="text-center"><a href="/login">Back to Sign In</a></p>
    <div class="sign-image"></div>
</div>
@endsection
