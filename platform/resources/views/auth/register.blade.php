@extends('layouts.auth')

@section('content')
<div class="container login-register-pages login-register">
    <a href="https://getwoohoo.com/"><h3 class="text-center padding-header icon-White_Logo1"></h3></a>
    <div class="col-md-5 auth">
        <form class="form-signin" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <input type="hidden" id="referral" name="ref" value="0">
            <input type="hidden" name="plan_name" value="{{ $selectedPlan }}">
            <div class="form-input text-center">
                <p class="form-signin-heading">Create new account</p>
            </div>
            <p class="text-center already-registered">
                @if($selectedPlan === '')
                    Already registered ?  <a href="/login">Sign In</a>
                @else
                    Special Deal Link - Only use with licence key.
                @endif
            </p>
            <div class="form-input{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Name</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-input{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="inputEmail">Email</label>
                <input type="email" id="inputEmail" class="form-control" name="email" value="{{ old('email') }}" required="" autofocus="">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
    
            @if(\Request::is('special-deal'))
                <div class="form-input{{ $errors->has('license_key') ? ' has-error' : '' }}">
                    <label for="licenseKey">Licence key</label>
                    <input type="text" id="licenseKey" class="form-control" name="license_key" value="{{ old('license_key') }}" required="" autofocus="">
                    @if ($errors->has('license_key'))
                        <span class="help-block">
                            <strong>{{ $errors->first('license_key') }}</strong>
                        </span>
                    @endif
                </div>
            @endif
            
            <div class="form-input{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="inputPassword">Password</label>
                <input type="password" id="inputPassword" class="form-control" name="password" required="">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-input">
                <label for="password-confirm">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
            <div class="woohoo-terms">By clicking "Sign Up" I agree to Woohoo Terms of Service and Privacy Policy.</div>
            <div class="form-input text-center">
                <button class="btn btn-lg btn-primary btn-lk" type="submit">Sign Up</button>
            </div>
        </form>
    </div>
    @if(!$selectedPlan && !\Request::is('special-deal'))
        <div class="shopify-login">
            <span class="shopify-image"></span><span class="have-shopify">Have a Shopify store?  <a class="link-shopify" href="https://apps.shopify.com/woohoo" target="_blank">Install from Shopify market</a></span>
        </div>
    @endif
    <div class="sign-image"></div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('/js/register.js') }}"></script>
@endsection