@extends('layouts.auth')

@section('content')
<div class="container login-register-pages login-page">
    <a href="https://getwoohoo.com/"><h3 class="text-center padding-header icon-White_Logo1"></h3></a>
    <div class="col-md-5 auth">
        <form class="form-signin" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-input text-center">
                <p class="form-signin-heading">Sign in to your account</p>
            </div>
            <p class="text-center already-registered">New user? <a href="{{ route('register') }}">Create an account</a></p>
            <div class="form-input{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="inputEmail">Email</label>
                <input type="email" id="inputEmail" class="form-control" name="email" value="{{ old('email') }}" required="" autofocus="">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-input{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="inputPassword">Password</label>
                <input type="password" id="inputPassword" class="form-control" name="password" required="">
            </div>
            <div class="form-input">
                <div class="checkbox">
                    <label class="forgot-label">
                        <a href="{{ route('password.request') }}">Forgot password?</a>
                    </label>
                    <span class="form-input text-center">
                        <button class="btn btn-lg btn-primary btn-lk sign-login-btn" type="submit">Sign in</button>
                    </span>
                </div>
            </div>
        </form>
    </div>
    <div class="sign-image"></div>
</div>
@endsection
