@extends('layouts.shopify')
@if ($isPayed)
	<h1 style="text-align: center;">Payment made successfully.</h1>
	<h2 style="text-align: center;">Please <a href="javascript:void(0)" onclick="window.parent.location.reload();">reload</a> page.</h2>
	<script>
		window.top.dataLayer = window.top.dataLayer || [];
		window.top.dataLayer.push({ 'event': 'purchaseComplete' });
		try {fbq('track', 'Purchase');} catch(e){}
	</script>
@else
	<h1 style="text-align: center;">Waiting for answer from payment gateway too long.</h1>
	<h2 style="text-align: center;">Please wait a few minutes until payment gateway return response then try to <a href="javascript:void(0)" onclick="window.parent.location.reload();">reload</a> page.</h2>
@endif