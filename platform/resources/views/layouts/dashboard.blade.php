<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MKS99QB');</script>
    <!-- End Google Tag Manager -->
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '681450462230256');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=681450462230256&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Hotjar Tracking Code for https://app.getwoohoo.com/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1010017,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="G_i2jmaLy2V3lbp736OCEJaP_Ttzu_TY5sdB5HAC93I" />
	
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'GetWooHoo') }}</title>

	<link rel="icon" href="/img/favicon.ico">

	<link href="{{ mix('/css/compiled/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/style.css?v=21') }}" rel="stylesheet">
	
	<!-- Scripts -->
	<script>
		window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
	</script>
	<script async type='text/javascript' src='https://editor.kodeless.io/files/5bf520af6c964d15ec2a9e29/production/kodeless.js'></script>
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MKS99QB"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
	<div id="app">
		
		<div class="vue-app woohoo-wrapper">
			<!-- Top menu -->
			<div class="navbar navbar-default navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						
						<!-- Collapsed Hamburger -->
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						
						<!-- Branding Image -->
						<a class="icon-White_Logo1 navbar-brand" href="/"></a>
					</div>
					
					<div class="collapse navbar-collapse" id="app-navbar-collapse">
						<div class="col-md-8 col-xs-12 no-padding"></div>
						<!-- Left Side Of Navbar -->
						
						<!-- Right Side Of Navbar -->
						<ul class="nav navbar-nav navbar-right nav-right-center">
							<!-- Authentication Links -->
							<li class="dropdown">
								<a href="#" class="dropdown-toggle profile-dropdown" id="login-dropdown"
								   data-toggle="dropdown" role="button" aria-expanded="false">
									{{ Auth::user()->name }} <span class="caret"></span>
								</a>
								<ul class="dropdown-menu profile" role="menu">
									<li>
										<a href="/">Platform</a>
										<a href="/admin/all-statistics">All by month</a>
										<a href="/admin/by-games">All by games</a>
										<a href="/admin/shopify">Shopify stores</a>
										<a href="/admin/shopify/statistics">Shopify statistics</a>
										<a href="/admin/shopify/subscribers">Shopify emails</a>
										<a href="/admin/shopify/orders">Shopify Sales</a>
										<a href="/admin/regular">Regular stores</a>
										<a href="{{ route('logout') }}"
										   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
											Logout
										</a>
										<a href="/admin/shopify/duplicate-stores">Duplicate stores</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div><!-- End top menu -->
			
			<div class="container fixed-width no-padding" style="width: 1280px">
				
				<div id="app">
					@yield('content')
				</div>
				
			</div>
		</div>
		
	</div>

	<!-- Scripts -->
	@section('scripts')
		<script src="{{ asset('/js/app.js') }}"></script>
	@show
	<!-- Scripts -->
</body>
</html>
