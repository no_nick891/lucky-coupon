<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Lucky Coupon') }}</title>

    <!-- Styles -->
    @if(!App::environment('local'))
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
    @endif
    <link href="{{ mix('/css/compiled/app.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet"/>

    <link rel="stylesheet" href="{{ asset('/css/auth.css') }}"/>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div id="app">

    @yield('content')

</div>

@section('scripts')
@show
</body>
</html>