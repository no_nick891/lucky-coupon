@extends('layouts.dashboard')

@section('content')
	@if(Request::is('admin/regular'))
		{{ $users->links() }}
		@include('admin.dashboard.search_store')
	@else
		{!! $users->appends($currentPage)->render() !!}
		@include('admin.shopify.dashboard_filter', ['payingApps' => $payingApps])
	@endif
	@if(count($apps) > 0)
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Data</th>
					<th>Games statistic</th>
				</tr>
			</thead>
			<tbody>
				@php
					$i = 1;
				@endphp
				@foreach($apps as $key => $value)
					<tr>
						<td>
							{{ $i }}
						</td>
						<td>
							<table class="table table-striped text-center">
								@php
									$isracard = isset($value['isracard']) && $value['isracard'];
									$installed = $value['installed'] ? 'Yes' : 'No';
								@endphp
								<tr>
									<td style="border-right: 1px solid #ddd;">User id</td>
									<td>{{ $value['id'] }}</td>
								</tr>
								<tr>
									<td style="border-right: 1px solid #ddd;">url</td>
									<td>
										@if($isracard)
											{!! $value['name'] !!}
										@else
											<a href="https://{{ $value['name'] }}" target="_blank">https://{{ $value['name'] }}</a>
										@endif
									</td>
								</tr>
								<tr>
									<td style="border-right: 1px solid #ddd;">Install date</td>
									<td>{{ $value['date_create'] }}</td>
								</tr>
								<tr>
									<td style="border-right: 1px solid #ddd;">Paying</td>
									<td>{{ $installed }}</td>
								</tr>
								<tr>
									<td style="border-right: 1px solid #ddd;">Last login data</td>
									<td>{{ $value['last_login'] or 'none' }}</td>
								</tr>
								<tr>
									<td colspan="2" class="text-left">
										<ul class="list-unstyled">
											<li>
												<a href="/admin/app-auth/{{ $value['id'] }}">
													<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
													 Login
												</a>
											</li>
											@if(Request::is('admin/shopify'))
												<li>
													<a href="{{ url('/admin/shopify/scripts/' . $value['id']) }}">
														<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
														Installed scripts
													</a>
												</li>
												<li>
													<a href="{{ url('/admin/shopify/orders/' . $value['id']) }}" target="_blank">
														<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
														 Orders
													</a>
												</li>
												<li>
													<a href="javascript:void(0)" class="fetch-orders" data-id="{{ $value['id'] }}">
														<span class="glyphicon glyphicon-hdd" aria-hidden="true"></span>
														Fetch orders from shopify
														&nbsp;<span class="glyphicon glyphicon-cd" style="display: none" aria-hidden="true"></span>
													</a>
												</li>
												<li>
													<a href="{{ url('/admin/charges/' . $value['id']) }}">
														<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
														Transactions
													</a>
												</li>
												<li>
													<a href="{{ url('/admin/shopify/webhooks/' . $value['id']) }}" target="_blank">
														<span class="glyphicon glyphicon-send" aria-hidden="true"></span>
														 Installed WebHooks
													</a>
												</li>
												<li>
													<button class="btn btn-success set-trial">
														<span class="text">Set trial days</span>
														<span class="glyphicon glyphicon-refresh" style="display:none;"></span>
													</button>
													<input
															type="number"
															data-user_id="{{$value['id']}}"
															class="trial-value" value=""
													/>
													<div class="form-group">
														<label class="control-label"></label>
													</div>
												</li>
											@endif
										</ul>
									</td>
								</tr>
								@if(Request::is('admin/shopify'))
									<tr style="display: none">
										<td style="border-right: 1px solid #ddd;">Set counter</td>
										<td>
											<button class="btn btn-success set-counter">
												<span class="text">Set to</span><span class="glyphicon glyphicon-refresh" style="display:none;"></span>
											</button>
											<input class="counter-value"
												   type="number"
												   data-user_id="{{$value['id']}}"
												   value="{{ isset($value['counter']) && $value['counter'] === 0 ? 0 : data_get($value, 'counter', '') }}"
											/>
										</td>
									</tr>
								@endif
							</table>
						</td>
						<td>
							<table style="text-align:left;" class="table table-striped text-center">
								<thead>
									<tr>
										<th>Game ID</th>
										<th>Type</th>
										<th>Active</th>
										<th>Desktop Impressions</th>
										<th>Desktop Hits</th>
										<th>Desktop Ctr.</th>
										<th>Mobile Impressions</th>
										<th>Mobile Hits</th>
										<th>Mobile Ctr.</th>
										<th>Days</th>
									</tr>
								</thead>
								<tbody>
								@if(isset($games[$key]))
									@foreach($games[$key] as $k => $game)
										<tr>
											<td>{{ $game->id }}</td>
											<td>{{ $game->type }}</td>
											<td>{{ $game->active ? 'Yes' : 'No' }}</td>
											<td>{{ $game->adminStatistic->impressions}}</td>
											<td>{{ $game->adminStatistic->hits }}</td>
											<td>
												{{ number_format($game->adminStatistic->hits === 0 || ! $game->adminStatistic->hits ? 0 : floatVal($game->adminStatistic->hits/$game->adminStatistic->impressions * 100), 2) }}
											</td>
											<td>{{ $game->adminStatistic->device_impressions }}</td>
											<td>{{ $game->adminStatistic->device_hits }}</td>
											<td>
												{{ number_format($game->adminStatistic->device_hits === 0 || ! $game->adminStatistic->device_hits ? 0 : floatVal($game->adminStatistic->device_hits/$game->adminStatistic->device_impressions * 100), 2) }}
											</td>
											<td>
												{{ intVal(abs(time() - strtotime($game->created_at))/86400) }}
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="6">There is no games</td>
									</tr>
								@endif
								</tbody>
							</table>
							<hr>
						</td>
					</tr>
					@php
						$i++;
					@endphp
				@endforeach
			</tbody>
		</table>
		@if(Request::is('admin/regular'))
			{{ $users->links() }}
		@else
			{!! $users->appends($currentPage)->render() !!}
		@endif
	@else
		There is no statistics here.
	@endif
@endsection

@section('scripts')
	@parent
	<script>
		window['requestUrl'] = '{{Request::url()}}';
	</script>
	<script src="/js/admin/dashboard.js?v1.3"></script>
	<script src="/js/admin/store-action.js"></script>
@stop