@extends('layouts.dashboard')

@section('content')
	<h2>Charges for shop {{ $name }}</h2>
	<table class="table table-striped table-bordered text-center">
		<thead>
			<tr>
				<th>#</th>
				<th>is test mode</th>
				<th>price</th>
				<th>status</th>
				<th>billing on</th>
				<th>created at</th>
				<th>updated at</th>
				<th>activated on</th>
				<th>trial ends on</th>
				<th>canceled on</th>
			</tr>
		</thead>
		<tbody>
			@if(count($charges) > 0)
				@foreach($charges as $charge)
					<tr>
						<td>{{ $charge->id }}</td>
						<td>{{ var_export($charge->test, true) }}</td>
						<td>{{ $charge->price }}</td>
						<td>{{ $charge->status }}</td>
						<td>{{ $charge->billing_on or ' - ' }}</td>
						<td>{{ $charge->created_at or ' - ' }}</td>
						<td>{{ $charge->updated_at or ' - ' }}</td>
						<td>{{ $charge->activated_on or ' - ' }}</td>
						<td>{{ $charge->trial_ends_on or ' - ' }}</td>
						<td>{{ $charge->cancelled_on or ' - ' }}</td>
					</tr>
				@endforeach
			@else
				<tr>
					<td colspan="10">There is no charges.</td>
				</tr>
			@endif
		</tbody>
	</table>

@endsection