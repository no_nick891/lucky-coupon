@extends('layouts.dashboard')

@section('content')
	@if(count($apps) > 0)
		<p>Total shops: {{ count($apps) }}</p>
		<table class="table table-striped table-bordered text-center">
			<thead>
			<tr>
				<th class="text-center">Total Active Shops</th>
				<th class="text-center">Total Impressions</th>
				<th class="text-center">Total Hits</th>
				<th class="text-center">Total Ctr.</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>{{ $totalActive }}</td>
				<td>{{ $gamesStatistics['total']['impressions'] }}</td>
				<td>{{ $gamesStatistics['total']['hits'] }}</td>
				<td>{{ $gamesStatistics['total']['ctr'] }}</td>
			</tr>
			</tbody>
		</table>
		<table class="table table-striped table-bordered text-center">
			<thead>
			<tr>
				<th class="text-center">Total Desktop Impressions</th>
				<th class="text-center">Total Desktop Hits</th>
				<th class="text-center">Total Desktop Ctr.</th>
				<th class="text-center">Total Mobile Impressions</th>
				<th class="text-center">Total Mobile Hits</th>
				<th class="text-center">Total Mobile Ctr.</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>{{ $gamesStatistics['desktop']['impressions'] }}</td>
				<td>{{ $gamesStatistics['desktop']['hits'] }}</td>
				<td>{{ $gamesStatistics['desktop']['ctr'] }}</td>
				<td>{{ $gamesStatistics['mobile']['impressions'] }}</td>
				<td>{{ $gamesStatistics['mobile']['hits'] }}</td>
				<td>{{ $gamesStatistics['mobile']['ctr'] }}</td>
			</tr>
			</tbody>
		</table>
	@endif
@endsection