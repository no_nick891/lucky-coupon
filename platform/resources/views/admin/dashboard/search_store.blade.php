<div class="row">
	<div class="col-md-7">
		<div class="col-md-2 no-right-padding">
			Search store:
			<h6 style="margin: 0">by name</h6>
		</div>
		<div class="col-md-6 no-padding" style="margin-bottom: 30px">
			<input type="text" class="form-control" name="shopify-url" value="{{data_get($_GET, 'filter', 'created') !== 'created' ? data_get($_GET, 'value', '') : ''}}">
		</div>
		<div class="col-md-1">
			<button class="btn btn-default search-by-name">Search</button>
		</div>
	</div>
</div>