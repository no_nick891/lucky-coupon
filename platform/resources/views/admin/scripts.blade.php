@extends('layouts.dashboard')

@section('content')
	<h2>Charges for shop {{ $name }}</h2>
	<table class="table table-striped table-bordered text-center">
		<thead>
		<tr>
			<th>id</th>
			<th>src</th>
			<th>Event</th>
			<th>Created at</th>
			<th>Updated at</th>
			<th>Display scope</th>
			<th>Actions</th>
		</tr>
		</thead>
		<tbody>
		@if(count($scripts) > 0)
			@foreach($scripts as $script)
				<tr>
					<td>{{ $script->id }}</td>
					<td>{{ $script->src }}</td>
					<td>{{ $script->event }}</td>
					<td>{{ $script->created_at }}</td>
					<td>{{ $script->updated_at }}</td>
					<td>{{ $script->display_scope }}</td>
					<td>
						<form action="/admin/shopify/scripts/delete/{{ $script->id }}" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="delete" />
							<input type="hidden" name="app_id" value="{{$appId}}">
							<button class="btn btn-link">Delete</button>
						</form>
					</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="10">
					There is no scripts.
					<a href="{{ url('/admin/shopify/scripts/install/' . $userId) }}">Install script</a>
				</td>
			</tr>
		@endif
		</tbody>
	</table>

@endsection