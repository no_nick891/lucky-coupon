@extends('layouts.dashboard')

@section('content')
	@foreach($statistics as $date => $data)
		<h1><b>{{ substr($date, 0, -3) }}</b></h1>
		<table class="table table-striped table-bordered text-center">
			<thead>
			<tr>
				<th class="text-center">Total Impressions</th>
				<th class="text-center">Total Hits</th>
				<th class="text-center">Total Ctr. %</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>{{ data_get($data, 'all.impressions', 0) }}</td>
				<td>{{ data_get($data, 'all.hits', 0) }}</td>
				<td>{{ number_format(data_get($data, 'all.ctr', 0), 2) }}</td>
			</tr>
			</tbody>
		</table>
		<table class="table table-striped table-bordered text-center">
			<thead>
			<tr>
				<th class="text-center">Total Desktop Impressions</th>
				<th class="text-center">Total Desktop Hits</th>
				<th class="text-center">Total Desktop Ctr. %</th>
				<th class="text-center">Total Mobile Impressions</th>
				<th class="text-center">Total Mobile Hits</th>
				<th class="text-center">Total Mobile Ctr. %</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>{{ data_get($data, 'pc.impressions', 0) }}</td>
				<td>{{ data_get($data, 'pc.hits', 0) }}</td>
				<td>{{ number_format(data_get($data, 'pc.ctr', 0), 2) }}</td>
				<td>{{ data_get($data, 'mobile.impressions', 0) }}</td>
				<td>{{ data_get($data, 'mobile.hits', 0) }}</td>
				<td>{{ number_format(data_get($data, 'mobile.ctr', 0), 2) }}</td>
			</tr>
			</tbody>
		</table>
		<br/>
	@endforeach
@endsection