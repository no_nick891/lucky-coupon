@extends('layouts.dashboard')

@section('content')
	<h3>Shopify subscribers:</h3>
	@if(count($subscribers) > 0)
		{{ $subscribers->links() }}
		<table class="table table-striped table-bordered text-center">
			<thead>
			<tr>
				<th class="text-center">Date & time</th>
				<th class="text-center">Platform</th>
				<th class="text-center">Game type</th>
				<th class="text-center">Store name</th>
				<th class="text-center">Email</th>
			</tr>
			</thead>
			<tbody>
				@foreach($subscribers as $subscriber)
					<tr>
						<td>{{ $subscriber->hit_date }}</td>
						<td>{{ $subscriber->device_type }}</td>
						<td>{{ $subscriber->game_type }}</td>
						<td>{{ $subscriber->shop_name }}</td>
						<td>{{ $subscriber->subscriber_email }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		{{ $subscribers->links() }}
	@else
		<p>There is no data for subscribers.</p>
	@endif
@endsection