@extends('layouts.dashboard')

@section('content')
	@foreach($usersList as $date => $users)
		<h2>Users created at {{ $date }}</h2>
		<table class="table table-striped table-bordered text-center">
			<thead>
			<tr>
				<th>Original store</th>
				<th>Original created at</th>
				<th>Duplicated store</th>
				<th>Duplicated created at</th>
			</tr>
			</thead>
			<tbody>
				@if(count($users) > 0)
					@foreach($users as $user)
						<tr>
							<td>{{$user->originalName}}</td>
							<td>{{$user->originalCreatedAt}}</td>
							<td>{{$user->duplicateName or '-'}}</td>
							<td>{{$user->duplicateCreatedAt or '-'}}</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="4">
							There is no users.
						</td>
					</tr>
				@endif
			</tbody>
		</table>
	@endforeach
@endsection