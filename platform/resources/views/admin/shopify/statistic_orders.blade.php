@extends('layouts.dashboard')

@section('content')
	<div class="row">
		@if (count($ordersHitsStatistics) > 0)
			@foreach($ordersHitsStatistics as $date => $ordersHitsStatistic)
				@if (data_get($ordersHitsStatistic, 'orders_count', 0) === 0)
					@continue
				@endif
				<h1 style="font-weight: bold">{{ substr($date, 0, 7) }}</h1>
				<table class="table table-striped table-bordered text-center">
					<thead>
						<tr>
							<th class="text-center">Total Sales</th>
							<th class="text-center">Total Sales Amount</th>
							<th class="text-center">% Total Emails / Total Sales</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{ data_get($ordersHitsStatistic, 'orders_count', 0) }}</td>
							<td>${{ number_format(data_get($ordersHitsStatistic, 'orders_sum', 0), 2) }}</td>
							<td>{{
								data_get($ordersHitsStatistic, 'orders_count', 0) === 0 ? 0 :
							 	number_format((int)data_get($ordersHitsStatistic, 'hit_count', 0) / (int)data_get($ordersHitsStatistic, 'orders_count', 0), 2)
							}}</td>
						</tr>
					</tbody>
				</table>
			@endforeach
		@endif
	</div>
	<div class="row">
		@if (count($orders) > 0)
			{{ $orders->links() }}
			<table class="table table-striped table-bordered">
				<thead>
				<tr>
					<th>Order Date & Time</th>
					<th>Store name</th>
					<th>Game type</th>
					<th>Value</th>
					<th>Coupon code</th>
					<th>Total sale</th>
					<th>Email</th>
				</tr>
				</thead>
				<tbody>
				@foreach($orders as $order)
					<tr>
						<td>{{ $order->created_at }}</td>
						<td>{{ $order->name }}</td>
						<td>{{ $order->type }}</td>
						<td>{{ $order->coupon_type === 'type' ? urldecode($order->value) : (
							($order->coupon_type === 'discount' ? '%' . $order->value : '') .
							($order->coupon_type === 'cash' ? '$' . $order->value : '') .
							(strstr($order->coupon_type, 'free') ? $order->coupon_type : '')
						) }}</td>
						<td>{{ $order->coupon_code }}</td>
						<td><span class="show-items" style="color: #5A64FF; position: relative; cursor: pointer;">
									${{ $order->order_total_price / 100 }}
								@php
									$payloadDecoded = json_decode($order->payload);
									$lineItems = data_get($payloadDecoded, 'line_items', []);
								@endphp
								<div class="items" style="display: none; position: absolute; left: 50%; right: 50%; color: #000;">
										<table class="table table-striped table-bordered">
											<tbody>
												@foreach($lineItems as $item)
													<tr>
														<td><img src="{{asset('/shopify/picture/' . $order->user_id . '/' . $item->product_id . '.jpg')}}" height="45" width="45" alt=""></td>
														<td>{{ $item->title }}</td>
														<td>${{ (float)$item->price * (int)$item->quantity * getTax($item->tax_lines) }}</td>
													</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</span></td>
						<td>{{ data_get($payloadDecoded, 'email', '') }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			{{ $orders->links() }}
		@else
			<span>No orders here</span>
		@endif
	</div>
@endsection

@section('scripts')
	@parent
	<script src="/js/admin/orders.js"></script>
@endsection