@extends('layouts.dashboard')

@section('content')
	<div class="row">
		@if (count($orders) > 0)
			{{ $orders->links() }}
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Order ID</th>
						<th>Last action</th>
						<th>Coupon code</th>
						<th>Last order status</th>
						<th>Order total price(include discount)</th>
						<th>Created</th>
						<th>Updated</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
						<tr>
							<td>{{ $order->order_id }}</td>
							<td>{{ $order->action }}</td>
							<td>{{ $order->coupon_code }}</td>
							<td>{{ $order->order_status }}</td>
							<td><span class="show-items" style="color: #5A64FF; position: relative; cursor: pointer;">
									${{ $order->order_total_price / 100 }}
									<div class="items" style="display: none; position: absolute; left: 50%; right: 50%; color: #000;">
										<table class="table table-striped table-bordered">
											<tbody>
												@php $lineItems = json_decode($order->payload)->line_items; @endphp
												@foreach($lineItems as $item)
													<tr>
														<td><img src="{{asset('/shopify/picture/' . $userId . '/' . $item->product_id . '.jpg')}}" height="45" width="45" alt=""></td>
														<td>{{ $item->title }}</td>
														<td>${{ (float)$item->price * (int)$item->quantity * getTax($item->tax_lines) }}</td>
													</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</span></td>
							<td>{{ $order->created_at }}</td>
							<td>{{ $order->updated_at }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{ $orders->links() }}
		@else
			<span>No orders here</span>
		@endif
	</div>
	
@endsection

@section('scripts')
	@parent
	<script src="/js/admin/orders.js"></script>
@endsection