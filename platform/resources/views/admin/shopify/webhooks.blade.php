@extends('layouts.dashboard')

@section('content')
	
	<div class="row">
		@if($shopWebHooks && count($shopWebHooks) > 0)
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Address</th>
						<th>Topic</th>
						<th>Created</th>
						<th>Updated</th>
					</tr>
				</thead>
				<tbody>
					@foreach($shopWebHooks as $webHook)
						<tr>
							<td>{{$webHook->id}}</td>
							<td>{{$webHook->address}}</td>
							<td>{{$webHook->topic}}</td>
							<td>{{$webHook->created_at}}</td>
							<td>{{$webHook->updated_at}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@else
			There is no WebHooks
		@endif
	</div>
	
@endsection