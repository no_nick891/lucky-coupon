<div class="row">
	<form action="">
		<div class="col-md-6">
			<div class="col-md-2" style="padding-top: 5px">
				Order by:
			</div>
			<div class="col-md-3" style="margin-bottom: 30px">
				<select class="form-control" name="filter">
					<option {{ data_get($_GET, 'filter') === 'created' ? 'selected' : '' }} value="created">Created</option>
					<option {{ data_get($_GET, 'filter') === 'paying' ? 'selected' : '' }} value="paying">Paying</option>
					<option {{ data_get($_GET, 'filter') === 'best-total-ctr' ? 'selected' : '' }} value="best-total-ctr">Best total ctr</option>
					<option {{ data_get($_GET, 'filter') === 'best-total-hits' ? 'selected' : '' }} value="best-total-hits">Best total hits</option>
				</select>
			</div>
			<div class="created-filter-wrapper" style="{{
						data_get($_GET, 'filter', 'created') === 'created' ? 'display:block' : 'display:none'
					}}">
				<div class="col-md-4 no-padding">
					<label style="margin-top: 6px;min-height: 20px;padding-left: 20px;margin-bottom: 0;font-weight: 400">
						<input type="checkbox" {{ in_array(data_get($_GET, 'value', ''), ['hide-small', '']) ? 'checked' : '' }}> Hide small assets
						<h6 style="margin: 0;display:none;">without stores games hits < 11</h6>
					</label>
				</div>
				<button class="btn btn-default">Apply</button>
			</div>
		</div>
	</form>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="col-md-6">
			Number of paying users: {{ $payingApps }}
		</div>
	</div>
</div>
<div class="row">&nbsp;</div>
@include('admin.dashboard.search_store')