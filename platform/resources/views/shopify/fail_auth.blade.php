@extends('layouts.shopify')

<div style="
		position: relative;
		float: left;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);">
	@if (Session::get('message'))
		{{ Session::get('message') }}
	@else
		You should accept payment conditions to install application.
	@endif
</div>