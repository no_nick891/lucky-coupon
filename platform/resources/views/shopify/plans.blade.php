@extends('layouts.shopify')

<div class="container fixed-width no-padding shopify" style="margin-top: 36px; display: none;">
	<div>
		<p class="plan-header text-center" style="font-size: 36px">Straightforward, affordable pricing</p>
		<p class="text-center">Choose the right plan to match your needs</p>
	</div>
	<div class="row plans-wrapper">
		@foreach($plans as $index => $plan)
			<div class="plan-popular-tag {{ $plan->name === 'Pro' ? 'hot' : '' }}"
				 style="opacity: {{ $plan->name !== 'Pro' ? 0 : 1 }}"
			>
				<i class="em">🔥&nbsp;</i>
				<span>&nbsp;Most popular</span>
				<div class="plan-close-shadow install-page {{ $plan->name === 'Pro' ? 'hot' : '' }}"></div>
			</div>
		@endforeach
		@foreach($plans as $plan)
			<div class="plan {{ $plan->name === 'Pro' ? 'tagged' : '' }} {{ $plan->name === 'Starter' ? 'free-plan' : '' }}">
				@if($plan->amount_screen === 'FREE')
					<p class="plan-price">
						<span class="currency">Free</span>
					</p>
				@else
					<p class="plan-price">
						<span class="currency">{{ $plan->amount_screen }}</span>
						<span class="period">/{{ $plan->period }}</span>
					</p>
				@endif
				<a class="btn btn-default new-games-button button-plan"
				   data-plan="{{ $plan->id }}"
				   href="javascript:void(0)">Choose plan</a>
				<p class="plan-name">{{ $plan->name }}</p>
				<ul class="plan-list">
					<li class="bold">{{ $plan->condition }}</li>
					@if (!in_array($plan->name, ['Starter', 'Basic']))
					    <li class="bold">Recart Messenger Integration</li>
						<li class="bold">MailChimp Integration</li>
						<li class="bold">Klaviyo Integration</li>
					@endif
					<li>Game selection</li>
					<li>Smart triggers</li>
					<li>Real-time statistics</li>
					<li>Design customization</li>
					<li>Set coupons</li>
					<li>Image Upload</li>
					<li>Frequency settings</li>
					<li>Real-time statistics</li>
				</ul>
			</div>
		@endforeach
	</div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"
></script>
<script>
	$(document).ready(function() {
		var queryObject = JSON.parse('{!! $query !!}'),
			plansWrapper = document.querySelector('.plans-wrapper');
		plansWrapper.addEventListener('mouseover', popularBorderAdd, false);
		plansWrapper.addEventListener('click', planSelectHandler, false);
		
		document.querySelector('.shopify').style.display = 'block';
		
		function popularBorderAdd(e) {
			var target = $(e.target),
				plan = target.parents('.plan'),
				color = 'transparent';
			plan = plan.hasClass('plan') ? plan : target;
			if (plan.hasClass('tagged')) {
				color = '#06B0FF';
			}
			$('.plan-popular-tag.hot').css(getSideBorder(color, true));
		}
		
		function getSideBorder(color, top = false) {
			let object = {
				'border-left-color': color,
				'border-right-color': color
			};
			if (top) {
				object['border-top-color'] = color;
			}
			return object;
		}
		
		function getPlanButton(target) {
			let $element = $(target);
			if ($element.hasClass('button-plan')) {
				return target;
			} else if ($element.hasClass('plan')) {
				return $element.find('.button-plan')[0];
			} else {
				return $element.parents('.plan').find('.button-plan')[0];
			}
		}
		
		function planSelectHandler(e) {
			var target = e.target,
				button = getPlanButton(target);
			inactiveButtons(button);
			updateApp(button);
		}
		
		function inactiveButtons(target) {
			target.setAttribute('disabled', true);
			target.innerHTML = 'Loading...';
			blockButtons(true);
			target.blur();
		}
		
		function blockButtons(state) {
			var buttons = document.querySelectorAll('.button-plan');
			for(var button of buttons) {
				if (state) {
					button.setAttribute('disabled', state);
				} else {
					button.removeAttribute('disabled');
				}
			}
		}
		
		function updateApp(target) {
			var query = copyObject(queryObject);
			query.plan_id = target.dataset.plan;
			patch('/shopify/app', query, handleResponse);
		}
		
		function handleResponse(response) {
			if(response.shopify_app > 0) {
				var queryObj = copyObject(queryObject);
				window.location = '/shopify/auth/install?' + objToQuery(queryObj);
			}
		}
		
		function objToQuery(obj) {
			var str = "";
			for (var key in obj) {
				if (str != "") {
					str += "&";
				}
				str += key + "=" + encodeURIComponent(obj[key]);
			}
			return str;
		}
		
		function patch(url, data, handler) {
			ajax('PATCH', url, data, handler);
		}
		
		function ajax(method, url, data, handler) {
			var request = new XMLHttpRequest();
			request.addEventListener('load', function(event){
				eventDispatcher(event, handler);
			});
			request.open(method, url);
			request.setRequestHeader('X-CSRF-Token', getCsrfToken());
			request.setRequestHeader('Content-type', 'application/json');
			request.send(JSON.stringify(data));
		}
		
		function getCsrfToken() {
			return document.querySelector('meta[name="csrf-token"]').getAttribute('content');
		}
		
		function eventDispatcher(event, handler) {
			var response = responseHandler(event);
			if(response) {
				handler.call(null, JSON.parse(response))
			}
		}
		
		function responseHandler(event) {
			var target = event.target;
			if (target.readyState === 4) {
				return target.response;
			}
			return false;
		}
		
		function copyObject(object){
			return JSON.parse(JSON.stringify(object));
		}
		
		String.prototype.find = function(string) {
			return this.valueOf().indexOf(string) !== -1;
		}
	});
</script>