@extends('layouts.shopify')

@if (isset($newUser))
	Thank you for installing application.
@else
	Welcome back.
@endif

<p>
	After two seconds you will redirect to application.
</p>
<p>
	If it didn't happen click link below manually.
</p>
<p>
	<a href="/">Go to application</a>
</p>
<script>
	! function() {
		setTimeout(function() {
			window.location.href = '/';
		}, 2000);
	}()
</script>