@extends('layouts.shopify')

@section('content')
    <div class="row">
        <form action="/shopify/auth/install">
            {{csrf_field()}}
            <div class="group-form">
                <input type="text" name="shop_url" value="{{ old('shop_url') }}"/>
            </div>
            <div class="group-form">
                <button type="submit">Install application Lucky Coupon</button>
            </div>
        </form>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection