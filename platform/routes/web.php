<?php

Route::group(['middleware' => ['auth']], function() {

	Route::get('/', 'EntryPoint@index');

});

Auth::routes();

Route::get('special-deal', 'Auth\RegisterController@showRegistrationForm');

Route::get('/{id}/script.js', 'Api\TemplateController@getScript');

Route::get('/{game_id}/wheel.svg', 'Api\TemplateController@getWheel');

Route::get('/background-image/{user_id}/{game_id}/{file_name?}', 'Api\TemplateController@getBackgroundImage');

Route::group(['prefix' => 'shopify'], function () {
	
	Route::get('auth', 'Shopify\InstallAppController@getInstallForm');
	
	Route::get('auth/install', 'Shopify\InstallAppController@install');
	
	Route::get('auth/update-token', 'Shopify\InstallAppController@updateToken');
	
	Route::get('auth/install/complete', 'Shopify\InstallAppController@installComplete');
	
	Route::get('auth/install/fail', 'Shopify\InstallAppController@installFail');
	
	Route::get('plans', 'Shopify\InstallAppController@getPlans');
	
	Route::get('charge', 'Shopify\InstallAppController@charge');
	
	Route::get('charge/complete', 'Shopify\InstallAppController@chargeComplete');
	
	Route::patch('app', 'Shopify\InstallAppController@updateApp');
	
	Route::get('picture/{product_id}.jpg', 'Api\ShopifyOrderController@getProductImage');
	
	Route::get('picture/{user_id}/{product_id}.jpg', 'Api\ShopifyOrderController@getAdminProductImage');
	
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'isAdmin']], function() {
	
	Route::get('/charges/{id}', 'Admin\DashboardController@charges');
	
	Route::get('/app-auth/{id}', 'Admin\DashboardController@appAuth');
	
	Route::get('/regular', 'Admin\DashboardController@regular');
	
	Route::group(['prefix' => 'shopify'], function() {
		
		Route::get('/', 'Admin\DashboardController@index');
		
		Route::get('/statistics', 'Admin\DashboardController@shopifyStatistics');
		
		Route::get('/subscribers', 'Admin\DashboardController@allShopifySubscribers');
		
		Route::post('/counter', 'Admin\DashboardController@setCounter');
		
		Route::get('/webhooks/{user_id}', 'Admin\DashboardController@getWebHooks');
		
		Route::get('/orders/', 'Admin\DashboardController@getShopifyOrdersStatistics');
		
		Route::get('/orders/{user_id}', 'Admin\DashboardController@getShopifyOrders');
		
		Route::post('/orders/fetch', 'Admin\DashboardController@fetchShopifyOrders');
		
		Route::get('/scripts/{user_id}', 'Admin\DashboardController@getShopifyScript');
		
		Route::get('/scripts/install/{user_id}', 'Admin\DashboardController@installShopifyScript');
		
		Route::delete('/scripts/delete/{script_id}', 'Admin\DashboardController@deleteShopifyScript');
		
		Route::post('/set-trial', 'Admin\DashboardController@extendShopifyTrial');
		
		Route::get('duplicate-stores', 'Admin\DashboardController@duplicateStores');
	});
	
	Route::get('/all-statistics', 'Admin\DashboardController@allStoresStatistics');
	
	Route::get('/by-games', 'Admin\DashboardController@perGameStatistics');
	
});

Route::group(['middleware' => ['auth']], function(){
	
	Route::get('/admin', 'Admin\DashboardController@exitToAdmin');
	
	Route::group(['prefix' => 'integrations'], function () {
			
		Route::get('get-auth-url', 'Integrations\InstallController@getAuthUrl');
		
	});
	
});

Route::group(['prefix' => 'integrations'], function (){
	
	Route::get('mailchimp-handler', 'Integrations\InstallController@mailChimp');
	
});

Route::group(['prefix' => 'webhooks'], function() {
	
	Route::group(['prefix' => 'isracard'], function() {
		
		Route::post('subscription', 'Webhooks\IsracardController@subscription');
		
		Route::get('/', 'Webhooks\IsracardController@index');
		
	});
	
	Route::group(['prefix' => 'shopify'], function() {
	
		Route::post('orders', 'Webhooks\ShopifyController@orders');
	
		Route::post('setup', 'Webhooks\ShopifyController@setup');
		
	});
	
});
/*
Route::get('test', function (){

});*/