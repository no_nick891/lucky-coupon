$(document).ready(function() {
	/** namespace window.requestUrl */
	setUp();
	
	$('select[name=filter]').on('change', function(e) {
		var value = $(this).val(),
			created = $('.created-filter-wrapper');
		if(value !== 'created') {
			created.hide();
			window.location.href = window['requestUrl'] + '?filter=' + value;
		} else {
			created.show();
		}
	});
	
	$('.created-filter-wrapper button').on('click', function(e){
		e.preventDefault();
		let checked = $('.created-filter-wrapper input[type=checkbox]').prop('checked'),
			value = checked ? 'hide-small' : 'all';
		window.location.href = window['requestUrl'] + '?filter=created&value=' + value;
	});
	
	$('button.search-by-name').on('click', function(e) {
		e.preventDefault();
		searchUserByName.call(this);
	});
	
	$('input[name=shopify-url]').on('keyup', function(e){
		if (e && e.keyCode === 13) {
			searchUserByName.call(this);
		}
	});
	
	function searchUserByName() {
		var	form = $(this.parentNode.parentNode),
			input = form.find('input[name=shopify-url]'),
			shopifyUrl = input.val(),
			regExp = getRegexp();
		if (shopifyUrl === '') {
			window.location.href = window['requestUrl'];
		} else if (regExp.test(shopifyUrl)) {
			window.location.href = window['requestUrl'] + '?filter=shop-url&value=' + shopifyUrl;
		} else {
			input.css('border-color', 'red');
		}
	}
	
	$('input[name=shopify-url]').on('keyup', function(e) {
		var borderColor = $(this).css('border-color');
		if(borderColor === 'rgb(255, 0, 0)') {
			$(this).css('border-color', '#ccd0d2')
		}
	});
	
	$('.set-counter').on('click', function(){
		let button = $(this),
			input = $(this.parentNode).find('.counter-value'),
			value = parseInt(input.val()),
			userId = input.data('user_id');
		setLoading(button);
		$.post({
			'url': '/admin/shopify/counter',
			'data': {
				'counter': value,
				'user_id': userId
			}
		}).done(function(data){
			if('counter' in data && data.counter){
				setFinishLoading(button);
			}
		})
	});
	
	$('.set-trial').on('click', function() {
		let button = $(this),
			input = $(this.parentNode).find('.trial-value'),
			value = parseInt(input.val()),
			userId = input.data('user_id');
		setLoading(button);
		$.post({
			'url': '/admin/shopify/set-trial',
			'data': {
				'trial': value,
				'user_id': userId
			}
		}).done(function(data) {
			setFinishLoading(button);
			let text, error = true;
			if (!data.hasOwnProperty('trial' )) {
				text = 'Something goes wrong.';
			} else if (!data.trial) {
				text = 'Failed to set trial days.';
			} else {
				error = false;
				text = 'Trial days successfully set.';
			}
			showMessage(button, text, error);
		});
	});
	
	function setUp() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		setCookie('adminArea', 1);
	}
	
	function setLoading(element) {
		element.attr('disabled', true);
		element.find('.text').hide();
		element.find('.glyphicon').show();
	}
	
	function setFinishLoading(element) {
		element.attr('disabled', false);
		element.find('.text').show();
		element.find('.glyphicon').hide();
	}
	
	function showMessage(element, text, isError) {
		let parent = element.parent();
		parent.show();
		if (isError) {
			parent.find('.control-label').parent().addClass('has-warning');
		}
		parent.find('.control-label').text(text);
	}
	
	function clearError(element) {
		let parent = element.parent();
		parent.show();
		parent.find('.control-label').text('');
	}
	
	function setCookie(cookieName, cookieValue, expirationDays) {
		expirationDays = expirationDays ? expirationDays : 365;
		let domain = window.location.hostname,
			d = new Date();
		d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
		let expires = 'expires='+d.toUTCString();
		document.cookie = cookieName + '=' + cookieValue + ';' + expires + ';' + 'domain=' + domain + ';path=/';
	}
	
	function getRegexp() {
		return window.location.pathname.search('regular') > -1
			? new RegExp(/(.+)@(.+)\.([a-zA-Z]+)/gm)
			: new RegExp(/([a-zA-Z0-9]+)\.([a-zA-Z0-9]+)\.(com)/gm)
	}
	
});