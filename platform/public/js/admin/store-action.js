$(function(){
	
	let fetchOrderButton = new FetchOrdersButton();
	fetchOrderButton.init();
	
	function FetchOrdersButton() {
	
		this.button = $('.fetch-orders');
		
		this.init = function() {
			if (!this.button[0]) return false;
			this.button.on('click', this.fetchData.bind(this));
		};
		
		this.fetchData = function(e) {
			let link = e.target.tagName === 'A' ? $(e.target) : $(e.target).parents('a'),
				id = link.data('id'),
				postObj = {
					'url': '/admin/shopify/orders/fetch',
					'data': {'user_id': id}
				};
			link.find('.glyphicon-cd').addClass('spin').show();
			$.post(postObj).done(this.getResult.bind(this));
		};
		
		this.getResult = function(data) {
			if (data.fetch) {
				console.log('Orders updated');
			}
			if (data.reason) {
				alert(data.reason);
			}
			this.button.find('.glyphicon-cd').removeClass('spin').hide();
		};
	}
	
});