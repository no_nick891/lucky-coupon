webpackJsonp([3],{

/***/ "./resources/assets/js/helpers/templates/default/text.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultText", function() { return defaultText; });
var defaultText = {
	"startScreen": {
		"title": "Here's your chance to win a BIG prize!",
		"description": "Enter your email address to find out if you%22ve the winner",
		"email": "Enter your email address",
		"userName": "Enter your full name",
		"button": {
			"startLine": "Start",
			"endLine": "GAME"
		},
		"coupon": "Coupon",
		"freeShipping": {
			"free": "Free",
			"shipping": "Shipping"
		},
		"cash": "Cash",
		"freeProduct": {
			"free": "Free",
			"product": "Product"
		},
		"note": "From time to time, we may send you more special offers. You can unsubscribe at any time.",
		"gdpr": "I agree to subscribe to the mailing list"
	},
	"playScreen": {
		"waitingForResults": "Let%22s see what you won..."
	},
	"winScreen": {
		"congratulations": "Congratulations",
		"youGot": "You got a",
		"discount": "Discount ",
		"yourDiscountCodeIs": "Your Discount Code Is:",
		"button": "Continue & Use Discount",
		"note": "In order to use this discount add it to the relevant field in checkout"
	},
	"bar": {
		"your": "Your ",
		"couponCode": "coupon code",
		"reservedFor": "is reserved for",
		"copyCode": "Copy Code",
		"copied": "Copied",
		"failedCopy": "Failed to copy"
	},
	"trigger": "Win A Prize"
};

/***/ })

});