webpackJsonp([2],{

/***/ "./resources/assets/js/helpers/templates/gift/text.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "giftText", function() { return giftText; });
var giftText = {
	"startScreen": {
		"title": "Choose your mystery gift to reveal your prize",
		"email": "Enter your email address",
		"button": "PLAY",
		"note": "From time to time, we may send you more special offers. You can unsubscribe at any time.",
		"gdpr": "I agree to subscribe to the mailing list"
	},
	"playScreen": {
		"waitingForResults": "Pick a gift to see what you won"
	},
	"winScreen": {
		"yourDiscountCodeIs": "Your Discount Code Is:",
		"button": "Continue & Use Discount",
		"note": "In order to use this discount add it to the relevant field in checkout"
	},
	"bar": {
		"your": "Your ",
		"couponCode": "coupon code",
		"reservedFor": "is reserved for",
		"copyCode": "Copy Code",
		"copied": "Copied",
		"failedCopy": "Failed to copy"
	},
	"trigger": "Win A Prize"
};

/***/ })

});