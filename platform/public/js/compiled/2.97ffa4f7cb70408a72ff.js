webpackJsonp([2],{

/***/ "./resources/assets/js/helpers/templates/scratch/text.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "scratchText", function() { return scratchText; });
var scratchText = {
	"startScreen": {
		"title": "You've been Chosen! For a shot at a BIG discount",
		"description": "Enter your email address to find out if you%22ve the winner",
		"note": "Scratch off all areas! Find 3 similar to win a prize!"
	},
	"emailScreen": {
		"title": "Wow! You almost won!",
		"description": "Please enter your email to continue",
		"email": "Enter your email address",
		"userName": "Enter your full name",
		"button": "Continue",
		"note": "From time to time, we may send you more special offers. You can unsubscribe at any time.",
		"gdpr": "I agree to subscribe to the mailing list"
	},
	"winScreen": {
		"congratulations": "Congratulations",
		"youGot": "You got a",
		"yourDiscountCodeIs": "Your Discount Code Is:",
		"button": "Continue & Use Discount"
	},
	"bar": {
		"your": "Your ",
		"couponCode": "coupon code",
		"reservedFor": "is reserved for",
		"copyCode": "Copy Code",
		"copied": "Copied",
		"failedCopy": "Failed to copy"
	},
	"trigger": "Win A Prize"
};

/***/ })

});