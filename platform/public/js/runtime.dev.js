"use strict";
! function(w, d, isFrame, isAdmin, scriptPath) {
	
	var randomColor = (function() {
		
		// Seed to get repeatable colors
		var seed = null;
		
		// Shared color dictionary
		var colorDictionary = {};
		
		// Populate the color dictionary
		loadColorBounds();
		
		var randomColor = function (options) {
			
			options = options || {};
			
			// Check if there is a seed and ensure it's an
			// integer. Otherwise, reset the seed value.
			if (options.seed !== undefined && options.seed !== null && options.seed === parseInt(options.seed, 10)) {
				seed = options.seed;
				
				// A string was passed as a seed
			} else if (typeof options.seed === 'string') {
				seed = stringToInteger(options.seed);
				
				// Something was passed as a seed but it wasn't an integer or string
			} else if (options.seed !== undefined && options.seed !== null) {
				throw new TypeError('The seed value must be an integer or string');
				
				// No seed, reset the value outside.
			} else {
				seed = null;
			}
			
			var H,S,B;
			
			// Check if we need to generate multiple colors
			if (options.count !== null && options.count !== undefined) {
				
				var totalColors = options.count,
					colors = [];
				
				options.count = null;
				
				while (totalColors > colors.length) {
					
					// Since we're generating multiple colors,
					// incremement the seed. Otherwise we'd just
					// generate the same color each time...
					if (seed && options.seed) options.seed += 1;
					
					colors.push(randomColor(options));
				}
				
				options.count = totalColors;
				
				return colors;
			}
			
			// First we pick a hue (H)
			H = pickHue(options);
			
			// Then use H to determine saturation (S)
			S = pickSaturation(H, options);
			
			// Then use S and H to determine brightness (B).
			B = pickBrightness(H, S, options);
			
			// Then we return the HSB color in the desired format
			return setFormat([H,S,B], options);
		};
		
		function pickHue (options) {
			
			var hueRange = getHueRange(options.hue),
				hue = randomWithin(hueRange);
			
			// Instead of storing red as two seperate ranges,
			// we group them, using negative numbers
			if (hue < 0) {hue = 360 + hue;}
			
			return hue;
			
		}
		
		function pickSaturation (hue, options) {
			
			if (options.hue === 'monochrome') {
				return 0;
			}
			
			if (options.luminosity === 'random') {
				return randomWithin([0,100]);
			}
			
			var saturationRange = getSaturationRange(hue);
			
			var sMin = saturationRange[0],
				sMax = saturationRange[1];
			
			switch (options.luminosity) {
				
				case 'bright':
					sMin = 55;
					break;
				
				case 'dark':
					sMin = sMax - 10;
					break;
				
				case 'light':
					sMax = 55;
					break;
			}
			
			return randomWithin([sMin, sMax]);
			
		}
		
		function pickBrightness (H, S, options) {
			
			var bMin = getMinimumBrightness(H, S),
				bMax = 100;
			
			switch (options.luminosity) {
				
				case 'dark':
					bMax = bMin + 20;
					break;
				
				case 'light':
					bMin = (bMax + bMin)/2;
					break;
				
				case 'random':
					bMin = 0;
					bMax = 100;
					break;
			}
			
			return randomWithin([bMin, bMax]);
		}
		
		function setFormat (hsv, options) {
			
			switch (options.format) {
				
				case 'hsvArray':
					return hsv;
				
				case 'hslArray':
					return HSVtoHSL(hsv);
				
				case 'hsl':
					var hsl = HSVtoHSL(hsv);
					return 'hsl('+hsl[0]+', '+hsl[1]+'%, '+hsl[2]+'%)';
				
				case 'hsla':
					var hslColor = HSVtoHSL(hsv);
					var alpha = options.alpha || Math.random();
					return 'hsla('+hslColor[0]+', '+hslColor[1]+'%, '+hslColor[2]+'%, ' + alpha + ')';
				
				case 'rgbArray':
					return HSVtoRGB(hsv);
				
				case 'rgb':
					var rgb = HSVtoRGB(hsv);
					return 'rgb(' + rgb.join(', ') + ')';
				
				case 'rgba':
					var rgbColor = HSVtoRGB(hsv);
					var alpha = options.alpha || Math.random();
					return 'rgba(' + rgbColor.join(', ') + ', ' + alpha + ')';
				
				default:
					return HSVtoHex(hsv);
			}
			
		}
		
		function getMinimumBrightness(H, S) {
			
			var lowerBounds = getColorInfo(H).lowerBounds;
			
			for (var i = 0; i < lowerBounds.length - 1; i++) {
				
				var s1 = lowerBounds[i][0],
					v1 = lowerBounds[i][1];
				
				var s2 = lowerBounds[i+1][0],
					v2 = lowerBounds[i+1][1];
				
				if (S >= s1 && S <= s2) {
					
					var m = (v2 - v1)/(s2 - s1),
						b = v1 - m*s1;
					
					return m*S + b;
				}
				
			}
			
			return 0;
		}
		
		function getHueRange (colorInput) {
			
			if (typeof parseInt(colorInput) === 'number') {
				
				var number = parseInt(colorInput);
				
				if (number < 360 && number > 0) {
					return [number, number];
				}
				
			}
			
			if (typeof colorInput === 'string') {
				
				if (colorDictionary[colorInput]) {
					var color = colorDictionary[colorInput];
					if (color.hueRange) {return color.hueRange;}
				} else if (colorInput.match(/^#?([0-9A-F]{3}|[0-9A-F]{6})$/i)) {
					var hue = HexToHSB(colorInput)[0];
					return [ hue, hue ];
				}
			}
			
			return [0,360];
			
		}
		
		function getSaturationRange (hue) {
			return getColorInfo(hue).saturationRange;
		}
		
		function getColorInfo (hue) {
			
			// Maps red colors to make picking hue easier
			if (hue >= 334 && hue <= 360) {
				hue-= 360;
			}
			
			for (var colorName in colorDictionary) {
				var color = colorDictionary[colorName];
				if (color.hueRange &&
					hue >= color.hueRange[0] &&
					hue <= color.hueRange[1]) {
					return colorDictionary[colorName];
				}
			} return 'Color not found';
		}
		
		function randomWithin (range) {
			if (seed === null) {
				return Math.floor(range[0] + Math.random()*(range[1] + 1 - range[0]));
			} else {
				//Seeded random algorithm from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
				var max = range[1] || 1;
				var min = range[0] || 0;
				seed = (seed * 9301 + 49297) % 233280;
				var rnd = seed / 233280.0;
				return Math.floor(min + rnd * (max - min));
			}
		}
		
		function HSVtoHex (hsv){
			
			var rgb = HSVtoRGB(hsv);
			
			function componentToHex(c) {
				var hex = c.toString(16);
				return hex.length == 1 ? '0' + hex : hex;
			}
			
			var hex = '#' + componentToHex(rgb[0]) + componentToHex(rgb[1]) + componentToHex(rgb[2]);
			
			return hex;
			
		}
		
		function defineColor (name, hueRange, lowerBounds) {
			
			var sMin = lowerBounds[0][0],
				sMax = lowerBounds[lowerBounds.length - 1][0],
				
				bMin = lowerBounds[lowerBounds.length - 1][1],
				bMax = lowerBounds[0][1];
			
			colorDictionary[name] = {
				hueRange: hueRange,
				lowerBounds: lowerBounds,
				saturationRange: [sMin, sMax],
				brightnessRange: [bMin, bMax]
			};
			
		}
		
		function loadColorBounds () {
			
			defineColor(
				'monochrome',
				null,
				[[0,0],[100,0]]
			);
			
			defineColor(
				'red',
				[-26,18],
				[[20,100],[30,92],[40,89],[50,85],[60,78],[70,70],[80,60],[90,55],[100,50]]
			);
			
			defineColor(
				'orange',
				[19,46],
				[[20,100],[30,93],[40,88],[50,86],[60,85],[70,70],[100,70]]
			);
			
			defineColor(
				'yellow',
				[47,62],
				[[25,100],[40,94],[50,89],[60,86],[70,84],[80,82],[90,80],[100,75]]
			);
			
			defineColor(
				'green',
				[63,178],
				[[30,100],[40,90],[50,85],[60,81],[70,74],[80,64],[90,50],[100,40]]
			);
			
			defineColor(
				'blue',
				[179, 257],
				[[20,100],[30,86],[40,80],[50,74],[60,60],[70,52],[80,44],[90,39],[100,35]]
			);
			
			defineColor(
				'purple',
				[258, 282],
				[[20,100],[30,87],[40,79],[50,70],[60,65],[70,59],[80,52],[90,45],[100,42]]
			);
			
			defineColor(
				'pink',
				[283, 334],
				[[20,100],[30,90],[40,86],[60,84],[80,80],[90,75],[100,73]]
			);
			
		}
		
		function HSVtoRGB (hsv) {
			
			// this doesn't work for the values of 0 and 360
			// here's the hacky fix
			var h = hsv[0];
			if (h === 0) {h = 1;}
			if (h === 360) {h = 359;}
			
			// Rebase the h,s,v values
			h = h/360;
			var s = hsv[1]/100,
				v = hsv[2]/100;
			
			var h_i = Math.floor(h*6),
				f = h * 6 - h_i,
				p = v * (1 - s),
				q = v * (1 - f*s),
				t = v * (1 - (1 - f)*s),
				r = 256,
				g = 256,
				b = 256;
			
			switch(h_i) {
				case 0: r = v; g = t; b = p;  break;
				case 1: r = q; g = v; b = p;  break;
				case 2: r = p; g = v; b = t;  break;
				case 3: r = p; g = q; b = v;  break;
				case 4: r = t; g = p; b = v;  break;
				case 5: r = v; g = p; b = q;  break;
			}
			
			var result = [Math.floor(r*255), Math.floor(g*255), Math.floor(b*255)];
			return result;
		}
		
		function HexToHSB (hex) {
			hex = hex.replace(/^#/, '');
			hex = hex.length === 3 ? hex.replace(/(.)/g, '$1$1') : hex;
			
			var red = parseInt(hex.substr(0, 2), 16) / 255,
				green = parseInt(hex.substr(2, 2), 16) / 255,
				blue = parseInt(hex.substr(4, 2), 16) / 255;
			
			var cMax = Math.max(red, green, blue),
				delta = cMax - Math.min(red, green, blue),
				saturation = cMax ? (delta / cMax) : 0;
			
			switch (cMax) {
				case red: return [ 60 * (((green - blue) / delta) % 6) || 0, saturation, cMax ];
				case green: return [ 60 * (((blue - red) / delta) + 2) || 0, saturation, cMax ];
				case blue: return [ 60 * (((red - green) / delta) + 4) || 0, saturation, cMax ];
			}
		}
		
		function HSVtoHSL (hsv) {
			var h = hsv[0],
				s = hsv[1]/100,
				v = hsv[2]/100,
				k = (2-s)*v;
			
			return [
				h,
				Math.round(s*v / (k<1 ? k : 2-k) * 10000) / 100,
				k/2 * 100
			];
		}
		
		function stringToInteger (string) {
			var total = 0
			for (var i = 0; i !== string.length; i++) {
				if (total >= Number.MAX_SAFE_INTEGER) break;
				total += string.charCodeAt(i)
			}
			return total
		}
		
		return randomColor;
	})();
	
	(function () {
		window.confettiKit = function (params) {
			var app = this;
			app.version = '1.1.0';
			app.config = {
				colors: [
					'#a864fd',
					'#29cdff',
					'#78ff44',
					'#ff718d',
					'#fdff6a',
				],
				el: 'body',
				elements: {
					'confetti': {
						direction: 'down',
						rotation: true,
					},
					'star': {
						count: 15,
						direction: 'up',
						rotation: true,
					},
					'ribbon': {
						count: 10,
						direction: 'down',
						rotation: true,
					},
					
				},
				confettiCount: 75,
				x: 0,
				y: 0,
				angle: 90,
				decay: 0.9,
				spread: 45,
				startVelocity: 45,
				position: null,
			};
			// Extend defaults with parameters
			for (var param in params) {
				app.config[param] = params[param];
			}
			var config = app.config;
			
			app.renderStar = function (width, color) {
				return '<div style="width:' + width + 'px;fill:' + color + '"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 75 75" ><title>star</title><polygon points="37.5 18.411 56.342 8.505 52.743 29.486 67.987 44.345 46.921 47.406 37.5 66.495 28.079 47.406 7.013 44.345 22.257 29.486 18.658 8.505 37.5 18.411" /></svg></div>';
			};
			
			app.renderRibbon = function (width, color) {
				return '<div style="width:' + width + 'px;stroke:' + color + '"><svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 75 75" ><path d="m24.453125,3.774663c0.193689,-0.193689 0.743394,-0.671232 1.743199,-0.968444c0.94668,-0.28142 2.333125,-0.396423 4.648531,-0.193689c3.330842,0.291645 6.861105,1.826145 7.941241,2.711643c1.270993,1.041963 2.540047,2.890054 3.873776,4.84222c1.575816,2.306511 2.817321,4.217926 3.486399,6.004353c0.924008,2.467092 1.294523,4.846337 1.162133,6.972797c-0.097034,1.558552 -0.857702,3.568397 -1.743199,4.648531c-1.041962,1.270991 -2.406342,2.560381 -4.261154,3.29271c-2.228395,0.87983 -4.466154,0.810093 -6.779108,0.581066c-1.965634,-0.194637 -3.568395,-0.857702 -4.648531,-1.743199c-0.211833,-0.17366 -0.533673,-0.606635 -0.968444,-1.355822c-0.350522,-0.604015 -0.818174,-1.325931 -0.774755,-1.936888c0.030701,-0.432012 0.413426,-1.271858 0.968444,-1.936888c0.620528,-0.743524 1.284527,-0.912889 1.936888,-1.162133c1.279386,-0.488809 3.691399,-0.422715 6.004353,-0.193689c1.965634,0.194637 3.31667,0.736573 4.261154,1.355822c1.393387,0.91357 2.991613,1.63628 4.454843,2.905332c1.03466,0.897355 2.019835,2.599953 2.905332,3.680087c0.694641,0.847327 1.193205,1.542398 1.355822,2.324266c0.239907,1.153479 0.535004,2.743879 0,6.58542c-0.356447,2.55943 -1.282205,4.880811 -2.324266,6.779108c-1.086946,1.980061 -2.178706,3.113269 -3.486399,3.873776c-1.43055,0.831959 -3.243129,1.2571 -5.810664,1.54951c-2.886673,0.328756 -6.579154,-0.410019 -7.941241,-0.774755c-0.771421,-0.20657 -1.049573,-0.27482 -1.355822,-0.581066c-0.612498,-0.612498 -0.774755,-1.54951 -0.774755,-2.517955c0,-0.774755 -0.059414,-1.775875 0.387378,-2.517955c0.859423,-1.427419 2.413861,-2.195096 4.067465,-2.711643c1.307291,-0.408368 2.573952,-0.18426 3.486399,0.193689c1.29039,0.534498 3.294961,1.907296 4.648531,3.29271c1.164388,1.191782 1.717201,2.44142 2.711643,4.84222c0.703176,1.697624 1.065142,3.478355 1.162133,5.229598c0.10711,1.933923 0.089293,3.896049 -0.387378,5.616976c-0.326992,1.180547 -0.698642,2.16461 -1.162133,3.099021c-0.50186,1.011759 -1.936888,2.905332 -3.29271,4.261154c-1.162133,1.162133 -2.324266,1.54951 -3.29271,1.936888l-0.581066,0.193689" id="svg_6" stroke-width="4" fill="none"/></svg></div>';
			};
			
			app.customRender = function (content, type, color, width, size) {
				if (type == 'text') {
					return '<p style="color:' + color + ';font-size:' + size + 'px">' + content + '</p>';
				} else if (type == 'svg') {
					return '<div style="width:' + width + 'px;fill:' + color + '">' + content + '</div>';
				} else if (type == 'image') {
					return '<img style="width:' + width + 'px;" src="' + content + '"/>';
				}
			};
			
			app.createElements = function (root, elementCount) {
				var starCount = config.elements['star'] ? config.elements['star'].count : 0;
				var ribbonCount = config.elements['ribbon'] ? config.elements['ribbon'].count : 0;
				//var customCount = config.elements["custom"] ? config.elements["custom"].count : 0;
				var customCount = [];
				
				if (config.elements['custom'] && config.elements['custom'].length > -1) {
					for (var i = 0; i <= config.elements['custom'].length; i++) {
						if (config.elements['custom'][i]) {
							customCount.push({
								'count': config.elements['custom'][i]['count']
							})
						}
					}
				}
				var cl = 0;
				var all = [];
				for (var index = 0; index <= elementCount; index++) {
					var element = document.createElement('div');
					element.classList = ['fetti'];
					var color = config.colors[index % config.colors.length];
					var w = Math.floor((Math.random() * 10) + 1) + 'px';
					var h = Math.floor((Math.random() * 10) + 1) + 'px';
					element.style.width = w;
					element.style.height = h;
					element.style.position = 'absolute';
					element.style.zIndex = '999999';
					
					if (config.elements['star'] && starCount > 0) {
						var s = starCount - 1
						if (s <= config.elements['star'].count && s >= 0) {
							element.style['background-color'] = '';
							element.innerHTML = app.renderStar(25, color);
							
							element.direction = config.elements['star'].direction;
							element.rotation = config.elements['star'].rotation;
							starCount = s
						}
					} else if (config.elements['ribbon'] && ribbonCount > 0) {
						var r = ribbonCount - 1
						if (r <= config.elements['ribbon'].count && r >= 0) {
							element.style['background-color'] = '';
							element.innerHTML = app.renderRibbon(30, color);
							
							element.direction = config.elements['ribbon'].direction;
							element.rotation = config.elements['ribbon'].rotation;
							ribbonCount = r
						}
					}
					else if (config.elements['custom'] && config.elements['custom'].length > -1 && customCount[cl]) {
						if (customCount[cl]) {
							var c = customCount[cl].count - 1
							if (c <= customCount[cl].count) {
								if (c <= customCount[cl].count && c >= 0) {
									element.style['background-color'] = '';
									var type = config.elements['custom'][cl].contentType;
									var content = config.elements['custom'][cl].content;
									var width = config.elements['custom'][cl].width;
									var textSize = config.elements['custom'][cl].textSize;
									element.innerHTML = app.customRender(content, type, color, width, textSize);
									
									element.direction = config.elements['custom'][cl].direction;
									element.rotation = config.elements['custom'][cl].rotation;
									customCount[cl].count = c
									if (customCount[cl].count == 0) {
										cl++;
									}
								}
							}
						}
						
						
					}
					
					else {
						if (w == h) {
							element.style['background-color'] = color;
							element.style.borderRadius = '50%';
						} else {
							element.style['background-color'] = color;
						}
						
						element.direction = config.elements['confetti'] ? (config.elements['confetti'].direction ? config.elements['confetti'].direction : 'down') : 'down';
						element.rotation = config.elements['confetti'] ? (config.elements['confetti'].rotation ? config.elements['confetti'].rotation : true) : true;
					}
					
					//root.prepend(element);
					root.insertBefore(element, root.firstChild); //older browser
					all.push(element)
				}
				return all;
			};
			app.randomPhysics = function (x, y, angle, spread, startVelocity) {
				var radAngle = angle * (Math.PI / 180);
				var radSpread = spread * (Math.PI / 180);
				return {
					x: x,
					y: y,
					wobble: Math.random() * 10,
					velocity: (startVelocity * 0.3) + (Math.random() * startVelocity),
					angle2D: -radAngle + ((0.3 * radSpread) - (Math.random() * radSpread)),
					angle3D: -(Math.PI / 4) + (Math.random() * (Math.PI / 2)),
					tiltAngle: Math.random() * Math.PI
				};
			};
			
			app.updateFetti = function (fetti, progress, decay) {
				fetti.physics.x += Math.cos(fetti.physics.angle2D) * fetti.physics.velocity;
				fetti.physics.y += Math.sin(fetti.physics.angle2D) * fetti.physics.velocity;
				fetti.physics.z += Math.sin(fetti.physics.angle3D) * fetti.physics.velocity;
				fetti.physics.wobble += 0.1;
				fetti.physics.velocity *= decay;
				if (fetti.element.direction == 'up') {
					fetti.physics.y -= 3;
				} else {
					fetti.physics.y += 3;
				}
				
				fetti.physics.tiltAngle += 0.1;
				var x = fetti.physics.x,
					y = fetti.physics.y,
					tiltAngle = fetti.physics.tiltAngle,
					wobble = fetti.physics.wobble;
				var wobbleX = x + (10 * Math.cos(wobble));
				var wobbleY = y + (10 * Math.sin(wobble));
				var transform;
				if (fetti.element.rotation) {
					transform = 'translate3d(' + wobbleX + 'px, ' + wobbleY + 'px, 0) rotate3d(1, 1, 1, ' + tiltAngle + 'rad)';
				} else {
					transform = 'translate3d(' + wobbleX + 'px, ' + wobbleY + 'px, 0)';
				}
				fetti.element.style.transform = transform;
				fetti.element.style.opacity = 1 - progress;
				
			};
			
			app.animate = function (root, fettis, decay) {
				var totalTicks = 200;
				var tick = 0;
				
				function update() {
					fettis.forEach(function (fetti) {
						app.updateFetti(fetti, tick / totalTicks, decay);
					});
					
					tick += 1;
					if (tick < totalTicks) {
						requestAnimationFrame(update);
					} else {
						fettis.forEach(function (fetti) {
							root.removeChild(fetti.element);
						});
					}
				}
				
				requestAnimationFrame(update);
			};
			app.confetti = function (root, x, y) {
				var angle = config.angle,
					decay = config.decay,
					spread = config.spread,
					startVelocity = config.startVelocity,
					elementCount = config.confettiCount
				var elements = app.createElements(root, elementCount);
				var fettis = [];
				elements.map(function (element) {
					var s = {
						'element': element,
						'physics': app.randomPhysics(x, y, angle, spread, startVelocity)
					}
					fettis.push(s);
				});
				app.animate(root, fettis, decay);
			}
			var attach = document.querySelector(config.el)
			
			if (config.position != null) {
				if (config.position == 'bottomLeftRight') {
					config.angle = 45;
					app.confetti(attach, 0, window.innerHeight - 200);
					var rightConfig = params;
					rightConfig['position'] = null;
					rightConfig['angle'] = 135;
					rightConfig['x'] = window.innerWidth;
					rightConfig['y'] = window.innerHeight - 200;
					new confettiKit(rightConfig);
				} else if (config.position == 'topLeftRight') {
					config.angle = 340;
					app.confetti(attach, 0, 0);
					var rightConfig = params;
					rightConfig['position'] = null;
					rightConfig['angle'] = 190;
					rightConfig['x'] = window.innerWidth;
					rightConfig['y'] = 0;
					new confettiKit(rightConfig);
				}
				
			} else {
				app.confetti(attach, config.x, config.y);
			}
		};
	})();
	
	function launchConfetti(delay) {
		if (iOS() || isSafari() || hasDevScript(scriptPath)) return false;
		delay = typeof delay !== 'undefined' ? delay : 1200;
		setTimeout(function(){
			new confettiKit({colors:randomColor({hue:"red",count:8}),confettiCount:60,angle:90,startVelocity:50,elements:{confetti:{direction:"down",rotation:!0},star:{count:10,direction:"down",rotation:!0},ribbon:{count:5,direction:"down",rotation:!0}},position:"topLeftRight"});
		}.bind(this), delay);
	}
	
	var isAdminMobile = false;
	
	/**
	 * Svg object helper to use chains
	 * @param svgElement
	 * @constructor
	 */
	function Svg(svgElement) {
		var $svg = svgElement;
		
		this.get = function() {
			return $svg;
		};
		
		this.select = function(selector) {
			var result = selector.indexOf('#') > -1
				? $svg.getElementById(selector.replace('#', ''))
				: $svg.getElementsByTagNameNS($svg.tagName, selector)[0];
			return result ? new Svg(result) : this;
		};
		
		this.selectParent = function(selector) {
			return this.select(selector).parent();
		};
		
		this.parent = function() {
			return new Svg($svg.parentNode);
		};
		
		this.children = function() {
			var children = $svg.children,
				svgArray = [];
			for(var i in children) {
				if (!children.hasOwnProperty(i)) continue;
				svgArray.push(new Svg(children[i]));
			}
			return svgArray;
		};
		
		this.setText = function(text) {
			$svg.textContent = text;
			return this;
		};
		
		this.setStyle = function(name, value) {
			$svg.setAttribute(name, value);
			return this;
		};
		
		this.getAttr = function(attr, namespace) {
			namespace = namespace ? namespace : null;
			return $svg.getAttributeNS(namespace, attr);
		};
		
		this.setAttr = function(attribute, value, namespace) {
			$svg.setAttributeNS(namespace, attribute, value);
			return this;
		};
		
		this.setLink = function(link) {
			this.setAttr('xlink:href', link, 'http://www.w3.org/1999/xlink');
			return this;
		};
	}
	
	/**
	 *
	 * @param $this
	 * @param finishGame
	 * @constructor
	 */
	function Wheel($this, finishGame) {
		
		var requestObject = new Request($this),
			animationObject = new Animation($this),
			calculationsObject = new Calculations(),
			$wheel = {},
			$svg = {};
		
		this.finishGame = finishGame;
		
		this.init = function () {
			$wheel = $this.select('.wheel-wrapper');
			this.setSvg();
		};
		
		this.setSvg = function() {
			var selector = getWheelSelector(),
				wrapper = $this.select(selector),
				path = getData($this, 'gameInfo.gameId', 'preview') + '/wheel.svg';
			requestObject.get($this.getHostUrl(path), this.addResponseContent.bind(this), wrapper);
		};
		
		this.addResponseContent = function(data, wrapper) {
			if (data.response) {
				if (wrapper.tagName === 'IMG') {
					wrapper.src = 'data:image/svg+xml;base64,' + btoa(data.response);
				} else {
					wrapper.innerHTML = data.response;
					this.setCurrentCouponValues(wrapper);
					this.setTempData(wrapper);
				}
			}
		};
		
		this.setCurrentCouponValues = function(svgWrapper) {
			var svg = new Svg(svgWrapper.querySelector('svg')),
				slices = svgWrapper.nextElementSibling.childNodes;
			for (var index in slices) {
				if (!slices.hasOwnProperty(index)) continue;
				var text = decodeURI(slices[index].dataset.value),
					lines = text.length < 13 ? [text, ''] : splitLines(text),
					couponId = '#coupon' + (parseInt(index) + 1);
				svg.select(couponId + 'curve2text').setText(lines[1]);
				svg.select(couponId + 'curve1text').setText(lines[0]);
			}
		};
		
		this.launchGame = function(slices) {
			var winnerItem = calculationsObject.getWinnerItem(slices);
			animationObject.rotate($this.select(getWheelSelector()), {
				stop: parseInt(winnerItem.dataset.stop)
			}, function(winnerItem) {
				setTimeout(this.finishGame.bind(null, winnerItem), 1000);
			}.bind(this, winnerItem));
		};
		
		this.animateHide = function() {
			$this.gameScreen($this);
		};
		
		this.setTempData = function(wrapper) {
			var slicesColors = getData(window, 'wheel.slices.colors.' + $this.gameId, false),
				slicesTexts = getData(window, 'wheel.slices.texts.' + $this.gameId, false),
				textColor = getData(window, 'wheel.font.color.' + $this.gameId, false);
			$this.svg = new Svg(wrapper.querySelector('svg'));
			this.changeSlicesSchema(slicesColors);
		};
		
		this.changeSlicesSchema = function(colors) {
			if (!colors) return false;
			for (var color of colors) {
				$this.svg.select('#' + color.name).setAttr('fill', color.hex);
			}
		};
	}
	
	/**
	 * Logic for gift game
	 * @param $this
	 * @param finishGame function
	 * @constructor
	 */
	function Gift($this, finishGame) {
		
		var requestObject = new Request($this),
			animationObject = new Animation($this),
			calculationsObject = new Calculations(),
			isDesktop = !isMobile(),
			giftDesktop = {
				width: 92,
				height: 84,
				bottom: 145
			},
			giftMobile = {
				width: 52,
				height: 48,
				bottom: 118
			};
		
		this.gifts = {};
		this.giftClass = 'gift';
		this.itemClass = '.' + this.giftClass;
		this.finishGame = finishGame;
		this.giftVariables = {};
		this.animationType = '';
		this.animationObject = animationObject;
		
		this.initDeviceVariables = function(isDesktop) {
			this.setGiftAnimation(isDesktop);
			this.setGiftVariables(isDesktop);
		};
		
		this.setGiftVariables = function (isDesktop) {
			this.giftVariables = isDesktop ? giftDesktop : giftMobile;
			this.giftVariables.bottom--;
		};
		
		this.setGiftAnimation = function(isDesktop) {
			this.animationType = isDesktop ? 'winnerGift' : 'winnerGiftMobile';
		};
		
		this.initDeviceVariables(isDesktop);
		
		/**
		 * Animation section
		 */
		
		this.animateOpen = function(effect) {
			animationObject.animateBySelector(effect, '.' + $this.closeClass);
			this.animateStatic('In');
			setTimeout(function() {
				this.animateGifts();
			}.bind(this), 600);
		};
		
		this.animateStatic = function (direction) {
			if (direction !== 'In') {
				this.animateHeadPart('fade' + direction +'Up');
			}
			this.animateFootPart('fade' + direction);
		};
		
		this.animateHeadPart = function(effect) {
			var items = [
				$this.select('.big-text'),
				$this.select('.input-wrapper')
			];
			setTimeout(function() {
				for (var index in items) {
					if (items.hasOwnProperty(index)) {
						animationObject.animateBySelector(effect, items[index], this.hideBehindBackground.bind(this, items[index], effect));
					}
				}
			}.bind(this), 300);
		};
		
		this.animateFootPart = function(effect) {
			var footerNote = $this.select('.text.small-message');
			setTimeout(function() {
				animationObject.animateBySelector(effect, footerNote, this.hideBehindBackground.bind(this, footerNote, effect));
			}.bind(this), 400);
		};
		
		this.hideBehindBackground = function(element, effect) {
			this.setItemOpacity(element, effect);
			this.setItemZindex(element, effect);
		};
		
		this.animateGifts = function() {
			var gifts = $this.selectAll('.gift');
			for (var index in gifts) {
				if (gifts.hasOwnProperty(index)) {
					this.animateGift(gifts[index], index);
				}
			}
		};
		
		this.animateGift = function (gift, index) {
			setTimeout(function() {
				animationObject.animateBySelector('popup', gift, this.setItemOpacity.bind(this));
			}.bind(this), index * 50);
		};
		
		this.setItemOpacity = function(node, effect) {
			node.style.opacity = this.isShowEffect(effect);
		};
		
		this.isShowEffect = function (effect) {
			return (typeof effect === 'undefined' || effect.indexOf('In') > -1) ? 1 : 0;
		};
		
		this.setItemZindex = function(node, effect) {
			node.style.zIndex = this.isShowEffect(effect) ? 2 : 0;
		};
		
		/**
		 * Events
		 */
		this.bindEvents = function(startGame, send) {
			var allGifts = $this.selectAll('.gift');
			if (!$this.emailCollect) {
				for (var gift of allGifts) {
					gift.addEventListener('click', function(e) {
						send();
						setTimeout(this.showFinalScene.bind(this, e), 400);
					}.bind(this), false);
				}
			} else {
				var gifts = $this.select('.gifts');
				gifts.addEventListener('click', this.showFinalScene.bind(this), false);
				for (var gift of allGifts) {
					gift.addEventListener('click', startGame, false);
				}
			}
		};
		
		this.submitOnGift = function(e, send) {
			e.preventDefault();
			if (
				e.target !== e.currentTarget
				&& ! $this.gameButtonPushed
			) {
				$this.gameButtonPushed = true;
				var inputWrapper = $this.select('.input-wrapper');
				send(inputWrapper, e);
			}
		};
		
		/**
		 * Game logic
		 */
		this.launchGame = function(gifts) {
			var winnerGift = this.getWinnerGift(gifts);
			this.finishGame(winnerGift);
			this.gifts = gifts;
			this.hideStartScreen();
			this.prepareGiftWrapper(gifts[0].parentNode);
		};
		
		this.hideStartScreen = function () {
			var emailWrapper = $this.select('.input-wrapper'),
				checkboxWrapper = $this.select('.checkbox-wrapper'),
				bigText = $this.select('.big-text');
			this.fadeOut(emailWrapper, this.setItemOpacity.bind(this, emailWrapper, 'fadeOut'));
			this.fadeOut(checkboxWrapper, this.setItemOpacity.bind(this, checkboxWrapper, 'fadeOut'));
			this.fadeOut(bigText, this.setNewBigText.bind(this, bigText, 'fadeOut'));
		};
		
		this.getWinnerGift = function(gifts) {
			var chances = this.getChances(gifts),
				winnerGiftIndex = this.getWinnerGiftIndex(chances);
			return gifts[winnerGiftIndex];
		};
		
		this.getChances = function (gifts) {
			var prevIndex = undefined,
				result = [], addition = 0;
			for (var index in gifts) {
				if (gifts.hasOwnProperty(index)) {
					addition = gifts.hasOwnProperty(prevIndex) ? result[prevIndex] : 0;
					result[index] = parseFloat(gifts[index].dataset.chance) + addition;
					prevIndex = index;
				}
			}
			return result;
		};
		
		this.getWinnerGiftIndex = function (chances) {
			var random = Math.random() * 100;
			for (var index in chances) {
				if (chances.hasOwnProperty(index)) {
					if (chances[index] > random) return index;
				}
			}
		};
		
		this.setNewBigText = function(bigText, effect) {
			var newBigText = $this.select('.big-text[data-name=pickGift]');
			hide(bigText);
			show(newBigText);
			this.setItemOpacity(newBigText, effect);
			newBigText.style.height = getStyle(bigText, 'height') + 'px';
			this.fadeIn(newBigText);
		};
		
		this.prepareGiftWrapper = function(giftWrapper) {
			if (!giftWrapper) return false;
			addClass(giftWrapper, ' bigger');
		};
		
		this.showFinalScene = function(event) {
			if ($this.select('.second-screen').style.display === 'block') return false;
			if (!this.isInitialScreen() && $this.emailCollect) return false;
			var gift = event.target;
			this.showFinalScreen(gift);
		};
		
		this.showFinalScreen = function (gift) {
			var gifts = $this.selectAll('.gift');
			removeClass(gift.parentNode, ' bigger');
			this.prepareFinalScene('fadeOut');
			this.highlightWinnerGift(gifts, gift);
			this.animateWinnerGift(gift, this.getGiftIndex(gifts, gift));
		};
		
		this.isInitialScreen = function() {
			var inputWrapper = $this.select('.input-wrapper');
			if (parseInt(getStyle(inputWrapper, 'opacity')) === 1) {
				var email = $this.select('input').value;
				if(!validateEmail(email)) {
					this.shakeForm('input');
					return false;
				}
				requestObject.isValidEmail(email, this.emailValidationHandler.bind(this));
				return false;
			}
			return true;
		};
		
		this.emailValidationHandler = function(data) {
			var response = JSON.parse(data.response),
				isValidEmail = (response.hasOwnProperty('subscriber_email')
							&& typeof response.subscriber_email === 'boolean'),
				selector = isValidEmail ? 'button' : 'input';
			this.shakeForm(selector);
		};
		
		this.shakeForm = function (selector) {
			var inputWrapper = $this.select('.input-wrapper');
			animationObject.animateBySelector('shake', inputWrapper.querySelector(selector), function () {});
		};
		
		this.prepareFinalScene = function(effect) {
			this.animateBigText(effect);
			this.animateFootPart(effect);
		};
		
		this.animateBigText = function(effect) {
			var bigText = $this.select('.big-text'),
				newBigText = $this.select('.big-text[data-name=pickGift]');
			animationObject.animateBySelector(effect, bigText, this.hideBehindBackground.bind(this, bigText, effect));
			animationObject.animateBySelector(effect, newBigText, this.hideBehindBackground.bind(this, newBigText, effect));
		};
		
		this.animateInit = function() {
			setTimeout(function() {
				this.animateStatic('Out');
			}.bind(this), 10);
		};
		
		this.highlightWinnerGift = function(gifts, excludedGift) {
			for (var index in gifts) {
				if (gifts.hasOwnProperty(index)) {
					var gift = gifts[index];
					if (this.isSelectedGift(excludedGift, gifts, index)) continue;
					this.fadeOutGift(gift);
				}
			}
		};
		
		this.fadeOutGift = function(gift) {
			this.animationObject.animateBySelector(
				'fadeOut',
				gift,
				this.hideGift.bind(this, gift, 'fadeOut')
			);
		};
		
		this.isSelectedGift = function(gift, gifts, index) {
			return (gift.dataset.id !== 'null' && gifts[index].dataset.id === gift.dataset.id)
				|| (isAdmin && gift.dataset.id === 'null' && gifts[index].dataset.temp_id === gift.dataset.temp_id);
		};
		
		this.getGiftIndex = function(gifts, gift) {
			for (var index in gifts) {
				if(gifts.hasOwnProperty(index)) {
					if (this.isSelectedGift(gift, gifts, index)) return parseInt(index);
				}
			}
			return false;
		};
		
		this.hideGift = function(gift, effect) {
			this.hideBehindBackground(gift, effect);
			gift.style.visibility = 'hidden';
		};
		
		this.animateWinnerGift = function(gift, index) {
			setTimeout(function() {
				animationObject.animateBySelector(this.animationType, gift, this.giftWinner.bind(this, gift));
				setTimeout(this.animateDirection.bind(this, gift, index), 300);
			}.bind(this), 900);
		};
		
		this.giftWinner = function(element) {
			this.fixStyles(element);
			this.animateFinalScreen();
		};
		
		this.fixStyles = function (element) {
			var giftWrapper = element.parentNode;
			element.style.width = this.giftVariables.width + 'px';
			element.style.height = this.giftVariables.height + 'px';
			element.style.bottom = getStyle(giftWrapper, 'margin').split(' ')[0] + 'px';
			giftWrapper.style.bottom = '0px';
		};
		
		this.animateDirection = function(gift, index) {
			var containerWidth = getStyle(gift.parentNode, 'width'),
				giftPosition = this.getSidePosition(index, containerWidth),
				direction = this.getDirection(index);
			requestAnimationFrame(this.animateBottom.bind(this, gift, giftPosition, direction));
		};
		
		this.getDirection = function(index) {
			var direction = index < 3 ? 'left' : 'right',
				isHebrew = $this.select('.small-window').className.indexOf('he') > -1;
			return isHebrew ? this.reverseDirection(direction) : direction;
		};
		
		this.reverseDirection = function (direction) {
			return direction === 'right' ? 'left' : 'right';
		};
		
		this.animateBottom = function(gift, endPoint, direction) {
			var stopAnimation = this.giftVariables.bottom,
				currentBottom = getStyle(gift, 'bottom'),
				number = (currentBottom/(stopAnimation/100)) * (endPoint/100);
			gift.style[direction] = number + 'px';
			if (currentBottom < stopAnimation) {
				requestAnimationFrame(this.animateBottom.bind(this, gift, endPoint, direction));
			}
		};
		
		this.animateFinalScreen = function() {
			if(!$this.modalNode) return false;
			var firstScreen = $this.select('.start-screen'),
				lastScreen = $this.select('.second-screen');
			this.prepareLastScreen(firstScreen, lastScreen);
			this.showLastScreen(lastScreen);
			launchConfetti(1200);
		};
		
		this.prepareLastScreen = function (firstScreen, secondScreen) {
			hide($this.select('.big-text'));
			hide($this.select('.big-text[data-name=pickGift]'));
			hide($this.select('.input-wrapper'));
			hide(firstScreen);
			show(secondScreen);
		};
		
		this.showLastScreen = function(secondScreen) {
			setTimeout(function() {
				this.fadeIn(secondScreen.querySelector('.gift-final-header'));
				this.fadeIn(secondScreen.querySelector('.lower-text'));
				this.fadeIn($this.select('.continue-btn'));
			}.bind(this), 200);
			setTimeout(function() {
				this.fadeIn($this.select('.final-message'));
			}.bind(this), 600);
		};
		
		this.animateBackground = function (image) {
			animationObject.fadeOut(image, this.addConfetti.bind(this, image));
		};
		
		this.addConfetti = function(customImage) {
			customImage.style.background = 'url(//' + $this.hostName + '/img/game/gifts/end_game.svg) top center / 100% 60% no-repeat';
			setTimeout(function(){
				customImage.style.opacity = '1';
			}, 1000);
		};
		
		// @todo: remove fadeIn, fadeOut and animate effect to Animate object
		// @todo: compare Gift.fadeOut and Animate.fadeOut
		this.fadeIn = function(element) {
			this.animate(element, 'fadeIn');
		};
		
		this.fadeOut = function(element, handler) {
			var opacity = getStyle(element, 'opacity');
			if (opacity > 0) {
				element.style.opacity = opacity - 0.1;
				requestAnimationFrame(this.fadeOut.bind(this, element, handler));
			} else {
				handler();
			}
		};
		
		this.animate = function(element, effect) {
			animationObject.animateBySelector(
				effect,
				element,
				this.setItemOpacity.bind(this, element, effect)
			);
		};
		
		this.getElementHeightByContent = function(element) {
			var children = element.childNodes,
				height = 0;
			for (var index in children) {
				if(children.hasOwnProperty(index)) {
					height += this.getElementHeight(children[index]);
				}
			}
			return height;
		};
		
		this.getElementHeight = function(element) {
			var margin = getStyle(element, 'margin').split(' '),
				marginHeight = margin.length > 2
					? parseInt(margin[0]) + parseInt(margin[2])
					: parseInt(margin[0]) * 2;
			return getStyle(element, 'height') + marginHeight;
		};
		
		this.getSidePosition = function (index, containerWidth) {
			var divider = 0;
			switch (index) {
				case 0: case 4: divider = 2; break;
				case 1: case 3: divider = 4; break;
			}
			return divider ? containerWidth/divider - this.giftVariables.width/divider : 0;
		};
		
		/**
		 * Hooks for text tab
		 */
		
		this.showStartScreen = function() {
			hide($this.select('.lucky-coupon-bar'));
			this.showFirstScreen();
			this.showFirstTitle();
		};
		
		this.showFirstScreen = function() {
			var gifts = $this.select('.gifts');
			gifts.style.bottom = '';
			hide($this.select('.second-screen'));
			show($this.select('.start-screen'));
			this.showInputWrapper();
			this.removePropsFromGifts(gifts);
			this.removePropsFromSmallMessage();
			this.removePropsFromCustomImage();
		};
		
		this.showInputWrapper = function () {
			var inputWrapper = $this.select('.input-wrapper');
			show(inputWrapper);
			inputWrapper.style.opacity = '';
		};
		
		this.removePropsFromSmallMessage = function() {
			var props = ['opacity', 'z-index', 'display'],
				elements = [$this.select('.small-message')];
			elements.push(
				$this.checkBehavior === '1'
				? $this.select('.checkbox-wrapper')
				: $this.select('.small-message div[data-name="note"]')
			);
			this.removeProps(elements, props);
		};
		
		this.showFirstTitle = function() {
			var bigText = $this.select('.big-text'),
				newBigText = $this.select('.big-text[data-name=pickGift]'),
				props = ['opacity', 'height', 'z-index', 'display'];
			hide(newBigText);
			newBigText.style.opacity = '0';
			this.removeProps([bigText], props);
		};
		
		this.showSecondTitle = function() {
			var bigText = $this.select('.big-text'),
				newBigText = $this.select('.big-text[data-name=pickGift]'),
				props = ['opacity', 'height', 'z-index', 'display'];
			hide(bigText);
			bigText.style.opacity = '0';
			this.removeProps([newBigText], props);
		};
		
		this.removePropsFromGifts = function(gifts) {
			var giftProps = ['opacity', 'visibility', 'left', 'bottom', 'width', 'height', 'z-index'],
				giftNodes = gifts.childNodes;
			this.removeProps(giftNodes, giftProps);
		};
		
		this.removePropsFromCustomImage = function() {
			var nodes = [$this.select('.custom-image')],
				props = ['opacity', 'background'];
			this.removeProps(nodes, props);
		};
		
		this.removeProps = function (nodes, props) {
			for (var gift of nodes) {
				for (var prop of props) {
					gift.style[prop] = '';
				}
			}
		};
		
		this.showPlayScreen = function() {
			hide($this.select('.lucky-coupon-bar'));
			this.showFirstScreen();
			this.showSecondTitle();
			$this.select('.input-wrapper').style.opacity = '0';
		};
		
		this.showWinScreen = function() {
			hide($this.select('.lucky-coupon-bar'));
			var gift = $this.select('.gifts .gift:nth-child(3)');
			this.hideBigText();
			this.hideEmailForm();
			this.changeScreens();
			this.showOneGift($this.selectAll('.gift'), gift);
			this.fixStyles(gift);
		};
		
		this.hideBigText = function() {
			hide($this.select('.big-text'));
			hide($this.select('.big-text[data-name=pickGift]'));
		};
		
		this.hideEmailForm = function() {
			hide($this.select('.input-wrapper'));
		};
		
		this.changeScreens = function() {
			hide($this.select('.start-screen'));
			show($this.select('.second-screen'));
			this.setItemsOpacity($this.select('.second-screen').childNodes)
		};
		
		this.setItemsOpacity = function(nodes) {
			for (var node of nodes) {
				node.style.opacity = '1';
			}
		};
		
		this.showOneGift = function(gifts, excludedGift) {
			for (var index in gifts) {
				if (gifts.hasOwnProperty(index)) {
					var gift = gifts[index];
					if (gift.dataset.id === excludedGift.dataset.id) continue;
					gift.style.opacity = '0';
				}
			}
		};
		
	}
	
	/**
	 * Logic for coupon game
	 * @param $this
	 * @param finishGame
	 * @constructor
	 */
	function Coupon($this, finishGame) {
		
		var animationObject = new Animation($this);
		
		this.prizeClassName = 'prize-item';
		this.couponClass = 'coupon';
		this.itemClass = '.' + this.couponClass;
		this.finishGame = finishGame;
		
		/**
		 * Events
		 */
		
		this.submitOnCoupon = function(e, send) {
			e.preventDefault();
			if (e.target !== e.currentTarget
				&& ! this.isRedButton(e.target)
				&& ! $this.gameButtonPushed
			) {
				$this.gameButtonPushed = true;
				var inputWrapper = $this.select('.input-wrapper');
				send(inputWrapper);
			}
		};
		
		this.isRedButton = function(target) {
			var element = findParentElement(target, this.getButtonOrCoupon);
			return element && element.className.indexOf('prize-item btn-push') !== -1;
		};
		
		this.getButtonOrCoupon = function (element) {
			var className = element.className;
			return className.indexOf('prize-item btn-push') !== -1
				|| className.indexOf('prize-item coupon') !== -1;
		};
		
		/**
		 * Game logic
		 */
		
		this.animateHide = function(element) {
			animationObject.animateBySelector('fadeOut', element, function(content) {content.style.display = 'none'});
		};
		
		this.launchGame = function(target) {
			var coupons = this.getCoupons(target),
				couponsArray = this.getSortedCoupons(coupons),
				stopIndex = this.getStopIndex(couponsArray);
			$this.gameScreen();
			this.playGame(couponsArray, 0, 190, stopIndex);
		};
		
		this.getCoupons = function (target) {
			var couponsWrapper = findParentElement(target, this.tagSelector);
			return couponsWrapper && couponsWrapper.querySelectorAll(this.itemClass);
		};
		
		this.getStopIndex = function(couponsArray) {
			var random = Math.random(),
				chances = this.getCouponsChances(),
				couponId = this.getCouponIdByRandom(chances, random);
			for (var coupIndex in couponsArray) {
				if (!couponsArray.hasOwnProperty(coupIndex)) continue;
				if (this.isNodeId(couponsArray[coupIndex], couponId)) {
					return parseInt(coupIndex);
				}
			}
		};
		
		this.isNodeId = function (coupon, couponId) {
			var dataAttributes = coupon.dataset;
			return parseInt(dataAttributes.id) === couponId || parseInt(dataAttributes.temp_id) === couponId;
		};
		
		this.getCouponsChances = function() {
			var chances = {},
				previousChance = 0;
			for (var coupIndex in $this.couponsList) {
				if ($this.couponsList.hasOwnProperty(coupIndex)) {
					var li = $this.couponsList[coupIndex];
					if (li.className === this.prizeClassName + ' ' + this.couponClass) {
						var couponData = li['data'];
						if (couponData.hasOwnProperty('id') || couponData.hasOwnProperty('temp_id')) {
							var chance = parseFloat(couponData['chance']) / 100,
								temp = chance,
								id = couponData['id'] ? couponData['id'] : couponData['temp_id'];
							chances[id] = chance + previousChance;
							previousChance = previousChance + temp;
						}
					}
				}
			}
			return chances;
		};
		
		this.getCouponIdByRandom = function(chances, random) {
			for (var index in chances) {
				if (chances[index] >= random) {
					return parseInt(index);
				}
			}
		};
		
		this.getSortedCoupons = function(coupons) {
			coupons = Array.prototype.slice.call(coupons);
			var temp = coupons[3];
			coupons.splice(3, 1);
			coupons.splice(4, 0, temp);
			temp = coupons.splice(4, 4);
			temp.reverse();
			Array.prototype.push.apply(coupons, temp);
			return coupons;
		};
		
		this.tagSelector = function(element) {
			return element.tagName === 'UL';
		};
		
		this.playGame = function(coupons, index, speed, stopIndex, iteration) {
			iteration = typeof iteration === 'undefined' ? 0 : iteration;
			var couponsCount = coupons.length - 1,
				previousIndex = index === 0 ? couponsCount : index - 1,
				currentCoupon = coupons[index],
				previousCoupon = coupons[previousIndex];
			speed = this.getSpeed(index, speed, iteration, stopIndex);
			this.showCurrentCoupon(previousCoupon, currentCoupon);
			setTimeout(
				this.gameIteration.bind(this, index, speed, stopIndex, coupons, couponsCount, iteration),
				speed
			);
		};
		
		/**
		 * @todo: refactor this method
		 * @param index
		 * @param speed
		 * @param iteration
		 * @param stopIndex
		 * @returns {*}
		 */
		this.getSpeed = function(index, speed, iteration, stopIndex) {
			var lastIteration = 25 + (stopIndex + 1),
				additionalSpeed = 10,
				halfBeforeEnd = lastIteration - 3,
				hafAndQuarterBeforeEnd = lastIteration - 4,
				oneRoundBeforeEnd = lastIteration - 5;
			if (iteration >= halfBeforeEnd) {
				additionalSpeed = 30;
			} else if (iteration >= hafAndQuarterBeforeEnd) {
				additionalSpeed = 10;
			} else if (iteration >= oneRoundBeforeEnd) {
				additionalSpeed = 5;
			} else if (iteration < 4) {
				additionalSpeed = -8;
			} else if (iteration < 8) {
				additionalSpeed = -7;
			} else if (iteration < 16) {
				additionalSpeed = -5;
			}
			return speed + additionalSpeed;
		};
		
		this.showCurrentCoupon = function (previousCoupon, currentCoupon) {
			previousCoupon.className = (previousCoupon.className).replace(' won', '');
			currentCoupon.className = currentCoupon.className + ' won';
		};
		
		this.gameIteration = function (index, speed, stopIndex, coupons, couponsCount, iteration) {
			if (this.stopGame(index, speed, stopIndex, iteration)) {
				setTimeout(this.finishGame.bind(null, coupons[index]), 1700);
			} else {
				var newIndex = (index === couponsCount) ? 0 : index + 1;
				iteration++;
				this.playGame(coupons, newIndex, speed, stopIndex, iteration);
			}
		};
		
		this.stopGame = function (index, speed, stopIndex, iteration) {
			var endNumber = 25;
			return (
					iteration > endNumber
					&& !(iteration < (endNumber + stopIndex + 1))
				)
				&& stopIndex === index;
		};
		
		/**
		 * Hooks
		 */
		
		this.getCoupon = function(coupons, li, event) {
			var coupon = coupons[li];
			if (this.isButton(coupon)) return this.getButton(coupon, event);
			if (event.value.length > 0) {
				var elements = event.value.map(function(objEvent){
					if (coupon.meta.id === objEvent.id) {
						return this.changeCoupon(coupon, {value: objEvent});
					}
				}.bind(this)).filter(function(val) { return !!val; });
				coupon = elements[0];
			} else if (event.value && coupon.meta.id === event.value.id) {
				coupon = this.changeCoupon(coupon, event);
			} else if (event.target === 'translation') {
				coupon.content = this.getCouponContent(coupon.data)
			}
			return coupon;
		};
		
		this.isButton = function(coupon) {
			return coupon.className === 'filler';
		};
		
		this.changeCoupon = function (coupon, event) {
			coupon.content = this.getCouponContent(event.value);
			coupon.data = this.getCouponDataAttribute(event.value);
			coupon.pure = true;
			return coupon;
		};
		
		this.getCouponDataAttribute = function (coup) {
			var data = {};
			for (var prop in coup) {
				if (this.couponConstructCondition(coup, prop)) {
					data[prop] = coup[prop];
				}
			}
			return data;
		};
		
		this.couponConstructCondition = function (coup, prop) {
			return typeof coup[prop] !== 'undefined'
				&& prop !== 'code'
				&& prop !== 'updated_at'
				&& prop !== 'created_at';
		};
		
		this.getCouponContent = function (coup) {
			var result;
			switch (coup.type) {
				case 'discount':
					result = this.getItem(coup.value + '%', $this.text(getData(coup, 'line.1'), 'coupons.coupon'), 'coupon');
					break;
				case 'cash':
					result = this.getItem('$' + coup.value, $this.text(getData(coup, 'line.1'), 'coupons.cash'), 'cash');
					break;
				case 'free shipping':
					var freeShipping = $this.translation('coupons.freeShipping').split(' ');
					result = this.getItem(
						getData(coup, 'line.0', freeShipping[0]),
						getData(coup, 'line.1', freeShipping[1]),
						'freeShipping'
					);
					break;
				case 'free product':
					var freeProduct = $this.translation('coupons.freeProduct').split(' ');
					result = this.getItem(
						getData(coup, 'line.0', freeProduct[0]),
						getData(coup, 'line.1', freeProduct[1]),
						'freeProduct'
					);
					break;
			}
			return result;
		};
		
		this.getItem = function(upperText, lowerText, type) {
			return '<div class="right"></div><div class="left"></div>' +
				'<div class="upper-text" data-text="startScreen.' + type + '.0">' + upperText + '</div>' +
				'<div class="lower-text" data-text="startScreen.' + type + '.1">' + lowerText + '</div>';
		};
		
		this.getButton = function (button, event) {
			var buttonEvent = event.value.length > 8 ? event.value.pop() : false;
			button.content[0].content[0].textNode = $this.text(getData(buttonEvent, 'line.0'), 'coupons.start');
			button.content[0].content[1].textNode = $this.text(getData(buttonEvent, 'line.1'), 'coupons.game');
			return button;
		}
		
	}
	
	/**
	 * Logic for slot game
	 * @param $this variables scope
	 * @param finishGame
	 * @constructor
	 */
	
	function Slot($this, finishGame) {
		
		var animationObject = new Animation($this);
		
		this.finishGame = finishGame;
		
		/**
		 * Events
		 */
		
		this.submitOnSlot = function(e, send) {
			e.preventDefault();
			if (
				e.target !== e.currentTarget
				&& ! $this.gameButtonPushed
			) {
				$this.gameButtonPushed = true;
				var inputWrapper = $this.select('.input-wrapper');
				send(inputWrapper);
			}
		};
		
		/**
		 * Game logic
		 */
		
		this.animateHide = function(element) {
			animationObject.animateBySelector('fadeOut', element, function(content) {content.style.display = 'none'});
		};
		
		this.launchGame = function(reels) {
			this.playGame(reels);
		};
		
		this.playGame = function(reels) {
			$this.gameScreen();
			this.runGame(reels)
		};
		
		/**
		 * Animate reels
		 */
		
		this.runGame = function(reels) {
			var children = reels[0].children,
				couponSize = this.getElementSize(children[0]),
				stopIndex = this.getStopIndex(children);
			this.runReels(reels, couponSize, stopIndex);
		};
		
		this.runReels = function(reels, couponSize, stopIndex) {
			for (var i in reels) {
				if (reels.hasOwnProperty(i)) {
					var options = copyObject(this.getOptions(couponSize,
						getStyle(reels[i], 'top'),
						stopIndex, parseInt(i)
					));
					this.animateReel(i, reels[i], options, stopIndex);
				}
			}
		};
		
		this.getOptions = function (couponSize, position, stopIndex, loops) {
			return {
				stopIndex: stopIndex,
				loops: 3 + loops,
				initValue: couponSize,
				top: position,
				changeValue: 17,
				initChangeValue: 1,
				stepChangeValue: 0.3819,
				endValue: 0,
				startValue: (isMobile() || isAdminMobile ? -578 : -880),
				counter: 0,
				requestID: 0
			};
		};
		
		this.animateReel = function (i, reel, options, stopIndex) {
			var that = this,
				index = parseInt(i),
				handler = this.getHandler(i, reel, stopIndex, options.initValue);
			setTimeout(function(){
				that.runReel(reel, options, handler)
			}, index * 300);
		};
		
		this.getHandler = function(i, reel, stopIndex, initValue) {
			return (i === '2'
				? this.setEndGameScreen.bind(this, reel, stopIndex, initValue)
				: this.setWinSlot.bind(this, reel, stopIndex, initValue));
		};
		
		this.setWinSlot = function (reel, stopIndex, initValue) {
			reel.style.top = -initValue * stopIndex + 'px';
		};
		
		this.runReel = function (reel, options, resolve) {
			if (this.stopGame(options)) {
				cancelAnimationFrame(options.requestID);
				this.bounceAnimation(reel, resolve);
			} else {
				this.setTopValue(reel, options);
				var that = this;
				options.requestID = requestAnimationFrame(function(){
					that.runReel(reel, options, resolve);
				});
			}
		};
		
		this.stopGame = function(options) {
			return options.counter > options.loops;
		};
		
		this.setTopValue = function (reel, options) {
			this.changeTopValue(reel, options);
			if (options.top > options.endValue) {
				this.initNewLoop(options);
			}
		};
		
		this.changeTopValue = function (reel, options) {
			reel.style.top = options.top + 'px';
			if (options.loops === options.counter) {
				var selectedCoupon = -options.stopIndex * options.initValue;
				if (this.isPositionWinCoupon(selectedCoupon, options)) {
					options.counter += 1;
					reel.style.top = selectedCoupon + 'px';
				} else {
					options.top += this.getChangeValue(options);
				}
			} else {
				options.top += this.getChangeValue(options);
			}
		};
		
		this.isPositionWinCoupon = function (selectedCoupon, options) {
			var cutChangeValue = options.changeValue/1.5;
			return selectedCoupon <= options.top + (cutChangeValue)
				&& selectedCoupon >= options.top - (cutChangeValue);
		};
		
		this.getChangeValue = function(options) {
			if ((options.loops - 1) <= options.counter) {
				this.subtractChangeValue(options);
				return (options.initChangeValue);
			} else if (this.isChangeValue(options)) {
				this.addInitChangeValue(options);
				return (options.initChangeValue);
			} else {
				return (options.changeValue);
			}
		};
		
		this.isChangeValue = function(options) {
			return options.changeValue > options.initChangeValue;
		};
		
		this.addInitChangeValue = function(options) {
			options.initChangeValue += options.stepChangeValue;
		};
		
		this.subtractChangeValue = function(options) {
			if (options.initChangeValue > 1) {
				options.initChangeValue -= 0.042; // options.stepChangeValue/9;
			}
		};
		
		this.initNewLoop = function (options) {
			options.top = options.startValue;
			options.counter += 1;
		};
		
		/**
		 * Bounce animation started
		 */
		
		this.bounceAnimation = function(reel, resolve) {
			return new Promise(this.bouncePromise.bind(this, resolve, function(){}, reel));
		};
		
		this.bouncePromise = function(resolve, reject, reel){
			var initTime = 75,
				bounceTime = 110,
				initStep = 20,
				bounceStep = 5;
			getStyle(reel, 'top');
			this.setAnimateStyles(reel, initTime);
			this.dragAnimate(reel, initStep, 0)
				.then(function (dragAnimate) {
					return dragAnimate(reel, -initStep, initTime);
				}.bind(this))
				.then(function (dragAnimate) {
					this.setAnimateStyles(reel, bounceTime);
					return dragAnimate(reel, bounceStep, bounceTime);
				}.bind(this))
				.then(function (dragAnimate) {
					this.setAnimateStyles(reel, bounceTime);
					return dragAnimate(reel, -bounceStep, bounceTime);
				}.bind(this))
				.then(resolve);
		};
		
		this.dragAnimate = function(reel, step, time) {
			return new Promise(function(resolve) {
				setTimeout(function(){
					reel.style.top = (getStyle(reel, 'top') + step) + 'px';
					return resolve(this.dragAnimate.bind(this));
				}.bind(this), time);
			}.bind(this));
		};
		/**
		 * End of Bounce animation started
		 */
		
		this.setEndGameScreen = function(reel, stopIndex, initValue) {
			this.setWinSlot(reel, stopIndex, initValue);
			setTimeout(function() {
				var reelItems = reel.querySelectorAll('.reel-item');
				this.finishGame(this.getWinItem(reelItems, stopIndex));
			}.bind(this), 1700);
		};
		
		this.getWinItem = function(reelItems, index) {
			for (var item in reelItems) {
				if (reelItems.hasOwnProperty(item)) {
					if (index === parseInt(item)) {
						return reelItems[item];
					}
				}
			}
		};
		
		this.setAnimateStyles = function(reel, time) {
			var browsers = ['-o-', '-webkit-', '-moz-', ''];
			for (var i in browsers) {
				if (browsers.hasOwnProperty(i)) {
					this.setAnimation(reel, browsers[i], time);
				}
			}
		};
		
		this.setAnimation = function (reel, prefix, time) {
			reel.style[prefix + 'transition'] = 'top cubic-bezier(0.17, 0.67, 0.83, 0.67) ' + time / 1000 + 's';
		};
		
		this.setDefaultStyles = function(reel) {
			var browsers = ['-o-', '-webkit-', '-moz-', ''];
			for (var i in browsers) {
				if (browsers.hasOwnProperty(i)) {
					this.setDefaultAnimation(reel, browsers[i]);
				}
			}
		};
		
		this.setDefaultAnimation = function (reel, prefix) {
			reel.style[prefix + 'transition'] = 'none ease 0s';
		};
		
		this.getElementSize = function(coupon) {
			return getStyle(coupon, 'margin-top')
				+ getStyle(coupon, 'height');
		};
		
		this.getStopIndex = function(couponsArray) {
			var random = Math.random(),
				chances = this.getCouponChances(couponsArray);
			return parseInt(this.getIndexByRandom(chances, random));
		};
		
		this.getIndexByRandom = function(chances, random) {
			for (var i in chances) {
				if (chances.hasOwnProperty(i)) {
					if (random < chances[i]) {
						return i;
					}
				}
			}
		};
		
		this.getCouponChances = function(couponsArray) {
			var chance = 0,
				chances = [],
				previousChance = 0;
			for (var i in couponsArray) {
				if (couponsArray.hasOwnProperty(i)) {
					var data = couponsArray[i].dataset;
					chance = parseFloat(data.chance)/100;
					chances.push(chance + previousChance);
					previousChance = previousChance + chance;
				}
			}
			return chances;
		};
		
		/**
		 * Hooks
		 */
		this.getCoupon = function(coupons, index, event) {
			var coupon = coupons[index],
				eventCoupon = event.value;
			if (coupon.data.id === eventCoupon.id) {
				coupon.data.type = eventCoupon.type;
				coupon.data.value = eventCoupon.value;
				this.refreshFields(coupon);
				if (this.isFreeShipping(eventCoupon)) {
					coupon.content = '<img src="//' + (scriptPath.split( '/' ))[2] + '/img/game/truck_slot.svg" />';
					coupon.pure = true;
				} else {
					coupon.textNode = this.getTextValue(eventCoupon);
				}
			}
			return coupon;
		};
		
		this.refreshFields = function(coupon) {
			delete coupon.content;
			delete coupon.pure;
			delete coupon.textNode;
		};
		
		this.isFreeShipping = function(eventCoupon) {
			return ['cash', 'discount'].indexOf(eventCoupon.type) < 0;
		};
		
		this.getTextValue = function(eventCoupon) {
			var value = eventCoupon.value;
			switch (eventCoupon.type) {
				case 'cash': return '$' + value;
				case 'discount': return value + '%';
				default: return value;
			}
		}
	}
	
	/**
	 * Countdown logic
	 */
	function Bar($this, closeModal) {
		
		this.continueUseDiscount = function(modalObject) {
			if (isAdmin) return false;
			if (
				$this.isContinueDiscountHaveURL === '1'
				&& $this.continueDiscountURL !== ''
			) {
				openNewTab($this.continueDiscountURL, $this.targetDiscountURL);
			}
			setBarShowed(0, $this.gameInfo, $this.cookieExpirationDays);
			if ($this.showBar && $this.isBarNodeAvailable) {
				this.setTime($this.barNode.querySelector('.coupon-code-time'), $this.barCountDown, 0);
				this.showCountdownBar($this.barCountDown, 0, true, modalObject);
				this.setCountDownShowed();
			} else {
				modalObject.closeAnimatedModal(false);
			}
		};
		
		/**
		 * TODO: refactor this method
		 * @param minutes
		 * @param seconds
		 * @param endOfGame
		 * @param modalObject
		 */
		this.showCountdownBar = function(minutes, seconds, endOfGame, modalObject) {
			seconds = typeof seconds === 'number' ? seconds : 0;
			var bar = d.body.querySelector('.lucky-coupon-bar.game-' + $this.gameInfo.gameId);
			if (bar) {
				bar.remove();
			}
			d.body.appendChild($this.barNode);
			$this.barNode.style.display = 'block';
			var input = $this.barNode.querySelector('.coupon-code-code input');
			input.value = getGameCode($this.gameInfo);
			input.style.width = ((input.value.length * 14.5) + 2) + 'px';
			if (endOfGame) {
				modalObject.closeAnimatedModal(false);
			}
			var countDown = $this.barNode.querySelector('.coupon-code-time'),
				date = new Date(),
				now = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() + minutes, date.getSeconds() + seconds);
			clearInterval($this.countDownIntervalId);
			this.countDown(now, countDown);
		};
		
		this.setCountDownShowed = function() {
			setCookie(getCountDownShowedName($this.gameInfo), new Date().getTime())
		};
		
		this.setTime = function(countDown, minutes, seconds) {
			countDown.innerHTML = '<span>' + minutes + 'm:</span><span>' + seconds + 's</span>';
		};
		
		this.countDown = function(countDownDate, countDown) {
			$this.countDownIntervalId = setInterval(function() {
				var now = new Date().getTime(),
					distance = countDownDate - now,
					minutes = this.getDistanceMinutes(distance),
					seconds = this.getDistanceSeconds(distance);
				this.setTime(countDown, minutes, seconds);
				if (distance <= 0) {
					clearInterval($this.countDownIntervalId);
					countDown.remove();
				}
			}.bind(this), 950);
		};
		
		this.getDistanceMinutes = function(timestamp) {
			return Math.floor((timestamp % (1000 * 60 * 60)) / (1000 * 60));
		};
		
		this.getDistanceSeconds = function(timestamp) {
			return Math.floor((timestamp % (1000 * 60)) / 1000);
		};
		
		this.initCountdownBar = function() {
			if ($this.showBar) {
				var timeShowed = parseInt(this.getCountDownShowed()),
					closeBar = $this.barNode.querySelector('.fa.fa-times-thin.fa-2x');
				setTimeout(this.appendClipboardEvents.bind(this), 100);
				closeBar.addEventListener('click', this.closeBar);
				this.showCalculatedTime(timeShowed);
			}
		};
		
		this.appendClipboardEvents = function() {
			var clipboard = new Clipboard('.copy-code', {
				text: function(target) {
					return d.querySelector('body > .lucky-coupon-bar input' + target.dataset.clipboardTarget).value;
				}
			});
			clipboard.on('success', this.copyCodeSuccess.bind(this));
			clipboard.on('error', this.copyCodeError.bind(this));
		};
		
		this.showCalculatedTime = function(timeShowed) {
			if (timeShowed) {
				var distance = this.getDistance(timeShowed),
					calculatedMinutes = this.getCalculatedMinutes(distance),
					calculatedSeconds = this.getCalculatedSeconds(distance);
				if (calculatedMinutes > 0 && calculatedSeconds > 0) {
					this.showCountdownBar(calculatedMinutes, calculatedSeconds, false);
				}
			}
		};
		
		this.getDistance = function(timeShowed) {
			var now = new Date().getTime();
			return now - timeShowed;
		};
		
		this.getCalculatedMinutes = function(distance) {
			return $this.barCountDown - 1 - this.getDistanceMinutes(distance)
		};
		
		this.getCalculatedSeconds = function(distance) {
			return 60 - this.getDistanceSeconds(distance);
		};
		
		this.getCountDownShowed = function() {
			return getCookie(getCountDownShowedName($this.gameInfo));
		};
		
		this.copyCodeSuccess = function(e) {
			e.trigger.innerHTML = $this.getNewText('bar.copied', 'bar.copied');
		};
		
		this.copyCodeError = function(e) {
			e.trigger.innerHTML = $this.getNewText('bar.failedCopy', 'bar.failedCopy');
		};
		
		this.closeBar = function(e) {
			e.target.parentNode.remove();
			closeModal(e);
		};
		
		this.destroy = function () {
			clearInterval($this.countDownIntervalId);
			$this.barNode.remove();
		}
	}
	
	function Trigger ($this) {
		
		var $node = $this.triggerNode;
		
		this.initTrigger = function () {
			if (isFrame) {
				this.insert();
				if ((isDetailFrame() && $this.showTrigger) && $this.isAdminDesktop) {
					$node.style.zIndex = '2147483648';
					this.show();
				}
			} else if (this.triggerShowCondition() && !isAdmin) {
				this.insertAndShow();
			}
		};
		
		this.triggerShowCondition = function() {
			return $this.showTrigger && !isCurrentGamePlayed($this.gameInfo);
		};
		
		this.insertAndShow = function() {
			this.insert();
			if (getModalTriggerClosedFlag($this.gameInfo) !== '1') {
				this.show();
				if (getModalShowedFlag($this.gameInfo) === '1') {
					show($node.querySelector('.close'));
				}
			}
		};
		
		this.show = function () {
			$node.style.display = 'block';
		};
		
		this.hide = function() {
			$node.style.display = 'none';
		};
		
		this.insert = function() {
			this.remove();
			d.body.appendChild($node);
		};
		
		this.remove = function () {
			var triggerSelector = '.' + $node.className.replace(/ /g, '.'),
				insertedTrigger = d.body.querySelector(triggerSelector);
			if (insertedTrigger) {
				insertedTrigger.remove(insertedTrigger.selectedIndex);
			}
		};
		
		this.destroy = function () {
			this.hide();
			this.remove();
		};
	}
	
	//@todo: move ajax functions from Modal to Request object
	function Request($this) {
		
		this.isValidEmail = function(email, handler) {
			this.ajax({
				url: '/api/v1/check/email',
				method: 'POST',
				data: {
					id: $this.gameId,
					site_id: $this.siteId,
					subscriber_email: email
				}
			}, handler);
		};
		
		this.get = function (url, handler, handlerParams) {
			this.ajax({
				url: url,
				method: 'GET'
			}, handler, handlerParams);
		};
		
		this.post = function(url, data, handler, handlerParams) {
			this.ajax({
				url: url,
				method: 'POST'
			}, handler, handlerParams);
		};
		
		this.ajax = function(data, handler, handlerParams) {
			handler = handler ? handler.bind(this) : function () {}.bind(this);
			var ajax = new XMLHttpRequest(),
				params = this.serialize(data.data),
				url = data.url.indexOf($this.hostName) > -1 ? data.url : '//' + $this.hostName + data.url;
			ajax.open(data.method, url, true);
			ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			ajax.onreadystatechange = function () {
				if (ajax.readyState === 4 && ajax.status === 200) {
					handler(ajax, handlerParams);
				} else if (ajax.readyState === 4 && ajax.status > 0) {
					handler(ajax, handlerParams);
				}
			};
			ajax.send(params);
		};
		
		this.serialize = function(obj) {
			var str = [];
			for(var p in obj)
				if (obj.hasOwnProperty(p)) {
					str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
				}
			return str.join('&');
		}
	}
	
	function Animation($this) {
		
		this.rotate = function(element, object, handler) {
			function animate(element, object, handler) {
				handler = typeof handler === 'function' ? handler : function() {};
				if (object['speed'] !== 0) {
					var speed = 0;
					if (object['speed'] > 0) {
						speed = decimalPlus(object['speed'], object['friction']);
					}
					object['angle'] = decimalPlus(object['angle'], object['speed'] = speed);
					element.style.transform = 'translateZ(0) rotate(' + object['angle'] + 'deg)';
					requestAnimationFrame(animate.bind(null, element, object, handler));
				} else {
					handler.call();
				}
			}
			requestAnimationFrame(animate.bind(null, element, this.getRotateDefaults(object), handler));
		};
		
		this.getRotateDefaults = function(object) {
			var values = {
					'stop': 0, 'angle': 0, 'speed': 20
				},
				defaults = this.getDefaults(object, values);
			defaults['friction'] = this.getFriction(defaults);
			return defaults;
		};
		
		this.getFriction = function(params) {
			var endAngle = (3240 + params['stop']),
				iterations = Math.round(endAngle/9.908256881),
				triangularNumber = this.getTriangularNumber(iterations);
			return -decimalMinus(params['speed'] * iterations/triangularNumber, endAngle/triangularNumber);
		};
		
		this.getTriangularNumber = function(n) {
			return (n * (n+1)) / 2;
		};
		
		this.getFrictionRange = function(start, end, step) {
			var result = [];
			while (start < end) {
				start = start + step;
				result.push(start);
			}
			return result;
		};
		
		this.getDefaults = function(object, defaults) {
			for (var key in defaults) {
				if(!(key in object)) {
					object[key] = defaults[key];
				}
			}
			return object;
		};
		
		this.moveTo = function(element, moveObject, handler) {
			handler = this.getFunction(handler);
			var currentSide = getStyle(element, moveObject['side']);
			if (Math.abs(currentSide) <= Math.abs(moveObject['number'])) {
				element.style[moveObject['side']] = currentSide + this.getMoveToStep(moveObject);
				requestAnimationFrame(this.moveTo.bind(this, element, moveObject, handler));
			} else {
				handler();
			}
		};
		
		this.getMoveToStep = function(moveObject) {
			return isUndefined(moveObject['step'])
				? moveObject['number'] / 10
				: moveObject['step'];
		};
		
		this.fadeOut = function(element, handler) {
			var effect = 'fadeOut';
			handler = this.getFadeOutHandler(handler, element, effect);
			if (['0', '1'].indexOf(getStyle(element, 'opacity')) > -1) {
				this.animateBySelector(effect, element, handler);
			} else {
				this.partlyFadeOut(element, handler);
			}
		};
		
		this.getFadeOutHandler = function(handler, element, effect) {
			return this.isFunction(handler)
				? handler
				: this.setItemOpacity.bind(this, element, effect);
		};
		
		this.partlyFadeOut = function (element, handler, step) {
			var step = typeof step === 'undefined' ? getStyle(element, 'opacity')/10 : step,
				opacity = getStyle(element, 'opacity');
			if (opacity > 0) {
				element.style.opacity = opacity - step;
				requestAnimationFrame(this.partlyFadeOut.bind(this, element, handler, step));
			} else {
				handler();
			}
		};
		
		this.animate = function(element, effect) {
			this.animateBySelector(
				effect,
				element,
				this.setItemOpacity.bind(this, element, effect)
			);
		};
		
		this.setItemOpacity = function(node, effect) {
			node.style.opacity = this.isShowEffect(effect);
		};
		
		this.isShowEffect = function (effect) {
			return (typeof effect === 'undefined' || effect.indexOf('In') > -1) ? 1 : 0;
		};
		
		this.animateBySelector = function (effect, element, afterAnimation) {
			var node = this.getNode(element),
				direction = this.getDirection(),
				animation = this.getAnimation(direction, effect);
			this[this.getMethodName(direction)](animation, node, afterAnimation);
		};
		
		this.getMethodName = function(direction) {
			return direction && !isMobile() ? 'sidesAnimation' : 'otherAnimation';
		};
		
		this.getNode = function (element) {
			return typeof element === 'string'
				? $this.select(element)
				: element;
		};
		
		this.getAnimation = function(direction, effect) {
			if (effect === 'css-animate') {
				return ' ' + effect;
			}
			return direction ? ' animate-' + direction + '-out' : ' animated ' + effect;
		};
		
		this.getDirection = function() {
			var content = $this.select($this.wrapperClass),
				positionClass = content.parentNode.className;
			return this.getSide(positionClass);
		};
		
		this.getSide = function(positionClass) {
			var right = this.isSide(positionClass, 'right');
			return right ? right : this.isSide(positionClass, 'left');
		};
		
		this.isSide = function (haystack, needle) {
			return haystack.indexOf(needle) !== -1 ? needle : false;
		};
		
		this.sidesAnimation = function(outAnimation, node, afterAnimation) {
			afterAnimation = this.getFunction(afterAnimation);
			var inAnimation = ' animate-in';
			addClass(node, outAnimation);
			this.displayContent();
			setTimeout(function() {
				removeClass(node, outAnimation);
				addClass(node, inAnimation);
				afterAnimation(node);
			}.bind(this), 300);
		};
		
		this.otherAnimation = function(animation, node, afterAnimation) {
			afterAnimation = this.getFunction(afterAnimation);
			if (animation === ' css-animate') {
				this.displayContent();
			}
			setTimeout(function () {
				var animationTime = 500;
				if (animation !== ' css-animate') {
					this.displayContent();
					animationTime = 900;
				}
				this.displayContent();
				addClass(node, animation);
				setTimeout(function () {
					if (animation !== ' css-animate') {
						removeClass(node, animation);
					}
					afterAnimation(node);
				}.bind(this), animationTime);
			}.bind(this), 100);
		};
		
		this.getFunction = function(handler) {
			return this.isFunction(handler) ? handler : function(){};
		};
		
		this.isFunction = function(handler) {
			return typeof handler === 'function';
		};
		
		this.displayContent = function() {
			$this.select('div').style.display = 'inline-table';
		};
		
		this.showCssFadeIn = function(selector) {
			var element = $this.select(selector);
			addClass(element, ' css-fade-in');
			setTimeout(this.animateBySelector.bind(this, 'css-animate', element), 100);
		};
		
		this.showCssSlideBottom = function(selector) {
			var element = $this.select(selector);
			addClass(element, ' css-slide-bottom');
			setTimeout(this.animateBySelector.bind(this, 'css-animate', element), 100);
		};
		
		this.setVisible = function (invisibleNodes) {
			for (var index in invisibleNodes) {
				if (invisibleNodes.hasOwnProperty(index)) {
					invisibleNodes[index].style.opacity = 1;
				}
			}
		};
		
		this.showWithoutAnimation = function() {
			var invisibleNodes = $this.selectAll('.start-screen [style*="opacity: 0"]'),
				invisibleGifts = $this.selectAll('.gifts [style*="opacity: 0"]');
			this.setVisible(invisibleNodes);
			this.setVisible(invisibleGifts);
		};
		
		this.isShownModal = function() {
			var gamesNode = d.body.querySelectorAll($this.modalSelector),
				modalsArray = Array.apply(null, gamesNode).map(this.isDisplayed);
			return modalsArray.indexOf(true) !== -1;
		};
		
		this.isDisplayed = function(item) {
			return item && item.style.display === 'block';
		};
		
		//@todo: make fadeOutBySelectors function
		/*for (var i in selectors) {
		 if (!selectors.hasOwnProperty(i)) continue;
		 var element = $this.select(selectors[i]),
		 handler = selectors.length === (parseInt(i) + 1)
		 ? this.fadeOutWheelTopHandler.bind(animationObject, element, handler)
		 : undefined;
		 animationObject.fadeOut(element, handler);
		 }*/
		
		// Context of Animation() function
		this.fadeOutWheelTopHandler = function(element, handlerParam) {
			this.setItemOpacity(element, 'fadeOut');
			handlerParam.call();
		};
	}
	
	function Calculations() {
		
		this.getWinnerItem = function(items) {
			var chances = this.getChances(items),
				winnerItemIndex = this.getWinnerItemIndex(chances);
			return items[winnerItemIndex];
		};
		
		this.getChances = function (items) {
			var prevIndex = undefined,
				result = [], addition = 0;
			for (var index in items) {
				if (items.hasOwnProperty(index)) {
					addition = items.hasOwnProperty(prevIndex) ? result[prevIndex] : 0;
					result[index] = parseFloat(items[index].getAttribute('data-chance')) + addition;
					prevIndex = index;
				}
			}
			return result;
		};
		
		this.getWinnerItemIndex = function (chances) {
			var random = Math.random() * 100;
			for (var index in chances) {
				if (chances.hasOwnProperty(index)) {
					if (chances[index] > random) return index;
				}
			}
		};
		
	}
	
	/**
	 * Class for modal popup
	 * @param w object window
	 * @param d object document
	 * @param mw object modal wrapper
	 * @param mc object modal content
	 * @param st object styles for modal content
	 * @param mb object of behavior
	 */
	function Modal(w, d, mw, mc, st, mb) {
		
		var $this = {},
			animationObject = {},
			giftObject = {},
			couponObject = {},
			slotObject = {},
			wheelObject = {},
			barObject = {},
			triggerObject = {};
		
		this.gameObject = {};
		
		this.dev = {};
		
		this.init = function() {
			$this = this.getVariables();
			if (!isAdmin && !isFrame && this.isFilteredByUrl()) return false;
			animationObject = new Animation($this);
			this.initGameObject();
			this.initCreate();
			this.initEvents();
			this.initBehavior();
			this.setDevEnv();
		};
		
		/**
		 * @TODO: clear other code from mw, mc, st, mb variables
		 *		@todo: and remove this method to Launcher object.
		 * construct $this variable
		 */
		this.getVariables = function() {
			var splitId = mw.id.split('-'),
				gameId = parseInt(mw.meta.gameId),
				modalWrapperClass = 'lucky-coupon-popup',
				defaultNode = d.createElement('div'),
				gameInfo = {
					type: mw.meta.type,
					gameId: gameId
				},
				checkBehavior = typeof mb.makeGPDRCompliance === 'boolean' && mb.makeGPDRCompliance
					? '1' : mb.makeGPDRCompliance,
				hostName = getHostName(scriptPath);
			st.position = isMobile() ? 'center' : st.position;
			mb.makeGPDRCompliance = checkBehavior;
			// move to separate object
			return {
				positionClass: '.lucky-coupon-' + st.position,
				checkBehavior: checkBehavior,
				gameButtonPushed: false,
				modalNode: {},
				barNode: defaultNode,
				isBarNodeAvailable: getData(mb, 'countDownTime', '1') === '1',
				triggerNode: defaultNode,
				gameId: gameId,
				siteId: parseInt(splitId[splitId.length - 1]),
				countDownIntervalId: 0,
				style: {},
				gameInfo: gameInfo,
				gameType: mw.meta.type,
				lang: mw.meta.language,
				trans: {
					coupons: mw.meta.translations.coupons,
					changeable: mw.meta.translations.changeable,
					bar: mw.meta.translations.bar
				},
				textObject: getData(mw, 'meta.text', []),
				hostName: hostName,
				hostUrl: '//' + hostName + '/',
				couponsList: findCoupons(mc.content, []),
				barCountDown: getCountDownMinutes(mb),
				showBar: getBarShowed(gameInfo) !== '1',
				showTrigger: isTriggerOpens(mb),
				isUserOpened: false,
				modalWrapperClass: modalWrapperClass,
				wrapperClass: '.' + modalWrapperClass,
				modalSelector: '#' + decodeURI(mw.id) + '.game-' + getData(mw, 'meta.gameId', 'preview'),
				closeClass: mw.meta.closeItem.className,
				errorTextClass: 'arrow-box',
				isAdminDesktop: true,
				previewTrigger: getAdminPreviewTrigger(),
				modalTimerId: null,
				sendedEmail: '',
				emailCollect: getData(mb, 'collectEmailFromUsers.yes', '1') === '1',
				nameCollect: getData(mb, 'collectNameFromUsers.yes', '0') === '1',
				isGameEmbedded: getData(mb, 'startDisplayTheGame.onSpecialPagePlace', '0') === '1',
				isPoweredByVisible: getData(mb, 'isPoweredByVisible', '1') === '1',
				cookieExpirationDays: getCookieExpirationDays(mb),
				isRecartEnabled: getData(mb, 'collectEmailWithRecart', '0') === '1',
				isRecartChecked: false,
				filtersByUrl: getData(mb, 'whereShouldTheGameAppear', []),
				isContinueDiscountHaveURL: getData(mb, 'continueUseDiscount.value', '0'),
				continueDiscountURL: getData(mb, 'continueUseDiscount.url', ''),
				targetDiscountURL: getData(mb, 'continueUseDiscount.target', '1'),
				getNewText: function(textPath, namespaceWord) {
					return this.text(getData(this.textObject, textPath, false), namespaceWord);
				},
				text: function(text, namespaceWord) {
					return (text !== false) ? text : this.translation(namespaceWord);
				},
				translation: function(namespaceWord) {
					var split = namespaceWord.split('.');
					return translation(this.lang, this.trans[split[0]], split[1])
				},
				select: function(selector) {
					return this.selectType('querySelector', selector);
				},
				selectAll: function(selector) {
					return this.selectType('querySelectorAll', selector);
				},
				selectType: function(selectName, selector) {
					if (typeof this.modalNode[selectName] !== 'undefined') {
						return this.modalNode[selectName](selector);
					} else {
						throw new ModalHelper('Modal removed from DOM');
					}
				},
				gameScreen: function() {
					var middleDiv = this.select('.middle'),
						startText = this.select('.start-text');
					removeClass(middleDiv, ' hide');
					addClass(startText, ' hide');
				},
				getHostUrl: function(path) {
					return '//' + this.hostName + '/' + path;
				}
			};
		};
		
		function ModalHelper(message) {
			Error.apply(this, arguments);
			this.name = 'ModalHelper';
			this.message = message;
		}
		
		ModalHelper.prototype = Object.create(Error.prototype);
		
		this.initGameObject = function () {
			// @todo: make instance only for one game
			giftObject = new Gift($this, this.finishGame.bind(this));
			couponObject = new Coupon($this, this.finishGame.bind(this));
			slotObject = new Slot($this, this.finishGame.bind(this));
			wheelObject = new Wheel($this, this.finishGame.bind(this));
			switch ($this.gameType) {
				case 'gift': this.gameObject = giftObject; break;
				case 'coupon': this.gameObject = couponObject; break;
				case 'slot': this.gameObject = slotObject; break;
				case 'wheel': this.gameObject = wheelObject; break;
			}
		};
		
		/**
		 * Create structure
		 */
		this.initCreate = function() {
			$this.style = this.getCompiledStyle();
			this.constructNodes([this.getModalObject()], d.body);
			this.initModalNode();
			this.initNameInput();
			this.initButton();
			this.gamesSettings();
			this.showAdditionalObjects();
		};
		
		this.initModalNode = function() {
			$this.modalNode = d.body.querySelector($this.modalSelector);
			$this.modalNode.style.height = '100%';
			$this.modalNode.appendChild($this.style.cloneNode(true));
			this.extractToDocument();
			this.showStartNote();
		};
		
		this.getBackgroundUrl = function(element) {
			var backgroundStyle = getStyle(element, 'background'), result;
			if (backgroundStyle === '') return false;
			if (isSafari()) {
				result = this.getBackgroundStringPath(this.getBackground(element));
			} else {
				var	background = backgroundStyle.split('"');
				if (!background.hasOwnProperty('1')) return false;
				result = this.getBackgroundStringPath(background[1]);
			}
			return result;
		};
		
		this.getBackground = function(element) {
			return w.getComputedStyle(element, null).getPropertyValue('background');
		};
		
		this.getBackgroundStringPath = function(background) {
			var split = background.split($this.hostUrl),
				leftPart = split.hasOwnProperty('1')
					? split[1].replace('storage/images/', '')
					: false;
			if (!leftPart) return false;
			var withoutQuery = leftPart.split('?');
			if (!withoutQuery.hasOwnProperty('0')) return false;
			return withoutQuery[0].indexOf('gift') > -1 ? withoutQuery[0].replace('img/', '') : withoutQuery[0];
		};
		
		this.isBackgroundAvailable = function(backgroundUrl) {
			return backgroundUrl !== false && backgroundUrl !== '' &&
				typeof backgroundUrl !== 'undefined';
		};
		
		this.backgroundLoader = function() {
			if (isAdmin || isFrame) return false;
			var element = $this.select('.custom-image'),
				backgroundUrl = this.getBackgroundUrl(element);
			if (!this.isBackgroundAvailable(backgroundUrl)) return false;
			this.ajax({
				method: 'GET',
				url: '/background-image/' + backgroundUrl
			}, function(data) {
				var response = 'response' in data ? JSON.parse(data.response) : false;
				if (!response && !response.hasOwnProperty('background_file')) return false;
				var images = $this.selectAll('.custom-image'),
					base64Image = response.background_file;
				for (var index in images) {
					if(typeof images[index] !== 'object') continue;
					images[index].style.backgroundImage = 'url(' + base64Image + ')';
				}
			}.bind(this));
		};
		
		this.initNameInput = function() {
			var inputWrapper = $this.select('.input-wrapper'),
				emailInput = inputWrapper.querySelector('.email-input'),
				nameInput = inputWrapper.querySelector('.username-input');
			if($this.emailCollect && $this.nameCollect) {
				addClass(emailInput, ' name-case');
			} else {
				hide(nameInput);
			}
		};
		
		this.extractToDocument = function () {
			this.cloneElementToDocument('barNode', '.lucky-coupon-bar');
			this.cloneElementToDocument('triggerNode', '.lucky-coupon-trigger');
		};
		
		this.cloneElementToDocument = function(varName, selector) {
			$this[varName] = $this.select(selector);
			if (!$this[varName]) return false;
			$this[varName] = $this[varName].cloneNode(true);
			var className = ' game-' + $this.gameInfo.gameId + ' ' + mw.meta.language;
			addClass($this[varName], className);
		};
		
		this.showStartNote = function() {
			if(!this.isOnGPDR()){
				show($this.select('.text.small-message div[data-name=note]'));
			}
		};
		
		this.getModalObject = function() {
			var modalObject = copyObject(mw),
				classNames = mc.content[1].className.split(' ');
			this.adminPanelFix();
			mc.content[1].className = this.getConstructedClass(classNames);
			modalObject.content = copyObject(mc.content);
			return modalObject;
		};
		
		this.adminPanelFix = function() {
			if (getDocumentWidth() > 600 && $this.gameType === 'gift') {
				//@todo: make tree search instead selecting "lucky-coupon-popup" from array
				mc.content[1].content[0].style = {'width': '440px'};
			}
		};
		
		this.getConstructedClass = function(classNames) {
			var additionalClass = classNames.hasOwnProperty('1') ? classNames[1] + ' ' : '';
			if (isFrame && window['DETAIL_PAGE']) {
				additionalClass += 'game-editor ';
			}
			if (!$this.emailCollect) {
				additionalClass += 'no-emails ';
			}
			return 'lucky-coupon-' + st.position + ' ' + additionalClass + mw.meta.language;
		};
		
		this.getCompiledStyle = function () {
			st.font = st.font.replace(/%27/g, '\'').replace(/%22/g, '"');
			return this.getStyleElement(st.style()).cloneNode(true);
		};
		
		this.getStyleElement = function(styleString) {
			var style = d.createElement('style');
			style.innerHTML = styleString;
			return style;
		};
		
		/**
		 * @todo: refactor this method
		 *
		 * @param elements
		 * @param parentNode
		 * @returns {*}
		 */
		this.constructNodes = function (elements, parentNode) {
			for (var propName in elements) {
				var element = this.preProcess(elements[propName]),
					newItem = this.addNode(element);
				if (typeof newItem.tagName === 'undefined') return parentNode;
				if (element.pure === true) {
					newItem.innerHTML = element.content;
				} else if (typeof element.content !== 'undefined' && newItem.tagName !== 'META') {
					newItem = this.constructNodes(element.content, newItem);
				}
				if (element.textNode) {
					var text = element.textNode.length
						? this.changeTextNode(element)
						: '';
					newItem.appendChild(d.createTextNode(text));
				}
				parentNode.appendChild(newItem);
				this.checkboxGPDR(newItem);
			}
			return parentNode;
		};
		
		this.checkboxGPDR = function(newItem) {
			if (newItem.className !== 'checkbox-wrapper') return false;
			newItem.style.display = this.isOnGPDR() ? 'block' : 'none';
		};
		
		this.isOnGPDR = function() {
			return mb.makeGPDRCompliance === '1';
		};
		
		this.changeTextNode = function(element) {
			var text = element.textNode.replace(/%22/g, '\'').replace(/%27/g, '"');
			if (element.meta === 'congratulation' && this.isSpecialShop('eagle-energy.myshopify.com')) {
				return 'High Five';
			}
			return element.className === 'slice' ? decodeURI(text) : text;
		};
		
		this.isSpecialShop = function(shopName) {
			var script = document.querySelector('script[src*="' + shopName + '"]');
			if (script && script.src) {
				return script.src.search(shopName) !== -1;
			}
			return false;
		};
		
		this.preProcess = function(element) {
			if (
				isMobile()
				&& element.className
				&& element.className.search(/lucky-coupon-(left|right)/g) > -1
			) {
				element.className = element.className.replace(/left|right/g, 'center');
				return element;
			}
			var isProcessed = isMobile() && element.className === 'reel-wrapper';
			if (!isProcessed) return element;
			var top = (element.style.hasOwnProperty('top') && element.style.top !== '0px'
					? element.style.top : 0),
				topNumber;
			if (top) {
				var number = parseInt(top.replace('px', ''));
				topNumber = number + (30 * Math.abs(number/110));
			} else {
				topNumber = top;
			}
			element.style.top = topNumber + 'px';
			return element;
		};
		
		this.addNode = function(object) {
			var element = d.createElement(object.tagName);
			for (var key in object) {
				var value = isSet(object, key);
				if (avoidProperty(key) && !this.metaContent(element.tagName, key)) continue;
				if (key === 'data') {
					element = this.setDataAttr(value, element);
				} else if (key === 'attributes') {
					element = this.setAttribute(value, element);
				} else if (isObject(value) && isNaN(key)) {
					element = this.setMultipleAttribute(object, key, value, element);
				} else if (value) {
					element[key] = value;
				}
			}
			return element;
		};
		
		this.setDataAttr = function(value, element) {
			for (var attr in value) {
				element.dataset[attr] = value[attr];
			}
			return element;
		};
		
		this.setAttribute = function(value, element) {
			for (var attrName in value) {
				element.setAttribute(attrName, value[attrName]);
			}
			return element;
		};
		
		this.setMultipleAttribute = function(object, key, value, element) {
			if (this.isMultipleTranslations(object, key, value)) {
				element.innerText = value[this.getTranslationType()];
			} else {
				for (var k in value) {
					element[key][k] = value[k];
				}
			}
			return element;
		};
		
		this.metaContent = function(tagName, key) {
			return tagName === 'META' && key === 'content'
		};
		
		this.isMultipleTranslations = function(object, key, value) {
			return typeof object.meta !== 'undefined'
				&& object.meta === 'title'
				&& key === 'textNode'
				&& value.hasOwnProperty('other');
		};
		
		this.getTranslationType = function () {
			return $this.gameType === 'gift' ? $this.gameType : 'other';
		};
		
		this.gamesSettings = function () {
			this.frameSettings();
			this.sizeSlotGame();
			if ($this.gameType === 'wheel')
				wheelObject.init();
		};
		
		this.frameSettings = function() {
			if (isFrame) {
				$this.modalNode.style.background = '';
				setTimeout(function () {
					if (typeof DETAIL_PAGE === 'undefined') {
						window['DETAIL_PAGE'] = false;
					}
					if (['coupon', 'slot'].indexOf($this.gameType) > -1 && !DETAIL_PAGE) {
						$this.select('.powered-by').style.opacity = 0;
						$this.select('.lucky-coupon-popup').style.boxShadow = '0 0 0';
					}
					this.sizeCouponGame();
				}.bind(this), 1);
			}
		};
		
		this.sizeCouponGame = function() {
			if ($this.gameType !== 'coupon') return false;
			var prizeList = $this.select('.prize-list');
			if (getDocumentWidth() < 600) {
				prizeList.style.overflow = 'hidden';
				$this.modalNode.style.height = 'auto';
			} else if (getDocumentWidth() > 600) {
				prizeList.style.height = 'auto';
			}
		};
		
		this.sizeSlotGame = function() {
			if ($this.gameType === 'slot') {
				if (getDocumentWidth() < 600) {
					$this.modalNode.style.height = 'auto';
					this.sizeReelWrapper(-72);
					isAdminMobile = true;
				} else {
					this.sizeReelWrapper(-110);
					isAdminMobile = false;
				}
			}
		};
		
		this.sizeReelWrapper = function(number) {
			if ( ! $this.modalNode) return false;
			var reelWrappers = $this.selectAll('.reel-wrapper');
			for(var index in reelWrappers) {
				if (reelWrappers.hasOwnProperty(index)) {
					reelWrappers[index].style.top = number * index + 'px';
				}
			}
		};
		
		this.getDocumentHeight = function() {
			var body = document.body,
				html = document.documentElement;
			return Math.max(body.scrollHeight, body.offsetHeight,
				html.clientHeight, html.scrollHeight, html.offsetHeight);
		};
		
		this.showAdditionalObjects = function() {
			barObject = new Bar($this, this.closeModal.bind(this));
			barObject.initCountdownBar();
			triggerObject = new Trigger($this);
			triggerObject.initTrigger();
		};
		
		this.setDevEnv = function() {
			if (this.hasDevScript()) {
				this.dev = {
					'$this': $this,
					'animationObject': animationObject,
					'giftObject': giftObject,
					'couponObject': couponObject,
					'slotObject': slotObject,
					'wheelObject': wheelObject,
					'barObject': barObject,
					'triggerObject': triggerObject,
					'svg': Svg
				};
			}
		};
		
		/**
		 * Hooks section
		 */
		
		this.dispatchUpdate = function(event) {
			this.resetVisibility();
			switch (true) {
				case event.target === 'screen':
					this.moveScreenTo(event.value); break;
				case event.target === 'svg' || event.target === 'wheelLogo':
					this.changeSVG(event); break;
				case event.hasOwnProperty('move'):
					this.shakeElementHook(event); break;
				case event.target === 'animation':
					this.changeAnimation(event); break;
				case event.target === 'opacity':
					this.changeOpacity(event.value); break;
				case this.isElementShow(event):
					this.showElement(event); break;
				case this.rebuildStylesCondition(event):
					this.rebuildStyles(event); break;
				default: this.rebuildModal(event); break;
			}
		};
		
		this.resetVisibility = function() {
			if (this.isModalHidden()) {
				this.displayModal();
				animationObject.displayContent();
			}
		};
		
		this.isModalHidden = function() {
			return $this.modalNode.style.display === 'none';
		};
		
		this.moveScreenTo = function(target) {
			var showMethodName = 'show' + ucFirst(target);
			if (['gift'].indexOf($this.gameType) > -1 && target !== 'bar') {
				this.otherScreenMove(showMethodName);
			} else {
				this.regularScreenMove(showMethodName);
			}
		};
		
		this.otherScreenMove = function(showMethodName) {
			if (getData(this.gameObject, showMethodName, false)) {
				this.gameObject[showMethodName].call(this.gameObject);
			}
		};
		
		this.regularScreenMove = function (showMethodName) {
			if (this.hasOwnProperty(showMethodName)) {
				this[showMethodName].call(this);
			}
		};
		
		this.changeSVG = function(event) {
			$this.svg = new Svg($this.select(getWheelSelector() + ' svg'));
			if (event.hasOwnProperty('color')) {
				this.changeSVGTextColor(event.color.hex);
			} else if (event.hasOwnProperty('coupon')) {
				if (event.coupon.hasOwnProperty('text')) {
					this.changeSliceText(event);
				} else if (event.coupon.hasOwnProperty('code')) {
					this.changeSliceCode(event);
				} else if (event.coupon.hasOwnProperty('bunch')) {
					this.changeSlicesText(event);
				}
			} else if (event.hasOwnProperty('schema')) {
				this.changeSliceScheme(event);
			} else if (event.target === 'wheelLogo') {
				this.changeSvgLogo(event);
			}
		};
		
		this.getSVGCoupons = function() {
			return $this.svg.get()
				? $this.svg.select('#coupons').children()
				: [];
		};
		
		this.changeSVGTextColor = function(color) {
			var coupons = this.getSVGCoupons();
			for (var coupon of coupons) {
				this.changeTextColor(coupon, color);
			}
		};
		
		this.changeTextColor = function(coupon, color) {
			var coupId = '#' + coupon.getAttr('id'),
				text = $this.svg.selectParent(coupId + 'curve1text'),
				text2 = $this.svg.selectParent(coupId + 'curve2text');
			text.setStyle('fill', color);
			text2.setStyle('fill', color);
		};
		
		this.changeSlicesText = function(event) {
			var index = 1;
			for (var coupon of event.coupon.bunch) {
				var lines = coupon.value.length < 13 ? [coupon.value, ''] : splitLines(coupon.value);
				$this.svg.select('#coupon' + index + 'curve1text').setText(lines[0]);
				$this.svg.select('#coupon' + index + 'curve2text').setText(lines[1]);
				index++;
			}
		};
		
		this.changeSliceText = function (event) {
			var coupons = this.getSVGCoupons();
			for (var index in coupons) {
				if (event.coupon.index === parseInt(index)) {
					var coupId = '#' + coupons[index].getAttr('id'),
						text = this.getCouponText(event.coupon.text);
					$this.svg.select(coupId + 'curve1text').setText(text[0]);
					$this.svg.select(coupId + 'curve2text').setText(text[1]);
				}
			}
		};
		
		this.getCouponText = function(text) {
			return text.length < 13 ? [text, ''] : splitLines(text);
		};
		
		this.changeSliceCode = function(event) {
			//@todo: make code edit
		};
		
		this.changeSliceScheme = function(event) {
			for (var schema of event.schema) {
				$this.svg.select('#' + schema.name).setAttr('fill', schema.hex);
			}
		};
		
		this.changeSvgLogo = function(event) {
			$this.svg.select('#gameLogo').setLink(event.file);
		};
		
		this.shakeElementHook = function(event) {
			var dataValue = this.getElementsDataText(event),
				elements = dataValue.map(function(el) {
					return this.getHookElement(event, el)
				}.bind(this));
			if (elements.length === 0) return false;
			elements.map(this.shakeHookedElement.bind(this));
		};
		
		this.getElementsDataText = function(event) {
			return this.isCouponButton(event)
				? [event.target + '.0', event.target + '.1']
				: [event.target];
		};
		
		this.isCouponButton = function(event) {
			return event.target === 'startScreen.button' && $this.gameType === 'coupon';
		};
		
		this.getHookElement = function(event, dataValue) {
			var dataAttr = event.target.indexOf('.') > -1 ? 'text' : 'name',
				selector = '[data-' + dataAttr + '="' + dataValue + '"]';
			return this.getElementToShake(event.target, selector);
		};
		
		this.shakeHookedElement = function(element) {
			if (!element) return false;
			var animationClass = ' animated ' + (hasClass(element, 'lucky-coupon-trigger') ? 'triggerShake' : 'shake');
			removeClass(element, animationClass);
			addClass(element, animationClass);
			setTimeout(function (element) {
				removeClass(element, animationClass);
			}.bind(this, element), 1000);
		};
		
		this.getElementToShake = function(target, selector) {
			return target === 'trigger'
				? window.self.document.querySelector('body > .lucky-coupon-trigger ' + selector).parentNode
				: $this.select(selector)
		};
		
		this.rebuildStylesCondition = function(event) {
			return event.hasOwnProperty('color')
			|| event.target === 'font'
			|| event.target === 'image'
			|| event.target === 'giftImage';
		};
		
		this.rebuildStyles = function(event) {
			var attribute = ['color', 'file', 'value'].find(function(element) {
					return event.hasOwnProperty(element);
				}),
				name = event.target,
				value = event[attribute];
			if (st.hasOwnProperty(name)) {
				st[name] = value;
				d.body.querySelector('style').innerHTML = st.style();
				setTimeout(function(){
					if (event.target === 'button') {
						this.initButton();
					}
				}.bind(this), 40);
			}
		};
		
		this.changeAnimation = function(event) {
			var content = $this.select($this.wrapperClass),
				direction = event.value.indexOf('Right') !== -1 ? 'right' : (event.value.indexOf('Left') ? 'left' : false),
				isSides = content.parentNode.className.indexOf('lucky-coupon-center') === -1;
			if (!(isSides && direction)) {
				direction = undefined;
			}
			this.changeOtherAnimations($this.modalWrapperClass, content, event, direction);
		};
		
		this.changeOpacity = function(value) {
			var customImage = $this.select('.custom-image');
			customImage.style.opacity = value;
		};
		
		this.isSwitchTrigger = function (event) {
			return event.target === 'trigger' && typeof event.value === 'boolean';
		};
		
		this.isElementShow = function(event) {
			return this.isSwitchTrigger(event)
				|| event.target === 'GPDR'
				|| event.target === 'email_field_visibility'
				|| event.target === 'facebookRecart'
				|| event.target === 'name_field_visibility'
				|| event.target === 'bar';
		};
		
		this.showElement = function(event) {
			switch (event.target) {
				case 'trigger':
					this.showTarget(event.value); break;
				case 'GPDR':
					this.showCheckbox(event); break;
				case 'email_field_visibility':
					this.showEmailField(event); break;
				case 'facebookRecart':
					this.showFakeFacebook(event); break;
					this.showEmailField(event);
					break;
				case 'name_field_visibility':
					this.showNameField(event);
					break;
				case 'bar':
					this.showBarCounter(event); break;
			}
		};
		
		this.showTarget = function(value) {
			if(!$this.triggerNode) return false;
			$this.triggerNode.style.zIndex = '2147483648';
			$this.triggerNode.style.display = value ? 'block' : 'none';
		};
		
		this.changeSidesAnimations = function(className, content, event, direction) {
			content.style.display = 'none';
			if (event.value !== 'None') {
				setTimeout(function() {
					removeClass(content, ' animate-in');
					var animateOut = ' animate-' + direction + '-out';
					addClass(content, animateOut);
					content.style.display = 'block';
					setTimeout(function() {
						removeClass(content, animateOut);
						addClass(content, ' animate-in');
					}.bind(this), 300);
				}.bind(this), 100);
			}
		};
		
		this.changeOtherAnimations = function(className, content, event) {
			if (event.value !== 'None') {
				className += ' animated ' + event.value;
			}
			content.className = className;
		};
		
		this.rebuildModal = function(event) {
			this.destroyObject();
			this.changeModalPosition(event);
			this.changeLanguage(event);
			this.changeModalContent(event);
			this.initObject();
			this.changeViewVariables(event);
		};
		
		this.destroyObject = function() {
			if (typeof $this.modalNode.tagName !== 'undefined') {
				this.destroy();
			}
		};
		
		this.showCheckbox = function(event) {
			if (event.target !== 'GPDR') return false;
			mb.makeGPDRCompliance = event.value;
			$this.checkBehavior = event.value;
			var element = $this.select('.checkbox-wrapper'),
				note = $this.select('div[data-name=note]');
			event.value ? show(element) : hide(element);
			event.value ? hide(note) : show(note);
		};
		
		this.showEmailField = function(event) {
			var divContainer = $this.select('.lucky-coupon-' + st.position);
			if (event.value === 'show') {
				visible($this.select('.lucky-coupon-popup-inner .small-text'));
				visible($this.select('.lucky-coupon-popup-inner .input-wrapper'));
				removeClass(divContainer, 'no-emails');
				removeClass(divContainer, '  ');
			} else {
				invisible($this.select('.lucky-coupon-popup-inner .small-text'));
				invisible($this.select('.lucky-coupon-popup-inner .input-wrapper'));
				if (!hasClass(divContainer, 'no-emails')) {
					addClass(divContainer, ' no-emails');
				}
			}
		};
		
		this.showFakeFacebook = function(event) {
			var facebook = $this.select('.fake-fecebook-form'),
				emailInput = $this.select('.email-input');
			if (event.value) {
				show(facebook);
				show(facebook.parentNode);
				hide(emailInput);
			} else {
				hide(facebook);
				hide(facebook.parentNode);
				show(emailInput, true);
			}
		};
		
		this.showNameField = function(event) {
			var emailInput = $this.select('.input-wrapper .email-input'),
				nameInput = $this.select('.input-wrapper .username-input');
			if (event.value === 'show') {
				nameInput.style.display = 'inline-block';
				addClass(emailInput, ' name-case');
			} else {
				hide(nameInput);
				removeClass(emailInput, ' name-case');
			}
		};
		
		this.showBarCounter = function(event) {
			var barNode = $this.select('.lucky-coupon-bar');
			$this.select('.lucky-coupon-bar #copy-coupon').value = '{COUPON_CODE}';
			getData(event, 'value', false) ? show(barNode) : hide(barNode);
		};
		
		this.changeModalPosition = function(event) {
			if (event.target === 'position') {
				st.position = event.value;
			}
		};
		
		this.changeLanguage = function(event) {
			if (event.target === 'translation') {
				$this.lang = event.language;
				mw.meta.language = event.language;
			}
		};
		
		this.changeModalContent = function(event) {
			mc.content = this.getModalContent(mc.content, event);
		};
		
		this.changeViewVariables = function(event) {
			//waiting for initialize variables and objects then recalculate data regarding device
			if (event.target === 'device') {
				var isDesktop = event.value === 'desktop';
				setTimeout(function () {
					var gameObject = this.getGameObject($this.gameType);
					if (gameObject.hasOwnProperty('initDeviceVariables')) {
						gameObject.initDeviceVariables(isDesktop);
					}
					var node = $this.select('.game-editor');
					if (!isDesktop) {
						addClass(node, ' mobile scale');
					} else {
						removeClass(node, ' mobile scale');
					}
				}.bind(this), 5);
			}
		};
		
		this.destroy = function() {
			d.body.innerHTML = '';
			$this.modalNode = {};
		};
		
		this.getModalContent = function(elements, event) {
			return this.iterateElements(elements, this.getContent.bind(this), event);
		};
		
		this.getContent = function(elements, index, event) {
			var element = elements[index],
				meta = element['meta'];
			if (meta === 'coupons' && event.target === 'coupon') {
				if ($this.gameType === 'coupon') {
					element.content = this.iterateElements(element.content, couponObject.getCoupon.bind(couponObject), event);
				} else if ($this.gameType === 'slot') {
					var newReel = this.iterateElements(element.content[1].content[1].content, slotObject.getCoupon.bind(slotObject), event);
					element.content[2].content[1].content = newReel;
					element.content[3].content[1].content = newReel;
				}
			} else if (this.isTextChangeEvent(element, event)) {
				element = this.changeElementText(event, meta, element);
			} else if (this.isRecursive(element)) {
				element.content = this.getModalContent(elements[index].content, event);
			}
			return element;
		};
		
		this.isTextChangeEvent = function(element, event) {
			return getData(element, 'data.text', '') === event.target
				&& typeof event.value !== 'boolean';
		};
		
		this.isRecursive = function(element) {
			return element.content && element.tagName !== 'meta' && typeof element.content !== 'string';
		};
		
		this.initObject = function() {
			if (typeof $this.modalNode.tagName === 'undefined') {
				this.init();
			}
		};
		
		this.changeElementText = function(event, meta, element) {
			var attrName = ['email', 'userName'].indexOf(meta) > -1 ? 'placeholder' : 'textNode';
			element[attrName] = event.value;
			return element;
		};
		
		this.iterateElements = function(elements, handler, params) {
			var result = {};
			for (var index in elements) {
				if (elements.hasOwnProperty(index)) {
					result[index] = handler(elements, index, params, this);
				}
			}
			return result;
		};
		
		/**
		 * @todo: refactor method using different game objects
		 *  Events section
		 */
		
		this.initEvents = function() {
			var inputEmail = $this.select('.email-input'),
				startGame = $this.select('.btn-push.red'),
				continueBtn = $this.select('.continue-btn'),
				coupons = $this.select('.prize-list'),
				slot = $this.select('.game-area'),
				chatchamp = $this.select('.chatchamp-messenger-checkbox'),
				woohooSelector = '#woohoo-trigger-' + $this.gameType,
				userLaunchItems = d.querySelectorAll(woohooSelector),
				woohooSpecific = woohooSelector + '.woohoo-' + $this.gameInfo.gameId,
				userLaunchSpecific = d.querySelector(woohooSpecific),
				triggerClose = d.querySelector('body > .lucky-coupon-trigger .close');
			if (startGame && startGame.tagName) {
				startGame.addEventListener('click', this.submit.bind(this));
			}
			if (inputEmail) {
				inputEmail.addEventListener('keypress', this.submitOnEnter.bind(this));
			}
			if (chatchamp) {
				var chatchampId = chatchamp.getAttribute('chatchamp_app_id');
				if (chatchampId) {
					this.appendChatchampScript(document, 'script', 'chatchamp-jssdk', chatchampId);
				}
			}
			$this.modalNode.addEventListener('click', this.closeModal.bind(this));
			document.addEventListener('click', function(e){
				if (isAdmin || isFrame) return false;
				var linkCart = findParentElement(e.target, function(target) {
					return target.getAttribute('href') === '/cart';
				}.bind(this));
				if (linkCart) {
					setTimeout(this.addDiscountToCartPage.bind(this), 1000);
				}
			}.bind(this));
			if (continueBtn && continueBtn.tagName && !$this.isGameEmbedded) {
				continueBtn.addEventListener('click', barObject.continueUseDiscount.bind(barObject, this));
			}
			w.onresize = this.resizeWrapper.bind(this);
			if (coupons && coupons.tagName) {
				coupons.addEventListener('click', this.submitOnCoupon.bind(this));
			}
			if (slot && slot.tagName) {
				slot.addEventListener('click', this.submitOnSlot.bind(this));
			}
			this.addLaunchListeners(userLaunchItems);
			if (userLaunchSpecific && userLaunchSpecific.tagName) {
				userLaunchSpecific.addEventListener('click', this.userLaunchGame.bind(this));
			}
			if ($this.gameType === 'gift') {
				giftObject.bindEvents(function(e) {
					var inputWrapper = $this.select('.input-wrapper');
					removeClass(inputWrapper, ' animated shake');
					setTimeout(function(inputWrapper) {
						this.handleErrorCase(inputWrapper, {startButton: 'Use start button to launch the game.'});
					}.bind(this, inputWrapper), 10);
				}.bind(this), function(e) {
					if (!$this.emailCollect) {
						var wrapper = $this.select('.input-wrapper'),
							emailInput = wrapper.querySelector('.email-input'),
							usernameInput = wrapper.querySelector('.username-input');
						this.sendGamePlayedStatistic(emailInput.value, usernameInput.value, function (data) {
							var response = JSON.parse(data.response);
							if (typeof response.subscriber_email === 'object') {
								this.handleErrorCase(emailInput, response, wrapper);
							}
						}.bind(this));
					}
					giftObject.launchGame.call(giftObject, this.getItems('gift'), e);
				}.bind(this));
			}
			if ($this.triggerNode && $this.triggerNode.className && $this.showTrigger) {
				if (isAdmin && isDetailFrame()) {
					$this.triggerNode.addEventListener('click', $this.previewTrigger.executor);
				} else {
					$this.triggerNode.addEventListener('click', this.triggerShowModal.bind(this));
				}
			}
			if (triggerClose) {
				triggerClose.addEventListener('click', this.triggerClose.bind(this));
			}
			// this.addDiscountToCartPage();
		};
		
		this.addDiscountToCartPage = function() {
			if (isAdmin || isFrame) return false;
			var cartForm = document.querySelector('form[action="/cart"]');
			if (getGameCode($this.gameInfo) === '' || !cartForm) return false;
			var element = {
				'tagName': 'input',
				'type': 'hidden',
				'name': 'discount',
				'value': getGameCode($this.gameInfo)
			},
			isDiscountAdded = cartForm ? cartForm.querySelector('input[name=discount]') : {};
			!isDiscountAdded ? cartForm.appendChild(this.addNode(element)) : '';
		};
		
		this.triggerShowModal = function(e) {
			if (e.target.className === 'inner') return false;
			this.showGame(st.animation, $this.gameInfo);
		};
		
		this.triggerClose = function(e) {
			setModalTriggerClosedFlag($this.gameInfo, $this.cookieExpirationDays);
			hide(e.target.closest('.lucky-coupon-trigger'));
		};
		
		this.addLaunchListeners = function(userLaunchItems) {
			for(var index in userLaunchItems) {
				var userLaunch = userLaunchItems[index];
				if (this.isSetupable(userLaunch)) {
					userLaunch.dataset.click = 'bind';
					userLaunch.addEventListener('click', this.userLaunchGame.bind(this));
				}
			}
		};
		
		this.isSetupable = function (userLaunch) {
			return userLaunch
				&& userLaunch.tagName
				&& !hasClass(userLaunch, 'woohoo-')
				&& typeof userLaunch.dataset.click === 'undefined';
		};
		
		this.appendChatchampScript = function(d, s, id, chatchampId) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = 'https://api.chatchamp.io/js/chatchamp-loader.js?id=' + chatchampId;
			js.async = true; fjs.parentNode.insertBefore(js, fjs);
		};
		
		this.initButton = function() {
			var styleChecker = setInterval(function() {
				var button = $this.select('.btn-push.red');
				if (!button) return false;
				var rgb = getStyle(button, 'background-color');
				if (rgb.indexOf('rgba(') > -1) return false;
				clearInterval(styleChecker);
				if ($this.gameType !== 'wheel') this.colorButtonBoxShadow(rgb, button);
				this.colorButtonBoxShadow(rgb, $this.select('.continue-btn'));
			}.bind(this), 40);
		};
		
		this.colorButtonBoxShadow = function (rgb, button) {
			var darkerColor = colorLuminance(rgb, this.getPercent()),
				boxShadowValue = this.getBoxShadowValue(darkerColor);
			button.style.borderColor = darkerColor;
			button.style.webkitAppearance = 'none';
			this.setBoxShadowStyles(button, boxShadowValue);
		};
		
		this.getPercent = function() {
			switch ($this.gameType) {
				case 'gift': return -0.4;
				default: return -0.2;
			}
		};
		
		this.setBoxShadowStyles = function(button, boxShadowValue) {
			var types = ['oBoxShadow', 'mozBoxShadow', 'msBoxShadow', 'webkitBoxShadow', 'boxShadow'];
			for(var i in types) {
				button.style[types[i]] = boxShadowValue;
			}
		};
		
		this.getBoxShadowValue = function(color) {
			return 'inset 0px ' + this.getPixelSize() + 'px 0px ' + color;
		};
		
		this.getPixelSize = function() {
			switch ($this.gameType) {
				case 'wheel':
				case 'gift': return '-2';
				default: return '-5';
			}
		};
		
		this.showLastPageFromFirst = function() {
			var startText = $this.select('.start-text');
			addClass(startText, ' hide');
			this.drawFinishScene();
		};
		
		this.resizeWrapper = function() {
			if ($this.modalNode.style) {
				if (getDocumentWidth() < 600) {
					$this.modalNode.style.height = '100vh';
				} else {
					$this.modalNode.style.height = this.getDocumentHeight() + 'px';
				}
			}
			this.sizeSlotGame();
		};
		
		this.isDevEnv = function(id) {
			return this.hasDevScript() && (!!id && $this.modalSelector.indexOf(id) !== -1);
		};
		
		this.hasDevScript = function() {
			return hasDevScript($this.hostName);
		};
		
		this.closeModal = function (e) {
			var className = e.target instanceof SVGElement ? '' : e.target.parentNode.className;
			this.setBarData(className);
			if (this.getCloseCondition(className) || this.isDevEnv(e.target.id)) {
				this.closeAnimatedModal(!!isAdmin);
			} else if (this.getRefreshCondition(className)) {
				this.refreshFrame();
			}
		};
		
		this.setBarData = function(className) {
			if (!$this.isUserOpened) {
				$this.isUserOpened = false;
				if (className && className.indexOf('lucky-coupon-bar') > -1) {
					setBarShowed(1, $this.gameInfo, $this.cookieExpirationDays);
					$this.showBar = false;
				}
			}
		};
		
		this.getCloseCondition = function (className) {
			return !isFrame && ((className).search($this.closeClass)) >= 0;
		};
		
		this.getRefreshCondition = function (className) {
			return isFrame && ((className).search($this.closeClass)) >= 0;
		};
		
		this.closeAnimatedModal = function(destroy) {
			addClass($this.modalNode, ' animated fadeOut');
			setTimeout(this.handleClose.bind(this, destroy), 280);
		};
		
		this.handleClose = function(destroy) {
			this.hideGame();
			this.setDefaultState();
			destroy ? this.destroyingModal() : this.resetGame();
		};
		
		this.hideGame = function() {
			$this.modalNode.style.display = 'none';
			$this.select('.lucky-coupon-' + st.position).style.display = 'none';
		};
		
		this.setDefaultState = function() {
			removeClass($this.modalNode, ' animated fadeOut');
			d.body.style.overflowX = '';
			if (this.getDocumentHeight() > 664 || isFrame) {
				d.body.style.overflowY = 'inherit';
			}
		};
		
		this.destroyingModal = function() {
			if (!isFrame && isAdmin) {
				this.destroyModal();
			} else if (!isFrame && !isAdmin) {
				this.removeModal();
			}
		};
		
		this.refreshFrame = function() {
			this.destroy();
			this.init();
		};
		
		this.destroyModal = function() {
			this.removeScript();
			this.removeModal();
		};
		
		this.removeScript = function() {
			if (d.body.querySelector('script[src="' + scriptPath + '"]')) {
				d.body.querySelector('script[src="' + scriptPath + '"]').remove();
			}
		};
		
		this.resetGame = function () {
			this.removeModal();
			this.init();
		};
		
		this.removeModal = function() {
			if (d.body.querySelector($this.modalSelector).id === mw.id) {
				d.body.querySelector($this.modalSelector).remove();
				$this.modalNode = false;
			}
		};
		
		this.submit = function(e) {
			e.preventDefault();
			if (!$this.gameButtonPushed) {
				$this.gameButtonPushed = true;
				var inputWrapper = $this.select('.input-wrapper');
				this.send(inputWrapper);
			}
		};
		
		this.submitOnEnter = function(e) {
			if (e.keyCode === 13) {
				e.preventDefault();
				$this.gameButtonPushed = true;
				this.send(e.target.parentNode);
			}
		};
		
		this.submitOnCoupon = function(e) {
			couponObject.submitOnCoupon(e, this.send.bind(this));
		};
		
		this.submitOnSlot = function(e) {
			slotObject.submitOnSlot(e, this.send.bind(this));
		};
		
		this.submitOnGift = function(e) {
			giftObject.submitOnGift(e, this.send.bind(this));
		};
		
		this.send = function(wrapper) {
			if ($this.gameButtonPushed) {
				if (!isAdmin && !isFrame) {
					this.sendReal(wrapper);
				} else {
					this.startGame({response: '{ "result": 1 }'}, wrapper);
				}
			}
		};
		
		/**
		 * @param data
		 * @param wrapper
		 */
		this.startGame = function(data, wrapper) {
			var input = wrapper.querySelector('input'),
				jsonData = JSON.parse(data.response);
			removeClass(input, ' animated shake');
			if (jsonData.result !== 1) {
				this.handleErrorCase(input, jsonData);
			} else {
				this.handleSuccessCase(wrapper);
			}
		};
		
		this.handleErrorCase = function(input, jsonData, wrapper) {
			addClass(input, ' animated shake');
			wrapper && (wrapper.querySelector('span').style.display = 'inline');
			this.showErrors(jsonData, input);
			$this.gameButtonPushed = false;
		};
		
		this.showErrors = function (jsonData, input) {
			var errorElement = input.parentNode.querySelector('.' + $this.errorTextClass);
			for (var fieldName in jsonData) {
				if (jsonData.hasOwnProperty(fieldName)) {
					if (jsonData[fieldName].hasOwnProperty(0)) {
						errorElement.parentNode.style.display = 'inline';
						errorElement.innerHTML = jsonData[fieldName][0];
					}
				}
			}
		};
		
		this.handleSuccessCase = function (wrapper) {
			var gameType = $this.gameType,
				gameObject = this.getGameObject(gameType);
			this.prepareStart(wrapper, gameObject);
			gameObject.launchGame.call(gameObject, this.getItems(gameType));
		};
		
		this.getGameObject = function(gameType) {
			var game;
			switch (gameType) {
				case 'coupon': game = couponObject; break;
				case 'slot': game = slotObject; break;
				case 'gift': game = giftObject; break;
				case 'wheel': game = wheelObject; break;
			}
			return game;
		};
		
		this.getItems = function(gameType) {
			var result;
			switch (gameType) {
				case 'coupon': result = $this.select('ul'); break;
				case 'slot': result = $this.selectAll('.reel-wrapper'); break;
				case 'gift': result = $this.selectAll('.gift'); break;
				case 'wheel': result = $this.selectAll('.slice'); break;
			}
			return result;
		};
		
		this.prepareStart = function(wrapper, gameObject) {
			if($this.gameType !== 'wheel') this.handleButton();
			if (gameObject.hasOwnProperty('animateHide'))
				gameObject.animateHide(wrapper);
		};
		
		this.handleButton = function() {
			var button = $this.select('.btn-push');
			if (!button) return false;
			removeClass(button, ' red');
			addClass(button, ' grey');
		};
		
		this.validate = function(emailInput, usernameInput) {
			var checkInput = $this.select('.checkbox-container input');
			return (
					! $this.emailCollect
					|| ($this.isRecartEnabled && $this.isRecartChecked)
					|| (
						validateEmail(emailInput.value)
						&& (
							$this.nameCollect
								? (usernameInput.value.length > 2 && usernameInput.value.length < 50)
								: true
						)
					)
				)
				&& this.subscribeCheck(checkInput);
		};
		
		this.sendReal = function(wrapper) {
			var emailInput = wrapper.querySelector('.email-input'),
				usernameInput = wrapper.querySelector('.username-input');
			this.clearErrors(wrapper);
			if (this.validate(emailInput, usernameInput)) {
				this.hideCheckbox();
				this.sendGamePlayedStatistic(emailInput.value, usernameInput.value, function (data) {
					var response = JSON.parse(data.response);
					if (typeof response.subscriber_email === 'object') {
						this.handleErrorCase(emailInput, response, wrapper);
					} else {
						this.startGame(data, wrapper);
					}
				}.bind(this));
			} else {
				this.setErrors(wrapper);
				$this.gameButtonPushed = false;
			}
		};
		
		this.clearErrors = function(wrapper) {
			var errorElement = wrapper.querySelector('.' + $this.errorTextClass),
				errorWrapper = errorElement.parentNode,
				spanError = wrapper.querySelector('span'),
				emailInput = wrapper.querySelector('.email-input'),
				nameInput = wrapper.querySelector('.username-input'),
				checkInputWrapper = $this.select('.checkbox-wrapper');
			spanError.style.display = 'none';
			errorWrapper.style.display = 'none';
			errorElement.innerHTML = '';
			removeClass(nameInput, ' animated shake');
			removeClass(emailInput, ' animated shake');
			removeClass(checkInputWrapper, ' animated shake');
			removeClass($this.select('.checkbox-container span'), ' error');
		};
		
		this.hideCheckbox = function() {
			if($this.gameType !== 'gift') {
				var checkInputWrapper = $this.select('.checkbox-wrapper');
				hide(checkInputWrapper);
			}
		};
		
		this.setErrors = function(wrapper) {
			var emailInput = wrapper.querySelector('.email-input'),
				usernameInput = wrapper.querySelector('.username-input'),
				checkInput = $this.select('.checkbox-container input'),
				checkInputWrapper = $this.select('.checkbox-container').parentNode,
				isChecked = checkInput && checkInput.checked;
			if (!validateEmail(emailInput.value)) {
				this.shakeElement(emailInput);
			}
			if (usernameInput.value.length < 3 || usernameInput.value.length > 50) {
				this.shakeElement(usernameInput);
			}
			if ($this.checkBehavior === '1' && !isChecked) {
				this.shakeElement(checkInputWrapper);
				addClass($this.select('.checkbox-container span'), ' error');
			}
		};
		
		this.shakeElement = function(element) {
			setTimeout(function() {
				addClass(element, ' animated shake');
			}.bind(this), 1);
		};
		
		this.subscribeCheck = function(checkInput) {
			return $this.checkBehavior === '1' ? checkInput.checked : true;
		};
		
		/**
		 * @param coupon
		 */
		this.finishGame = function(coupon) {
			if (this.isModalOnPage()) {
				var couponData = coupon.dataset;
				if (isFrame) {
					this.setDataInFrame(couponData);
				} else {
					this.getCouponCode(couponData);
				}
				this.setResult(this.drawFinishScene(), couponData);
				setCookie(gamePlayedName($this.gameInfo), 1, $this.cookieExpirationDays);
			}
		};
		
		this.setDataInFrame = function(couponData) {
			var data = {}, code = '';
			try {
				code = window.top['frameCodes'][$this.siteId][$this.gameId][couponData.id];
			} catch(error) {console.log(error);}
			data.response = JSON.stringify({code: code});
			this.setCouponCode(data);
		};
		
		this.drawFinishScene = function() {
			var selector = '.finish-body';
			if (['gift'].indexOf($this.gameType) > -1) {
				selector = '.background';
			} else {
				this.setLastScreenClass();
			}
			return $this.select(selector);
		};
		
		this.showStartScreen = function() {
			this.hideAllScreens();
			this.showStartScreenTextBody();
		};
		
		this.showPlayScreen = function() {
			this.hideAllScreens();
			this.showPlayScreenTextBody();
		};
		
		this.showWinScreen = function() {
			this.hideAllScreens();
			this.showWinScreenTextBody();
		};
		
		this.showBar = function() {
			show($this.select('.lucky-coupon-bar'));
			$this.select('.lucky-coupon-bar #copy-coupon').value = '{COUPON_CODE}';
		};
		
		this.hideAllScreens = function () {
			hide($this.select('.lucky-coupon-bar'));
			this.hideStartScreenTextBody();
			this.hidePlayScreenTextBody();
			this.hideWinScreenTextBody();
		};
		
		this.setLastScreenClass = function () {
			this.hidePlayScreenTextBody();
			this.showWinScreenTextBody();
		};
		
		this.hideStartScreenTextBody = function() {
			addClass($this.select('.start-body'), ' hide');
			addClass($this.select('.start-text'), ' hide');
		};
		
		this.showStartScreenTextBody = function() {
			removeClass($this.select('.start-body'), ' hide');
			removeClass($this.select('.start-text'), ' hide');
		};
		
		this.showPlayScreenTextBody = function() {
			removeClass($this.select('.start-body'), ' hide');
			removeClass($this.select('.middle'), ' hide');
		};
		
		this.hidePlayScreenTextBody = function() {
			addClass($this.select('.start-body'), ' hide');
			addClass($this.select('.middle'), ' hide');
		};
		
		this.showWinScreenTextBody = function() {
			removeClass($this.select('.finish-text'), ' hide');
			removeClass($this.select('.finish-body'), ' hide');
		};
		
		this.hideWinScreenTextBody = function() {
			addClass($this.select('.finish-text'), ' hide');
			addClass($this.select('.finish-body'), ' hide');
		};
		
		this.setResult = function(finishBody, couponData) {
			var upperText, lowerText;
			if (couponData.type.indexOf('free ') !== -1) {
				var typeArray = couponData.type.split(' '),
					translationKey = 'coupons.free' + ucFirst(typeArray[1]),
					free = $this.translation(translationKey).split(' '),
					path = 'startScreen.free' + ucFirst(typeArray[1]);
				upperText = getData($this.textObject, path + '.' + typeArray[0], free[0]);
				lowerText = getData($this.textObject, path + '.' + typeArray[1], free[1]);
			} else {
				var couponType = $this.getNewText('startScreen.' + couponData.type, 'coupons.' + couponData.type);
				upperText = this.getCouponValue(couponData);
				lowerText = $this.gameType !== 'wheel' ? (couponType ? couponType : '&nbsp;') : '&nbsp;';
			}
			finishBody.querySelector('.upper-text div:first-child').innerHTML = upperText;
			finishBody.querySelector('.upper-text div+div').innerHTML = lowerText;
			if ($this.gameType !== 'gift') {
				launchConfetti(400);
			}
		};
		
		this.getCouponValue = function (couponData) {
			switch(couponData.type) {
				case 'cash': return '$' + parseFloat(couponData.value);
				case 'discount': return parseFloat(couponData.value) + '%';
				case 'free shipping': return $this.getNewText('startScreen.freeShipping.free', 'coupons.freeShipping');
				case 'free product': return $this.getNewText('startScreen.freeProduct.free', 'coupons.freeProduct');
				default: return decodeURI(couponData.value);
			}
		};
		
		this.isModalOnPage = function() {
			return d.querySelector($this.modalNode.tagName + '#' + $this.modalNode.id);
		};
		
		this.getCouponCode = function(coupon) {
			isAdmin
				? this.setCouponCode({response: '{"code": "TEST_CODE"}'})
				: this.getCode({
					id: parseInt(coupon.id),
					game_id: parseInt(coupon.game_id),
					email: $this.sendedEmail
				}, this.setCouponCode.bind(this));
		};
		
		this.setCouponCode = function (data) {
			var response = JSON.parse(data.response),
				code = response.code, result;
			if (code) {
				var lowerText = $this.select('.lower-text div+div'),
					barText = $this.barNode.querySelector('.coupon-code-code input');
				if (typeof code === 'string') {
					result = code;
				} else {
					lowerText.style.margin = '30px 0';
					lowerText.style.fontSize = '21px';
					$this.showBar = false;
					result = code.text;
				}
				setGameCode(result, $this.gameInfo);
				// this.addDiscountToCartPage();
				barText.value = result;
				lowerText.innerHTML = result;
			}
		};
		
		/**
		 * Behavior section
		 */
		
		this.initBehavior = function() {
			var timer;
			timer = setInterval(function() {
				if (this.getBackground($this.select('.custom-image')).indexOf($this.hostName) > -1) {
					this.backgroundLoader();
					clearInterval(timer);
				}
			}.bind(this), 10);
			var effect = st.animation;
			// @todo: refactor into switch condition and one line case
			if (isFrame) {
				this.displayModal();
				animationObject.displayContent();
				animationObject.showWithoutAnimation();
			} else if (isAdmin) {
				this.showModal(effect);
			} else {
				this.setFrequencyBehavior(effect);
				this.setStopDisplayBehavior();
				this.setShowBehavior(effect);
			}
			this.elementsVisibility();
		};
		
		this.elementsVisibility = function() {
			this.emailCollect();
			this.poweredByVisibility();
			this.facebookFieldVisibility();
		};
		
		this.poweredByVisibility = function () {
			if (!$this.isPoweredByVisible) {
				invisible($this.select('.powered-by'));
			}
		};
		
		this.emailCollect = function() {
			if (!$this.emailCollect) {
				invisible($this.select('.lucky-coupon-popup-inner .small-text'));
				invisible($this.select('.lucky-coupon-popup-inner .input-wrapper'));
			}
		};
		
		this.facebookFieldVisibility = function() {
			if (!$this.isRecartEnabled) return false;
			this.fakeFacebookForm();
			window.onRecartMessengerPluginLoaded = this.recartLoadEvents.bind(this);
		};
		
		this.fakeFacebookForm = function() {
			if (!isFrame && !isAdmin) return false;
			hide($this.select('.email-input'));
			show($this.select('.recart-messenger-widget'));
			show($this.select('.fake-fecebook-form'));
		};
		
		this.recartLoadEvents = function() {
			_rmp.initWidget(['.game-area', '.btn-push.red', '.prize-list']);
			_rmp.on('state_change', this.recartStateChange.bind(this));
			_rmp.on('subscribe_clicked', this.recartActions.bind(this));
		};
		
		this.recartStateChange = function(e) {
			if (this.isWoohooNotAvailable(e)) return false;
			if (e.state === 'STATE_INITIAL') {
				if ($this.gameInfo.type === 'gift') {
					addClass($this.select('.lucky-coupon-popup'), ' lucky-coupon-messenger');
				}
				if ($this.emailCollect) {
					show($this.select('.recart-messenger-widget'));
				}
			} else if (['STATE_NOT_LOADED', 'STATE_ERROR'].includes(e.state)) {
				hide($this.select('.recart-messenger-widget'));
				hide($this.select('.email-input'));
			}
		};
		
		this.recartActions = function(e) {
			if (this.isWoohooNotAvailable(e)) return false;
			var wrapperClass = '.input-wrapper';
			if (e.widget.isCheckboxChecked === true) {
				$this.isRecartChecked = true;
			} else {
				animationObject.animateBySelector('shake', wrapperClass);
			}
		};
		
		this.isWoohooNotAvailable = function(e) {
			return e.widget && e.widget.type !== 'woohoo';
		};
		
		this.setFrequencyBehavior = function() {
			var conditions = mb.frequency,
				conditionName = this.getSelectedCondition(conditions, '');
			if (this.isEveryPageGame(conditionName, conditions)) {
				this.setModalNotShowedFlag();
			} else if (this.notMoreThanOnceEveryTime(conditionName, conditions)) {
				this.setModalShowedFlag();
			}
		};
		
		this.isEveryPageGame = function (conditionName, conditions) {
			return ((conditionName === 'onEveryPageView'
				&& conditions[conditionName] === '1')
				|| typeof conditionName === 'undefined')
				&& isInitExecuted === 0;
		};
		
		this.notMoreThanOnceEveryTime = function (conditionName, conditions) {
			return (conditionName === 'notMoreThanOnceEveryNumberTimePerUser'
			&& this.isChecked(conditions[conditionName])
			&& !this.isAvailableToShow(this.getTimestamp($this.gameInfo), conditions[conditionName]));
		};
		
		this.isAvailableToShow = function(timestamp, timeObject) {
			var now = Math.round(new Date().getTime() / 1000),
				selectedPeriod = this.getSelectedPeriod(timeObject.time);
			return (now - timestamp) > (selectedPeriod * parseInt(timeObject.number));
		};
		
		this.getSelectedPeriod = function (timeObject) {
			var seconds = this.getSeconds();
			for (var prop in timeObject) {
				if (timeObject[prop] === '1') {
					return seconds[prop];
				}
			}
		};
		
		this.getSeconds = function () {
			var seconds = {day: 24 * 60 * 60};
			seconds.week = seconds.day * 7;
			seconds.month = seconds.week * 4;
			return seconds;
		};
		
		this.setStopDisplayBehavior = function() {
			var conditions = mb.stopToDisplayTheGame,
				conditionName = this.getSelectedCondition(conditions, '');
			if (this.stopDisplayBySpecialConditions(conditionName, conditions)) {
				this.afterShowingItNumberTimesToTheUser(conditions.underTheFollowingConditions.afterShowingItNumberTimesToTheUser);
				this.userPerformTheAction(conditions.underTheFollowingConditions.afterTheUserPerformsTheAction);
			}
		};
		
		this.afterShowingItNumberTimesToTheUser = function(conditions) {
			if (this.isChecked(conditions) && !(this.getNumber(conditions.number) > this.getShowCounter($this.gameInfo))) {
				this.setModalShowedFlag();
			}
		};
		
		this.userPerformTheAction = function(condition) {
			if (condition === '1' && this.getGamePlayedState() === 1) {
				this.setModalShowedFlag();
			}
		};
		
		this.getGamePlayedState = function () {
			return this.getNumber(getCookie(gamePlayedName($this.gameInfo)), 0);
		};
		
		this.stopDisplayBySpecialConditions = function (conditionName, conditions) {
			return !this.isCheckedNever(conditionName, conditions)
				&& conditionName === 'underTheFollowingConditions';
		};
		
		this.isCheckedNever = function (conditionName, conditions) {
			return (conditionName === 'Never' && conditions[conditionName] === '1')
				|| typeof conditionName === 'undefined';
		};
		
		this.showModal = function(effect) {
			this.reDrawModal();
			if (!animationObject.isShownModal()) {
				if (!isAdmin && this.getShowCounter($this.gameInfo) < 2) {
					this.sendShowStatistic();
				}
				this.animateShow(effect);
			}
		};
		
		this.animateShow = function(effect) {
			this.displayModal();
			setTimeout(this.animateModal.bind(this, effect), 100);
		};
		
		this.displayModal = function() {
			$this.modalNode.style.display = 'block';
			d.body.style.overflowX = 'hidden';
			if ($this.gameType !== 'wheel') {
				var inner = $this.selectAll('.lucky-coupon-popup-inner.coupon-wrapper');
				for (var index in inner) {
					if (inner.hasOwnProperty(index) && index !== 'length') {
						inner[index].style.height = 'auto';
					}
				}
			}
			if (this.getDocumentHeight() > 664 || isFrame) {
				d.body.style.overflowY = 'auto';
			}
		};
		
		this.reDrawModal = function() {
			if (this.isModalInitialized()) {
				this.init();
			}
		};
		
		this.isModalInitialized = function () {
			return !d.querySelector($this.modalSelector);
		};
		
		this.animateModal = function (effect) {
			if (isMobile()) {
				animationObject.showCssSlideBottom($this.positionClass);
			} else {
				if (effect === 'fadeIn' && $this.select('.lucky-coupon-center')) {
					animationObject.showCssFadeIn('.lucky-coupon-center');
				} else {
					animationObject.animateBySelector(effect, $this.wrapperClass);
				}
			}
			if ($this.gameType === 'gift') {
				giftObject.animateOpen();
			}
		};
		
		this.setShowBehavior = function(effect) {
			var showConditions = mb.startDisplayTheGame,
				showConditionName = this.getSelectedCondition(showConditions, '');
			if (this.showAtOnce(showConditions, showConditionName)) {
				this.showGame(effect, $this.gameInfo);
			} else if (this.showBySpecialCondition(showConditionName)) {
				var showCondition = showConditions[showConditionName];
				this.specialConditions(showCondition, effect, $this.gameInfo);
			} else if ($this.isGameEmbedded) {
				this.insertGameInPage();
			}
		};
		
		this.showAtOnce = function(showConditions, conditionName) {
			return (( conditionName === 'atOnce'
				&& showConditions[conditionName] === '1' )
				|| typeof conditionName === 'undefined')
				&& this.getModalShowedFlag() === 0
		};
		
		this.showBySpecialCondition = function(conditionName) {
			return conditionName === 'underTheFollowingConditions';
		};
		
		this.specialConditions = function (showCondition, effect, gameInfo) {
			this.reachesPercent(showCondition.whenTheUserReachesPercentOfThePage, effect, gameInfo);
			this.afterSomeTimePass(showCondition.afterSeconds, effect, gameInfo);
			this.whenLeaveWebsite(showCondition.whenTheUserIsLivingTheWebsite, effect, gameInfo);
		};
		
		this.reachesPercent = function(showConditions, effect, gameInfo) {
			if (this.gameShowCondition(showConditions)) {
				var percent = this.getNumber(showConditions.percent, 100);
				window.onscroll = this.showWhenReaches.bind(this, percent, effect, gameInfo);
			}
		};
		
		this.afterSomeTimePass = function(showConditions, effect, gameInfo) {
			if (this.gameShowCondition(showConditions)) {
				var seconds = this.getNumber(showConditions.seconds, 5);
				$this.modalTimerId = setTimeout(this.showGame.bind(this, effect, gameInfo), seconds * 1000);
			}
		};
		
		this.gameShowCondition = function (showConditions) {
			return this.isChecked(showConditions) && !this.isGameShowed();
		};
		
		this.whenLeaveWebsite = function(showConditions, effect, gameInfo) {
			if (showConditions === '1' && !this.isGameShowed()) {
				this.leaveSiteAddEventListener(effect, gameInfo);
			}
		};
		
		this.isFilteredByUrl = function() {
			var filters = $this.filtersByUrl,
				currentUrl = window.location.pathname.replace('/', ''),
				hideIndex = -1, showIndex = -1, showPageExists = false;
			for (var index in filters) {
				var url = filters[index];
				if (!showPageExists && url.type === '1') {
					showPageExists = true;
				}
				if (url.url === currentUrl || url.url === '*') {
					url.type === '0'
						? hideIndex = index
						: showIndex = index;
				}
			}
			return this.isFilterOff(hideIndex, showIndex, filters)
					? false
					: this.isFilteredPage(hideIndex, showIndex, showPageExists);
		};
		
		this.isFilterOff = function(hide, show, filters) {
			return hide === -1
				&& hide === show
				&& filters.length === 0
		};
		
		this.isFilteredPage = function(hide, show, showPage) {
			return hide === -1 && show === -1
				? showPage
				: hide > show;
		};
		
		/**
		 * Insert game into page behavior
		 * 
		 */
		
		this.insertGameInPage = function() {
			this.preInitElements();
			var pageSelector = '#woohoo-game-' + $this.gameInfo.type + '.woohoo-game-' + $this.gameId;
			this.initGameOnPage(d.querySelector(pageSelector));
		};
		
		this.preInitElements = function() {
			barObject.destroy();
			triggerObject.destroy();
			hide($this.select('.' + $this.closeClass));
			hide($this.select('.continue-btn'));
		};
		
		this.initGameOnPage = function(gameDiv) {
			if (!gameDiv) return false;
			gameDiv.appendChild($this.modalNode);
			this.changeStyles();
			this.showGameOnPage();
			this.updateGameStatistics()
		};
		
		this.showGameOnPage = function() {
			show($this.modalNode);
			$this.select($this.positionClass).style.display = 'inline-table';
			if ($this.gameType === 'gift') {
				giftObject.animateOpen();
			}
		};
		
		this.updateGameStatistics = function() {
			this.addToShowCounter($this.gameInfo);
			if (!isAdmin && this.getShowCounter($this.gameInfo) < 2) {
				this.sendShowStatistic();
			}
		};
		
		this.changeStyles = function () {
			this.clearModalNodeStyles();
			this.setModalWrapperStyles();
			this.clearPositionGameDiv();
		};
		
		this.clearModalNodeStyles = function() {
			var mainDivStyles = [
				'position', 'top', 'bottom', 'z-index',
				'width', 'height', 'background-color'
			];
			for (var style of mainDivStyles) {
				$this.modalNode.style[style] = '';
			}
		};
		
		this.setModalWrapperStyles = function() {
			var modalNodeWrapper = $this.modalNode;
			modalNodeWrapper.style.width = getStyle($this.select('.lucky-coupon-popup'), 'width') + 'px';
			modalNodeWrapper.style.position = 'relative';
			modalNodeWrapper.style.overflow = 'hidden';
            modalNodeWrapper.style.margin = 'auto';
		};
		
		this.clearPositionGameDiv = function() {
			var positionDiv = $this.select($this.positionClass);
			positionDiv.style['left'] = '0';
			positionDiv.style['top'] = '0';
			positionDiv.style['-webkit-transform'] = 'none';
			positionDiv.style['transform'] = 'none';
			positionDiv.style['position'] = 'relative';
			positionDiv.querySelector('.lucky-coupon-popup').style['box-shadow'] = 'none';
		};
		/**
		 * End Insert game into page
		 */
		
		this.isGameShowed = function() {
			return this.getModalShowedFlag() === 1;
		};
		
		this.leaveSiteAddEventListener = function(effect, gameInfo) {
			if (!isMobile()) {
				this.desktopLeaveEvent(effect, gameInfo);
			} else {
				this.mobileLeaveEvent(effect, gameInfo);
			}
		};
		
		this.desktopLeaveEvent = function(effect, gameInfo) {
			d.addEventListener('mouseleave', function(e) {
				if (this.isTopDirection(e) && !this.isGameShowed()) {
					this.showGame(effect, gameInfo);
				}
			}.bind(this));
		};
		
		this.mobileLeaveEvent = function(effect, gameInfo) {
			this.backButtonIntentExit(effect, gameInfo);
			// this.scrollIntentExit(effect, gameInfo);
			this.tabsChangeIntentExit(effect, gameInfo);
		};
		
		this.backButtonIntentExit = function(effect, gameInfo) {
			location.hash = '#!';
			window.addEventListener('hashchange', function(event){
				var hash = (event.oldURL && typeof event.oldURL !== 'undefined')
					? event.oldURL.split('#')[1]
					: false;
				if (!this.isGameShowed() && hash === '!') {
					this.showGame(effect, gameInfo);
				}
			}.bind(this));
		};
		
		this.scrollIntentExit = function(effect, gameInfo) {
			var percentUp = 0.2,
				scrollStart = getBodyScrollTop(),
				pageHeight = document.documentElement.offsetHeight,
				interval = null,
				complete = false,
				scrollInterval = 100;
			if (pageHeight > 0) {
				interval = setInterval(function() {
					var scrollAmount = scrollStart - getBodyScrollTop();
					if (scrollAmount < 0) {
						scrollAmount = 0;
						scrollStart = getBodyScrollTop();
					}
					var upScrollPercent = parseFloat(scrollAmount) / parseFloat(pageHeight);
					if (upScrollPercent > parseFloat(percentUp) / 100 && !complete) {
						clearInterval(interval);
						interval = null;
						complete = true;
						if (!this.isGameShowed()) {
							this.showGame(effect, gameInfo);
						}
					}
					if (this.isGameShowed()) {
						clearInterval(interval);
						interval = null;
						complete = true;
					}
				}.bind(this), scrollInterval);
			}
		};
		
		this.tabsChangeIntentExit = function(effect, gameInfo) {
			w.addEventListener('blur', function(e) {
				if (!this.isGameShowed()) {
					this.showGame(effect, gameInfo);
				}
			}.bind(this));
		};
		
		this.isTopDirection = function(e) {
			return e.clientY <= 0;
		};
		
		this.isChecked = function (showConditions) {
			return showConditions.hasOwnProperty('value') ? showConditions.value === '1' : false;
		};
		
		this.getNumber = function(rawString, defaultValue) {
			return isNumber(rawString) && rawString !== '' ? parseFloat(rawString) : defaultValue
		};
		
		this.showWhenReaches = function(percent, effect, gameInfo) {
			if (this.isAheadOfPercents(percent) && !this.isGameShowed()) {
				this.showGame(effect, gameInfo);
			}
		};
		
		this.isAheadOfPercents = function(number) {
			return (w.innerHeight + w.scrollY) >= (this.getDocumentHeight() / 100 * number);
		};
		
		this.userLaunchGame = function() {
			$this.isUserOpened = true;
			$this.showBar = false;
			this.showGame(st.animation, $this.gameInfo);
		};
		
		this.showGame = function (effect, gameInfo) {
			if (!animationObject.isShownModal()) {
				this.hideTrigger();
				this.addToShowCounter(gameInfo);
				this.showModal(effect);
				clearTimeout($this.modalTimerId);
			}
		};
		
		this.hideTrigger = function() {
			if($this.triggerNode) {
				$this.triggerNode.style.opacity = '0';
			}
		};
		
		this.addToShowCounter = function(gameInfo) {
			var showedTimes = this.getShowCounter(gameInfo);
			this.setShowCounter(showedTimes + 1);
			this.setModalShowedFlag();
		};
		
		this.getShowCounter = function(gameInfo) {
			var typeShowed = getCookie(getGameName(gameInfo) + 'Showed');
			return isNumber(typeShowed) && (typeShowed !== '') ? parseInt(typeShowed) : 0;
		};
		
		this.setShowCounter = function(showedTimes) {
			setCookie(getGameName($this.gameInfo) + 'Showed', showedTimes, $this.cookieExpirationDays);
			var date = Math.round(new Date().getTime() / 1000);
			this.setTimestamp($this.gameInfo, date);
		};
		
		this.getModalShowedFlag = function() {
			return this.getNumber(getCookie(getGameName($this.gameInfo) + 'ShowFlag'), 0);
		};
		
		this.setModalShowedFlag = function() {
			setCookie(getGameName($this.gameInfo) + 'ShowFlag', 1, $this.cookieExpirationDays);
		};
		
		this.setModalNotShowedFlag = function() {
			setCookie(getGameName($this.gameInfo) + 'ShowFlag', 0, $this.cookieExpirationDays);
		};
		
		this.setTimestamp = function(gameInfo, date) {
			if (this.getTimestamp(gameInfo) === 0) {
				setCookie(getGameName(gameInfo) + 'Timestamp', date, $this.cookieExpirationDays);
			}
		};
		
		this.getTimestamp = function(gameInfo) {
			return this.getNumber(getCookie(getGameName(gameInfo) + 'Timestamp'), 0);
		};
		
		this.valueSelected = function (value) {
			return value === '1' || value.value === '1';
		};
		
		this.getProperty = function (showConditions, prop) {
			var result,
				value = showConditions[prop];
			if (this.valueSelected(value)) {
				result = prop;
			} else if (typeof value === 'object') {
				result = this.getSelectedCondition(value, prop);
			}
			return result;
		};
		
		this.getSelectedCondition = function(showConditions, initial) {
			var result;
			for (var prop in showConditions) {
				result = this.getProperty(showConditions, prop, initial);
				if (initial !== '' && result) {
					return initial;
				} else if (result) {
					return result;
				}
			}
		};
		
		this.detectDevice = function () {
			return navigator.userAgent.match(/Android/i)
				|| navigator.userAgent.match(/webOS/i)
				|| navigator.userAgent.match(/iPhone/i)
				|| navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i)
				|| navigator.userAgent.match(/BlackBerry/i)
				|| navigator.userAgent.match(/Windows Phone/i)
		};
		
		/**
		 * Statistic functions
		 */
		
		this.sendShowStatistic = function() {
			var device = this.detectDevice() ? 'mobile' : 'pc';
			this.ajax({
				url: '/api/v1/game/add/impression',
				method: 'POST',
				data: {
					id: $this.gameId,
					site_id: $this.siteId,
					device_type: device
				}
			});
		};
		
		this.sendGamePlayedStatistic = function(email, username, handler) {
			var device = this.detectDevice() ? 'mobile' : 'pc',
				ajaxObject = {
					url: '/api/v1/game/add/hit',
					method: 'POST',
					data: {
						id: $this.gameId,
						site_id: $this.siteId,
						subscriber_email: email,
						subscriber_name: username,
						device_type: device
					}
				};
			$this.sendedEmail = email;
			if (!$this.emailCollect || ($this.isRecartEnabled && $this.isRecartChecked)) {
				ajaxObject.data.subscriber_email = 'non-collect-option-on';
				$this.sendedEmail = 'non-collect-option-on';
			}
			this.ajax(ajaxObject, handler);
		};
		
		this.getCode = function(coupon, handler) {
			this.ajax({
				url: '/api/v1/coupon/code',
				method: 'POST',
				data: coupon
			}, handler);
		};
		
		/**
		 * @deprecated should use Request object
		 */
		this.ajax = function(ajax, handler) {
			handler = handler ? handler.bind(this) : function () {}.bind(this);
			var http = new XMLHttpRequest(),
				params = this.serialize(ajax.data);
			http.open(ajax.method, '//' + $this.hostName + ajax.url, true);
			http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			http.onreadystatechange = function () {
				if (http.readyState === 4 && http.status === 200) {
					handler(http);
				} else if (http.readyState === 4 && http.status > 0) {
					handler(http);
				}
			};
			http.send(params);
		};
		
		this.serialize = function(obj) {
			var str = [];
			for(var p in obj)
				if (obj.hasOwnProperty(p)) {
					str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
				}
			return str.join('&');
		}
	}
	
	/**
	 * @constructor
	 */
	function Launcher() {
		
		this.executeGames = function(windowObj) {
			/** ide support */
			/** @namespace windowObj._lkda */
			if (windowObj.hasOwnProperty('_lkda')) {
				var frameId = 0,
					lkda = windowObj._lkda;
				try { if (isFrame) frameId = window.self['FRAME_ID']; } catch (e) {}
				if ('preview_trigger' in lkda && !('game_preview' in lkda) && frameId === 0) {
					var gameId = lkda['preview_trigger']['game_id'];
					this.executeGame(gameId, lkda['game_' + gameId].modal, false);
				} else if (frameId === 0 && 'game_preview' in lkda) {
					this.executeGame('game_preview', lkda['game_preview'].modal);
				} else {
					this.executeListGames(lkda, frameId);
				}
			}
		};
		
		this.executeListGames = function(lkda, frameId) {
			for (var index in lkda) {
				if (lkda.hasOwnProperty(index)) {
					var idArray = index.split('_'),
						id = parseInt(idArray[1]);
					if (frameId === id) {
						this.executeGame(index, lkda[index].modal);
						break;
					} else if (frameId === 0) {
						this.executeGame(index, lkda[index].modal);
					}
				}
			}
		};
		
		this.executeGame = function(id, data) {
			var modalWrapper = copyObject(data.modalWrapper),
				modalContent = copyObject(data.modalContent),
				modalBehavior = copyObject(data.modalBehavior),
				modalStyle = copyObject(data.modalStyle),
				currentWindow = isFrame ? w.self : w.top,
				game;
			/** bind modalStyle because of copyObject happened */
			modalStyle.style = data.modalStyle.style.bind(modalStyle);
			game = new Modal(currentWindow, d, modalWrapper, modalContent, modalStyle, modalBehavior);
			for (var i in game) {
				var p = game[i];
				if( typeof p === 'function' ) {
					game[i] = tcWrapper(p, modalWrapper.meta.gameId);
				}
			}
			game.init();
			isInitExecuted = 1;
			this.injectGame(id, game);
		};
		
		this.injectGame = function(id, game) {
			if (isFrame || game.hasDevScript()) w.top['__' + id] = game;
		};
	}
	
	var isInitExecuted = 0,
		gameLauncher = new Launcher(),
		windowObj = (w.parent !== w.top) ? w : w.top;
	gameLauncher.executeGames(windowObj);
	
	/**
	 * Helper functions
	 */
	function tcWrapper(f, gameId) {
		return function() {
			try {
				return f.apply(this, arguments);
			} catch(e) {
				sendErrorToServer(e, gameId);
			}
		}
	}
	
	function sendErrorToServer(exception, gameId) {
		var request = new Request({hostName: (scriptPath.split( '/' ))[2]});
		request.ajax({
			url: '/api/v1/game/log-errors',
			method: 'POST',
			data: {
				id: parseInt(gameId),
				error: location.toString() + '\\n' + JSON.stringify(exception.stack)
			}
		});
	}
	
	function isSet(object, name) {
		if (object.hasOwnProperty(name)) {
			return object[name] !== null ? object[name] : null;
		}
		return null;
	}
	
	function isObject(object) {
		return (typeof(object) !== 'string'
			&& typeof(object) !== 'number'
			&& typeof(object) !== 'boolean'
			&& typeof(object) !== 'undefined'
			&& typeof(object) !== 'symbol'
		);
	}
	
	function avoidProperty(key) {
		return key === 'tagName' || key === 'content' || key === 'meta';
	}
	
	function ucFirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	
	function findCoupons(content, coupons) {
		for(var property in content) {
			if(content.hasOwnProperty(property)) {
				if(content[property].hasOwnProperty('meta')) {
					if(content[property]['meta'] === 'coupons') {
						coupons = content[property]['content'];
					}
				} else if(content[property].hasOwnProperty('content')) {
					if (content[property].tagName !== 'meta' && coupons.length === 0) {
						coupons = findCoupons(content[property]['content'], coupons);
					}
				}
			}
		}
		if(coupons) return coupons;
	}
	
	function setCookie(cookieName, cookieValue, expireDays) {
		var d = new Date();
		d.setTime(d.getTime() + (expireDays * 24 * 60 * 60 * 1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = 'lkda_' + cookieName + "=" + cookieValue + ";" + expires + ";path=/";
	}
	
	function getCookie(cookieName) {
		var name = 'lkda_' + cookieName + "=",
			decodedCookie, ca;
		try {
			decodedCookie = decodeURIComponent(document.cookie);
		} catch (e) {
			decodedCookie = document.cookie;
		}
		ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}
	
	function isNumber(string) {
		return !isNaN(string);
	}
	
	function isUndefined(value) {
		return typeof value === 'undefined';
	}
	
	function removeClass(node, className) {
		if (!node.className) return false;
		node.className = node.className.replace(className, '');
	}
	
	function addClass(node, className) {
		if (!hasClass(node, className)) {
			node.className = node.className + className;
		}
	}
	
	function hasClass(node, className) {
		return node && node.className ? node.className.indexOf(className) > -1 : false;
	}
	
	function findParentElement(entryPoint, condition) {
		if (condition.call(this, entryPoint)) {
			return entryPoint;
		} else {
			return entryPoint && entryPoint.tagName !== 'HTML'
				? findParentElement(entryPoint.parentNode, condition)
				: false;
		}
	}
	
	function setBarShowed(data, gameInfo, days) {
		setCookie(getBarClosedName(gameInfo), data, days);
	}
	
	function getBarShowed(gameInfo) {
		return getCookie(getBarClosedName(gameInfo));
	}
	
	function getBarClosedName(gameInfo) {
		return getGameName(gameInfo) + 'BarClosed';
	}
	
	function isCurrentGamePlayed(gameInfo) {
		return getCookie(gamePlayedName(gameInfo)) === '1';
	}
	
	function setModalTriggerClosedFlag(gameInfo, expirationDays) {
		setCookie(getGameName(gameInfo) + 'TriggerClosedFlag', 1, expirationDays);
	}
	
	function getModalTriggerClosedFlag(gameInfo) {
		return getCookie(getGameName(gameInfo) + 'TriggerClosedFlag');
	}
	
	function getModalShowedFlag(gameInfo) {
		return getCookie(getGameName(gameInfo) + 'ShowFlag');
	}
	
	function gamePlayedName(gameInfo) {
		return getGameName(gameInfo) + 'Played';
	}
	
	function getGameCodeName(gameInfo) {
		return getGameName(gameInfo) + 'Code';
	}
	
	function setGameCode(code, gameInfo) {
		setCookie(getGameCodeName(gameInfo), code);
	}
	
	function getGameCode(gameInfo) {
		return getCookie(getGameCodeName(gameInfo));
	}
	
	function getGameName(gameInfo) {
		return gameInfo.type + gameInfo.gameId;
	}
	
	function getCountDownShowedName(gameInfo) {
		return getGameName(gameInfo) + 'CountDownShowed';
	}
	
	function copyObject(object) {
		return JSON.parse(JSON.stringify(object));
	}
	
	function translation(language, translations, word) {
		if(translations.hasOwnProperty(word)) {
			if(translations[word].hasOwnProperty(language)) {
				return translations[word][language];
			}
		}
		return '';
	}
	
	function getCountDownMinutes(mb) {
		return mb.hasOwnProperty('countDownTimeTimeMin')
				&& isNumber(mb.countDownTimeTimeMin)
				&& mb.countDownTimeTimeMin !== ''
				&& !isObject(mb.countDownTimeTimeMin)
			? Math.abs(parseInt(mb.countDownTimeTimeMin)) : 15
	}
	
	function isTriggerOpens(behavior) {
		return behavior.hasOwnProperty('showPlayGameTrigger') ? behavior.showPlayGameTrigger.yes === '1' : false;
	}
	
	function getDocumentWidth() {
		var body = d.body,
			html = d.documentElement;
		return Math.max(body.scrollWidth, body.offsetWidth,
			html.clientWidth, html.scrollWidth, html.offsetWidth);
	}
	
	function getAdminPreviewTrigger() {
		return getData(window, 'top._lkda.preview_trigger', {});
	}
	
	function show(element, inline) {
		inline = !inline ? false : inline;
		element && (element.style.display = inline ? 'inline-block' : 'block');
	}
	
	function hide(element) {
		element && (element.style.display = 'none');
	}
	
	function visible(element) {
		changeVisibility(element, 'visible');
	}
	
	function invisible(element) {
		changeVisibility(element, 'hidden');
	}
	
	function changeVisibility(element, value) {
		changeStyle(element, 'visibility', value);
	}
	
	function changeStyle(element, name, value){
		element && (element.style[name] = value);
	}
	
	function getWheelSelector() {
		return isAdmin || isDetailFrame() ? '.wheel' : '.wheel-image';
	}
	
	function getData(object, path, defaultValue) {
		defaultValue = typeof defaultValue !== 'undefined' ? defaultValue : null;
		var splitPath = path.split('.');
		return splitPath.reduce(function(xs, x) { return (xs && xs[x]) ? xs[x] : defaultValue}, object
			// (xs, x) => (xs && xs[x]) ? xs[x] : defaultValue, object
		)
	}
	
	function colorLuminance(color, percent) {
		var f=color.split(','),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=parseInt(f[0].slice(4)),G=parseInt(f[1]),B=parseInt(f[2]);
		return 'rgb('+(Math.round((t-R)*p)+R)+','+(Math.round((t-G)*p)+G)+","+(Math.round((t-B)*p)+B)+')';
	}
	
	function getStyle(element, styleName) {
		var computedStyles = w.getComputedStyle(element, null),
			integer = nonIntegerStyles().indexOf(styleName) === -1;
		return getStyleValue(computedStyles, styleName, integer);
	}
	
	function nonIntegerStyles() {
		return ['margin', 'opacity', 'background', 'background-color'];
	}
	
	function getStyleValue(computedStyles, styleName, isIntResult) {
		var result = computedStyles.getPropertyValue(styleName).replace(/px/g, '');
		return getStyleResult(isIntResult, result);
	}
	
	function getStyleResult(isIntResult, result) {
		if (isIntResult) {
			return parseInt(result);
		} else {
			var isValidFloat = result.indexOf('.') > -1 && result.indexOf(' ') === -1;
			return isValidFloat ? parseFloat(result) : result;
		}
	}
	
	function validateEmail(email) {
		var regExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return regExp.test(email);
	}
	
	function isDetailFrame() {
		return isFrame
			&& typeof DETAIL_PAGE !== 'undefined'
			&& DETAIL_PAGE;
	}
	
	function inFrame() {
		try {
			return window.self !== window.top;
		} catch (e) {
			return true;
		}
	}
	
	function isMobile() {
		var check = false;
		(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	}
	
	function iOS() {
		var iDevices = [
			'iPad Simulator',
			'iPhone Simulator',
			'iPod Simulator',
			'iPad',
			'iPhone',
			'iPod'
		];
		
		if (!!navigator.platform) {
			while (iDevices.length) {
				if (navigator.platform === iDevices.pop()){ return true; }
			}
		}
		
		return false;
	}
	
	function isSafari() {
		var userAgent = navigator.userAgent.toLowerCase();
		return userAgent.indexOf('safari') !== -1
			&& userAgent.indexOf('chrome') === -1;
	}
	
	function splitLines(text) {
		var splited = text.split(' ').filter(Boolean),
			concat = '', result = '',
			lines = splited.map(function(el, index){
				if ((concat + el).length <= 13) {
					concat += el + ' ';
					if (index === (splited.length - 1)) {
						result = concat;
						concat = '';
						return result;
					}
				} else {
					result = concat;
					concat = el + ' ';
					return result.trim();
				}
			});
		lines = lines.filter(Boolean);
		var data = getData(lines, '1', '');
		lines[1] = data ? data + ' ' + concat : concat;
		return lines;
	}
	
	//math functions
	
	function decimalPlus(num, num2) {
		var normal = getNormalizer(num, num2);
		return (num * normal + num2 * normal) / normal;
	}
	
	function getMaxDecimal(num, num2) {
		var places = decimalPlaces(num),
			places2 = decimalPlaces(num2);
		return places > places2 ? places : places2;
	}
	
	function getNormalizer(num, num2) {
		return Math.pow(10, getMaxDecimal(num, num2));
	}
	
	function decimalMinus(num, num2) {
		var normal = getNormalizer(num, num2);
		return (num * normal - num2 * normal) / normal;
	}
	
	function decimalDivision(num, num2) {
		var commonPlaces = Math.pow(10, getMaxDecimal(num, num2));
		return (num * commonPlaces) / (num2 * commonPlaces) / commonPlaces;
	}
	
	function decimalPow(number, multiply) {
		var multilayer = Math.pow(10, decimalPlaces(number));
		return number * multilayer * multiply / multilayer;
	}
	
	function decimalPlaces(num) {
		var split = num.toString().split('.');
		return split.hasOwnProperty('1') ? split[1].length : 0;
	}
	
	function getCookieExpirationDays(behavior) {
		var frequencyPeriod = getData(behavior, 'frequency.notMoreThanOnceEveryNumberTimePerUser'),
			isFrequencyPeriodSet = getData(frequencyPeriod, 'value', '0') === '1';
		return isFrequencyPeriodSet
			? getData(frequencyPeriod, 'number', 1) * getDaysMultiplier(getData(frequencyPeriod, 'time', 'day'))
			: 365;
	}
	
	function getDaysMultiplier(period) {
		switch (period) {
			case 'month': return 30;
			case 'week': return 7;
			default: case 'day': return 1;
		}
	}
	
	function getBodyScrollTop() {
		var el = document.scrollingElement || document.documentElement;
		return el.scrollTop;
	}
	
	// Url functions
	
	function hasDevScript(hostName) {
		var script = d.body.querySelector('script[src^="//' + hostName + '/js/runtime.dev.js"]');
		return !!(script && script.tagName);
	}
	
	function getHostName(scriptPath) {
		return (scriptPath.split( '/' ))[2];
	}
	
	function openNewTab(url, blank) {
		blank = blank === '1';
		var target = blank ? '_blank' : '_self',
			regexp = new RegExp(/(http|https):\/\//g);
		url = url.match(regexp) ? url : 'http://' + url;
		window.open(url, target).focus();
	}
	
	
}(window, document,
	function(w) {
		try {
			return w.self !== w.top;
		} catch (e) {
			return true;
		}
	}(window),
	function(w) {
		return '_lkda' in w.top
			? getLKValueSafe(w.top['_lkda'], '__adminPanel', false)
			: false;
	}(window),
	function(w) {
		return '_lkda' in w.top
			? getLKValueSafe(w.top['_lkda'], '__scriptPath', '/js/runtime.js')
			: false;
	}(window)
);

function getLKValueSafe(lkda, key, defaultValue) {
	return isLKKeyExists(lkda, key)
		? getLKValue(lkda, key)
		: defaultValue
}
function isLKKeyExists(object, key) {
	return typeof getLKValue(object, key) !== 'undefined';
}
function getLKValue(object, key) {
	return (object.hasOwnProperty(getObjectKey(object))
			&& object[getObjectKey(object)].hasOwnProperty(key))
		? object[getObjectKey(object)][key]
		: false;
}
function getObjectKey(obj) {
	for(var key in obj)
		if (key !== 'preview_trigger' && obj.hasOwnProperty(key)) return key;
}
/**
 * Plugin for copy and paste.
 * No pain with copy to clipboard for ios devices.
 */
!function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{var e;e="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,e.Clipboard=t()}}(function(){var t,e,n;return function t(e,n,o){function i(a,c){if(!n[a]){if(!e[a]){var l="function"==typeof require&&require;if(!c&&l)return l(a,!0);if(r)return r(a,!0);var s=new Error("Cannot find module '"+a+"'");throw s.code="MODULE_NOT_FOUND",s}var u=n[a]={exports:{}};e[a][0].call(u.exports,function(t){var n=e[a][1][t];return i(n||t)},u,u.exports,t,e,n,o)}return n[a].exports}for(var r="function"==typeof require&&require,a=0;a<o.length;a++)i(o[a]);return i}({1:[function(t,e,n){function o(t,e){for(;t&&t.nodeType!==i;){if("function"==typeof t.matches&&t.matches(e))return t;t=t.parentNode}}var i=9;if("undefined"!=typeof Element&&!Element.prototype.matches){var r=Element.prototype;r.matches=r.matchesSelector||r.mozMatchesSelector||r.msMatchesSelector||r.oMatchesSelector||r.webkitMatchesSelector}e.exports=o},{}],2:[function(t,e,n){function o(t,e,n,o,r){var a=i.apply(this,arguments);return t.addEventListener(n,a,r),{destroy:function(){t.removeEventListener(n,a,r)}}}function i(t,e,n,o){return function(n){n.delegateTarget=r(n.target,e),n.delegateTarget&&o.call(t,n)}}var r=t("./closest");e.exports=o},{"./closest":1}],3:[function(t,e,n){n.node=function(t){return void 0!==t&&t instanceof HTMLElement&&1===t.nodeType},n.nodeList=function(t){var e=Object.prototype.toString.call(t);return void 0!==t&&("[object NodeList]"===e||"[object HTMLCollection]"===e)&&"length"in t&&(0===t.length||n.node(t[0]))},n.string=function(t){return"string"==typeof t||t instanceof String},n.fn=function(t){return"[object Function]"===Object.prototype.toString.call(t)}},{}],4:[function(t,e,n){function o(t,e,n){if(!t&&!e&&!n)throw new Error("Missing required arguments");if(!c.string(e))throw new TypeError("Second argument must be a String");if(!c.fn(n))throw new TypeError("Third argument must be a Function");if(c.node(t))return i(t,e,n);if(c.nodeList(t))return r(t,e,n);if(c.string(t))return a(t,e,n);throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList")}function i(t,e,n){return t.addEventListener(e,n),{destroy:function(){t.removeEventListener(e,n)}}}function r(t,e,n){return Array.prototype.forEach.call(t,function(t){t.addEventListener(e,n)}),{destroy:function(){Array.prototype.forEach.call(t,function(t){t.removeEventListener(e,n)})}}}function a(t,e,n){return l(document.body,t,e,n)}var c=t("./is"),l=t("delegate");e.exports=o},{"./is":3,delegate:2}],5:[function(t,e,n){function o(t){var e;if("SELECT"===t.nodeName)t.focus(),e=t.value;else if("INPUT"===t.nodeName||"TEXTAREA"===t.nodeName){var n=t.hasAttribute("readonly");n||t.setAttribute("readonly",""),t.select(),t.setSelectionRange(0,t.value.length),n||t.removeAttribute("readonly"),e=t.value}else{t.hasAttribute("contenteditable")&&t.focus();var o=window.getSelection(),i=document.createRange();i.selectNodeContents(t),o.removeAllRanges(),o.addRange(i),e=o.toString()}return e}e.exports=o},{}],6:[function(t,e,n){function o(){}o.prototype={on:function(t,e,n){var o=this.e||(this.e={});return(o[t]||(o[t]=[])).push({fn:e,ctx:n}),this},once:function(t,e,n){function o(){i.off(t,o),e.apply(n,arguments)}var i=this;return o._=e,this.on(t,o,n)},emit:function(t){var e=[].slice.call(arguments,1),n=((this.e||(this.e={}))[t]||[]).slice(),o=0,i=n.length;for(o;o<i;o++)n[o].fn.apply(n[o].ctx,e);return this},off:function(t,e){var n=this.e||(this.e={}),o=n[t],i=[];if(o&&e)for(var r=0,a=o.length;r<a;r++)o[r].fn!==e&&o[r].fn._!==e&&i.push(o[r]);return i.length?n[t]=i:delete n[t],this}},e.exports=o},{}],7:[function(e,n,o){!function(i,r){if("function"==typeof t&&t.amd)t(["module","select"],r);else if(void 0!==o)r(n,e("select"));else{var a={exports:{}};r(a,i.select),i.clipboardAction=a.exports}}(this,function(t,e){"use strict";function n(t){return t&&t.__esModule?t:{default:t}}function o(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}var i=n(e),r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},a=function(){function t(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}return function(e,n,o){return n&&t(e.prototype,n),o&&t(e,o),e}}(),c=function(){function t(e){o(this,t),this.resolveOptions(e),this.initSelection()}return a(t,[{key:"resolveOptions",value:function t(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};this.action=e.action,this.container=e.container,this.emitter=e.emitter,this.target=e.target,this.text=e.text,this.trigger=e.trigger,this.selectedText=""}},{key:"initSelection",value:function t(){this.text?this.selectFake():this.target&&this.selectTarget()}},{key:"selectFake",value:function t(){var e=this,n="rtl"==document.documentElement.getAttribute("dir");this.removeFake(),this.fakeHandlerCallback=function(){return e.removeFake()},this.fakeHandler=this.container.addEventListener("click",this.fakeHandlerCallback)||!0,this.fakeElem=document.createElement("textarea"),this.fakeElem.style.fontSize="12pt",this.fakeElem.style.border="0",this.fakeElem.style.padding="0",this.fakeElem.style.margin="0",this.fakeElem.style.position="absolute",this.fakeElem.style[n?"right":"left"]="-9999px";var o=window.pageYOffset||document.documentElement.scrollTop;this.fakeElem.style.top=o+"px",this.fakeElem.setAttribute("readonly",""),this.fakeElem.value=this.text,this.container.appendChild(this.fakeElem),this.selectedText=(0,i.default)(this.fakeElem),this.copyText()}},{key:"removeFake",value:function t(){this.fakeHandler&&(this.container.removeEventListener("click",this.fakeHandlerCallback),this.fakeHandler=null,this.fakeHandlerCallback=null),this.fakeElem&&(this.container.removeChild(this.fakeElem),this.fakeElem=null)}},{key:"selectTarget",value:function t(){this.selectedText=(0,i.default)(this.target),this.copyText()}},{key:"copyText",value:function t(){var e=void 0;try{e=document.execCommand(this.action)}catch(t){e=!1}this.handleResult(e)}},{key:"handleResult",value:function t(e){this.emitter.emit(e?"success":"error",{action:this.action,text:this.selectedText,trigger:this.trigger,clearSelection:this.clearSelection.bind(this)})}},{key:"clearSelection",value:function t(){this.trigger&&this.trigger.focus(),window.getSelection().removeAllRanges()}},{key:"destroy",value:function t(){this.removeFake()}},{key:"action",set:function t(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"copy";if(this._action=e,"copy"!==this._action&&"cut"!==this._action)throw new Error('Invalid "action" value, use either "copy" or "cut"')},get:function t(){return this._action}},{key:"target",set:function t(e){if(void 0!==e){if(!e||"object"!==(void 0===e?"undefined":r(e))||1!==e.nodeType)throw new Error('Invalid "target" value, use a valid Element');if("copy"===this.action&&e.hasAttribute("disabled"))throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');if("cut"===this.action&&(e.hasAttribute("readonly")||e.hasAttribute("disabled")))throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');this._target=e}},get:function t(){return this._target}}]),t}();t.exports=c})},{select:5}],8:[function(e,n,o){!function(i,r){if("function"==typeof t&&t.amd)t(["module","./clipboard-action","tiny-emitter","good-listener"],r);else if(void 0!==o)r(n,e("./clipboard-action"),e("tiny-emitter"),e("good-listener"));else{var a={exports:{}};r(a,i.clipboardAction,i.tinyEmitter,i.goodListener),i.clipboard=a.exports}}(this,function(t,e,n,o){"use strict";function i(t){return t&&t.__esModule?t:{default:t}}function r(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}function a(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}function c(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}}),e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function l(t,e){var n="data-clipboard-"+t;if(e.hasAttribute(n))return e.getAttribute(n)}var s=i(e),u=i(n),f=i(o),d="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},h=function(){function t(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}return function(e,n,o){return n&&t(e.prototype,n),o&&t(e,o),e}}(),p=function(t){function e(t,n){r(this,e);var o=a(this,(e.__proto__||Object.getPrototypeOf(e)).call(this));return o.resolveOptions(n),o.listenClick(t),o}return c(e,t),h(e,[{key:"resolveOptions",value:function t(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};this.action="function"==typeof e.action?e.action:this.defaultAction,this.target="function"==typeof e.target?e.target:this.defaultTarget,this.text="function"==typeof e.text?e.text:this.defaultText,this.container="object"===d(e.container)?e.container:document.body}},{key:"listenClick",value:function t(e){var n=this;this.listener=(0,f.default)(e,"click",function(t){return n.onClick(t)})}},{key:"onClick",value:function t(e){var n=e.delegateTarget||e.currentTarget;this.clipboardAction&&(this.clipboardAction=null),this.clipboardAction=new s.default({action:this.action(n),target:this.target(n),text:this.text(n),container:this.container,trigger:n,emitter:this})}},{key:"defaultAction",value:function t(e){return l("action",e)}},{key:"defaultTarget",value:function t(e){var n=l("target",e);if(n)return document.querySelector(n)}},{key:"defaultText",value:function t(e){return l("text",e)}},{key:"destroy",value:function t(){this.listener.destroy(),this.clipboardAction&&(this.clipboardAction.destroy(),this.clipboardAction=null)}}],[{key:"isSupported",value:function t(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:["copy","cut"],n="string"==typeof e?[e]:e,o=!!document.queryCommandSupported;return n.forEach(function(t){o=o&&!!document.queryCommandSupported(t)}),o}}]),e}(u.default);t.exports=p})},{"./clipboard-action":7,"good-listener":4,"tiny-emitter":6}]},{},[8])(8)});