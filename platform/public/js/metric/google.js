function initStatistics(search) {
	window.dataLayer = window.dataLayer || [];
	var eventName, faceBookEvent, google, query = new URLSearchParams(search.value);
	if (query.get('signup') === '1') {
		eventName = 'registrationComplete';
		faceBookEvent = 'CompleteRegistration';
		google = 'Free';
		query.delete('signup');
	}
	if (query.get('paidsignup') === '1') {
		eventName = 'purchaseComplete';
		faceBookEvent = 'Purchase';
		google = 'Paid';
		query.delete('paidsignup');
	}
	try {if (faceBookEvent) fbq('track', faceBookEvent);} catch(e) {}
	try {
		if (google) {
			ga('create', 'UA-105799379-3', 'auto');
			ga('send', 'event', {
				hitType: 'event',
				eventCategory: google === 'Paid' ? 'purchase' : 'registration',
				eventAction: google === 'Paid' ? 'purchase' : 'registration',
				eventLabel: google + 'PlanUser',
				eventValue: google === 'Paid' ? 2 : 1
			});
		}
	}
	catch(e){}
	if(eventName) window.dataLayer.push({ 'event': eventName });
	setTimeout(function(){
		window.history.replaceState({}, document.title, '/' + window.location.hash);
	}, 10);
}
var search = {value: window.location.search};
setTimeout(initStatistics.bind(null, search), 1500);