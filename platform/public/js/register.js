! function() {
	
	function Injector() {
		
		this.init = function() {
			var referral = getCookie('referral');
			if (referral) {
				var referralInput = document.body.querySelector('#referral');
				referralInput.value = referral;
			}
		};
		
	}
	
	var referenceInjector = new Injector();
	referenceInjector.init();
	
	function getCookie(cookieName) {
		var name = cookieName + '=',
			ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	}
}();