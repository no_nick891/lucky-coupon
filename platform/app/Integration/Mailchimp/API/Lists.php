<?php namespace Integration\Mailchimp\API;

use GuzzleHttp\Client;
use LuckyCoupon\MailchimpApps\MailchimpApp;

/**
 * Class Lists
 * @package Integration\Mailchimp\API
 */
class Lists
{
	private $mailchimp;
	
	private $version = '3.0';
	
	/**
	 * Lists constructor.
	 * @param MailchimpApp $mailchimp
	 * @throws \Exception
	 */
	public function __construct($mailchimp)
	{
		if (!$mailchimp->id)
		{
			throw new \Exception('Empty mailchimp object.');
		}
		
		$this->mailchimp = $mailchimp;
	}
	
	/**
	 * @return \Exception|mixed|\Psr\Http\Message\ResponseInterface
	 */
	public function get()
	{
		return $this->callRequest('GET', '', ['count' => 100]);
	}
	
	/**
	 * @param $listId
	 * @param $options
	 * @return \Exception
	 */
	public function subscribeAddress($listId, $options)
	{
		return $this->callRequest(
			'PUT',
			'/' . $listId . '/members/' . md5(strtolower($options['email_address'])),
			$options
		);
	}
	
	/**
	 * @param $method
	 * @param string $url
	 * @param array $params
	 * @param bool $async
	 * @return \Exception
	 */
	public function callRequest($method, $url = '', $params = [], $async = false)
	{
		return $this->_secureHandler(
			[$this, 'request'],
			[$method, $url, $params, $async]
		);
	}

    /**
     * @param $method
     * @param string $url
     * @param array $params
     * @param bool $async
     * @return \Exception|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
	public function request($method, $url = '', $params = [], $async = false)
	{
		$authArray = [$this->mailchimp->accountname, $this->mailchimp->access_token];
		
		$paramsType = $this->_getParamsType($method, $params);
		
		$options = [
			'auth' => $authArray,
			$paramsType => $params
		];
		
		$uri = $this->mailchimp->api_endpoint . '/' . $this->version . '/lists' . $url;
		
		$client = new Client();
		
		if (!$async)
		{
			$result = $client->request($method, $uri, $options);
			
			return json_decode($result->getBody());
		}
		else
		{
			$promise = $client->requestAsync($method, $uri, $options);
			
			return $promise->wait()->getBody();
		}
	}
	
	/**
	 * @param $response
	 */
	public function responseHandler($response)
	{
		if (is_object($response)
			&& strpos(strtolower(get_class($response)), 'exception') !== false
		)
		{
			\Log::error(print_r($response, true));
		}
	}
	
	/**
	 * @param $function
	 * @param $params
	 * @return \Exception
	 */
	private function _secureHandler($function, $params)
	{
		try
		{
			return call_user_func_array($function, $params);
		}
		catch (\Exception $e)
		{
			return $e;
		}
	}
	
	/**
	 * @param $method
	 * @param $params
	 * @return string
	 */
	private function _getParamsType($method, $params)
	{
		switch (strtolower($method))
		{
			case 'get':
				return 'query';
			case 'put':
				return 'json';
			case 'post':
				return 'form_params';
		}
	}
	
	/**
	 * @param $params
	 * @return bool
	 */
	private function _isJson($params)
	{
		$result = false;
		
		foreach ($params as $param)
		{
			if (is_array($param))
			{
				return true;
			}
		}
		
		return $result;
	}
}