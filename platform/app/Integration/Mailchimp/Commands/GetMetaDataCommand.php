<?php namespace Integration\Mailchimp\Commands;

use GuzzleHttp\Client;

class GetMetaDataCommand
{
	/**
	 * @var mixed
	 */
	private $accessToken;
	
	private $userId;
	private $siteId;
	
	/**
	 * GetMetaDataCommand constructor.
	 * @param mixed $accessToken
	 * @param $userId
	 * @param $siteId
	 */
	public function __construct($accessToken, $userId, $siteId)
	{
		$this->accessToken = $accessToken;
		
		$this->userId = $userId;
		
		$this->siteId = $siteId;
	}
	
	public function handle()
	{
		$client = new Client(['base_uri' => 'https://login.mailchimp.com']);
		
		$result = $client->get('/oauth2/metadata', [
			'headers' => [
				'User-Agent' => 'oauth2-draft-v10',
				'Host' => 'login.mailchimp.com',
				'Accept' => 'application/json',
				'Authorization' => 'OAuth ' . $this->accessToken
			]
		]);
		
		$statusCode = $result->getStatusCode();
		
		$decodedResult = json_decode($result->getBody());
		
		if (isset($decodedResult->dc))
		{
			$decodedResult->active = 1;
			
			$decodedResult->mailchimp_id = $decodedResult->user_id;
			
			$decodedResult->user_id = $this->userId;
			
			$decodedResult->site_id = $this->siteId;
			
			unset($decodedResult->login);
		}
		
		return $statusCode == 200 ? $decodedResult : false;
	}
}