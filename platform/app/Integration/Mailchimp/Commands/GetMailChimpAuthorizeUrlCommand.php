<?php namespace Integration\Mailchimp\Commands;

class GetMailChimpAuthorizeUrlCommand
{
	private $siteId;
	
	/**
	 * GetMailChimpAuthorizeUrlCommand constructor.
	 * @param $siteId
	 */
	public function __construct($siteId)
	{
		$this->siteId = $siteId;
	}
	
	public function handle()
	{
		$url = parse_url(config('app.url'));
		
		$clientId = config('services.mailchimp.app.client_id');
		
		$host = 'https://login.mailchimp.com/oauth2/authorize';
		
		$redirectUrlParams = http_build_query([
			'user' => \Auth::user()->id,
		    'site_id' => $this->siteId
		]);
		
		$redirectUri = 'https://' . $url['host'] . '/integrations/mailchimp-handler?' . $redirectUrlParams;
		
		$params = http_build_query([
			'response_type' => 'code',
		    'client_id' => $clientId,
		    'redirect_uri' => $redirectUri
		]);
		
		return $host . '?' . $params;
	}
}