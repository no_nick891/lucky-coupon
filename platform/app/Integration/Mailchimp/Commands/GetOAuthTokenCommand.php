<?php namespace Integration\Mailchimp\Commands;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class GetOAuthTokenCommand extends BaseCommand
{
	/**
	 * @var mixed
	 */
	private $code;
	
	private $userId;
	
	/**
	 * GetOAuthTokenCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$this->code = $this->request->get('code');
		
		$this->userId = $this->request->get('user');
		
		$url = parse_url(config('app.url'));
		
		$redirectUrlParams = http_build_query([
			'user' => $this->userId,
			'site_id' => $this->request->get('site_id')
		]);
		
		$uri = 'https://' . $url['host'] . '/integrations/mailchimp-handler?' . $redirectUrlParams;
		
		$params = [
			'grant_type' => 'authorization_code',
			'client_id' => config('services.mailchimp.app.client_id'),
			'client_secret' => config('services.mailchimp.app.client_secret'),
			'redirect_uri' => $uri,
			'code' => $this->code
		];
		
		$options = [
			'form_params' => $params
		];
		
		$client = new Client();
		
		$result = $client->post('https://login.mailchimp.com/oauth2/token', $options);
		
		$statusCode = $result->getStatusCode();
		
		$decodedResult = json_decode($result->getBody());
		
		return $statusCode == 200 && isset($decodedResult->access_token) ? $decodedResult->access_token : false;
	}
}