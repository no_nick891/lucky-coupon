<?php namespace Integration\Mailchimp\Commands;

use Integration\Mailchimp\API\Lists;
use LuckyCoupon\Users\UserEloquentRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AddMailchimpSubscriptionCommand implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;
	
	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;
	
	private $userId;
	
	private $email;
	
	private $siteId;
	
	/**
	 * AddMailchimpSubscriptionCommand constructor.
	 * @param $userId
	 * @param $siteId
	 * @param $email
	 */
	public function __construct($userId, $siteId, $email)
	{
		$this->userId = $userId;
		
		$this->siteId = $siteId;
		
		$this->email = $email;
	}
	
	public function handle(UserEloquentRepository $userRepo)
	{
		$user = $userRepo->getUserById($this->userId);
		
		if (!$user->mailchimp) return false;
		
		$mailchimps = $user->mailchimp
			->where('user_id', $this->userId)
			->where('site_id', $this->siteId)
			->get();
		
		$mailchimp = $mailchimps->count() > 0 ? $mailchimps[0] : false;
		
		if ($mailchimp && $mailchimp->active)
		{
			$listId = $mailchimp->selected_list_id;
			
			if ($mailchimp->active && $listId)
			{
				$mailchimpList = new Lists($mailchimp);
				
				$mailchimpList->subscribeAddress($listId, [
					'email_address' => $this->email,
				    'status' => 'subscribed'
				]);
			}
		}
	}
}