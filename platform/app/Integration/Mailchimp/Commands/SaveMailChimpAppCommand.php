<?php namespace Integration\Mailchimp\Commands;

use LuckyCoupon\MailchimpApps\MailchimpAppEloquentRepository;

class SaveMailChimpAppCommand
{
	/**
	 * @var mixed
	 */
	private $metaData;
	
	private $mailchimpRepo;
	
	/**
	 * SaveMailChimpAppCommand constructor.
	 * @param mixed $metaData
	 */
	public function __construct($metaData)
	{
		$this->metaData = $metaData;
		
		$this->mailchimpRepo = new MailchimpAppEloquentRepository();
	}
	
	public function handle()
	{
		$this->metaData['selected_list_id'] = NULL;
		
		$this->metaData['active'] = true;
		
		$metaDataArray = $this->metaData;
		
		$mailchimp = $this->mailchimpRepo->model
			->where('user_id', $metaDataArray['user_id'])
			->where('site_id', $metaDataArray['site_id']);
		
		if ($mailchimp->count() === 1)
		{
			return $mailchimp->update($metaDataArray);
		}
		else
		{
			return $this->mailchimpRepo->model->insert($metaDataArray);
		}
	}
}