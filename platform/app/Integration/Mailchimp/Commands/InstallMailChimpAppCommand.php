<?php namespace Integration\Mailchimp\Commands;

use Illuminate\Support\Facades\Log;
use LuckyCoupon\BaseCommand;
use Illuminate\Http\Request;

class InstallMailChimpAppCommand extends BaseCommand
{
	
	/**
	 * InstallMailchimpApp constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$code = $this->request->get('code');
		
		if ($code)
		{
			try
			{
				$userId = $this->request->get('user');
				
				$siteId = $this->request->get('site_id');
				
				$accessToken = dispatch(new GetOAuthTokenCommand($this->request));
				
				if ($accessToken && $userId)
				{
					$metaData = dispatch(new GetMetaDataCommand($accessToken, $userId, $siteId));
					
					$metaData->access_token = $accessToken;
					
					$metaDataArray = (array)$metaData;
					
					dispatch(new SaveMailChimpAppCommand($metaDataArray));
					
					return '<script>!function(){ window.close(); }();</script>';
				}
			}
			catch (\Exception $e)
			{
				Log::error(print_r([
					'error' => 'MailChimp token process error: ' . $e->getMessage() .'\n'. $e->getFile() .' '. $e->getLine(),
				    'request' => $this->request->all()
					], true));
				
				return ['Can\'t obtain mailchimp access token.'];
			}
		}
		else
		{
			return '<script>!function(){ window.location.href = \'/\'; }();</script>';
		}
	}
}