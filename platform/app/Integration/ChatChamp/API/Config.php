<?php namespace Integration\ChatChamp\API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Config
{
	private $apiKey;
	
	private $apiEndpoint = 'https://api.chatchamp.io/config/';
	
	/**
	 * Config constructor.
	 * @param $apiKey
	 */
	public function __construct($apiKey)
	{
		$this->apiKey = $apiKey;
	}
	
	public function get()
	{
		$client = new Client(['base_uri' => $this->apiEndpoint]);
		
		try
		{
			$request = $client->request('GET', $this->apiKey);
			
			return json_decode($request->getBody());
		}
		catch (ClientException $e)
		{
			if ($e->hasResponse())
			{
				return $e->getResponse();
			}
			
			return false;
		}
	}
}
