<?php namespace Integration\ChatChamp\Commands;

use Integration\ChatChamp\API\Config;
use LuckyCoupon\BaseCommand;

class GetConfigCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $apiKey;
	
	/**
	 * GetConfigCommand constructor.
	 * @param $apiKey
	 */
	public function __construct($apiKey)
	{
		$this->apiKey = $apiKey;
	}
	
	public function handle()
	{
		$config = new Config($this->apiKey);
		
		$request = $config->get();
		
		$result = [];
		
		if (isset($request->facebookAppId) &&
			isset($request->facebookPageId))
		{
			$result = [
				'facebookAppId' => $request->facebookAppId,
				'facebookPageId' => $request->facebookPageId
			];
		}
		
		return $result;
	}
}