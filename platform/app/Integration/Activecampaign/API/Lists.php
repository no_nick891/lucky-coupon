<?php namespace Integration\ActiveCampaign\API;

use App\Integration\Activecampaign\API\ActiveCampaign;
use LuckyCoupon\ActiveCampaignApps\ActiveCampaignApp;

/**
 * Class Lists
 * @package Integration\ActiveCampaign\API
 */
class Lists
{
    /**
     * @var ActiveCampaign
     */
    private $activeCampaignApi;

    /**
     * Lists constructor.
     * @param ActiveCampaignApp $app
     */
	public function __construct(ActiveCampaignApp $app)
	{
		$this->activeCampaignApi = new ActiveCampaign($app->api_url, $app->private_key);
	}
	
	/**
	 * @return \Exception|mixed
	 */
	public function get()
	{
        $lists = $this->activeCampaignApi->getLists();

	    return $lists;
	}
	
	/**
	 * @param $listId
	 * @param $params
	 * @return \Exception|mixed
	 */
	public function subscribeAddress($listId, $params)
	{
        return $this->activeCampaignApi->createContact($params['email_address']);
	}
}