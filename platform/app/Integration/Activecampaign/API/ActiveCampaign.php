<?php namespace App\Integration\Activecampaign\API;

/**
 * Class ActiveCampaign
 * @package App\Integration\Activecampaign\API
 */
class ActiveCampaign
{
    /**
     * @var string|null
     */
    private $apiKey;

    /**
     * @var string|null
     */
    private $apiUrl;

    /**
     * ActiveCampaign constructor.
     *
     * @param $apiUrl
     * @param $apiKey
     */
    public function __construct($apiUrl, $apiKey)
    {
        $this->apiUrl = $apiUrl;
        
        $this->apiKey = $apiKey;
    }

    /**
     * Sends query to the api
     *
     * @param $url
     * @param string $method
     * @param array $postData
     * @return array|mixed
     */
    private function sendQuery($url, $method="GET", $postData = [])
    {
        $url = $this->apiUrl . '/' . $url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if ($method == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Api-Token: " . $this->apiKey,
            'Content-Type: application/x-www-form-urlencoded',
        ]);

        $resultRaw = curl_exec($ch);

        curl_close($ch);


        if ($resultRaw) {
            return json_decode($resultRaw);
        }

        return [];
    }

    /**
     * Returns lists
     *
     * @return array|mixed
     */
    public function getLists()
    {
        return $this->sendQuery('api/3/lists');
    }

    /**
     * @param string $email
     * @param null $name
     * @param null $lastName
     * @return array|mixed
     */
    public function createContact($email, $name = null, $lastName = null)
    {
        $data = [
            'contact' => [
                'email' => $email,
                'name' => $name,
                'lastName' => $lastName
            ]
        ];

        return $this->sendQuery('api/3/contacts', 'POST', $data);
    }
}
