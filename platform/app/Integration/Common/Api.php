<?php namespace Integration\Common;

class Api
{
	protected $privateKey;
	
	protected $apiEndpoint = '';
	
	/**
	 * Lists constructor.
	 * @param $privateKey
	 */
	public function __construct($privateKey)
	{
		$this->privateKey = $privateKey;
	}
	
	/**
	 * @param string $method
	 * @param string $url
	 * @param array $params
	 * @return \Exception|mixed
	 */
	public function callRequest($method = 'GET', $url = '', $params = [])
	{
		return $this->_secureHandler(
			[$this, 'request'],
			[$method, $url, $params]
		);
	}
	
	/**
	 * @param $function
	 * @param $params
	 * @return \Exception|mixed
	 */
	protected function _secureHandler($function, $params)
	{
		try
		{
			return call_user_func_array($function, $params);
		}
		catch (\Exception $e)
		{
			\Log::error(print_r([
				'function' => $function,
			    'params' => $params,
			    'error' => $e->getFile() . ' ' . $e->getLine() . ' ' . $e->getMessage(),
			], true));
			
			return false;
		}
	}
	
	/**
	 * @param $method
	 * @return string
	 */
	protected function _getParamsType($method)
	{
		switch (strtolower($method))
		{
			case 'get':
				return 'query';
			case 'put':
				return 'json';
			case 'post':
				return 'form_params';
		}
	}
}