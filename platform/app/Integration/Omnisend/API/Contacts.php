<?php namespace Integration\Omnisend\API;

class Contacts extends Basic
{
	/**
	 * Basic constructor.
	 * @param $privateKey
	 */
	public function __construct($privateKey)
	{
		parent::__construct($privateKey);
		
		$this->apiEndpoint = 'https://api.omnisend.com/v3/';
	}
}