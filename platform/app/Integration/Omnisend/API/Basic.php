<?php namespace Integration\Omnisend\API;

use GuzzleHttp\Client;
use Integration\Common\Api;

class Basic extends Api
{
	/**
	 * Basic constructor.
	 * @param $privateKey
	 */
	public function __construct($privateKey)
	{
		parent::__construct($privateKey);
		
		$this->apiEndpoint = 'https://api.omnisend.com/v3/';
	}
	
	/**
	 * @param $method
	 * @param $url
	 * @param $params
	 * @return mixed
	 */
	public function request($method, $url, $params)
	{
		$client = new Client([
			'base_uri' => $this->apiEndpoint
		]);
		
		$result = $client->request(
			$method,
			$url,
			[
				'json' => $params,
				'headers' => [
					'X-API-KEY' => $this->privateKey,
					'Content-Type' => 'application/json'
				],
			]
		);
		
		return json_decode($result->getBody());
	}
}