<?php namespace Integration\Omnisend\API;

class Lists extends Basic
{
	/**
	 * Lists constructor.
	 * @param $privateKey
	 */
	public function __construct($privateKey)
	{
		parent::__construct($privateKey);
	}
	
	/**
	 * @return \Exception|mixed
	 */
	public function get()
	{
		return $this->callRequest('GET', 'lists');
	}
	
	/**
	 * @param $listName
	 * @return \Exception|mixed
	 */
	public function add($listName)
	{
		return $this->callRequest('POST', 'lists', ['name' => $listName]);
	}
	
	/**
	 * @param $email
	 * @param $listId
	 * @return \Exception|mixed
	 */
	public function subscribeAddress($listId, $email)
	{
		$emailArray = [
			'email' => $email,
			'status' => 'subscribed',
			'statusDate' => date('Y-m-d\TH:i:s\Z')
		];
		
		return $this->callRequest('POST', 'contacts',
			(
				$listId
				? array_merge($emailArray, ['lists' => [['listID' => $listId]]])
				: $emailArray
			)
		);
	}
}