<?php namespace Integration\Isracard\API;

class Subscription extends CommonAPI
{
	/**
	 * @param $params
	 * @return mixed
	 */
	public function generate($params)
	{
		return $this->call('generate-subscription', $params);
	}
	
	/**
	 * @param $params
	 * @return mixed
	 */
	public function cancel($params)
	{
		return $this->call('cancel-subscription', $params);
	}
	
	/**
	 * @param $params
	 * @return mixed
	 */
	public function select($params = [])
	{
		return $this->call('get-subscriptions', $params);
	}
}