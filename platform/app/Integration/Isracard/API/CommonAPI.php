<?php namespace Integration\Isracard\API;

class CommonAPI
{
	protected $url;
	
	protected $appKey;
	
	/**
	 * CommonAPI constructor.
	 */
	public function __construct()
	{
		$this->url = $this->_getUrl();
		
		$this->appKey = config('services.isracard.app.key');
	}
	
	/**
	 * @return string
	 */
	private function _getUrl()
	{
		return !config('services.isracard.app.test_mode')
				? 'https://ng.paymeservice.com/api/'
				: 'https://preprod.paymeservice.com/api/';
	}
	
	/**
	 * @param $params
	 * @return array
	 */
	protected function _getParams($params)
	{
		return json_encode(array_merge(['seller_payme_id' => $this->appKey], $params));
	}
	
	/**
	 * @param $endPoint
	 * @param $params
	 * @return mixed
	 */
	public function call($endPoint, $params)
	{
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $this->url . $endPoint);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		
		curl_setopt($ch, CURLOPT_POST, TRUE);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_getParams($params));
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type:application/json"]);
		
		$response = curl_exec($ch);
		
		curl_close($ch);
		
		return json_decode($response);
	}
}