<?php namespace Integration\Isracard\Commands;


use Integration\Isracard\API\Subscription as SubApi;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\IsracardSubscriptions\IsracardSubscriptionsEloquentRepository;

class GenerateIsracardSubscriptionCommand extends BaseCommand
{
	private $params;
	
	private $subApi;
	
	private $isracardSubRepo;
	
	private $userId;
	
	/**
	 * GenerateIsracardSubscriptionCommand constructor.
	 * @param $userId
	 * @param array $params
	 */
	public function __construct($userId, $params)
	{
		$this->userId = $userId;
		
		$this->params = $params;
		
		$this->subApi = new SubApi();
		
		$this->isracardSubRepo = new IsracardSubscriptionsEloquentRepository();
	}
	
	public function handle()
	{
		$generateArray = $this->_getGenerateSubArray($this->params);
		
		$response = $this->subApi->generate($generateArray);
		
		if (isset($response->status_error_details))
		{
			\Log::error(print_r($response, true));
			
			return $this->errorResponse(['Payment gateway send error code ' . $response->status_error_code]);
		}
		
		$isracardSubArray = $this->_getIsracardSubArray($response);
		
		if ($id = $this->isracardSubRepo->add($isracardSubArray))
		{
			$isracardSubArray['id'] = $id;
			
			return $isracardSubArray;
		}
		
		return false;
	}
	
	/**
	 * @param $params
	 * @return array
	 */
	private function _getGenerateSubArray($params)
	{
		return [
			'sub_price' => (float)$params['amount'] * 100,
			'sub_currency' => $params['currency'],
			'sub_description' => $params['name'],
			'sub_iteration_type' => $this->_getIterationType($params['period']),
			'sub_start_date' => $this->_getSubscriptionDate(),
			'sub_callback_url' => config('app.url') . 'webhooks/isracard/subscription',
			'sub_return_url' => config('app.url') . 'webhooks/isracard',
			'language' => 'en'
		];
	}
	
	/**
	 * @param $period
	 * @return string
	 */
	private function _getIterationType($period)
	{
		switch ($period)
		{
			case 'day': return '1';
			case 'week': return '2';
			case 'month': return '3';
			case 'year': return '4';
			default: return '3';
		}
	}
	
	/**
	 * @param $response
	 * @return array
	 */
	private function _getIsracardSubArray($response)
	{
		return [
			'user_id' => $this->userId,
			'payme_id' => $response->sub_payme_id,
			'payme_code' => $response->sub_payme_code,
			'payme_status' => $response->payme_status,
			'isracard_status' => $response->status_code,
			'currency' => $response->sub_currency,
			'amount' => ((int)$response->sub_price) / 100,
			'period' => $this->_getPeriod($response->sub_iteration_type),
			'description' => $response->sub_description,
			'sub_url' => $response->sub_url
		];
	}
	
	/**
	 * @param $iteration
	 * @return string
	 */
	private function _getPeriod($iteration)
	{
		switch ($iteration)
		{
			case '1': return 'day';
			case '2': return 'week';
			case '3': return 'month';
			case '4': return 'year';
			default: return 'month';
		}
	}
	
	
	/**
	 * @return false|int
	 */
	private function _getSubscriptionDate()
	{
		$timestamp = strtotime('+10 minute');
		
		if (in_array(date('d'), ['29', '30', '31']))
		{
			$curMonth = (int)date('n');
			
			$curYear  = (int)date('Y');
			
			if ($curMonth === 12)
			{
				$timestamp = mktime(0, 0, 0, 1, 1, $curYear + 1);
			}
			else
			{
				$timestamp = mktime(0, 0, 0, $curMonth + 1, 1);
			}
		}
		
		return $timestamp;
	}
}