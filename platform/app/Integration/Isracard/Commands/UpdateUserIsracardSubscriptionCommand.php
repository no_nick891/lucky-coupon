<?php namespace Integration\Isracard\Commands;

use App\LuckyCoupon\Users\User;
use Illuminate\Http\Request;
use Integration\Isracard\API\Subscription as SubAPI;
use LuckyCoupon\Affiliates\Commands\AddAffiliateCommissionCommand;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\IsracardSubscriptions\IsracardSubscriptionsEloquentRepository;
use LuckyCoupon\Subscriptions\Subscription;

class UpdateUserIsracardSubscriptionCommand extends BaseCommand
{
	private $user;
	
	private $isracardSub;
	
	private $isracardSubRepo;
	
	private $subModel;
	
	/**
	 * GetUserIsracardSubscriptionsCommand constructor.
	 * @param Request|User $user
	 * @param $request
	 */
	public function __construct($user, $request)
	{
		$this->user = $user;
		
		$this->request = $request;
		
		$this->subModel = new Subscription();
		
		$this->isracardSubRepo = new IsracardSubscriptionsEloquentRepository();
		
		$this->isracardSub = new SubAPI();
	}
	
	public function handle()
	{
		$this->_updateSubscription();
		
		if (
			$this->request->sub_status === '2'
			&& $this->user
		)
		{
			$this->_clearOldSubs();
		}
	}
	
	/**
	 * Update isracard_subscription row in database
	 */
	private function _updateSubscription()
	{
		$payMeCode = $this->request->sub_payme_code;
		
		$data = $this->_getSubArrayForUpdate();
		
		$this->isracardSubRepo->updateByCode($payMeCode, $data);
		
		if ($data['isracard_status'] == 2
			&& $this->request->sub_iterations_completed != 0)
		{
			$payment = $this->isracardSubRepo->getByPaymeId($this->request->sub_payme_id);
			
			if ($payment)
			{
				dispatch(new AddAffiliateCommissionCommand($payment));
			}
		}
	}
	
	/**
	 * @return mixed
	 */
	private function _getIsracardSubs()
	{
		return $this->subModel
			->select('s.id as sub_id', 's.*', 'is.id as isracard_sub_id', 'is.*')
			->from('subscriptions as s')
			->join('isracard_subscriptions as is', 's.user_id', '=', 'is.user_id')
			->where('s.user_id', $this->user->id)
			->where('is.payme_code', '!=', $this->request->sub_payme_code)
			->get()->toArray();
	}
	
	/**
	 * @param $isracardSubs
	 * @return array
	 */
	private function _getCodes($isracardSubs)
	{
		$codes = [];
		
		foreach ($isracardSubs as $isracardSub)
		{
			$codes[] = $isracardSub['payme_code'];
		}
		
		return $codes;
	}
	
	/**
	 * @param $codes
	 */
	private function _cancelByCodes($codes)
	{
		$codes = count($codes) === 1 ? $codes[0] : $codes;
		
		$isracardResponse = $this->isracardSub->select(['sub_payme_code' => $codes]);
		
		$items = $isracardResponse->items;
		
		$this->_cancelSubscriptions($items);
	}
	
	/**
	 * @param $items
	 */
	private function _cancelSubscriptions($items)
	{
		foreach ($items as $item)
		{
			$this->_cancelSubscription($item->sub_payme_id);
		}
	}
	
	/**
	 * @param $subPayMeId
	 */
	private function _cancelSubscription($subPayMeId)
	{
		$this->isracardSub->cancel([
			'sub_payme_id' => $subPayMeId,
			'language' => 'en'
		]);
		
		$this->isracardSubRepo->deleteByPaymeID($subPayMeId);
	}
	
	/**
	 * @return array
	 */
	private function _getSubArrayForUpdate()
	{
		$data = [];
		
		$data['isracard_status'] = (int)$this->request->sub_status;
		
		$data['sub_payment_date'] = $this->_isSubPayed($data) ? $this->request->sub_payment_date : null;
		
		return $data;
	}
	
	private function _clearOldSubs()
	{
		$isracardSubs = $this->_getIsracardSubs();
		
		$codes = $this->_getCodes($isracardSubs);
		
		$this->_cancelByCodes($codes);
	}
	
	/**
	 * @param $data
	 * @return bool
	 */
	private function _isSubPayed($data)
	{
		return ($this->request->notify_type === 'sub-active'
				|| $this->request->notify_type === 'sub-iteration-success')
				&& isset($this->request->sub_payment_date)
				&& $data['isracard_status'] === 2;
	}
}