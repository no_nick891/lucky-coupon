<?php namespace Integration\Isracard\Commands;

use Carbon\Carbon;
use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Subscriptions\SubscriptionEloquentRepository;

class GetIsracardSubscriptionCommand extends BaseCommand
{
	private $user;
	
	private $subRepo;
	
	/**
	 * CreateSubscriptionCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->user = $this->request->user();
		
		$this->subRepo = new SubscriptionEloquentRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->response($errs);
		
		$params = $this->getRequestData();
		
		$subObjects = $this->subRepo->getObject($this->user->id);
		
		if ($subObjects->count() === 0)
		{
			$isracardSub = $this->_generateIsracardSub($params);
			
			if (!is_array($isracardSub)
				&& get_class($isracardSub) === 'Illuminate\Http\Response'
			)
			{
				return $this->errorResponse(json_decode($isracardSub->original)[0]);
			}
			
			$isracardSubId = $isracardSub['id'];
			
			$sub = $this->_insertSub($isracardSubId);
		}
		else
		{
			$isracardSubs = $subObjects[0]->isracard->toArray();
			
			$isracardSub = $this->_getValidSub($params, $isracardSubs);
			
			if (!$isracardSub)
			{
				$isracardSub = $this->_generateIsracardSub($params);
			}
			
			$isracardSubId = $isracardSub['id'];
			
			$sub = $this->_updateSub($isracardSubId, $subObjects[0]);
		}
		
		return ['subscription' => $isracardSub];
	}
	
	/**
	 * @param $isracardSubId
	 * @param $subObject
	 * @return bool
	 */
	private function _updateSub($isracardSubId, $subObject)
	{
		$subObject->subscription_id = $isracardSubId;
		
		$subObject->subscription_gateway = 'isracard';
		
		if ($subObject->save())
		{
			$sub = $subObject->toArray();
		}
		else
		{
			$sub = false;
		}
		
		return $sub;
	}
	
	/**
	 * @param $isracardSubId
	 * @return array|bool
	 */
	private function _insertSub($isracardSubId)
	{
		$sub = [
			'user_id' => $this->user->id,
			'subscription_id' => $isracardSubId,
			'subscription_gateway' => 'isracard'
		];
		
		$subId = $this->subRepo->insert($sub);
		
		if ($subId)
		{
			$sub['id'] = $subId;
		}
		else
		{
			$sub = false;
		}
		
		return $sub;
	}
	
	/**
	 * @param $params
	 * @param $isracardItems
	 * @return bool
	 */
	private function _getValidSub($params, $isracardItems)
	{
		$candidate = false;
		
		foreach ($isracardItems as $current)
		{
			if ($this->_isSameConditions($params, $current))
			{
				$candidate = $current;
			}
		}
		
		return $candidate;
	}
	
	/**
	 * @param $params
	 * @param $isracard
	 * @return bool
	 */
	private function _isSameConditions($params, $isracard)
	{
		$createDate = new Carbon($isracard['created_at']);
		
		return $params['currency'] === $isracard['currency']
			&& $params['amount'] === $isracard['amount']
			&& $params['period'] === $isracard['period']
			&& $isracard['isracard_status'] < 2
			&& $createDate->isToday();
	}
	
	/**
	 * @param $params
	 * @return mixed
	 */
	private function _generateIsracardSub($params)
	{
		return dispatch(new GenerateIsracardSubscriptionCommand($this->user->id, $params));
	}
}