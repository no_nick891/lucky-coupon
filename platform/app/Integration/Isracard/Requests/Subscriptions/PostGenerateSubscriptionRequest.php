<?php  namespace Integration\Isracard\Requests\Subscriptions;

use Illuminate\Foundation\Http\FormRequest;

class PostGenerateSubscriptionRequest extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * @return mixed
	 */
	public function rules()
	{
		return \App::make(Rules::class)->postGenerateSubscription();
	}
}