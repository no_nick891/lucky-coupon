<?php  namespace Integration\Isracard\Requests\Subscriptions;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Sites
 */
class Rules extends BaseRules {

	/**
	 * @var array
	 */
	protected $_rules = [
		'name' => 'required',
	    'amount' => 'required',
	    'currency' => 'required',
	    'period' => 'required',
	];
	
	/**
	 * @return array
	 */
	public function postGenerateSubscription()
	{
		return $this->_getRulesArray(['name', 'amount', 'currency', 'period']);
	}
}