<?php namespace Integration\SendGrid\API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CommonAPI
{
	public $endPoint;
	
	protected $baseUrl;
	
	protected $apiKey;
	
	/**
	 * CommonAPI constructor.
	 */
	public function __construct()
	{
		$this->baseUrl = 'https://api.sendgrid.com/v3/';
		
		$this->apiKey = config('services.sendgrid.api.key');
		
		if (!$this->apiKey)
		{
			\Log::error(print_r(['SendGrid API key is empty'], true));
			
			die();
		}
	}
	
	/**
	 * @param $method
	 * @param $data
	 * @return bool|null|\Psr\Http\Message\ResponseInterface
	 */
	public function request($method, $data)
	{
		$client = new Client(['base_uri' => $this->baseUrl]);
		
		try
		{
			$response = $client->request(strtoupper($method), $this->endPoint, [
				'json' => $data,
			    'headers' => ['Authorization' => 'Bearer ' . $this->apiKey]
			]);
			
			return $response;
		}
		catch (ClientException $e)
		{
			if ($e->hasResponse())
			{
				return $e->getResponse();
			}
			
			return false;
		}
	}
}