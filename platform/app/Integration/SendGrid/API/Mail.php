<?php namespace Integration\SendGrid\API;

class Mail extends CommonAPI
{
	/**
	 * Mail constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->endPoint = 'mail/send';
	}
	
	/**
	 * @param $data
	 * <code>
	 * $data = [
	 *      'text' => 'Hello, world',
	 *      'type' => 'text/plain' || 'text/html',
	 *      'template_id' => 'c799cb3c-8dd2-4b62-ae10-c1ee8b30d833'
	 * ];
	 * </code>
	 * @return bool
	 */
	public function send($data)
	{
		if (!data_get($data, 'email', false))
		{
			return false;
		}
		
		$mailFormat = $this->_getMailData($data);
		
		return $this->request('POST', $mailFormat);
	}
	
	/**
	 * @param $data
	 * @return array
	 */
	private function _getMailData($data)
	{
		$mailData = $this->_getMainStructure($data);
		
		$mailData = array_merge($mailData, $this->_getMailBody($data));
		
		$this->_setSubstitutions($mailData, $data);
		
		$this->_setAttachments($mailData, $data);
		
		return $mailData;
	}
	
	/**
	 * @param $data
	 * @return array
	 */
	private function _getMainStructure($data)
	{
		return [
			'personalizations' => [
				[
					'to' => [
						['email' => data_get($data, 'email', false)]
					],
					'subject' => data_get($data, 'subject', 'no-reply')
				]
			],
			'from' => [
				'email' => data_get($data, 'from', 'no-reply@getwoohoo.com'),
				'name' => data_get($data, 'store','GetWooHoo team')
			]
		];
	}
	
	/**
	 * @param $data
	 * @return mixed
	 */
	private function _getMailBody($data)
	{
		if (data_get($data, 'template_id', false))
		{
			$result['template_id'] = $data['template_id'];
		}
		else
		{
			$result['content'] = [
				[
					'type' => data_get($data, 'type', 'text/plain'),
					'value' => data_get($data, 'text', 'Empty text')
				]
			];
		}
		
		return $result;
	}
	
	/**
	 * @param $mailData
	 * @param $data
	 */
	private function _setSubstitutions(&$mailData, $data)
	{
		$substitutions = data_get($data, 'substitutions', false);
		
		if ($substitutions)
		{
			$mailData['personalizations'][0]['substitutions'] = $substitutions;
		}
	}
	
	/**
	 * @param $mailData
	 * @param $data
	 */
	private function _setAttachments(&$mailData, $data)
	{
		$attachments = data_get($data, 'attachments', false);
		
		if ($attachments)
		{
			$mailData['attachments'][] = $attachments;
		}
	}
}