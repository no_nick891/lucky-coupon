<?php namespace Integration\SendGrid\Commands;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Integration\SendGrid\API\Mail;
use LuckyCoupon\ShopifyApps\ShopifyApp;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use LuckyCoupon\Users\UserEloquentRepository;
use ShopifyIntegration\Commands\GetShopDataCommand;

class SendEmailCommand implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;
	
	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;
	
	private $user;
	
	private $userRepo;
	
	/**
	 * @var Mail
	 */
	private $sendGrid;
	
	/**
	 * @var ShopifyAppEloquentRepository
	 */
	private $shopifyRepo;
	/**
	 * @var
	 */
	private $fields;
	
	/**
	 * SendOutOfImressions constructor.
	 * @param $user
	 * @param $fields
	 */
	public function __construct($user, $fields)
	{
		$this->user = $user;
		
		$this->fields = $fields;
	}
	
	public function handle(
		Mail $sendGrid,
		UserEloquentRepository $userRepo,
		ShopifyAppEloquentRepository $shopifyRepo
	)
	{
		try
		{
			$this->sendGrid = $sendGrid;
			
			$this->userRepo = $userRepo;
			
			$this->shopifyRepo = $shopifyRepo;
			
			return $this->_sendEmail($this->user);
			
		}
		catch (\Exception $e)
		{
			\Log::error(print_r(['Exception SendEmailCommand' => $e->getTraceAsString()], true));
		}
	}
	
	/**
	 * @param $data
	 * @return bool
	 */
	private function _sendEmail($data)
	{
		$email = $this->_getUserEmail($data);
		
		return $this->_sendTo($email);
	}
	
	/**
	 * @param $email
	 * @return bool
	 */
	private function _sendTo($email)
	{
		\Log::error(print_r(['Email to SendGrid' => $email, 'fields' => $this->fields], true));
		
		$emailData = array_merge(['email' => $email], $this->fields);
		
		return $this->sendGrid->send($emailData);
	}
	
	/**
	 * @param $user
	 * @return mixed
	 */
	private function _getUserEmail($user)
	{
		if (!$email = $this->_fetchUserEmail($user))
		{
			if (!$email = $this->_getEmailFromShopify($user))
			{
				sleep(1);
				
				$email = $this->_getEmailFromShopify($user);
			}
		}
		
		if (\App::environment('local'))
		{
			$email = 'volue.nik@gmail.com';
		}
		
		return $email;
	}
	
	/**
	 * @param $user
	 * @return mixed
	 */
	private function _getEmailFromShopify($user)
	{
		return data_get(dispatch(new GetShopDataCommand(data_get($user, 'name'))), 'email', null);
	}
	
	/**
	 * @param $user
	 * @return mixed
	 */
	private function _fetchUserEmail($user)
	{
		if (!$email = data_get($user, 'email', false))
		{
			if ($appId = data_get($user, 'app_id', false))
			{
				$app = ShopifyApp::where('id', $appId)->first();
				
				$email = data_get($app, 'email', false);
			}
		}
		
		return $email;
	}
}