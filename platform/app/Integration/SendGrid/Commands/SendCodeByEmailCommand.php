<?php namespace Integration\SendGrid\Commands;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Seeds\Helper;
use LuckyCoupon\Settings\Commands\GetSafeEmailTextSettingsCommand;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Settings\SettingsEloquentRepository;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use LuckyCoupon\Sites\EloquentSiteRepository;
use LuckyCoupon\Translations\Translation;
use ShopifyIntegration\API\Shop;

class SendCodeByEmailCommand implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Translation;
	
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;
	
	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;
	
	private $email;
	
	private $coupon;
	
	private $settings;
	
	private $gamesRepo;
	
	private $sitesRepo;
	
	private $shopifyRepo;
	
	private $settingRepo;
	
	private $defaultEmailTexts;
	
	/**
	 * SendCodeCommand constructor.
	 * @param $settings
	 * @param $coupon
	 * @param $email
	 */
	public function __construct($settings, $coupon, $email)
	{
		$this->email = $email;
		
		$this->coupon = $coupon;
		
		$this->settings = $settings;
		
		$this->gamesRepo = new EloquentGameRepository();
		
		$this->sitesRepo = new EloquentSiteRepository();
		
		$this->shopifyRepo = new ShopifyAppEloquentRepository();
		
		$this->settingRepo = new SettingsEloquentRepository();
	}
	
	public function handle()
	{
		$this->_setTranslations(data_get($this->settings, 'language', 'en'));
		
		$mailSettings = $this->_getReceivedEmailSettings($this->settings);
		
		if ($mailSettings && $this->_shouldEmail($mailSettings))
		{
			$emailFields = $this->_getEmailFields(data_get($this->coupon, 'game_id', 0));
			
			dispatch((new SendEmailCommand(['email' => $this->email], $emailFields))->onQueue('email'));
		}
	}
	
	/**
	 * @param $emailSettings
	 * @return bool
	 */
	private function _shouldEmail($emailSettings)
	{
		return data_get($emailSettings, 'winningGameScreenAndEmail') === '1'
			|| data_get($emailSettings,'sendTroughEmail') === '1';
	}
	
	/**
	 * @param $gameId
	 * @return array
	 */
	private function _getEmailFields($gameId)
	{
		$game = $this->gamesRepo->getById($gameId);
		
		$site = $this->sitesRepo->getById($game->site_id);
		
		$this->_setDefaultEmailTexts($game->type);
		
		$siteUrl = $this->_getSiteUrl($site);
		
		$storeName = $this->_getText('storeName', 'codeMail.storeName');
		
		$name = $storeName ? $storeName : $this->_getShopName($site->url, $siteUrl);
		
		return $this->_getEmailData($name, $siteUrl, $gameId);
	}
	
	/**
	 * @param $name
	 * @param $siteUrl
	 * @param $gameId
	 * @return array
	 */
	private function _getEmailData($name, $siteUrl, $gameId)
	{
		return [
			'from' => data_get($this->defaultEmailTexts, 'from', 'no-replay@getwoohoo.com'),
			'subject' => $this->_getText('couponCode', 'codeMail.couponCode') . ' ' . $name,
			'store' => $name,
			'template_id' => '51d9ea1b-e595-431a-925c-9142cdf17ba5',
			'substitutions' => [
				'%site_url%' => $siteUrl,
				'%code%' => data_get($this->coupon, 'code'),
				'%discount_name%' => $this->_getDiscountName($this->coupon),
			    '%congratulation%' => $this->_getText('congratulation', 'codeMail.congratulation'),
			    '%you_won%' => $this->_getText('youWon', 'codeMail.youWon'),
			    '%your_discount%' => $this->_getText('yourDiscount', 'codeMail.yourDiscount'),
			    '%go_to_site%' => $this->_getText('goToSite','codeMail.goToSite'),
			    '%un_subscribe%' => $this->_getText('unsubscribe','codeMail.unSubscribe'),
				'%advice%' => $this->_getText('dontForgetToApplyYourCouponToTheRelevantFieldDoingTheCheckoutProcess','codeMail.advice'),
			    '%logo_image%' => $this->_getLogoImage($gameId)
			]
		];
	}
	
	/**
	 * @param $coupon
	 * @return array
	 */
	private function _getDiscountName($coupon)
	{
		$result = '';
		
		switch ($coupon['type'])
		{
			case 'discount': $result = $coupon['value'] . '% ' . $this->_getText('discount','codeMail.' . $coupon['type']);
				break;
			case 'cash': $result = '$' . $coupon['value'] . ' ' . $this->_getText('cash','codeMail.' . $coupon['type']);
				break;
			case 'free product': $result = $this->_getText('freeProduct','codeMail.freeProduct');
				break;
			case 'free shipping': $result = $this->_getText('freeShipping','codeMail.freeShipping');
				break;
			case 'type': $result = urldecode($coupon['value']);
				break;
		}
		
		return $result;
	}
	
	/**
	 * @param $settings
	 * @return mixed
	 */
	private function _getReceivedEmailSettings($settings)
	{
		return data_get($settings, 'howDoYouWantYourCustomerToReceiveTheCoupon', false);
	}
	
	/**
	 * @param $shopifyUrl
	 * @return array|bool|object
	 */
	private function _getShop($shopifyUrl)
	{
		if (! $shopifyUrl) return false;
		
		$app = $this->shopifyRepo->getApp($shopifyUrl);
		
		if (! $app) return false;
		
		$shopifyShop = new Shop($app);
		
		return $shopifyShop->getShop();
	}
	
	/**
	 * @param $url
	 * @param $siteUrl
	 * @return mixed
	 */
	private function _getShopName($url, $siteUrl)
	{
		$shop = $this->_getShop($url);
		
		return $shop ? $shop->name : $siteUrl;
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	private function _getLogoImage($gameId)
	{
		$logoSetting = $this->settingRepo->getByGameId($gameId);
		
		$receiveLogo = $this->_getImagePath($logoSetting);
		
		$imageUrl = $this->_getImage($receiveLogo);
		
		return $this->_getImageTag($imageUrl);
	}
	
	/**
	 * @param $receiveLogo
	 * @return string
	 */
	private function _getImage($receiveLogo)
	{
		return 'https:' . getAppUrl() .
				($receiveLogo ? $receiveLogo . '?' . time() : '/img/email-logo.png');
	}
	
	/**
	 * @param $logoSetting
	 * @return mixed
	 */
	private function _getImagePath($logoSetting)
	{
		return explode(
			'?',
			trim(data_get($logoSetting, 'behavior.receiveEmailLogoImage'), '/')
       )[0];
	}
	
	/**
	 * @param $imageUrl
	 * @return string
	 */
	private function _getImageTag($imageUrl)
	{
		return '<img '
				. 'class="center fixedwidth" '
				. 'align="center" border="0" '
				. 'src="' . $imageUrl . '" '
				. 'alt="Image" title="Image" '
				. 'style="'
					. 'outline: none;'
					. 'text-decoration: none;'
					. '-ms-interpolation-mode: bicubic;'
					. 'clear: both;'
					. 'display: block !important;'
					. 'border: 0;'
					. 'height: auto;'
					. 'float: none;'
					. 'width: 100%;'
					. 'max-width: 371.25px'
				. '" '
				. 'width="371.25"/>';
	}
	
	/**
	 * @param $site
	 * @return string
	 */
	private function _getSiteUrl($site)
	{
		return preg_match('/http:|https:/', $site->url) ? $site->url : 'https://' . $site->url;
	}
	
	/**
	 * @param $gameType
	 */
	private function _setDefaultEmailTexts($gameType)
	{
		$gameType = $gameType === 'coupon' ? 'default' : $gameType;
		
		$this->defaultEmailTexts = dispatch(new GetSafeEmailTextSettingsCommand($gameType, $this->settings));
	}
	
	/**
	 * @param $textName
	 * @param $translationPath
	 * @return mixed
	 */
	private function _getText($textName, $translationPath)
	{
		return replaceCodeQuotes(data_get(
			$this->defaultEmailTexts,
			$textName,
			$this->_translate($translationPath)
		));
	}
}