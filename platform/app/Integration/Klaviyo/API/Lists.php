<?php namespace Integration\Klaviyo\API;

use GuzzleHttp\Client;

/**
 * Class Lists
 * @package Integration\Klaviyo\API
 */
class Lists
{
	private $privateKey;
	
	private $apiEndpoint = 'https://a.klaviyo.com/api/v1/';
	
	/**
	 * Lists constructor.
	 * @param $privateKey
	 */
	public function __construct($privateKey)
	{
		$this->privateKey = $privateKey;
	}
	
	/**
	 * @return \Exception|mixed
	 */
	public function get()
	{
		return $this->callRequest('GET', 'lists');
	}
	
	/**
	 * @param $listId
	 * @param $params
	 * @return \Exception|mixed
	 */
	public function subscribeAddress($listId, $params)
	{
		return $this->callRequest(
			'POST',
			'list/' . $listId . '/members',
			$params
		);
	}
	
	/**
	 * @param $method
	 * @param $url
	 * @param $params
	 * @return mixed
	 */
	public function request($method, $url, $params)
	{
		$client = new Client([
			'base_uri' => $this->apiEndpoint
		]);
		
		$result = $client->request(
			$method,
			$url,
			[
				$this->_getParamsType($method) => array_merge($params, ['api_key' => $this->privateKey])
			]
		);
		
		return json_decode($result->getBody());
	}
	
	/**
	 * @param string $method
	 * @param string $url
	 * @param array $params
	 * @return \Exception|mixed
	 */
	public function callRequest($method = 'GET', $url = '', $params = [])
	{
		return $this->_secureHandler(
			[$this, 'request'],
			[$method, $url, $params]
		);
	}
	
	/**
	 * @param $function
	 * @param $params
	 * @return \Exception|mixed
	 */
	private function _secureHandler($function, $params)
	{
		try
		{
			return call_user_func_array($function, $params);
		}
		catch (\Exception $e)
		{
			return $e;
		}
	}
	
	/**
	 * @param $method
	 * @param $params
	 * @return string
	 */
	private function _getParamsType($method, $params = [])
	{
		switch (strtolower($method))
		{
			case 'get':
				return 'query';
			case 'put':
				return 'json';
			case 'post':
				return 'form_params';
		}
	}
}