<?php namespace App\Helpers;

use Storage;

class FileStorageHelper
{
	private $userId;
	/**
	 * @var string
	 */
	private $extension;
	
	/**
	 * FileStorageHelper constructor.
	 * @param $userId
	 * @param string $extension
	 */
	public function __construct($userId, $extension = 'png')
	{
		$this->userId = $userId;
		
		$this->extension = $extension;
	}
	
	/**
	 * @param $fileName
	 * @param $fileBinary
	 * @param $gameId
	 * @return mixed
	 */
	public function saveFile($fileName, $fileBinary, $gameId)
	{
		$storagePath = $this->getStorageImage($fileName, $gameId);
		
		return Storage::disk('local')->put($storagePath, $fileBinary);
	}
	
	/**
	 * @param $firstName
	 * @param $files
	 */
	public function deleteFiles($firstName, $files)
	{
		array_map($this->deleteFile($firstName), $files);
	}
	
	/**
	 * @param $firstName
	 * @return \Closure
	 */
	public function deleteFile($firstName)
	{
		return function ($element) use ($firstName) {
			if (strstr($element, $firstName) !== false)
			{
				Storage::delete($element);
			}
		};
	}
	
	/**
	 * @param $fileName
	 * @param $gameId
	 * @return string
	 */
	public function getPublicImage($fileName, $gameId)
	{
		$directory = $this->getImageDirectory(false);
		
		return $directory . $gameId . '/' . $fileName . '.' . $this->extension;
	}
	
	/**
	 * @param $fileName
	 * @param $gameId
	 * @return string
	 */
	public function getStorageImage($fileName, $gameId)
	{
		$directory = $this->getImageDirectory(true);
		
		return $directory . $gameId . '/' . $fileName . '.' . $this->extension;
	}
	
	/**
	 * @param bool $storage
	 * @return string
	 */
	public function getImageDirectory($storage = false)
	{
		$folderName = $this->userId;
		
		$prefix = $storage ? '/public' : '/storage';
		
		return $prefix . '/images/' . $folderName . '/';
	}
	
	/**
	 * @param $filePath
	 */
	public function deletePublicFile($filePath)
	{
		$storagePath = str_replace('/storage/', 'public/', $filePath);
		
		if ($storagePath !== '')
		{
			Storage::delete($storagePath);
		}
	}
}