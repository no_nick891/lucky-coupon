<?php

if (! function_exists('d'))
{
	function d()
	{
		$args = func_get_args();
		
		foreach ($args as $arg)
		{
			var_dump($arg);
		}
		die();
	}
}

if (!function_exists('verifyShopifyWebHook'))
{
	function verifyShopifyWebHook($headers)
	{
		$sha256 = data_get($headers, 'x-shopify-hmac-sha256.0', false);
		
		if (!$sha256) return false;
		
		$data = file_get_contents('php://input');
		
		$secret = config('services.shopify.app.secret');
		
		$calculatedHMAC = base64_encode(hash_hmac('sha256', $data, $secret, true));
		
		return hash_equals($sha256, $calculatedHMAC);
	}
}

if (!function_exists('getTax'))
{
	function getTax($taxLines) {
		return 1 + (float)data_get($taxLines, '0.rate', 0);
	}
}

if (!function_exists('getDateFromShopifyFormat'))
{
	function getDateFromShopifyFormat($date)
	{
		$parsed = date_parse($date);
		
		$unixTimestamp = mktime(
			$parsed['hour'],
			$parsed['minute'],
			$parsed['second'],
			$parsed['month'],
			$parsed['day'],
			$parsed['year']
		);
		
		return date('Y-m-d H:i:s', $unixTimestamp);
	}
}

if (!function_exists('getAppUrl'))
{
	function getAppUrl() {
		return str_replace(['https:', 'http:'], '', config('app.url'));
	}
}

if (!function_exists('removeProtocol'))
{
	function removeProtocol($url) {
		return str_replace(['https://', 'http://', '/'], '', $url);
	}
}

if (!function_exists('replaceQuotes'))
{
	function replaceQuotes($string) {
		return str_replace(["'", '"'], ["'" => '%22', '"' => '%27'], $string);
	}
}

if (!function_exists('replaceCodeQuotes'))
{
	function replaceCodeQuotes($string) {
		return str_replace(['%22', '%27'], ['%22' => "'" , '%27' => '"'], $string);
	}
}

if (!function_exists('isObjOrArray'))
{
	function isObjOrArray($value) {
		return is_array($value) || is_object($value);
	}
}

if (!function_exists('calculateGroupedStatistic'))
{
	function calculateGroupedStatistic($result, $item, $key) {
		$pcImpressions = (int)data_get($item, 'pc.impressions', 0);
		
		$result[$key]['pc']['ctr'] = $pcImpressions ? ((int)data_get($item,
				'pc.hits',
				0) / $pcImpressions * 100) : 0;
		
		$mobileImpressions = (int)data_get($item, 'mobile.impressions', 0);
		
		$result[$key]['mobile']['ctr'] = $mobileImpressions ? ((int)data_get($item,
				'mobile.hits',
				0) / $mobileImpressions * 100) : 0;
		
		$result[$key]['all']['impressions'] = $pcImpressions + $mobileImpressions;
		
		$result[$key]['all']['hits'] = (int)data_get($result,
				$key . '.pc.hits',
				0) + (int)data_get($result, $key . '.mobile.hits', 0);
		
		$allImpressions = (int)data_get($result, $key . '.all.impressions', 0);
		
		$result[$key]['all']['ctr'] = $allImpressions ? (((int)data_get($result,
				$key . '.all.hits',
				0)) / $allImpressions * 100) : 0;
		
		return $result;
	}
}

if (!function_exists('getPayingCount')) {
	function getPayingCount($gameType = null) {
		
		$shopifyQuery = \DB::table('shopify_apps')
			->from('shopify_apps as sa')
			->where('is_paying', '=', 1)
			->where('deleted', '=', 0);
		
		if ($gameType)
		{
			$shopifyQuery = $shopifyQuery->leftJoin('users as u', 'sa.id', '=', 'u.app_id')
				->leftJoin('games as g', 'g.user_id', '=', 'u.id')
				->whereNotNull('g.id')
				->where('g.type', $gameType)
				->where('g.active', 1);
		}
		
		$shopifyUsersCount = $shopifyQuery->count();
		
		$isracardQuery = \DB::table('isracard_subscriptions')
			->from('isracard_subscriptions as is')
			->where('isracard_status', 2);
		
		if ($gameType)
		{
			$isracardQuery = $isracardQuery->leftJoin('games as g', 'g.user_id', '=', 'is.user_id')
				->whereNotNull('g.id')
				->where('g.type', $gameType)
				->where('g.active', 1);
		}
		
		$isracardUsersCount = $isracardQuery->count();
		
		$licenseQuery = \DB::table('license_keys')
			->from('license_keys as lk')
			->where('lk.user_id', '>', 0);
		
		if ($gameType)
		{
			$licenseQuery = $licenseQuery->leftJoin('games as g', 'g.user_id', '=', 'lk.user_id')
				->whereNotNull('g.id')
				->where('g.type', $gameType)
				->where('g.active', 1);
		}
		
		$otherUsersCount = $licenseQuery->count();
		
		return $shopifyUsersCount + $isracardUsersCount + $otherUsersCount;
	}
}

if (!function_exists('logLine')) {
	function logLine($variables)
	{
		\Log::info(print_r($variables, true));
	}
}

if (!function_exists('createPlan')) {
	function createPlan($fields = null)
	{
		return factory(LuckyCoupon\Plans\Plan::class)->create($fields);
	}
}

if (!function_exists('createPlans')) {
	function createPlans($plansList)
	{
		$plansRepo = new \LuckyCoupon\Plans\PlansEloquentRepository();
		
		foreach ($plansList as $plan)
		{
			$plansRepo->update($plan['name'], ['active' => 0]);
			
			createPlan($plan);
		}
	}
}

if (!function_exists('getDevice'))
{
	/**
	 * @param $type
	 * @return string
	 */
	function getDevice($type)
	{
		return $type === 'pc' ? '' : 'device_';
	}
}

if (!function_exists('isUpgradeCharge')) {
	function isUpgradeCharge()
	{
		return isset($_COOKIE['redirectUrl']) && $_COOKIE['redirectUrl'];

	}
}