<?php namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use LuckyCoupon\Coupons\CouponRepositoryInterface;
use LuckyCoupon\Coupons\EloquentCouponRepository;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Games\GameRepositoryInterface;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Settings\SettingRepositoryInterface;
use LuckyCoupon\Sites\EloquentSiteRepository;
use LuckyCoupon\Sites\SiteRepositoryInterface;
use LuckyCoupon\Statistics\EloquentStatisticRepository;
use LuckyCoupon\Statistics\Statistic;
use LuckyCoupon\Statistics\StatisticRepositoryInterface;
use LuckyCoupon\Templates\EloquentTemplateRepository;
use LuckyCoupon\Templates\TemplateRepositoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    if ($this->app->environment() === 'local')
	    {
		    $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
	    }
    	
	    Schema::defaultStringLength(191);
	
	    \Validator::extend('email_game', function ($attribute, $value, $parameters, $validator) {
		    
		    $data = $validator->getData();
		
		    if (isset($data['id']))
		    {
			    $field = isset($parameters[0]) ? $parameters[0] : 'game_id';
				
			    $count = DB::table('statistics')
				    ->where($field, (int)$data['id'])
				    ->where('subscriber_email', $value)
				    ->get()->count();
				
			    return $count > 0 ? false : true;
		    }
		
		    return true;
	    });
	
	    \Validator::extend('shopify_url', function ($attribute, $value, $parameters, $validator) {
		    return  strstr($value, 'myshopify.com');
	    });
	
	    \Validator::extend('license_key', function($attribute, $value, $parameters, $validator){
	    	
		    $license = \DB::table('license_keys')
			    ->where('license_key' , $value)
			    ->where('user_id', 0)
			    ->first();
		    
		    return (boolean)data_get($license, 'id', false);
	    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->bind(SiteRepositoryInterface::class, EloquentSiteRepository::class);
	    $this->app->bind(GameRepositoryInterface::class, EloquentGameRepository::class);
	    $this->app->bind(CouponRepositoryInterface::class, EloquentCouponRepository::class);
	    $this->app->bind(SettingRepositoryInterface::class, EloquentSettingRepository::class);
	    $this->app->bind(TemplateRepositoryInterface::class, EloquentTemplateRepository::class);
	    $this->app->bind(StatisticRepositoryInterface::class, EloquentStatisticRepository::class);
    }
}
