<?php namespace LuckyCoupon\MailchimpApps;

interface MailchimpAppRepositoryInterface
{
	public function getAppByUserId($userId);
	public function updateByUserId($userId, $fields);
}