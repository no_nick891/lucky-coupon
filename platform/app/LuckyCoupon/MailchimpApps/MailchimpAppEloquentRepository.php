<?php namespace LuckyCoupon\MailchimpApps;

class MailchimpAppEloquentRepository implements MailchimpAppRepositoryInterface
{
	public $model;
	
	/**
	 * MailchimpAppEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new MailchimpApp();
	}
	
	/**
	 * @param $userId
	 * @return array
	 */
	public function getAppByUserId($userId)
	{
		$user = $this->model->where('user_id', $userId)->get();
		
		return $user->count() > 0 ? $user[0]->toArray() : [];
	}
	
	/**
	 * @param $userId
	 * @param $fields
	 * @return mixed
	 */
	public function updateByUserId($userId, $fields)
	{
		return $this->model
			->where('user_id', $userId)
			->where('site_id', $fields['site_id'])
			->update($fields);
	}
}