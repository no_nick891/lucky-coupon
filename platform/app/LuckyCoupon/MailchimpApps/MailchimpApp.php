<?php namespace LuckyCoupon\MailchimpApps;

use Illuminate\Database\Eloquent\Model;

class MailchimpApp extends Model
{
	public $table = 'mailchimp_apps';
	
	public $timestamps = false;
	
	public $fillable = ['site_id', 'user_id', 'active', 'selected_list_id'];
}
