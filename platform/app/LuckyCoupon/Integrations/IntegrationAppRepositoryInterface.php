<?php namespace LuckyCoupon\Integrations;

interface IntegrationAppRepositoryInterface
{
	public function getAppByUserId($userId);
	public function updateByUserId($userId, $fields);
}