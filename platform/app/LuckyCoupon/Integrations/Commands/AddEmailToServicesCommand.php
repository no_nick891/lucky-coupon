<?php namespace LuckyCoupon\Integrations\Commands;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Users\UserEloquentRepository;

class AddEmailToServicesCommand extends BaseCommand implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;
	
	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;
	
	/**
	 * @var
	 */
	private $gameId;
	
	/**
	 * @var
	 */
	private $email;
	
	private $user;
	
	private $game;
	
	/**
	 * ShopifyIntegration constructor.
	 * @param $gameId
	 * @param $email
	 */
	public function __construct($gameId, $email)
	{
		$this->gameId = $gameId;
		
		$this->email = $email;
	}
	
	public function handle(
		EloquentGameRepository $gameRepo,
		UserEloquentRepository $userRepo
	)
	{
		$this->game = $gameRepo->getById($this->gameId);
		
		if ( ! $this->email || ! isset($this->game->user_id)) return false;
		
		$this->user = $userRepo->getUserById($this->game->user_id);
		
		try
		{
			$servicesArray = ['mailchimp', 'klaviyo', 'omnisend', 'activecampaign'];
			
			$this->_dispatchJobs($servicesArray);
		}
		catch (\Exception $e)
		{
			$this->_logError($e);
		}
	}
	
	/**
	 * @param $servicesArray
	 */
	private function _dispatchJobs($servicesArray)
	{
		foreach ($servicesArray as $serviceName)
		{
			if ( ! isset($this->user->{$serviceName})) continue;
			
			$serviceObj = $this->_getServiceObj($this->user->{$serviceName});
			
			if ( ! $this->_isPermitted($serviceObj)) continue;
			
			$this->_dispatchJob($serviceObj, $serviceName);
		}
	}
	
	/**
	 * @param $serviceObj
	 * @return bool
	 */
	private function _getServiceObj($serviceObj)
	{
		$services = $serviceObj
			->where('user_id', $this->user->id)
			->where('site_id', $this->game->site_id)
			->get();
		
		return $services->count() > 0 ? $services[0] : false;
	}
	
	/**
	 * @param $service
	 * @return bool
	 */
	private function _isPermitted($service)
	{
		return $service && $service->active && $service->selected_list_id;
	}
	
	/**
	 * @param $service
	 * @param $serviceName
	 */
	private function _dispatchJob($service, $serviceName)
	{
		$klaviyoJob = (new AddSubscriptionToServiceCommand(
			$service,
			$this->email,
			$serviceName
		))->onQueue($serviceName);
		
		dispatch($klaviyoJob);
	}
	
	/**
	 * @param \Exception $e
	 */
	private function _logError($e)
	{
		\Log::error(print_r(
			[$e->getMessage(),
			$e->getFile(),
			$e->getLine()],
			true));
	}
}