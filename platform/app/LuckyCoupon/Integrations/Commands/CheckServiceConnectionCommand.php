<?php namespace LuckyCoupon\Integrations\Commands;

use LuckyCoupon\BaseCommand;
use Integration\ChatChamp\Commands\GetConfigCommand;

class CheckServiceConnectionCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $serviceData;
	/**
	 * @var
	 */
	private $serviceRow;
	
	/**
	 * CheckServiceConnectionCommand constructor.
	 * @param $serviceData
	 * @param $serviceRow
	 */
	public function __construct($serviceData, $serviceRow)
	{
		$this->serviceData = $serviceData;
		
		$this->serviceRow = $serviceRow;
	}
	
	public function handle()
	{
		$result = false;
		
		$config = dispatch(new GetConfigCommand($this->serviceData['api_key']));
		
		if (isset($config['facebookAppId']) && isset($config['facebookPageId']))
		{
			$result = true;
			
			$this->serviceRow->active = $result;
			
			$this->serviceRow->save();
		}
		
		return $result;
	}
}