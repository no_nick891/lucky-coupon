<?php namespace LuckyCoupon\Integrations\Commands;

use Illuminate\Http\Request;
use Integration\ChatChamp\Commands\GetConfigCommand;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ChatChampApps\ChatChampApp;

class GetChatchampConfigCommand extends BaseCommand
{
	
	
	/**
	 * GetChatchampConfigCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $errs;
		
		$serviceData = $this->getRequestData();
		
		$chatchampObj = ChatChampApp::where('game_id', $serviceData['game_id'])->first();
		
		$apiKey = isset($chatchampObj->api_key) ? $chatchampObj->api_key : '';
		
		return ['chatchamp' => $apiKey];
	}
}