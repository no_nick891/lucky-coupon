<?php namespace LuckyCoupon\Integrations\Commands;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AddSubscriptionToServiceCommand implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;
	
	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;
	
	/**
	 * @var string
	 */
	private $serviceName;
	
	private $service;
	
	private $email;
	
	/**
	 * AddMailchimpSubscriptionCommand constructor.
	 * @param $service
	 * @param $email
	 * @param $serviceName
	 */
	public function __construct($service, $email, $serviceName)
	{
		$this->service = $service;
		
		$this->email = $email;
		
		$this->serviceName = $serviceName;
	}
	
	public function handle()
	{
		try
		{
			$serviceListObj = $this->_dispatchList($this->service);
			
			$this->_addEmail($this->service, $serviceListObj);
		}
		catch (\Exception $exception)
		{
			$this->_logIfError($exception);
		}
	}
	
	/**
	 * @param $service
	 * @return mixed
	 */
	private function _dispatchList($service)
	{
		$listClassName = '\Integration\\' . ucfirst($this->serviceName) . '\API\Lists';
		
		return new $listClassName($this->_getListParams($service));
	}
	
	/**
	 * @param $service
	 * @param $serviceListObj
	 */
	private function _addEmail($service, $serviceListObj)
	{
		$listId = $service->selected_list_id;

		$response = $serviceListObj->subscribeAddress($listId, $this->_getParams());
		
		$this->_logIfError($response);
	}
	
	/**
	 * @param $service
	 * @return mixed
	 */
	private function _getListParams($service)
	{
		switch ($this->serviceName)
		{
			case 'mailchimp': return $service;
			case 'omnisend':
			case 'klaviyo': return $service->private_key;
			case 'activecampaign': return $service;
			default: return false;
		}
	}
	
	/**
	 * @return array
	 */
	private function _getParams()
	{
		switch ($this->serviceName)
		{
			case 'klaviyo': return $this->_getKlaviyoParams();
			case 'mailchimp': return $this->_getMailchimpParams();
			case 'omnisend': return $this->_getOmniSendParams();
            case 'activecampaign': return $this->_getActiveCampaignParams();
			default: return [];
		}
	}
	
	/**
	 * @return array
	 */
	private function _getKlaviyoParams()
	{
		return [
			'email' => $this->email,
		    'confirm_optin' => 'false'
		];
	}
	
	/**
	 * @return array
	 */
	private function _getMailchimpParams()
	{
		return [
			'email_address' => $this->email,
			'status' => 'subscribed'
		];
	}

    /**
     * @return array
     */
    private function _getActiveCampaignParams()
    {
        return [
            'email_address' => $this->email,
        ];
    }
	
	/**
	 * @return mixed
	 */
	private function _getOmniSendParams()
	{
		return $this->email;
	}
	
	/**
	 * @param $response
	 */
	private function _logIfError($response)
	{
		if (strstr(get_class($response), 'Exception') !== false)
		{
			\Log::error(print_r([
				$response->getMessage(),
				$response->getFile(),
				$response->getLine()
			],
				true));
		}
	}
}