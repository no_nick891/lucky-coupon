<?php namespace LuckyCoupon\Integrations\Commands;

/**
 * Class GetServiceListsCommand
 * @package LuckyCoupon\Integrations\Commands
 */
class GetServiceListsCommand
{
	private $serviceRow;
	
	private $serviceName;
	
	/**
	 * GetIntegrationServiceListsCommand constructor.
	 * @param $serviceRow
	 * @param $serviceName
	 */
	public function __construct($serviceRow, $serviceName)
	{
		$this->serviceRow = $serviceRow;
		
		$this->serviceName = $serviceName;
	}
	
	/**
	 * @return array
	 */
	public function handle()
	{
		if ( !$this->serviceRow) return [];
		
		$listsObj = $this->_getListsObject();
		
		$lists = $this->_getListsArray($this->_getReturnedData($listsObj));
		
		return $lists;
	}
	
	/**
	 * @return mixed
	 */
	private function _getListsObject()
	{
		$className = $this->_getListClassName();
		
		return new $className($this->_getListClassParam($this->serviceRow));
	}
	
	/**
	 * @return string
	 */
	private function _getListClassName()
	{
		return '\Integration\\' . ucfirst($this->serviceName) . '\API\Lists';
	}
	
	/**
	 * @param $serviceRow
	 * @return mixed
	 */
	private function _getListClassParam($serviceRow)
	{
        if (get_class($serviceRow) == \LuckyCoupon\ActiveCampaignApps\ActiveCampaignApp::class) {
            return $serviceRow;
        }

		return isset($serviceRow->private_key) ? $serviceRow->private_key : $serviceRow;
	}
	
	/**
	 * @param $listsObj
	 * @return mixed
	 */
	private function _getReturnedData($listsObj)
	{
		$requestedLists = $listsObj->get();
		
		$result = $requestedLists && get_class($requestedLists) !== 'stdClass'
			? []
			: ($requestedLists
				? (isset($requestedLists->lists)
					? $requestedLists->lists
					: data_get($requestedLists, 'data', false)
				) : false);
		
		if ($this->serviceName === 'omnisend' && !$result)
		{
			$newList = $this->_getListsObject()->add('woohoo');
			
			$result = $newList ? [$newList] : false;
		}
		
		return $result;
	}
	
	/**
	 * @param $lists
	 * @return array|bool
	 */
	private function _getListsArray($lists)
	{
		if ($lists === false) return false;
		
		$result = [];
		
		foreach ($lists as $list)
		{
			if ( isset($list->list_type) && $list->list_type !== 'list') continue;
			
			$result[] = [
				'id' => data_get($list, $this->_getIdFieldName(), false),
				'name' => data_get($list, 'name', false)
			];
		}
		
		return $result;
	}
	
	public function _getIdFieldName()
	{
		switch ($this->serviceName)
		{
			case 'omnisend': return 'listID';
			case 'klaviyo':
			default: return 'id';
		}
	}
}