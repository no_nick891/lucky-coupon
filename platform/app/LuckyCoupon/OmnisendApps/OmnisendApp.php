<?php namespace LuckyCoupon\OmnisendApps;

use Illuminate\Database\Eloquent\Model;

class OmnisendApp extends Model
{
	public $table = 'omnisend_apps';
	
	public $timestamps = false;
	
	public $fillable = ['site_id', 'user_id', 'active', 'private_key', 'selected_list_id'];
}
