<?php namespace LuckyCoupon\OmnisendApps;

class OmnisendAppEloquentRepository implements OmnisendAppRepositoryInterface
{
	public $model;
	
	/**
	 * KlaviyoAppEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new OmnisendApp();
	}
	
	/**
	 * @param $userId
	 * @param $fields
	 * @return mixed
	 */
	public function updateByUserId($userId, $fields)
	{
		return $this->model
			->where('user_id', $userId)
			->where('site_id', $fields['site_id'])
			->update($fields);
	}
}