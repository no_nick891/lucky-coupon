<?php namespace LuckyCoupon\OmnisendApps;

interface OmnisendAppRepositoryInterface
{
	public function updateByUserId($userId, $fields);
}