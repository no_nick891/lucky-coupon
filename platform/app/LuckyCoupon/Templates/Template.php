<?php namespace LuckyCoupon\Templates;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Site
 * @package LuckyCoupon\Templates
 */
class Template extends Model
{

	/**
	 * @var string
	 */
	protected $table = 'templates';
}
