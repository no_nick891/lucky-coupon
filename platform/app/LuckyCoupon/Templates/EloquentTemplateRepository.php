<?php namespace LuckyCoupon\Templates;

class EloquentTemplateRepository implements TemplateRepositoryInterface
{
	public $model;
	
	/**
	 * EloquentRepositoryInterface constructor.
	 */
	public function __construct()
	{
		$this->model = new Template();
	}
	
	/**
	 * @param string $template
	 * @return mixed
	 */
	public function getModalWrapper($template = 'default')
	{
		$part = 'modalWrapper';
		
		$result = $this->getByPart($template, $part);
		
		return $result[$part];
	}
	
	/**
	 * @param string $template
	 * @return mixed
	 */
	public function getModalContent($template = 'default')
	{
		$part = 'modalContent';
		
		$result = $this->getByPart($template, $part);
		
		return $result[$part];
	}
	
	/**
	 * @param string $template
	 * @return array
	 */
	public function getModalStyle($template = 'default')
	{
		$style = 'modalStyle';
		
		return $this->getByPart($template, $style);
	}
	
	/**
	 * @param $template
	 * @param $parts
	 * @return array
	 */
	public function getByPart($template, $parts = null)
	{
		$result = [];
		
		$query = $this->model->where('name', $template);
		
		if(is_string($parts))
		{
			$query->where('part', $parts);
		}
		elseif(is_array($parts))
		{
			$query->where(function($query) use ($parts) {
				foreach ($parts as $part)
				{
					$query = $query->orWhere('part', $part);
				}
			});
		}
		
		$templateParts = $query->get();
		
		foreach ($templateParts as $templatePart)
		{
			$result[$templatePart->part] = $templatePart->content;
		}
		
		return $result;
	}
}