<?php namespace LuckyCoupon\Templates\Commands;

class GetGameStylesCommand
{
	private $settings;
	
	private $templatePath;
	
	private $gameId;
	
	private $gameType;
	
	/**
	 * GetGameStylesCommand constructor.
	 * @param $settings
	 * @param $templatePath
	 * @param $gameId
	 * @param $gameType
	 */
	public function __construct($settings, $templatePath, $gameId, $gameType)
	{
		$this->settings = $settings;
		
		$this->templatePath = $templatePath;
		
		$this->gameId = $gameId;
		
		$this->gameType = $gameType;
	}
	
	public function handle()
	{
		$modalStyle = [
			'gameId' => $this->gameId,
			'modalStyle' => dispatch(new AssembleGameStylesCommand($this->templatePath))
		];
		
		$modalStyle = $this->_getSettings($modalStyle);
		
		$modalStyle = array_merge($modalStyle, (array)data_get($this->settings, 'colors'));
		
		return str_replace("\\", '', json_encode($modalStyle, JSON_UNESCAPED_SLASHES));
	}
	
	/**
	 * @param $modalStyle
	 * @return mixed
	 */
	private function _getSettings($modalStyle)
	{
		$keys = $this->_getSettingsKeys();
		
		foreach ($keys as $key)
		{
			$modalStyle[$key] = $this->_getSetting($key, $this->settings);
		}
		
		return $modalStyle;
	}
	
	/**
	 * @return array
	 */
	private function _getSettingsKeys()
	{
		$result = ['position', 'animation', 'font', 'image', 'opacity', 'giftImage'];
		
		return $result;
	}
	
	/**
	 * @param $key
	 * @param $settings
	 * @return mixed
	 */
	private function _getSetting($key, $settings)
	{
		switch ($key)
		{
			case 'opacity': case 'image':
				$result = $this->_getBGImageInfo($key, $settings);
			break;
			default: $result = data_get($settings, $key, '');
		}
		
		return $result;
	}
	
	/**
	 * @param $key
	 * @param $settings
	 * @return mixed
	 */
	private function _getBGImageInfo($key, $settings)
	{
		$imageInfo = data_get($settings, 'backgroundImage');
		
		$functionName = '_get' . ucfirst($key); // _getImage || _getOpacity
		
		try
		{
			return $this->{$functionName}(data_get($imageInfo, $key));
		}
		catch (\Exception $exception)
		{
			\Log::error(print_r([
				'backgroundImage' => data_get($settings, 'backgroundImage'),
				'exception' => $exception,
				'settings' => $settings
			], true));
			
			return $key === 'image' ? '/img/game/default.jpg' : 0.5;
		}
	}
	
	/**
	 * @param $image
	 * @return string
	 */
	private function _getImage($image)
	{
		$clearImage = explode('?', $image);
		
		return $clearImage[0] === '' ? '' : $clearImage[0] . '?' . time();
	}
	
	/**
	 * @param $opacity
	 * @return float
	 */
	private function _getOpacity($opacity)
	{
		$floatOpacity = (float)$opacity;
		
		if ($floatOpacity > 1)
		{
			return ($floatOpacity) / 100;
		}
		
		return $floatOpacity;
	}
}