<?php namespace LuckyCoupon\Templates\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Coupons\EloquentCouponRepository;
use LuckyCoupon\Settings\Commands\GetDBSettingsCommand;
use SimpleXMLElement;
use Intervention\Image\ImageManagerStatic as Image;

class GenerateSvgCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $gameId;
	
	private $couponRepo;
	
	/**
	 * GenerateSvgCommand constructor.
	 * @param $gameId
	 */
	public function __construct($gameId)
	{
		$this->gameId = (int)$gameId;
		
		$this->couponRepo = new EloquentCouponRepository();
	}
	
	public function handle()
	{
		$settings = dispatch(new GetDBSettingsCommand($this->gameId));
		
		$textColor = data_get($settings, 'colors.couponText');
		
		$slicesColors = $this->_getSliceColors($settings);
		
		$coupons = $this->couponRepo->getByGameId($this->gameId)->toArray();
		
		$couponsText = $this->_getCouponsTextByLine($coupons);
		
		$simpleXml = $this->_getSimpleXml();
		
		$this->_setSlicesColors($simpleXml, $slicesColors);
		
		$this->_setSlicesText($simpleXml, $couponsText, $textColor);
		
		$this->_setLogoImage($simpleXml, data_get($settings, 'wheelLogoImage', false));
		
		return $simpleXml->asXML();
	}
	
	/**
	 * @param $coupons
	 * @return array
	 */
	private function _getCouponsTextByLine($coupons)
	{
		$couponsText = [];
		
		foreach ($coupons as $key => $coupon)
		{
			$decodedText = urldecode($coupon['value']);
			
			$couponsText[] = strlen($decodedText) <= 13 ? [$decodedText] : $this->_splitInLines($decodedText);
		}
		
		return $couponsText;
	}
	
	/**
	 * @param $decodedText
	 * @return array
	 * @internal param $words
	 */
	private function _splitInLines($decodedText)
	{
		$concat = '';
		
		$words = explode(' ', $decodedText);
		
		$newWords = array_map(function ($el) use (&$concat, $words) {
				if (strlen($concat . $el) <= 13)
				{
					$concat .= $el . ' ';
					
					if (array_search($el, $words) === (count($words) -1))
					{
						$result = $concat;
						
						$concat = '';
						
						return $result;
					}
				}
				else
				{
					$result = $concat;
					
					$concat = $el . ' ';
					
					return trim($result);
				}
			},
			$words
		);
		
		$newWords = array_values(array_filter($newWords));
		
		$data = data_get($newWords, '1', '');
		
		$newWords[1] = $data ? ($data . ' ' . $concat) : $concat;
		
		return $newWords;
	}
	
	/**
	 * @param $settings
	 * @return array
	 */
	private function _getSliceColors($settings)
	{
		$result = [];
		
		$colors = data_get($settings, 'colors.design');
		
		if (data_get($colors, 'custom.active') !== '0'
			|| data_get($colors, 'colorize.active') !== '0'
		)
		{
			$result = data_get($colors, 'custom.schema');
		}
		else if (data_get($colors, 'present.active') !== '0')
		{
			$result = data_get($colors, 'present.schema.' . ((int)data_get($colors, 'present.active', 1) - 1));
		}
		
		return $result;
	}
	
	/**
	 * @return SimpleXMLElement
	 */
	private function _getSimpleXml()
	{
		$svg = file_get_contents(public_path('img/game/wheel/wheel.svg'));
		
		return new SimpleXMLElement($svg);
	}
	
	/**
	 * @param $simpleXml
	 * @param $couponsText
	 * @param $textColor
	 */
	private function _setSlicesText(&$simpleXml, $couponsText, $textColor)
	{
		$couponNum = 0;
		
		foreach ($simpleXml->xpath('*/*[2]/*') as $coupon)
		{
			$arcNum = 1;
			
			foreach ($coupon->xpath('*') as $arc)
			{
				foreach ($arc->xpath('*') as $arcItem)
				{
					if (isset($arcItem->textPath))
					{
						$arcItem->addAttribute('fill', $textColor);
						
						if (current($arcItem->textPath['id']))
						{
							$arcItem->textPath[0] = data_get($couponsText, $couponNum . '.' . $arcNum, '');
						}
					}
				}
				
				$arcNum--;
			}
			
			$couponNum++;
		}
	}
	
	/**
	 * @param $simpleXml
	 * @param $slicesColors
	 */
	private function _setSlicesColors(&$simpleXml, $slicesColors)
	{
		foreach ($simpleXml->xpath('/*/*/*') as $wrappers)
		{
			foreach ($wrappers as $wrapper)
			{
				$id = (string)$wrapper['id'];
				
				if (strstr($id, 'polygon'))
				{
					$wrapper->attributes()->fill = data_get($slicesColors, $id);
				}
			}
		}
	}
	
	/**
	 * @param $simpleXml
	 * @param $wheelLogoImage
	 */
	private function _setLogoImage(&$simpleXml, $wheelLogoImage)
	{
		$wheelLogo = data_get(parse_url(data_get($wheelLogoImage, 'wheelLogo', false)),'path', '');
		
		foreach ($simpleXml->xpath('/*/*/*') as $wrappers)
		{
			foreach ($wrappers->xpath('*') as $item)
			{
				if ((string)$item->attributes()->id === 'gameLogo'
					&& $wheelLogoImage && $wheelLogo)
				{
					$item['opacity'] = $wheelLogoImage['opacityWheelLogo'];
					
					$fileContent = file_get_contents(public_path($wheelLogo));
					
					$intervention = Image::make($fileContent);
					
					$base64Image = 'data:' . $intervention->mime() . ';base64,' . base64_encode($fileContent);
					
					$item->attributes('http://www.w3.org/1999/xlink')['href'] = $base64Image;
				}
			}
		}
	}
}