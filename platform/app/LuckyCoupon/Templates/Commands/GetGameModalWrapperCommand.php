<?php namespace LuckyCoupon\Templates\Commands;

use LuckyCoupon\Seeds\Helper;

class GetGameModalWrapperCommand
{
	private $templatePath;
	
	private $game;
	
	private $language;
	
	private $text;
	
	/**
	 * GetGameModalWrapperCommand constructor.
	 * @param $templatePath
	 * @param $game
	 * @param $language
	 * @param $text
	 */
	public function __construct($templatePath, $game, $language, $text)
	{
		$this->templatePath = $templatePath;
		
		$this->game = $game;
		
		$this->language = $language;
		
		$this->text = $text;
		
		$this->helper = new Helper();
	}
	
	public function handle()
	{
		$templates = $this->helper->getJsConstants($this->templatePath);
		
		$jsonContent = json_decode($templates['modalWrapper']);
		
		$jsonContent->className = 'game-' . $this->game->id;
		
		$jsonContent->meta->gameId = $this->game->id;
		
		$jsonContent->meta->type = $this->game->type;
		
		$jsonContent->meta->active = $this->game->active;
		
		$jsonContent->meta->language = $this->language;
		
		$jsonContent->meta->translations = $this->_getLanguageFiles();
		
		$jsonContent->meta->text = $this->text;
		
		$jsonContent = json_encode($jsonContent);
		
		return $jsonContent;
	}
	
	/**
	 * @return \StdClass
	 */
	private function _getLanguageFiles()
	{
		$translations = new \StdClass();
		
		$fileNames = ['coupons', 'changeable', 'bar'];
		
		$languagePath = 'resources/assets/js/helpers/language/';
		
		foreach ($fileNames as $fileName)
		{
			$filePath = $languagePath . $fileName . '.js';
			
			$translations->{$fileName} = $this->helper->getObjectFromFile($filePath);
		}
		
		return $translations;
	}
}