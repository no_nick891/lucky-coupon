<?php namespace LuckyCoupon\Templates\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Counters\CounterEloquentRepository;
use LuckyCoupon\Users\UserEloquentRepository;

class GetOverConditionsCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $userId;
	
	private $counterRepo;
	
	private $userRepo;
	
	/**
	 * DetectOverConditionsCommand constructor.
	 * @param $userId
	 */
	public function __construct($userId)
	{
		$this->userId = $userId;
		
		$this->userRepo = new UserEloquentRepository();
		
		$this->counterRepo = new CounterEloquentRepository();
	}
	
	public function handle()
	{
		$result = false;
		
		$condition = $this->_getStopCondition();
		
		$counter = (int)$this->counterRepo->getByFilter([
			'column_name' => 'user_id',
		    'column_value' => $this->userId,
		    'counter_name' => 'impressions'
		]);
		
		if (
			$counter >= $condition
			&& $condition > 0
		)
		{
			$result = true;
		}
		
		return $result;
	}
	
	/**
	 * @return mixed
	 */
	private function _getStopCondition()
	{
		$plan = \DB::table('plans as p')
			->leftJoin('shopify_subscriptions as ss', 'ss.plan_id', 'p.id')
			->where('user_id', $this->userId)
			->orderBy('created_at', 'desc')
			->first();
		
		return $plan && $plan->conditions ? (int)$plan->conditions : 0;
	}
}