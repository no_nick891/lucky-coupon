<?php namespace LuckyCoupon\Templates\Commands;

use LuckyCoupon\Seeds\Helper;
use LuckyCoupon\ChatChampApps\ChatChampApp;
use LuckyCoupon\Coupons\EloquentCouponRepository;

class GetGameModalContentCommand
{
	private $templatePath;
	
	private $gameId;
	
	private $type;
	
	private $content;
	
	private $helper;
	
	private $couponRepo;
	
	private $changeable;
	
	private $language;
	
	/**
	 * GetGameModalContentCommand constructor.
	 * @param $templatePath
	 * @param $gameId
	 * @param $type
	 * @param $settings
	 * @param $language
	 */
	public function __construct($templatePath, $gameId, $type, $settings, $language)
	{
		$this->templatePath = $templatePath;
		
		$this->gameId = $gameId;
		
		$this->type = $type;
		
		$this->content = data_get($settings, 'content');
		
		$this->text = data_get($settings, 'text', []);
		
		$this->language = $language;
		
		$this->helper = new Helper();
		
		$changeableLangPath = 'resources/assets/js/helpers/language/changeable.js';
		
		$this->changeable = $this->helper->getObjectFromFile($changeableLangPath);
		
		$this->couponRepo = new EloquentCouponRepository();
		
		$this->translations = $this->_getTranslations();
	}
	
	public function handle()
	{
		$modalContent = $this->_getModalContent();
		
		$this->_setDatabaseText($modalContent);
		
		$coupons = $this->couponRepo->getByGameId($this->gameId);
		
		$couponsStructure = $this->_getCouponStructure($coupons);
		
		/**
		 * Uncomment for ChatChimp checkbox
		 */
		/*if ($this->inputFieldType === 'chatchamp')
		{
			$facebookCheckBox = $this->_getInput($siteId);
			
			$this->_setInput($jsonContent->content, $facebookCheckBox, 'input-wrapper');
		}*/
		
		$this->_setCoupons($modalContent->content, $couponsStructure, 'coupons');
		
		return json_encode($modalContent);
	}
	
	/**
	 * @return mixed
	 */
	private function _getModalContent()
	{
		$templates = $this->helper->getJsConstants($this->templatePath);
		
		return json_decode($templates['modalContent']);
	}
	
	/**
	 * @param $modalContent
	 */
	private function _setDatabaseText(&$modalContent)
	{
		$this->_setText($modalContent->content, $this->content);
		
		$this->_setText($modalContent->content, $this->translations, 'translate');
	}
	
	/**
	 * @return array
	 */
	private function _getTranslations()
	{
		$gameLanguagePath = 'resources/assets/js/helpers/language/game/';
		
		$translations = $this->helper->getJsConstants($gameLanguagePath . $this->language . '.js');
		
		return (array)json_decode(array_pop($translations));
	}
	
	/**
	 * @param $coupons
	 * @return mixed
	 */
	private function _getCouponStructure($coupons)
	{
		$params = [$coupons, $this->language];
		
		$namespace = '\LuckyCoupon\Coupons\Commands';
		
		switch ($this->type)
		{
			case 'slot': $className = '\GetReelsStructureCommand'; break;
			case 'gift': $className = '\GetGiftsStructureCommand'; break;
			case 'wheel': $className = '\GetWheelStructureCommand'; break;
			case 'default':
			default: $className = '\GetCouponsStructureCommand'; array_push($params, $this->text); break;
		}
		
		$className = $namespace . $className;
		
		return dispatch(new $className(...$params));
	}
	
	/**
	 * @param $content
	 * @param $text
	 * @param bool $type
	 */
	private function _setText(&$content, $text, $type = false)
	{
		if(count((array)$text) === 0 && $type === 'translate') exit;
		
		foreach ($content as &$item)
		{
			$meta = data_get($item, 'meta', false);
			
			$newText = $this->_getNewText($item);
			
			if (!is_null($newText) && $newText !== false)
			{
				$item->{$this->_getTagTextContainerName($item)} = $newText;
			}
			else if ($meta && array_key_exists($meta, $text))
			{
				$this->_setItemTranslation($text, $item, $type, $meta);
			}
			
			if (isset($item->content) && is_array($item->content))
			{
				$this->_setText($item->content, $text, $type);
			}
		}
	}
	
	/**
	 * @param $item
	 * @return string
	 */
	private function _getNewText($item)
	{
		return $this->_text(
			data_get($item, 'data.text', false),
			data_get($item, 'meta', false)
		);
	}
	
	/**
	 * @param $textKey
	 * @param $metaName
	 * @return string
	 */
	private function _text($textKey, $metaName)
	{
		return data_get($this->text, $textKey, '');
	}
	
	/**
	 * @param $item
	 * @param $text
	 * @return bool
	 * @internal param $meta
	 */
	private function _isDefaultText($item, $text)
	{
		if (!isset($item->meta)) return false;
		
		$meta = $item->meta;
		
		if (in_array($meta, ['title', 'note', 'description', 'email', 'trigger', 'gpdr']))
		{
			$trimmedText = $this->_getTextByTranslation($text, $meta);
			
			if ($this->_getChangeableText($meta) === $trimmedText)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @param $text
	 * @param $meta
	 * @return string
	 */
	private function _getTextByTranslation($text, $meta)
	{
		return is_string($text[$meta])
			? trim($text[$meta])
			: trim($text[$meta]->{$this->_getTransType()});
	}
	
	/**
	 * @param $meta
	 * @return string
	 */
	private function _getChangeableText($meta)
	{
		if ($meta === 'title')
		{
			$titleType = $this->_getTransType();
			
			$changeableText = trim($this->changeable->{$meta}->{$titleType}->{$this->language});
		}
		else
		{
			$changeableText = trim($this->changeable->{$meta}->{$this->language});
		}
		
		return $changeableText;
	}
	
	/**
	 * @param $content
	 * @param $coupons
	 * @param $meta
	 */
	private function _setCoupons(&$content, $coupons, $meta)
	{
		foreach ($content as &$item)
		{
			if (isset($item->meta) && $item->meta === $meta)
			{
				$item->content = $coupons;
			}
			else if(isset($item->content) && is_array($item->content))
			{
				$this->_setCoupons($item->content, $coupons, $meta);
			}
		}
	}
	
	/**
	 * @param $content
	 * @param $facebookCheckBox
	 * @param $className
	 */
	private function _setInput(&$content, $facebookCheckBox, $className)
	{
		foreach ($content as $key => &$item)
		{
			if (isset($item->className) && $item->className === $className)
			{
				$content[$key] = $facebookCheckBox;
			}
			elseif (isset($item->content) && is_array($item->content))
			{
				$this->_setInput($item->content, $facebookCheckBox, $className);
			}
		}
	}
	
	/**
	 * @param $siteId
	 * @return array|object
	 */
	private function _getInput($siteId)
	{
		$chatChampAppKey = $this->_getChatchampAppKey($siteId);
		
		$facebookCheckBox = [];
		
		if ($chatChampAppKey)
		{
			$facebookCheckBox = (object)[
				'tagName' => 'div',
				'className' => 'chatchamp-messenger-checkbox',
				'attributes' => (object)[
					'chatchamp_app_id' => $chatChampAppKey
				]
			];
		}
		
		return $facebookCheckBox;
	}
	
	/**
	 * @param $siteId
	 * @return string
	 */
	private function _getChatchampAppKey($siteId)
	{
		$apiKey = '';
		
		$data = ChatChampApp::where('site_id', $siteId)->first();
		
		if (isset($data->api_key))
		{
			$apiKey = $data->api_key;
		}
		
		return $apiKey;
	}
	
	/**
	 * @return string
	 */
	private function _getTransType()
	{
		return $this->type === 'gift' ? $this->type : 'other';
	}
	
	/**
	 * @param $item
	 * @return string
	 */
	private function _getTagTextContainerName($item)
	{
		return in_array(data_get($item, 'meta', ''), ['email', 'userName']) === false ? 'textNode' : 'placeholder';
	}
	
	/**
	 * @param $text
	 * @param $type
	 * @param $item
	 * @param $meta
	 */
	private function _setItemTranslation(&$text, &$item, $type, $meta)
	{
		$attributeName = $this->_getTagTextContainerName($item);
		
		if (!$type)
		{
			$item->{$attributeName} = data_get($text, $meta);
		}
		else
		{
			if (!$this->_isDefaultText($item, $text))
			{
				$item->{$attributeName} = data_get($text, $meta);
			}
		}
		
		if (is_array($text))
		{
			unset($text[$meta]);
		}
		else if (is_object($text))
		{
			unset($text->{$meta});
		}
	}
}