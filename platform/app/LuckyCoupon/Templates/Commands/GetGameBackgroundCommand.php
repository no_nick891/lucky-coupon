<?php  namespace LuckyCoupon\Templates\Commands;

use LuckyCoupon\BaseCommand;
use Symfony\Component\HttpFoundation\File\File;

class GetGameBackgroundCommand extends BaseCommand {

	/**
	 * @var
	 */
	private $userId;
	/**
	 * @var
	 */
	private $gameId;
	/**
	 * @var
	 */
	private $fileName;
	
	/**
	 * GetGameBackgroundCommand constructor.
	 * @param $userId
	 * @param $gameId
	 * @param $fileName
	 * @internal param Request $request
	 */
	public function __construct($userId, $gameId, $fileName)
	{
		$this->userId = $userId;
		
		$this->gameId = $gameId;
		
		$this->fileName = $fileName;
	}
	
	public function handle()
	{
		try
		{
			return strstr($this->fileName, 'default') > -1
				? $response = $this->_getDefaultImage()
				: $this->_getLoadedImage();
		}
		catch (\Exception $e)
		{
			return $this->response(['background_file' => 'data:' . ';base64,']);
		}
	}
	
	/**
	 * @return mixed
	 */
	protected function _getLoadedImage()
	{
		$dirPath = "public/images/{$this->userId}/{$this->gameId}/";
		
		$files = \Storage::files($dirPath);
		
		$filePath = '';
		
		foreach ($files as $file)
		{
			if ($this->fileName && strstr($file, $this->fileName) > -1)
			{
				$filePath = $file;
			}
			elseif (strstr($file, 'background_image') > -1)
			{
				$filePath = $file;
			}
		}
		
		$file = '';
		
		$mimeType = '';
		
		if (\Storage::disk('local')->exists($filePath))
		{
			$file = \Storage::disk('local')->get($filePath);
			
			$mimeType = $this->_getMimeType(storage_path('app/' . $filePath));
		}
		
		return $this->_getImageResponse($mimeType, $file);
	}
	
	/**
	 * @param $fullPath
	 * @return null|string
	 */
	protected function _getMimeType($fullPath)
	{
		return (new File($fullPath))->getMimeType();
	}
	
	/**
	 * @param $mimeType
	 * @param $file
	 * @return mixed
	 */
	protected function _getImageResponse($mimeType, $file)
	{
		return $this->response(['background_file' => 'data:' . $mimeType . ';base64,' . base64_encode($file)]);
	}
	
	/**
	 * @return mixed
	 */
	protected function _getDefaultImage()
	{
		$initPath = 'img/game/' . ($this->gameId === 'gifts' ? 'gifts/' : '');
		
		$fullPath = public_path($initPath . $this->fileName);
		
		$file = file_get_contents($fullPath);
		
		$mimeType = $this->_getMimeType($fullPath);
		
		$this->_getImageResponse($mimeType, $file);
		
		$response = $this->_getImageResponse($mimeType, $file);
		
		return $response;
	}
}