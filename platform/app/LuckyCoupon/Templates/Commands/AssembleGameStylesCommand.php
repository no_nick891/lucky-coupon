<?php namespace LuckyCoupon\Templates\Commands;

use LuckyCoupon\Seeds\Helper;

class AssembleGameStylesCommand
{
	private $gameStylesPath;
	
	/**
	 * AssembleGameStylesCommand constructor.
	 * @param $gameStylesPath
	 */
	public function __construct($gameStylesPath)
	{
		$this->gameStylesPath = $gameStylesPath;
		
		$this->helper = new Helper();
	}
	
	public function handle()
	{
		$commonStyles = $this->_getCommonStyles();
		
		$typeStyles = $this->helper->getProcessedStyles($this->gameStylesPath);
		
		return '%27' . $typeStyles . $commonStyles . '%27';
	}
	
	/**
	 * @return string
	 */
	private function _getCommonStyles()
	{
		$commonStyles = '';
		
		$styles = ['modal', 'bar', 'animation', 'trigger'];
		
		foreach ($styles as $style)
		{
			$commonStyles .= $this->helper->getProcessedStyles($this->_getCommonPath($style));
		}
		
		return $commonStyles;
	}
	
	/**
	 * @param $style
	 * @return string
	 */
	private function _getCommonPath($style)
	{
		return 'resources/assets/js/helpers/templates/common/' . $style . '.css';
	}
}