<?php namespace LuckyCoupon\Templates\Commands;

use App;
use LuckyCoupon\Seeds\Helper;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Settings\SettingsEloquentRepository;
use LuckyCoupon\Sites\EloquentSiteRepository;
use LuckyCoupon\Coupons\EloquentCouponRepository;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Templates\EloquentTemplateRepository;
use Redis;

class GenerateScriptCommand
{
	/**
	 * @var EloquentTemplateRepository
	 */
	private $templateRepo;
	
	/**
	 * @var EloquentCouponRepository
	 */
	private $couponRepo;
	
	/**
	 * @var EloquentSiteRepository
	 */
	private $siteRepo;
	
	/**
	 * @var EloquentGameRepository
	 */
	private $gameRepo;
	
	/**
	 * @var EloquentSettingRepository
	 */
	private $settingRepo;
	
	/**
	 * @var
	 */
	private $siteId;
	
	/**
	 * @var Helper
	 */
	private $helper;
	
	/**
	 * User email which ignore active/inactive game state
	 * @var string
	 */
	private $specialUser = 'homepage@gmail.com';
	
	private $siteUser;
	
	private $isPoweredByVisible;
	
	/**
	 * GenerateScript constructor.
	 * @param $siteId
	 */
	public function __construct($siteId)
	{
		$this->siteId = $siteId;
		
		$this->templateRepo = new EloquentTemplateRepository();
		
		$this->couponRepo = new EloquentCouponRepository();
		
		$this->siteRepo = new EloquentSiteRepository();
		
		$this->gameRepo = new EloquentGameRepository();
		
		$this->settingRepo = new SettingsEloquentRepository();
		
		$this->oldSettingRepo = new EloquentSettingRepository();
		
		$this->helper = new Helper();
	}
	
	/**
	 * @return string
	 */
	public function handle()
	{
		$site = $this->siteRepo->getById($this->siteId);
		
		if ($this->_isAccessDenied($site)) return '';
		
		$cachedScript = Redis::get('script:site:' . $this->siteId);
		
		return $cachedScript ? $cachedScript : $this->_getScript($this->siteId);
	}
	
	/**
	 * @param $site
	 * @return bool
	 */
	private function _isAccessDenied($site)
	{
		return $this->_isActiveSite(data_get($site, 'active', 0))
			|| $this->_detectIE()
			|| dispatch(new GetOverConditionsCommand(data_get($site, 'user_id', 0)));
	}
	
	/**
	 * @param $active
	 * @return bool
	 */
	private function _isActiveSite($active)
	{
		return !isset($active) || $active === 0;
	}
	
	/**
	 * @return bool
	 */
	private function _detectIE()
	{
		$ua = htmlentities(data_get($_SERVER, 'HTTP_USER_AGENT', ''), ENT_QUOTES, 'UTF-8');
		
		return preg_match('~MSIE|Internet Explorer~i', $ua)
			|| (strpos($ua, 'Trident/7.0; rv:11.0') !== false)
			|| (strpos($ua, 'Edge/') !== false);
	}
	
	/**
	 * @todo: refactor this method
	 * @param $siteId
	 * @return string
	 */
	private function _getScript($siteId)
	{
		$games = $this->gameRepo->getAllBySiteId(
			$siteId,
			! $this->_isSpecialAccount($siteId)
		);
		
		if ( ! ($games && $games->count())) return '';
		
		$this->isPoweredByVisible = $this->_getPoweredByVisibility();
		
		$modal = $this->_getGamesModal($games);
		
		$scriptString = $this->_getScriptString($siteId, $modal);
		
		Redis::set('script:site:' . $this->siteId, $scriptString);
		
		return $scriptString;
	}
	
	/**
	 * Check to show all games for special account
	 * even if the games became inactive
	 * @param $siteId
	 * @return bool
	 */
	private function _isSpecialAccount($siteId)
	{
		$this->siteUser = $this->siteRepo->model
			->select(['u.id', 'u.app_id', 'u.email', 'p.amount', 'p.description', 'lk.id as license_id'])
			->leftJoin('users as u', 'sites.user_id', '=', 'u.id')
			->leftJoin('shopify_subscriptions as ss', 'ss.user_id', '=', 'u.id')
			->leftJoin('plans as p', 'p.id', '=', 'ss.plan_id')
			->leftJoin('license_keys as lk', 'lk.user_id', '=', 'u.id')
			->where('sites.id', $siteId)
			->first();
		
		return $this->siteUser && $this->siteUser->email === $this->specialUser;
	}
	
	/**
	 * @param $games
	 * @return array|mixed
	 */
	private function _getGamesModal($games)
	{
		$modal = [];
		
		foreach ($games as $game)
		{
			$modal = $this->_getGameModal($game, $modal);
		}
		
		return array_map('json_encode', $modal);
	}
	
	/**
	 * @param $game
	 * @param $modal
	 * @return mixed
	 */
	private function _getGameModal($game, $modal)
	{
		$gameType = $game->type !== 'coupon' ? $game->type : 'default';
		
		$paths = $this->templateRepo->getByPart($gameType);
		
		$settings = $this->_getSettingsByGameId($game->id);
		
		$language = $this->_getLanguage($settings);
		
		$modal['modalWrapper'][$game->id] = base64_encode(
			dispatch(new GetGameModalWrapperCommand(
				$paths['modalWrapper'],
				$game,
				$language,
				data_get($settings, 'text', [])
			))
		);
		
		// @todo: rename jsonModalContent key to modalContent
		// @todo: check the scripts on front-end in .vue templates
		$modal['jsonModalContent'][$game->id] = base64_encode(
			dispatch(new GetGameModalContentCommand(
				$paths['modalContent'],
				$game->id,
				$gameType,
				$settings,
				$language
			))
		);
		
		$modal['style'][$game->id] = $this->_safeEncodeStyles(
			dispatch(new GetGameStylesCommand(
				$settings,
				$paths['modalStyle'],
				$game->id,
				$gameType
			))
		);
		
		data_set(
			$settings,
			'behavior.isPoweredByVisible',
			is_int($this->siteUser->license_id) ? '0' : $this->isPoweredByVisible
		);
		
		$modal['behavior'][$game->id] = base64_encode(json_encode(data_get($settings, 'behavior')));
		
		return $modal;
	}
	
	/**
	 * @param $styles
	 * @return string
	 */
	private function _safeEncodeStyles($styles)
	{
		return base64_encode(str_replace('\'', '%22', $styles));
	}
	
	/**
	 * @param $siteId
	 * @param $gamesData
	 * @return string
	 */
	private function _getScriptString($siteId, $gamesData)
	{
		return $this->_uglify(
			$this->_getJsLogic() . "
			(
				window,document,'_lkda','script',
				'{$this->_getRuntimePath()}',
				JSON.parse('{$gamesData['modalWrapper']}'),
				JSON.parse('{$gamesData['jsonModalContent']}'),
				JSON.parse('{$gamesData['style']}'),
				{$siteId},
				JSON.parse('{$gamesData['behavior']}')
			);"
		);
	}
	
	/**
	 * @return bool|string
	 */
	private function _getVersion()
	{
		return $this->_getResources('RuntimeVersion');
	}
	
	/**
	 * @return string
	 */
	private function _getJsLogic()
	{
		return $this->_uglify($this->_getResources('preprocess-modal-data.js'));
	}
	
	/**
	 * @param $filename
	 * @return bool|string
	 */
	private function _getResources($filename)
	{
		$path = app_path('LuckyCoupon/Templates/Resources/'. $filename);
		
		return file_get_contents($path);
	}
	
	/**
	 * @TODO: Make instead of str_replace function getter of file that will
	 *      @todo: be minified with google closure compiler and dev file which
	 *      @todo: will serve as source for minification.
	 * @param $fileString
	 * @return mixed
	 */
	private function _uglify($fileString)
	{
		return str_replace(["\t", "\n"], '', $fileString);
	}
	
	/**
	 * @return mixed
	 */
	private function _getRuntimePath()
	{
		$urlScript = $this->_getScriptPath();
		
		return $this->_protocolReplace($urlScript) . '?v' . $this->_getVersion();
	}
	
	/**
	 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
	 */
	private function _getScriptPath()
	{
		return App::environment('local')
			? url('/js/runtime.dev.js')
			: url('/js/runtime.js');
	}
	
	/**
	 * @param $urlScript
	 * @return mixed
	 */
	private function _protocolReplace($urlScript)
	{
		return str_replace(['https:', 'http:'], '', $urlScript);
	}
	
	/**
	 * @param $settings
	 * @return mixed
	 */
	private function _getInputType($settings)
	{
		$inputFieldType = 'email';
		
		if (isset($settings['behavior']['chooseInputFieldType']))
		{
			if ($settings['behavior']['chooseInputFieldType']['facebookMessengerField'] === '1')
			{
				$inputFieldType = 'chatchamp';
			}
		}
		
		return $inputFieldType;
	}
	
	/**
	 * @param $settings
	 * @return mixed
	 */
	private function _getLanguage($settings)
	{
		return data_get($settings, 'behavior.language', 'en');
	}
	
	/**
	 * @return string
	 */
	private function _getPoweredByVisibility()
	{
		if ($this->siteUser->app_id > 0)
		{
			return $this->_isFullPlan() ? '0' : '1';
		}
		
		return '1';
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	private function _getSettingsByGameId($gameId)
	{
		$settings = $this->settingRepo->getByGameId($gameId);
		
		return $settings ? $settings : $this->oldSettingRepo->getByGameId($gameId);
	}
	
	/**
	 * @return bool
	 */
	private function _isFullPlan()
	{
		return $this->siteUser->amount > 0
			&& !(strstr($this->siteUser->description, 'powered by on') > -1);
	}
}