<?php namespace LuckyCoupon\Templates;

interface TemplateRepositoryInterface
{
	public function getModalContent($template = 'default');
	public function getModalStyle($template = 'default');
	public function getModalWrapper($template = 'default');
}