(function (w,d,v,g,p,mw,mc,ms,id,mb,t,s) {
	var DEFAULT_MODAL = 'game_preview',
		hostName = (p.split( '/' ))[2];
	initGlobalData();
	if (typeof mw.id !== 'undefined') {
		setModalData(DEFAULT_MODAL, {
			'mw': mw, 'mc': mc, 'ms': ms,
			'mb': mb, 'p': p
		});
	} else {
		for (var index in mw) {
			if (!isDataHasIndex(index)) continue;
			setModalData(index, {
				'mw': decode(mw[index]), 'mc': decode(mc[index]),
				'ms': decode(ms[index]), 'mb': decode(mb[index]),
				'p': p
			});
		}
	}
	w[d] = w[d]||[];t=d.createElement(g);
	t.async=1;t.src=p;s=d.getElementsByTagName(g)[0];s.parentNode.insertBefore(t,s);
	function initGlobalData() {
		if (typeof w[v] === 'undefined') w[v] = {};
	}
	function decode(data) {
		return JSON.parse(atob(data));
	}
	function setModalData(gameId, data) {
		w[v]['game_' + gameId] = getStructuredData(getPreprocessed(data));
	}
	function getPreprocessed(data) {
		data['mw'].id = eval(decodeURI(data['mw'].id));
		data['ms'].assetUrl = '//' + hostName + '/img/game/';
		data['ms'].hostUrl = '//' + hostName;
		data['ms'].font = decodeURI(data['ms'].font);
		data['ms'].style = getModalStyle.bind(data['ms']);
		return data;
	}
	function isDataHasIndex(index) {
		return mw.hasOwnProperty(index)
			&& mc.hasOwnProperty(index)
			&& ms.hasOwnProperty(index)
			&& mb.hasOwnProperty(index);
	}
	function getStructuredData(data) {
		return {
			modal: {
				modalStyle: data['ms'],
				modalWrapper: data['mw'],
				modalContent: data['mc'],
				modalBehavior: data['mb']
			},
			__scriptPath: data['p']
		};
	}
	function getModalStyle() {
		return eval(this.modalStyle.replace(/%27/g, "'").replace(/%22/g, '"'));
	}
})