<?php namespace LuckyCoupon\LicenseKeys;

class LicenseKeyEloquentRepository implements LicenseKeyRepositoryInterface
{
	public $model;
	
	/**
	 * LicenseKeyRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new LicenseKey();
	}
	
	/**
	 * @param $userId
	 * @return array
	 */
	public function getUserLicenses($userId)
	{
		$result = $this->model
			->select(['id', 'license_key'])
			->where('user_id', $userId)
			->get();
		
		return $result->count() > 0 ? $result : [];
	}
	
	/**
	 * @param $userId
	 * @param $licenseKey
	 * @return mixed
	 */
	public function setUserLicense($userId, $licenseKey)
	{
		$license = $this->model
				->where('license_key', $licenseKey)
				->first();
		
		$result = false;
		
		if ($license)
		{
			if ($license->update(['user_id' => $userId]))
			{
				$result = $license->id;
			}
		}
		
		return $result;
	}
	
	/**
	 * @param bool $isAcquired
	 * @return array
	 */
	public function getAllLicenses($isAcquired = false)
	{
		$query = $this->model
			->select(['id', 'license_key'])
			->orderBy('id');
		
		if ($isAcquired)
		{
			$query = $query->whereUserId(0);
		}
		
		$result = $query->get();
		
		return $result->count() > 0 ? $result : [];
	}
	
	/**
	 * @param $licenseKey
	 * @return mixed
	 */
	public function getEmptyLicenseKey($licenseKey)
	{
		return $this->model
			->where('license_key', $licenseKey)
			->where('user_id', 0)
			->first();
	}
}