<?php namespace LuckyCoupon\LicenseKeys;

use Illuminate\Database\Eloquent\Model;

class LicenseKey extends Model
{
    public $fillable = ['license_key', 'user_id'];
}
