<?php  namespace LuckyCoupon\LicenseKeys\Commands; 

use LuckyCoupon\BaseCommand;
use LuckyCoupon\LicenseKeys\LicenseKeyEloquentRepository;

class AcquireLicenseToUserCommand extends BaseCommand {

	protected $request;
	/**
	 * @var
	 */
	private $licenseKey;
	/**
	 * @var
	 */
	private $user;
	
	private $licenseRepo;
	
	/**
	 * AcquireLicenseToUserCommand constructor.
	 * @param $licenseKey
	 * @param $user
	 */
	public function __construct($licenseKey, $user)
	{
		$this->licenseKey = $licenseKey;
		
		$this->user = $user;
		
		$this->licenseRepo = new LicenseKeyEloquentRepository();
	}
	
	public function handle()
	{
		$userId = data_get($this->user, 'id', false);
		
		if (!$userId) return false;
		
		$license = $this->licenseRepo->getEmptyLicenseKey($this->licenseKey);
		
		if (!data_get($license, 'id', false)) return false;
		
		return $license->fill(['user_id' => $userId])->save();
	}
}