<?php  namespace LuckyCoupon\LicenseKeys\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\LicenseKeys\LicenseKeyEloquentRepository;

class AddLicenseKeyCommand extends BaseCommand {

	protected $request;
	
	private $licenseRepo;
	
	/**
	 * AddLicenseKeyCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->licenseRepo = new LicenseKeyEloquentRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->response($errs);
		
		$data = $this->getRequestData();
		
		return ['license_key' => $this->licenseRepo->setUserLicense($data['user_id'], $data['license_key'])];
	}
}