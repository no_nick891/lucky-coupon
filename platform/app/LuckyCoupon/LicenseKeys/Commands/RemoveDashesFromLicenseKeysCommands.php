<?php  namespace LuckyCoupon\LicenseKeys\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\LicenseKeys\LicenseKeyEloquentRepository;

class RemoveDashesFromLicenseKeysCommands extends BaseCommand {
	
	private $licenseRepo;
	
	/**
	 * RemoveDashesFromLicenseKeys constructor.
	 */
	public function __construct()
	{
		$this->licenseRepo = new LicenseKeyEloquentRepository();
	}
	
	public function handle()
	{
		$licenseKeys = $this->licenseRepo->model->get();
		
		foreach ($licenseKeys as $key)
		{
			$key->license_key = str_replace('-', '', $key->license_key);
			
			$key->save();
		}
	}
}