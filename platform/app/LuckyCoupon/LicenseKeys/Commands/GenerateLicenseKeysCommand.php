<?php  namespace LuckyCoupon\LicenseKyes\Commands; 

use LuckyCoupon\LicenseKeys\LicenseKey;
use LuckyCoupon\BaseCommand;

class GenerateLicenseKeysCommand extends BaseCommand {
	/**
	 * @var
	 */
	private $count;
	
	private $model;
	
	
	/**
	 * GenerateLicenseKeysCommand constructor.
	 * @param $count
	 */
	public function __construct($count)
	{
		$this->count = $count;
		
		$this->model = new LicenseKey();
	}
	
	public function handle()
	{
		$num = 0;
		
		for ($i = 1; $i <= $this->count; $i++)
		{
			if ($this->_saveGeneratedCode())
			{
				$num++;
			}
		}
		
		return ['saved' => $num];
	}
	
	/**
	 * @return bool
	 */
	private function _saveGeneratedCode()
	{
		$time = microtime(true);
		
		$hash = mb_strtoupper(md5(uniqid($time)));
		
		$license = new LicenseKey(['license_key' => $hash]);
		
		return $license->save();
	}
}