<?php  namespace LuckyCoupon\LicenseKeys\Commands; 

use Illuminate\Support\Facades\Storage;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\LicenseKeys\LicenseKeyEloquentRepository;

class PutLicenseKeysFileCommand extends BaseCommand {
	/**
	 * @var bool
	 */
	private $onlyNotAcquired;
	
	private $licenseRepo;
	
	/**
	 * PutLicenseKeysFileCommand constructor.
	 * @param bool $onlyNotAcquired
	 */
	public function __construct($onlyNotAcquired = true)
	{
		$this->onlyNotAcquired = $onlyNotAcquired;
		
		$this->licenseRepo = new LicenseKeyEloquentRepository();
	}
	
	public function handle()
	{
		$licensesString = '';
		
		$fileName = 'LicenseKeys.csv';
		
		$licenses = $this->licenseRepo->getAllLicenses(!$this->onlyNotAcquired);
		
		foreach ($licenses as $license)
		{
			$licensesString .= $license->id . ', ' . $license->license_key . PHP_EOL;
		}
		
		$result = Storage::put($fileName, $licensesString);
		
		return ['file' => $fileName, 'status' => $result];
	}
}