<?php
/**
 * Created by PhpStorm.
 * User: nikita
 * Date: 7/23/18
 * Time: 5:39 PM
 */

namespace LuckyCoupon\LicenseKeys;


interface LicenseKeyRepositoryInterface
{
	public function getAllLicenses($isAcquired = false);
	public function getEmptyLicenseKey($licenseKey);
	public function getUserLicenses($userId);
}