<?php namespace LuckyCoupon\Users\Commands;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\Commands\GetGamesByUsersCommand;
use LuckyCoupon\ShopifyApps\ShopifyApp;
use LuckyCoupon\Statistics\Commands\GetFilteredUsersRows;
use ShopifyIntegration\Apps\Commands\GetAppsCommand;

class GetShopifyUsersAdminViewCommand extends BaseCommand
{
	/**
	 * @var Request
	 */
	protected $request;
	
	/**
	 * GetShopifyUsersAdminViewCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$query = $this->_getQuery();
		
		$usersRaw = dispatch(new GetFilteredUsersRows(
			data_get($query, 'filter', 'created'),
			'app_id > 0',
			$this->request->get('value', '')
		));
		
		$users = $this->_getPagination(
			$usersRaw,
			\App::environment('local') ? 3 : 25,
			['path' => '/admin/shopify', 'query' => $query]
		);
		
		$usersArray = $users->toArray()['data'];
		
		$currentPage = $users->currentPage();
		
		$games = dispatch(new GetGamesByUsersCommand($usersArray));
		
		$apps = dispatch(new GetAppsCommand($usersArray));
		
		$payingApps = $this->_payingApps();
		
		$view = view('admin.dashboard', compact(['users', 'apps', 'games', 'currentPage', 'payingApps']));
		
		return $this->getResponseWidthAdminExit($view);
	}
	
	/**
	 * @param $usersRaw
	 * @param $perPage
	 * @param $options
	 * @return LengthAwarePaginator
	 */
	private function _getPagination($usersRaw, $perPage, $options)
	{
		$currentPage = LengthAwarePaginator::resolveCurrentPage();
		
		$collection = new Collection($usersRaw);
		
		$pageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();
		
		$users = new LengthAwarePaginator($pageSearchResults,
			count($collection),
			$perPage,
			null,
			$options);
		return $users;
	}
	
	/**
	 * @return array
	 */
	private function _getQuery()
	{
		$query['filter'] = $this->request->get('filter', null);
		
		$query['value'] = $this->request->get('value', null);
		
		return array_filter($query);
	}
	
	/**
	 * @return int
	 */
	private function _payingApps()
	{
		return ShopifyApp::where('is_paying', '=', 1)
				->where('deleted', '=', 0)
				->count();
	}
	
}