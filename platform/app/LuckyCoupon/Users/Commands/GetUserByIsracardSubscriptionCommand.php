<?php namespace LuckyCoupon\Users\Commands;

use App\LuckyCoupon\Users\User;
use LuckyCoupon\BaseCommand;

class GetUserByIsracardSubscriptionCommand extends BaseCommand
{
	private $userModel;
	
	/**
	 * GetUserByIsracardSubscriptionCommand constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->userModel = new User();
	}
	
	public function handle()
	{
		if (isset($this->request->sub_payme_code))
		{
			$paymeCode = $this->request->sub_payme_code;
			
			$user = new User();
			
			$result = $user->whereHas('isracardSubscription', function($query) use ($paymeCode) {
				
				$query->where('payme_code', $paymeCode);
				
			})->get();
			
			return $result->count() > 0 ? $result[0] : false;
		}
		
		return false;
	}
}