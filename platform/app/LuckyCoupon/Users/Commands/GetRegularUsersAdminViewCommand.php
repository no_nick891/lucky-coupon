<?php namespace LuckyCoupon\Users\Commands;

use App\LuckyCoupon\Users\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\Commands\GetGamesByUsersCommand;
use LuckyCoupon\Games\Commands\GetGamesStatisticsCommand;

/**
 * Class GetRegularUsersAdminViewCommand
 * @package LuckyCoupon\Users\Commands
 */
class GetRegularUsersAdminViewCommand extends BaseCommand
{
	/**
	 * GetRegularUsersAdminViewCommand constructor.
	 * @param $request Request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function handle()
	{
		$users = $this->_getUsers();
		
		$games = dispatch(new GetGamesByUsersCommand($users));
		
		$apps = $this->_getFakeApps($users);
		
		$gamesStatistics = dispatch(new GetGamesStatisticsCommand($games));
		
		$totalActive = $this->_getActiveUsersCount($users);
		
		$view = view('admin.dashboard', compact(['users', 'apps', 'games', 'totalActive', 'gamesStatistics']));
		
		return $this->getResponseWidthAdminExit($view);
	}
	
	/**
	 * @param $regularUsers
	 * @return int
	 */
	private function _getActiveUsersCount($regularUsers)
	{
		$counter = 0;
		
		foreach ($regularUsers as $regularUser)
		{
			foreach ($regularUser->isracardSubscription as $sub)
			{
				if ($sub->isracard_status === 2)
				{
					$counter++;
					
					continue;
				}
			}
			
			if ($regularUser->otherSubsctiption)
			{
				$other = $regularUser->otherSubscription->toArray();
				
				if ($other['active'] === 1)
				{
					$counter++;
					
					continue;
				}
			}
		}
		
		return $counter;
	}
	
	/**
	 * @param $regularUsers
	 * @return array
	 */
	private function _getFakeApps($regularUsers)
	{
		$apps = [];
		
		foreach ($regularUsers as $regularUser)
		{
			$paymentDate = $this->_getUserSubscriptionPayDate($regularUser);
			
			$urls = $this->_getUserSites($regularUser);
			
			$apps[$regularUser->id] = [
				'id' => $regularUser->id,
				'name' => $urls,
				'date_create' => $paymentDate,
				'last_login' => $regularUser->last_login,
				'installed' => boolval($paymentDate),
			    'isracard' => true
			];
		}
		
		return $apps;
	}
	
	/**
	 * @param $regularUser
	 * @return bool
	 */
	private function _getUserSubscriptionPayDate($regularUser)
	{
		foreach ($regularUser->isracardSubscription as $sub)
		{
			if ($sub->isracard_status === 2 && $sub->sub_payment_date)
			{
				return $sub->sub_payment_date;
			}
		}
		
		return false;
	}
	
	/**
	 * @param $regularUser
	 * @return string
	 */
	private function _getUserSites($regularUser)
	{
		$urls = '';
		
		foreach ($regularUser->sites as $site)
		{
			$url = $site->url;
			
			$urls .= '<a href="' . $url . '">' . $url . '</a><br/>';
		}
		
		return $urls;
	}
	
	/**
	 * @return array
	 */
	private function _getUsers()
	{
		$filter = $this->request->get('filter', null);
		
		$value = $this->request->get('value', null);
		
		$query = User::where('app_id', '=', 0)
			->where('is_admin', 0);
		
		if ($value)
		{
			$query = $query->where('email', $value);
		}
		
		return $query->paginate(25);
	}
	
}