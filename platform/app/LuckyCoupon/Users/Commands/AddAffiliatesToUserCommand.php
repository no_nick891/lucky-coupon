<?php namespace LuckyCoupon\Users\Commands;

use App\LuckyCoupon\Users\User;
use LuckyCoupon\AffiliateOwners\AffiliateOwner;
use LuckyCoupon\Affiliates\Affiliate;
use LuckyCoupon\BaseCommand;

class AddAffiliatesToUserCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $user;
	/**
	 * @var
	 */
	private $ref;
	
	/**
	 * AddAffiliatesToUserCommand constructor.
	 * @param User $user
	 * @param int $ref
	 */
	public function __construct($user, $ref)
	{
		$this->user = $user;
		
		$this->ref = $ref;
	}
	
	public function handle()
	{
		$referenceInit = 350;
		
		$this->_saveAffiliateOwner($referenceInit);
		
		if ($this->ref && $this->ref > $referenceInit)
		{
			$referralUserId = (int)$this->ref - $referenceInit;
			
			$this->_saveAffiliate($referralUserId);
		}
	}
	
	/**
	 * @param $referenceInit
	 */
	private function _saveAffiliateOwner($referenceInit)
	{
		$id = $this->user->id;
		
		$affiliateOwner = new AffiliateOwner([
			'user_id' => $id,
			'reference' => $referenceInit + $id,
			'paypal_email' => ''
		]);
		
		$this->user->affiliateOwner()->save($affiliateOwner);
	}
	
	/**
	 * @param int $referralUserId
	 */
	private function _saveAffiliate($referralUserId)
	{
		$referralUser = User::whereId($referralUserId)->limit(1)->get();
		
		if ($referralUser->count() > 0)
		{
			$affiliate = new Affiliate([
				'owner_id' => $referralUserId,
				'user_id' => $this->user->id,
				'amount_paid' => 0,
				'commission' => 0,
				'is_payed' => 0
			]);
			
			$referralUser[0]->affiliates()->save($affiliate);
		}
	}
}