<?php namespace LuckyCoupon\Users\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Counters\Counter;
use LuckyCoupon\Counters\CounterEloquentRepository;
use LuckyCoupon\LicenseKeys\LicenseKeyEloquentRepository;
use LuckyCoupon\Plans\PlansEloquentRepository;
use ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrder;

class GetAuthUserDataCommand extends BaseCommand
{
	private $user;
	
	private $plansRepo;
	
	private $counterRepo;
	
	/**
	 * GetAuthUserDataCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->user = $this->request->user();
		
		$this->plansRepo = new PlansEloquentRepository();
		
		$this->counterRepo = new CounterEloquentRepository();
	}
	
	public function handle()
	{
		$this->_setUserSubscription();
		
		$this->user->affiliateOwner;
		
		$this->user->shopify_impressions = $this->counterRepo->getUserImpressions($this->user->id);;
		
		$this->user->shopify_hits = $this->counterRepo->getUserHits($this->user->id);
		
		$this->user->plans = $this->plansRepo->get($this->_getPlanType());
		
		$this->user->revenue_from_coupons = $this->_getUserOrdersSum();
		
		$this->_getLicenseKeys();
		
		return ['user' => $this->user];
	}
	
	private function _setUserSubscription()
	{
		$this->user->isracardSubscription = $this->user->isracardSubscription->where('isracard_status', 2)->first();
		
		$this->user->shopifySubscription = $this->user->shopifySubscription->first();
		
		$this->user->otherSubscription = $this->user->otherSubscription ? $this->user->otherSubscription : null;
	}
	
	/**
	 * @return null|string
	 */
	private function _getPlanType()
	{
		switch (true)
		{
			case $this->user->otherSubscription
				&& $this->user->otherSubscription->active:
				$planType = 'other';
				break;
			case $this->user->app_id > 0:
				$planType = 'shopify';
				break;
			case $this->user->app_id === 0:
				$planType = 'basic';
				break;
			default: $planType = null;
				break;
		}
		
		return $planType;
	}
	
	/**
	 * @return mixed
	 */
	private function _getUserOrdersSum()
	{
		$result = \DB::table('shopify_app_orders')
			->select([\DB::raw('sum(order_total_price) as total_price')])
			->where('app_id', $this->user->app_id)
			->where('order_status', 'paid')
			->where('coupon_id', '>', 0)
			->get();
		
		return (int)data_get($result, '0.total_price', 0) / 100;
	}
	
	/**
	 * @return bool
	 */
	private function _getLicenseKeys()
	{
		if (!data_get($this->user, 'otherSubscription.plan_id', false)) return false;
		
		$licenseRepo = new LicenseKeyEloquentRepository();
		
		foreach ($this->user->plans as $plan)
		{
			if ($this->user->otherSubscription->plan_id === $plan->id && $plan->name === 'Life time access')
			{
				$this->user->license_keys = $licenseRepo->getUserLicenses($this->user->id);
			}
		}
	}
	
}