<?php  namespace LuckyCoupon\Users\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Users\UserEloquentRepository;
use ShopifyIntegration\Apps\AppEloquentRepository;
use ShopifyIntegration\Commands\GetShopDataCommand;

class SaveShopUserDataCommand extends BaseCommand {

	private $shopUrl;
	
	private $appRepo;
	
	/**
	 * SaveShopUserEmailCommand constructor.
	 * @param $shopUrl
	 */
	public function __construct($shopUrl)
	{
		$this->shopUrl = $shopUrl;
		
		$this->appRepo = new AppEloquentRepository();
	}
	
	public function handle()
	{
		$shop = dispatch(new GetShopDataCommand($this->shopUrl));
		
		if ($app = $this->appRepo->getApp($this->shopUrl))
		{
			$app->email = $shop->email;
			
			$app->shopify_plan_name = $shop->plan_name;
			
			return $app->save();
		}
		
		return false;
	}
}