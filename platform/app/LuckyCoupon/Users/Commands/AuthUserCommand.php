<?php namespace LuckyCoupon\Users\Commands;

use Auth;
use LuckyCoupon\Users\UserEloquentRepository;

class AuthUserCommand
{
	private $userId;
	
	private $redirectUrl;
	
	/**
	 * AuthShopifyUserCommand constructor.
	 * @param int $userId
	 * @param $redirectUrl
	 */
	public function __construct($userId, $redirectUrl = '/')
	{
		$this->userId = $userId;
		
		$this->redirectUrl = $redirectUrl;
	}
	
	public function handle()
	{
		$userRepo = new UserEloquentRepository();
		
		$user = $userRepo->getUserById($this->userId);
		
		if ($user)
		{
			\Auth::login($user);
			
			return redirect($this->redirectUrl);
		}
		
		return redirect('/');
	}
}