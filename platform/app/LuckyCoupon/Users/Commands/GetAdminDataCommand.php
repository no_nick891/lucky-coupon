<?php namespace LuckyCoupon\Users\Commands;

use Illuminate\Encryption\Encrypter;
use LuckyCoupon\BaseCommand;

class GetAdminDataCommand extends BaseCommand
{
	
	/**
	 * ExitToAdminStores constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		return (int)base64_decode(str_replace('.p', '=', $this->request->cookie('exitToShops')));
	}
}