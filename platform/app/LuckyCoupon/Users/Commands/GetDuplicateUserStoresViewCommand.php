<?php  namespace LuckyCoupon\Users\Commands; 

use App\LuckyCoupon\Users\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateInterval;
use DatePeriod;
use LuckyCoupon\BaseCommand;

class GetDuplicateUserStoresViewCommand extends BaseCommand {
	
	public function handle()
	{
		$usersList = [];
		
		$selectedColumns = [
			'u.name as originalName',
			'u.created_at as originalCreatedAt',
			'ucp.name as duplicateName',
			'ucp.created_at as duplicateCreatedAt'
		];
		
		$datesBetween = $this->generateDateRange(new Carbon('2018-12-06'), new Carbon());
		
		foreach ($datesBetween as $date)
		{
			$dateFormatted = $date;
			
			$usersList[$dateFormatted] = User::select($selectedColumns)
				->from('users as u')
				->leftJoin('users as ucp', function($q){
					$q->on('u.name', '=', 'ucp.name')
						->on('ucp.created_at', '<>', 'u.created_at');
				})
				->whereRaw("DATE(u.created_at) = '{$dateFormatted}'")
				->where('u.app_id', '>', 0)
				->get();
		}
		
		return view('admin.shopify.duplicates', compact('usersList'));
	}
	
	private function generateDateRange(Carbon $start_date, Carbon $end_date)
	{
		$dates = [];
		
		for($date = $start_date; $date->lte($end_date); $date->addDay()) {
			$dates[] = $date->format('Y-m-d');
		}
		
		return $dates;
	}
}