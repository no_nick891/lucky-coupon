<?php namespace LuckyCoupon\Users\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Users\UserEloquentRepository;

class UpdateUserProfileCommand extends BaseCommand
{
	private $userRepo;
	
	private $user;
	
	/**
	 * UpdateUserProfileCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->user = $this->request->user();
		
		$this->userRepo = new UserEloquentRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->errorResponse($errs);
		
		$profileData = $this->getRequestData();
		
		$update = $this->_getUpdateArray($profileData);
		
		$updateResult = $this->userRepo->update($profileData['id'], $update);
		
		return ['user' => $updateResult];
	}
	
	/**
	 * @param $profileData
	 * @return array
	 */
	private function _getUpdateArray($profileData)
	{
		$result = [
			'password' => $this->_getPassword($profileData),
			'name' => $this->_getName($profileData),
			'email' => $this->_getEmail($profileData),
		    'is_first_time' => $this->_getIsFirstTime($profileData)
		];
		
		return array_filter($result, function($v) {
			return ($v !== NULL && $v !== FALSE && $v !== '');
		});
	}
	
	/**
	 * @param $profileData
	 * @return bool|string
	 */
	private function _getPassword($profileData)
	{
		return isset($profileData['password']) && $profileData['password'] !== ''
			? bcrypt($profileData['password']) : '';
	}
	
	/**
	 * @param $profileData
	 * @return string
	 */
	private function _getEmail($profileData)
	{
		return $this->user->email === $profileData['email'] ? '' : $profileData['email'];
	}
	
	/**
	 * @param $profileData
	 * @return mixed
	 */
	private function _getIsFirstTime($profileData)
	{
		return isset($profileData['is_first_time']) ? $profileData['is_first_time'] : $this->user->is_first_time;
	}
	
	/**
	 * @param $profileData
	 * @return string
	 */
	private function _getName($profileData)
	{
		return $this->user->name === $profileData['name'] ? '' : $profileData['name'];
	}
}