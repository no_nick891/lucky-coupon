<?php namespace LuckyCoupon\Users;

interface UserRepositoryInterface
{
	public function getUserByAppId($appId);
	public function getUserById($userId);
	public function getUser($fieldName, $value);
}