<?php namespace LuckyCoupon\Users;

use App\LuckyCoupon\Users\User;

class UserEloquentRepository implements UserRepositoryInterface
{
	public $model;
	
	/**
	 * UserEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new User();
	}
	
	/**
	 * @param $appId
	 * @return array
	 */
	public function getUserByAppId($appId)
	{
		return $this->getUser('app_id', $appId);
	}
	
	/**
	 * @param $userId
	 * @return array
	 */
	public function getUserById($userId)
	{
		return $this->getUser('id', $userId);
	}
	
	/**
	 * @param $field
	 * @param $value
	 * @return User|array
	 */
	public function getUser($field, $value)
	{
		$user = $this->model->where($field, $value)
			->orderBy('created_at', 'asc')
			->get();
		
		return $user->count() > 0 ? $user[0] : [];
	}
	
	/**
	 * @param $id
	 * @param $data
	 * @return mixed
	 */
	public function update($id, $data)
	{
		return $this->model->where('id', $id)->update($data);
	}
}