<?php namespace App\LuckyCoupon\Users;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'app_id', 'is_first_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function sites()
	{
		return $this->hasMany('LuckyCoupon\Sites\Site', 'user_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function games()
	{
		return $this->hasMany('App\LuckyCoupon\Games\Game', 'user_id', 'id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function apps()
	{
		return $this->hasOne('ShopifyIntegration\Apps\App', 'id', 'app_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function subscription()
	{
		return $this
			->hasOne('LuckyCoupon\Subscriptions\Subscription', 'user_id', 'id')
			->withDefault(['subscription_id' => 0,'payme_status' => false, 'isracard_status' => false, ]);
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function isracardSubscription()
	{
		return $this->hasMany('LuckyCoupon\IsracardSubscriptions\IsracardSubscription', 'user_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function shopifySubscription()
	{
		return $this->hasMany('LuckyCoupon\ShopifySubscriptions\ShopifySubscription', 'user_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function otherSubscription()
	{
		return $this->hasOne('LuckyCoupon\OtherSubscriptions\OtherSubscription', 'user_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function mailchimp()
	{
		return $this->hasOne('LuckyCoupon\MailchimpApps\MailchimpApp', 'user_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function klaviyo()
	{
		return $this->hasOne('LuckyCoupon\KlaviyoApps\KlaviyoApp', 'user_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function omnisend()
	{
		return $this->hasOne('LuckyCoupon\OmnisendApps\OmnisendApp', 'user_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function affiliates()
	{
		return $this->hasMany('LuckyCoupon\Affiliates\Affiliate', 'owner_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function affiliatesUser()
	{
		return $this->hasMany('LuckyCoupon\Affiliates\Affiliate', 'user_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function affiliateOwner()
	{
		return $this->hasOne('LuckyCoupon\AffiliateOwners\AffiliateOwner', 'user_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function chatchamp()
	{
		return $this->hasOne('LuckyCoupon\ChatChampApps\ChatChampApp', 'user_id');
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activecampaign()
    {
        return $this->hasOne('LuckyCoupon\ActiveCampaignApps\ActiveCampaignApp', 'user_id');
    }
}
