<?php namespace LuckyCoupon\IsracardSubscriptions\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class GetIsracardPaymentResultCommand extends BaseCommand
{
	/**
	 * GetIsracardResultWebhookCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$vars = $this->request->all();
		
		$isIsracardRequest = isset($vars['payme_status']) && $vars['payme_status'] === 'success';
		
		if ($isIsracardRequest)
		{
			$isPayed = dispatch(new WaitIsPayedIsracardSubCommand($this->request));
			
			return view('webhooks.payment_success', compact('isPayed'));
		}
		else
		{
			return view('webhooks.empty');
		}
	}
}