<?php namespace LuckyCoupon\IsracardSubscriptions\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\IsracardSubscriptions\IsracardSubscriptionsEloquentRepository;

class WaitIsPayedIsracardSubCommand extends BaseCommand
{
	private $subIsracard;
	
	/**
	 * GetPayedIsracardSubCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->subIsracard = new IsracardSubscriptionsEloquentRepository();
	}
	
	public function handle()
	{
		$subPayMeId = $this->request->sub_payme_id;
		
		return $this->_isActiveStatus($subPayMeId);
	}
	
	private function _isActiveStatus($subPayMeId)
	{
		$time = 0;
		
		while ($time <= 40)
		{
			$subscription = $this->subIsracard->getByPaymeId($subPayMeId)->toArray();
			
			if ($subscription['sub_payment_date'] !== null)
			{
				return true;
			}
			
			$step = $this->_getStep($time);
			
			$time = $time + $step;
			
			sleep($step);
		}
		
		return false;
	}
	
	/**
	 * @param $time
	 * @return int
	 */
	private function _getStep($time)
	{
		$step = 5;
		
		if ($time < 20)
		{
			$step = 10;
		}
		else if ($time < 40)
		{
			$step = 5;
		}
		
		return $step;
	}
}