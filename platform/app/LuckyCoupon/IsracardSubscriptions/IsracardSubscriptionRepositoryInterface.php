<?php namespace LuckyCoupon\IsracardSubscriptions;


interface IsracardSubscriptionRepositoryInterface
{
	public function add($subscription);
	public function updateByCode($code, $data);
	public function updateByPaymeID($paymeId, $data);
	public function deleteByPaymeID($paymeId);
}