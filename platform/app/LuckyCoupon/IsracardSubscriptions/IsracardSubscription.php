<?php namespace LuckyCoupon\IsracardSubscriptions;

use Illuminate\Database\Eloquent\Model;

class IsracardSubscription extends Model
{
	protected $table = 'isracard_subscriptions';
	
	protected $fillable = [
		'user_id',
		'payme_id',
		'payme_code',
		'payme_status',
		'isracard_status',
		'currency',
		'amount',
		'period',
		'description',
		'sub_url'
	];
}
