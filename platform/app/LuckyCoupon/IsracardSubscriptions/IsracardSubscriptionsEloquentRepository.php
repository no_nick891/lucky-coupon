<?php namespace LuckyCoupon\IsracardSubscriptions;

class IsracardSubscriptionsEloquentRepository implements IsracardSubscriptionRepositoryInterface
{
	private $model;
	
	public function __construct()
	{
		$this->model = new IsracardSubscription();
	}
	
	/**
	 * @param $subscription
	 * @return mixed
	 */
	public function add($subscription)
	{
		return $this->model->create($subscription)->id;
	}
	
	/**
	 * @param $paymeId
	 * @return mixed
	 */
	public function getByPaymeId($paymeId)
	{
		return $this->model->where('payme_id', $paymeId)->first();
	}
	
	/**
	 * @param $code
	 * @param $data
	 * @return mixed
	 */
	public function updateByCode($code, $data)
	{
		return $this->updateBy('payme_code', $code, $data);
	}
	
	/**
	 * @param $paymeId
	 * @param $data
	 * @return mixed
	 */
	public function updateByPaymeID($paymeId, $data)
	{
		return $this->updateBy('payme_id', $paymeId, $data);
	}
	
	/**
	 * @param $fieldName
	 * @param $needle
	 * @param $data
	 * @return mixed
	 */
	public function updateBy($fieldName, $needle, $data)
	{
		$where = is_array($needle) ? $needle : [$needle];
		
		return $this->model->whereIn($fieldName, $where)
			->update($data);
	}
	
	/**
	 * @param $paymeId
	 * @return mixed
	 */
	public function deleteByPaymeID($paymeId)
	{
		return $this->model
			->where('payme_id', $paymeId)
			->delete();
	}
}