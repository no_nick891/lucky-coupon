<?php namespace LuckyCoupon\Settings;

/**
 * Interface SettingRepositoryInterface
 * @package LuckyCoupon\Settings
 * @deprecated used only for users who have old way to store settings
 */
interface SettingRepositoryInterface {

	public function getById($id);
	public function getByGameId($gameId);
	public function getByGameIds($gameIds);
	public function deleteByGameId($gameId);
	public function insert($settings);
	public function update($settings);
	public function save($info, $bunch);
	public function delete($id);
	public function deleteGameSettings($gameId);
	public function getSettingByName($gameId, $current);
}