<?php namespace LuckyCoupon\Settings;

use App\LuckyCoupon\Settings\Setting;

class SettingsEloquentRepository implements InterfaceSettingsRepository
{
	private $modal;
	
	/**
	 * SettingsEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->modal = new Setting();
	}
	
	public function getById($id)
	{
		// TODO: Implement getById() method.
	}
	
	public function getByGameId($gameId)
	{
		$setting = $this->modal
			->whereGameId($gameId)
			->whereName('settings')
			->first();
		
		return json_decode(data_get($setting, 'value', false));
	}
	
	/**
	 * @param $data
	 * @return array
	 */
	private function _getSafeCharacters($data)
	{
		$result = [];
		
		foreach ($data as $key => $value)
		{
			$result[$key] = $this->_getValueCharacters($value);
		}
		
		return $result;
	}
	
	/**
	 * @param $value
	 * @return array|mixed
	 */
	private function _getValueCharacters($value)
	{
		return isObjOrArray($value)
			 ? $this->_getSafeCharacters($value)
			 : replaceQuotes($value);
	}
	
	public function getByGameIds($gameIds)
	{
		$settings = $this->modal
			->whereIn('game_id', $gameIds)
			->where('name', 'settings')
			->orderBy('game_id')
			->get()->toArray();
		
		return $this->_getSettingsByGameId($settings);
	}
	
	
	/**
	 * @param $settings
	 * @return array
	 */
	private function _getSettingsByGameId($settings)
	{
		$result = [];
		
		foreach ($settings as $setting)
		{
			$result[$setting['game_id']] = json_decode($setting['value']);
		}
		
		return $result;
	}
	
	public function deleteByGameId($gameId)
	{
		// TODO: Implement deleteByGameId() method.
	}
	
	/**
	 * @param $gameId
	 * @param $settings
	 * @return Setting|bool
	 */
	public function insert($gameId, $settings)
	{
		return $this->modal
			->create([
				'game_id' => $gameId,
				'name' => 'settings',
				'value' => $this->_convertSettings($settings)
			]);
	}
	
	public function update($gameId, $settings)
	{
		// TODO: Implement update() method.
	}
	
	/**
	 * @param $gameId
	 * @param $settings
	 * @return bool
	 */
	public function save($gameId, $settings)
	{
		$settings = $this->_convertSettings($settings);
		
		$gameSettings = $this->modal
			->whereGameId($gameId)
			->where('name', 'settings')
			->first();
		
		if (!$gameSettings) return $this->insert($gameId, $settings);
		
		return $gameSettings->update(['value' => $settings]);
	}
	
	/**
	 * @param $settings
	 * @return string
	 */
	private function _convertSettings($settings)
	{
		return isObjOrArray($settings) ? json_encode($this->_getSafeCharacters($settings)) : $settings;
	}
	
	public function delete($id)
	{
		// TODO: Implement delete() method.
	}
	
	public function deleteGameSettings($gameId)
	{
		// TODO: Implement deleteGameSettings() method.
	}
	
	public function getSettingByName($gameId, $current)
	{
		// TODO: Implement getSettingByName() method.
	}
}