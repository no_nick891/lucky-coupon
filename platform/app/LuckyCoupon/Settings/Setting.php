<?php namespace App\LuckyCoupon\Settings;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 * @package App\LuckyCoupon\Settings
 */
class Setting extends Model
{
	/**
	 * @var string
	 */
	protected $table = 'settings';
	
	/**
	 * @var bool
	 */
	public $timestamps = false;
	
	public $fillable = ['game_id', 'name', 'value'];
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function parent()
	{
		return $this->belongsTo('Setting', 'parent_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function children()
	{
		return $this->hasMany('Setting', 'parent_id');
	}
}
