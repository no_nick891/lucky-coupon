<?php namespace LuckyCoupon\Settings;

interface InterfaceSettingsRepository
{
	public function getById($id);
	public function getByGameId($gameId);
	public function getByGameIds($gameIds);
	public function deleteByGameId($gameId);
	public function insert($gameId, $settings);
	public function update($gameId, $settings);
	public function save($gameId, $settings);
	public function delete($id);
	public function deleteGameSettings($gameId);
	public function getSettingByName($gameId, $current);
}