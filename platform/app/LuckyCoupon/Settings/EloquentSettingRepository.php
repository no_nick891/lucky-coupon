<?php  namespace LuckyCoupon\Settings;

use App\LuckyCoupon\Settings\Setting;

/**
 * Class EloquentSettingRepository
 * @package LuckyCoupon\Settings
 * @deprecated used only for users who have old way to store settings
 */
class EloquentSettingRepository implements SettingRepositoryInterface
{
	/**
	 * @var Setting
	 */
	public $model;
	
	/**
	 * EloquentSettingRepository constructor.
	 * @deprecated used only for users who have old way to store settings
	 */
	function __construct()
	{
		$this->model = new Setting();
	}

	/**
	 * @param $id
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	public function getById($id)
	{
		$game = $this->model->findOrFail($id);

		return $game ? $game : [];
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function getByGameId($gameId)
	{
		$settings = $this->model->where('game_id', $gameId)->orderBy('parent_id')->get();
		
		$tree = $this->_makeTree($settings);
		
		return $tree;
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function deleteByGameId($gameId)
	{
		return $this->model->where('game_id', $gameId)->delete();
	}
	
	/**
	 * @param $gameIds
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	public function getByGameIds($gameIds)
	{
		$settings = $this->model
			->whereIn('game_id', $gameIds)
			->orderBy('parent_id', 'asc')
			->orderBy('game_id', 'asc')
			->get();
		
		$arrangedSettings = $this->_getArrangedByGameId($settings);
		
		return $this->_makeTreeFromArranged($arrangedSettings);
	}
	
	/**
	 * @param $settings
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _makeTree($settings)
	{
		if(count($settings) === 0) return [];
		
		$preTree = $this->_preTree($settings);
		
		return $this->_createTree($preTree, 0);
	}
	
	/**
	 * @param $settings
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _preTree($settings)
	{
		$tree = [];
		
		foreach ($settings as $setting)
		{
			$tree[$setting['parent_id']][] = $setting->toArray();
		}
		
		return $tree;
	}
	
	/**
	 * @param $list
	 * @param $id
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _createTree($list, $id)
	{
		$tree = [];
		
		$currentCoil = isset($list[$id]) ? $list[$id] : [];
		
		foreach ($currentCoil as $item)
		{
			if ($this->_isInlineSetting($item['name']))
			{
				$tree[$item['name']] = json_decode($item['value']);
			}
			else
			{
				$tree[$item['name']] = $this->_getValue($list, $item);
			}
		}
		
		return $tree;
	}
	
	/**
	 * @param $list
	 * @param $item
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _getValue($list, $item)
	{
		return ($item['value'] !== null) ? $this->_calculateValue($item) : $this->_createTree($list, $item['id']);
	}
	
	/**
	 * @param $settings
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function insert($settings)
	{
		return $this->model->insertGetId($settings);
	}
	
	/**
	 * @param $update
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function update($update)
	{
		$id = $update['id'];

		unset($update['id']);

		return $this->model->where('id', $id)->update($update);
	}
	
	/**
	 * @param $gameId
	 * @param $settingName
	 * @param $value
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function updateSetting($gameId, $settingName, $value)
	{
		$imageSetting = $this->getSettingByName($gameId, $settingName);
		
		$imageSetting->value = $value;
		
		return $imageSetting->save();
	}
	
	/**
	 * @param $gameId
	 * @param $current
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function getSettingByName($gameId, $current)
	{
		$setting = $this->model
			->where('game_id', $gameId)
			->where('name', $current)->get();
		
		return $setting->count() > 0 ? $setting[0] : [];
	}
	
	/**
	 * @param $info
	 * @param $bunch
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	public function save($info, $bunch)
	{
		$result = [];
		
		foreach ($bunch as $name => $value)
		{
			$result[] = $this->_addOrUpdate($info, $name, $value);
		}
		
		return $result;
	}
	
	/**
	 * @param $texts
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _handleArrayQuotes($texts)
	{
		$result = [];
		
		foreach ($texts as $key => $value)
		{
			$result[$key] = is_array($value) ? $this->_handleArrayQuotes($value) : $this->_replaceQuotes($value);
		}
		
		return $result;
	}
	
	/**
	 * @param $info
	 * @param $name
	 * @param $value
	 * @return array|mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _addOrUpdate($info, $name, $value)
	{
		$result = [];
		
		$settingOrm = $this->getSettingsByFilter($info, $name);
		
		$isGroup = $this->_isGroup($name, $value);
		
		if ($settingOrm)
		{
			$parentId = $settingOrm->id;
			
			if( ! $isGroup)
			{
				$settingOrm->value = $this->_preprocessors($value, $name);
				
				$result = $settingOrm->save();
			}
		}
		else
		{
			$result = $parentId = $this->insert([
				'game_id' => $info['game_id'],
				'parent_id' => $info['parent_id'],
				'name' => $name,
				'value' => $isGroup ? null : $this->_preprocessors($value, $name)
			]);
		}
		
		$info['parent_id'] = $parentId;
		
		if ($isGroup)
		{
			return $this->save($info, $value);
		}
		
		return $result;
	}
	
	/**
	 * @param $id
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function delete($id)
	{
		return $this->model->whereId($id)->delete();
	}
	
	/**
	 * @param $info
	 * @param $name
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function getSettingsByFilter($info, $name)
	{
		$result = $this->model
			->where('game_id', $info['game_id'])
			->where('parent_id', $info['parent_id'])
			->where('name', $name)->get();
		
		return isset($result[0]) ? $result[0] : false;
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	public function deleteGameSettings($gameId)
	{
		return $this->model->where('game_id', $gameId)->delete();
	}
	
	/**
	 * @param $item
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _calculateValue($item)
	{
		if ($item['name'] === 'opacity')
		{
			return ((float)$item['value']) * 100;
		}
		
		return $item['value'];
	}
	
	/**
	 * @param $settings
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _getArrangedByGameId($settings)
	{
		$arrangedSettings = [];
		
		foreach ($settings as $set)
		{
			$arrangedSettings[$set->game_id][] = $set;
		}
		
		return $arrangedSettings;
	}
	
	/**
	 * @param $arrangedSettings
	 * @return array
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _makeTreeFromArranged($arrangedSettings)
	{
		$result = [];
		
		foreach ($arrangedSettings as $id => $setting)
		{
			$result[$id] = $this->_makeTree($setting);
		}
		
		return $result;
	}
	
	/**
	 * @param $gameId
	 * @param $settingName
	 * @return bool
	 * @deprecated used only for users who have old way to store settings
	 */
	public function clearSettingValue($gameId, $settingName)
	{
		$setting = $this->getSettingByName($gameId, $settingName);
		
		$settingValue = $setting->value;
		
		$setting->value = '';
		
		return $setting->save() ? $settingValue : false;
	}
	
	/**
	 * @param $value
	 * @param string $name
	 * @return mixed
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _preprocessors($value, $name = '')
	{
		return $this->_isInlineSetting($name)
			? json_encode($this->_handleArrayQuotes($value))
			: replaceQuotes($value);
	}
	
	/**
	 * @param $name
	 * @return bool
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _isInlineSetting($name)
	{
		return in_array($name, ['text', 'couponReceiveEmailText']);
	}
	
	/**
	 * @param $name
	 * @param $value
	 * @return bool
	 * @deprecated used only for users who have old way to store settings
	 */
	private function _isGroup($name, $value)
	{
		return (is_array($value) && !$this->_isInlineSetting($name)) || $name === 0;
	}
}