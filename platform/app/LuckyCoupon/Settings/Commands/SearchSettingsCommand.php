<?php  namespace LuckyCoupon\Settings\Commands; 

use LuckyCoupon\BaseCommand;

class SearchSettingsCommand extends BaseCommand {

	protected $settings;
	/**
	 * @var
	 */
	private $name;
	/**
	 * @var
	 */
	private $value;
	
	private $currentValue;
	/**
	 * @var bool
	 */
	private $withOld;
	
	/**
	 * GetSettingsCommand constructor.
	 * @param $settings
	 * @param $name
	 * @param $value
	 * @param bool $withOld
	 */
	public function __construct($settings, $name, $value = null, $withOld = false)
	{
		$this->settings = $settings;
		
		$this->name = $name;
		
		$this->value = $value;
		
		$this->currentValue = null;
		
		$this->withOld = $withOld;
	}
	
	public function handle()
	{
		$result = $this->_getSettings($this->settings, is_null($this->value));
		
		return $this->withOld
			? ['settings' => $result, 'oldValue' => $this->currentValue]
			: $result;
	}
	
	/**
	 * @param $settings
	 * @param bool $onlyValue
	 * @return array|string
	 */
	private function _getSettings($settings, $onlyValue = false)
	{
		array_walk($settings, [$this, '_getSettingsValue']);
		
		return $onlyValue ? $this->currentValue : $settings;
	}
	
	/**
	 * @param $value
	 * @param $name
	 * @return array|string
	 */
	private function _getSettingsValue(&$value, $name)
	{
		return isObjOrArray($value)
			? $this->_getSettings($value)
			: $this->_getSettingValue($value, $name);
	}
	
	/**
	 * @param $value
	 * @param $key
	 * @return string
	 */
	public function _getSettingValue(&$value, $key)
	{
		if ($this->name === $key)
		{
			$this->currentValue = $value;
			
			if (!is_null($this->value))
			{
				$value = $this->value;
			}
		}
	}
}