<?php namespace LuckyCoupon\Settings\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class UpdateSettingEmailLogoCommand extends BaseCommand
{
	
	/**
	 * UpdateSettingEmailLogoCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->response($errs);
		
		$data = $this->getRequestData();
		
		$savedImageData = dispatch(new UpdateImageSettingCommand(
			'receiveEmailLogoImage',
			'logo_image',
			$data,
			$this->request->user()->id,
			440
		));
		
		return $this->response(['file' => $savedImageData]);
	}
}