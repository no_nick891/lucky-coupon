<?php  namespace LuckyCoupon\Settings\Commands; 

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Settings\SettingsEloquentRepository;

class GetDBSettingsCommand extends BaseCommand {

	protected $gameId;
	private $settingsRepo;
	private $oldSettingRepo;
	
	/**
	 * GetDBSettingsCommand constructor.
	 * @param $gameId
	 */
	public function __construct($gameId)
	{
		$this->gameId = $gameId;
		
		$this->settingsRepo = new SettingsEloquentRepository();
		
		$this->oldSettingRepo = new EloquentSettingRepository();
	}
	
	/**
	 * @return mixed
	 */
	public function handle()
	{
		if (!$settings = $this->settingsRepo->getByGameId($this->gameId))
		{
			$settings = $this->oldSettingRepo->getByGameId($this->gameId);
			
			// get settings from file and return it in case if user have no database settings
		}
		
		return $settings;
	}
}