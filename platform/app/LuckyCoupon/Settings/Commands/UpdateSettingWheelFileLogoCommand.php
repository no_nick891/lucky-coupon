<?php namespace LuckyCoupon\Settings\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class UpdateSettingWheelFileLogoCommand extends BaseCommand
{
	
	/**
	 * UpdateSettingFileLogoCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$imageData = [
			'width' => 275,
			'settingName' => 'wheelLogo',
			'fileName' => 'wheel_logo_image',
			'opacityName' => 'opacityWheelLogo'
		];
		
		return dispatch(new DispatchImageSettingUpdateCommand(
			$this->request,
			$imageData
		));
	}
}