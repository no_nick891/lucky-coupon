<?php  namespace LuckyCoupon\Settings\Commands; 

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Seeds\Helper;

class GetSafeEmailTextSettingsCommand extends BaseCommand {
	/**
	 * @var
	 */
	private $gameType;
	/**
	 * @var
	 */
	private $dbSettings;
	
	
	/**
	 * GetSettingsSafeCommand constructor.
	 * @param $gameType
	 * @param $dbSettings
	 */
	public function __construct($gameType, $dbSettings = [])
	{
		$this->gameType = $gameType;
		
		$this->dbSettings = $dbSettings;
	}
	
	public function handle()
	{
		return data_get(
			$this->dbSettings,
			'couponReceiveEmailText',
			data_get(
				$this->_getDefaultSettings(),
				'behavior.couponReceiveEmailText',
				'{}'
			)
		);
	}
	
	/**
	 * @return mixed
	 */
	private function _getDefaultSettings()
	{
		$gameType = $this->gameType === 'coupon' ? 'default' : $this->gameType;
		
		return json_decode(data_get(
			(new Helper())->getJsConstants('resources/assets/js/helpers/templates/' . $gameType . '/settings.js'),
			'gameSettings',
			''
		));
	}
}