<?php  namespace LuckyCoupon\Settings\Commands; 

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Seeds\Helper;

class GetDefaultGamesSettings extends BaseCommand {
	
	private $helper;
	
	private $defaultGameSettings;
	
	/**
	 * GetDefaultGamesSettings constructor.
	 */
	public function __construct()
	{
		$this->helper = new Helper();
		
		$this->defaultGameSettings = [];
	}
	
	public function handle()
	{
		$this->_setDefaultGamesSettings();
		
		return $this->defaultGameSettings;
	}
	
	/**
	 *
	 */
	private function _setDefaultGamesSettings()
	{
		foreach ($this->helper->gameTypes as $gameType)
		{
			$this->_setDefaultGameSettings($gameType);
		}
	}
	
	/**
	 * @param $gameType
	 */
	private function _setDefaultGameSettings($gameType)
	{
		$settingsObject = $this->_getDefaultSettings($gameType);
		
		$textObject = $this->_getDefaultText($gameType);
		
		$gameType = $gameType === 'default' ? 'coupon' : $gameType;
		
		$settingsObject->text = $textObject;
		
		$this->defaultGameSettings[$gameType] = $settingsObject;
	}
	
	/**
	 * @param $pathInResource
	 * @return string
	 */
	private function _getResourcePath($pathInResource)
	{
		return 'resources/assets/js/helpers/templates/' . $pathInResource;
	}
	
	/**
	 * @param $pathInResource
	 * @return mixed
	 */
	private function _getObject($pathInResource)
	{
		$textPath = $this->_getResourcePath($pathInResource);
		
		return $this->helper->getObjectFromFile($textPath);
	}
	
	/**
	 * @param $gameType
	 * @return mixed
	 */
	private function _getDefaultSettings($gameType)
	{
		$settingsPath = $gameType . '/settings.js';
		
		return $this->_getObject($settingsPath);
	}
	
	/**
	 * @param $gameType
	 * @return mixed
	 */
	private function _getDefaultText($gameType)
	{
		$textPath = $gameType . '/text.js';
		
		return $this->_getObject($textPath);
	}
}