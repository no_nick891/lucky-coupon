<?php namespace LuckyCoupon\Settings\Commands;

use App\Helpers\FileStorageHelper;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Settings\SettingsEloquentRepository;
use LuckyCoupon\Settings\Commands\SetSettingValueByNameCommand;

class DeleteSettingFileCommand extends BaseCommand
{
	private $oldSettingRepo;
	
	private $storageHelper;
	/**
	 * @var
	 */
	private $settingName;
	
	private $settingsRepo;
	
	/**
	 * DeleteSettingFileCommand constructor.
	 * @param $request
	 * @param $settingName
	 */
	public function __construct($request, $settingName)
	{
		$this->request = $request;
		
		$this->settingName = $settingName;
		
		$this->oldSettingRepo = new EloquentSettingRepository();
		
		$this->settingsRepo = new SettingsEloquentRepository();
		
		$this->storageHelper = new FileStorageHelper(null);
	}
	
	/**
	 * @return bool|mixed
	 */
	public function handle()
	{
		if ($errors = $this->getErrors($this->request)) return $errors;
		
		$data = $this->getRequestData();
		
		$previousSettingValue = dispatch(new SetSettingValueByNameCommand($data['game_id'], $this->settingName, ''));
		
		$this->storageHelper->deletePublicFile($previousSettingValue);
		
		if ($previousSettingValue)
		{
			return $this->response(['file' => 1]);
		}
		
		return $this->response(['file' => 0]);
	}
}