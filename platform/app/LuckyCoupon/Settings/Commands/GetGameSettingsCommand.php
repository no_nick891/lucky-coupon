<?php  namespace LuckyCoupon\Settings\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Settings\SettingRepositoryInterface;
use LuckyCoupon\Settings\SettingsEloquentRepository;

class GetGameSettingsCommand extends BaseCommand {

	protected $request;
	
	/**
	 * @var SettingRepositoryInterface
	 */
	private $settingRepo;
	
	private $oldSettingRepo;
	
	/**
	 * GetGameSettingsCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->settingRepo = new SettingsEloquentRepository();
		
		$this->oldSettingRepo = new EloquentSettingRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $errs;
		
		$gameId = $this->request->only('game_id');
		
		return ['setting' => $this->_getByGameId($gameId)];
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	private function _getByGameId($gameId)
	{
		$settings = $this->settingRepo->getByGameId($gameId);
		
		return $settings ? $settings : $this->oldSettingRepo->getByGameId($gameId);
	}
}