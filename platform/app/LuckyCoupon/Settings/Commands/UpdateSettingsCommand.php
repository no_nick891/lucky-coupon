<?php namespace LuckyCoupon\Settings\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Settings\SettingsEloquentRepository;
use Redis;

class UpdateSettingsCommand extends BaseCommand
{
	private $settingRepo;
	
	/**
	 * UpdateSettingsCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->settingRepo = new SettingsEloquentRepository();
	}
	
	/**
	 * @return mixed
	 */
	public function handle()
	{
		$result = [];
		
		$update = $this->request->all();
		
		$game = $update['game'];
		
		$settings = $update['settings'];
		
		data_set($settings, 'backgroundImage.image', data_get($settings, 'backgroundImage.image', ''));
		
		$getTriggerString = data_get($settings, 'text.trigger', '');
		
		$getTriggerString = $getTriggerString ? $getTriggerString : '';
		
		data_set($settings, 'text.trigger', $getTriggerString);
		
		$result['settings'] = $this->settingRepo->save($game['id'], $settings);
		
		if ($result['settings'])
		{
			Redis::del('script:site:' . $game['site_id']);
		}
		
		return $this->response($result);
	}
}