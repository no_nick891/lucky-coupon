<?php namespace LuckyCoupon\Settings\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class UpdateSettingFileCommand extends BaseCommand
{
	/**
	 * UpdateSettingFileCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	/**
	 * @return mixed
	 */
	public function handle()
	{
		$imageData = [
			'width' => 780,
			'settingName' => 'image',
			'fileName' => 'background_image',
		    'opacityName' => 'opacity'
		];
		
		return dispatch(new DispatchImageSettingUpdateCommand(
			$this->request,
			$imageData
		));
	}
}