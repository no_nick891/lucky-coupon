<?php namespace LuckyCoupon\Settings\Commands;

use Intervention\Image\ImageManagerStatic as Image;
use App\Helpers\FileStorageHelper;
use LuckyCoupon\Settings\EloquentSettingRepository;

class UpdateImageSettingCommand
{
	private $fileName;
	
	private $gameId;
	
	private $file;
	
	private $settingName;
	
	private $request;
	
	private $width;
	
	/**
	 * UpdateImageSettingCommand constructor.
	 * @param $settingName
	 * @param $fileName
	 * @param $request
	 * @param $userId
	 * @param $width
	 */
	public function __construct($settingName, $fileName, $request, $userId, $width)
	{
		$this->request = $request;
		
		$gameId = (int)$request['game_id'];
		
		$file = $request[$fileName];
		
		$this->settingName = $settingName;
		
		$this->fileName = $fileName;
		
		$this->width = $width;
		
		$this->gameId = $gameId;
		
		$this->file = $file;
		
		$this->extension = $file->getClientOriginalExtension();
		
		$this->storageHelper = new FileStorageHelper($userId, $this->extension);
		
		$this->settingRepo = new EloquentSettingRepository();
	}
	
	public function handle()
	{
		$image = $this->_getImage();
		
		return $this->_saveImage($image, $this->gameId);
	}
	
	/**
	 * @return \Intervention\Image\Image
	 */
	private function _getImage()
	{
		 $settings = dispatch(new GetDBSettingsCommand($this->gameId));

		 $imagePath = dispatch(new SearchSettingsCommand($settings, $this->settingName));
		
		if ($imagePath !== '')
		{
			$this->storageHelper->deleteFiles($this->fileName, [$imagePath]);
		}
		
		return $this->_getResizeImage($this->file);
	}
	
	/**
	 * @param $file
	 * @return \Intervention\Image\Image
	 */
	private function _getResizeImage($file)
	{
		$fileContent = base64_decode(file_get_contents($file->path()));
		
		$interventionImage = Image::make($fileContent);
		
		if ($this->settingName === 'wheelLogo')
		{
			$result = $interventionImage->resize($this->width, $this->width);
		}
		else
		{
			$result = $interventionImage->resize($this->width, null, function ($constraint){
				$constraint->aspectRatio();
			});
		}
		
		return $result;
	}
	
	/**
	 * @param $image
	 * @param $gameId
	 * @return mixed
	 */
	private function _saveImage($image, $gameId)
	{
		$fileSaved = $this->storageHelper->saveFile(
			$this->fileName,
			(string)$image->encode($this->extension, 70),
			$gameId
		);
		
		if ($fileSaved)
		{
			$this->_saveFileSetting($gameId);
			
			return $this->storageHelper->getPublicImage($this->fileName, $gameId);
		}
		
		return 0;
	}
	
	/**
	 * @param $gameId
	 */
	private function _saveFileSetting($gameId)
	{
		$publicPath = $this->storageHelper->getPublicImage($this->fileName, $gameId);
		
		dispatch(new SetSettingValueByNameCommand($gameId, $this->settingName, $publicPath));
	}
}