<?php  namespace LuckyCoupon\Settings\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Settings\SettingsEloquentRepository;

class SetSettingValueByNameCommand extends BaseCommand {

	/**
	 * @var
	 */
	private $gameId;
	/**
	 * @var
	 */
	private $name;
	/**
	 * @var
	 */
	private $value;
	
	private $settingsRepo;
	
	/**
	 * SetSettingValueByNameCommand constructor.
	 * @param $gameId
	 * @param $name
	 * @param $value
	 */
	public function __construct($gameId, $name, $value = null)
	{
		$this->gameId = $gameId;
		
		$this->name = $name;
		
		$this->value = $value;
		
		$this->settingsRepo = new SettingsEloquentRepository();
	}
	
	public function handle()
	{
		$settings = dispatch(new GetDBSettingsCommand($this->gameId));
		
		$settings = dispatch(new SearchSettingsCommand($settings, $this->name, $this->value, true));
		
		$isSaved = $this->settingsRepo->save($this->gameId, $settings['settings']);
		
		return $isSaved ? $settings['oldValue'] : false;
	}
	
	
}