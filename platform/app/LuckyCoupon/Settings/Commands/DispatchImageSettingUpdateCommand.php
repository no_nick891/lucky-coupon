<?php namespace LuckyCoupon\Settings\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Settings\EloquentSettingRepository;

class DispatchImageSettingUpdateCommand extends BaseCommand
{
	private $settingRepo;
	
	private $fileName;
	
	private $settingName;
	
	private $width;
	
	private $opacity;
	
	/**
	 * DispatchImageSettingUpdateCommand constructor.
	 * @param Request $request
	 * @param $imageData
	 */
	public function __construct($request, $imageData)
	{
		$this->request = $request;
		
		$this->fileName = $imageData['fileName'];
		
		$this->settingName = $imageData['settingName'];
		
		$this->width = $imageData['width'];
		
		$this->opacity = $imageData['opacityName'];
		
		$this->settingRepo = new EloquentSettingRepository();
	}
	
	public function handle()
	{
		$request = $this->request->only(['game_id', 'opacity', $this->fileName]);
		
		if ($this->request->hasFile($this->fileName))
		{
			return $this->_addFile($request, $this->fileName, $this->settingName, $this->width);
		}
		else
		{
			return $this->_updateOpacity($request, $this->fileName);
		}
	}
	
	/**
	 * @param $request
	 * @param $fileName
	 * @return mixed
	 */
	public function _updateOpacity($request, $fileName)
	{
		if ($this->_saveOpacity($request))
		{
			$imagePath = explode('?', $request[$fileName]);
			
			return $this->response(['file' => $imagePath[0]]);
		}
		
		return $this->response(['file' => 0]);
	}
	
	/**
	 * @param $request
	 * @param $fileName
	 * @param $optionName
	 * @param int $width
	 * @return array
	 */
	private function _addFile($request, $fileName, $optionName, $width = 780)
	{
		$this->_saveOpacity($request);
		
		$userId = $this->request->user()->id;
		
		$savedImageData = dispatch(new UpdateImageSettingCommand(
			$optionName,
			$fileName,
			$request,
			$userId,
			$width
		));
		
		return $this->response(['file' => $savedImageData]);
	}
	
	/**
	 * @param $request
	 * @return mixed
	 */
	private function _saveOpacity($request)
	{
		return dispatch(new SetSettingValueByNameCommand((int)$request['game_id'], $this->opacity, $request['opacity']));
	}
}