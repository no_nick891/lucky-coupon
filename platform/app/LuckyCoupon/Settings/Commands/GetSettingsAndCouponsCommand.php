<?php namespace LuckyCoupon\Settings\Commands;

use Illuminate\Http\Request;
use Integration\ChatChamp\Commands\GetConfigCommand;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ChatChampApps\ChatChampApp;
use LuckyCoupon\Coupons\EloquentCouponRepository;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Settings\SettingsEloquentRepository;
use LuckyCoupon\Sites\EloquentSiteRepository;

class GetSettingsAndCouponsCommand extends BaseCommand
{
	private $oldSettingRepo;
	
	private $couponRepo;
	
	private $settingsRepo;
	
	/**
	 * GetSettingsCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->settingsRepo = new SettingsEloquentRepository();
		
		$this->oldSettingRepo = new EloquentSettingRepository();
		
		$this->couponRepo = new EloquentCouponRepository();
	}
	
	/**
	 * @return mixed
	 */
	public function handle()
	{
		if ($err = $this->getErrors($this->request)) return $this->response($err);
		
		$data = $this->getRequestData();
		
		$gamesId = $data['game_id'];
		
		$settings = $this->settingsRepo->getByGameIds($gamesId);
		
		$selectedGameIds = array_keys($settings);
		
		$oldSettings = $this->oldSettingRepo->getByGameIds(array_diff($gamesId, $selectedGameIds));
		
		$allSettings = $settings + $oldSettings;
		
//		$this->_modifyOpacity($allSettings);
		
		$coupons = $this->couponRepo->getByGamesId($gamesId);
		
		return $this->response(['settings' => $allSettings, 'coupons' => $coupons]);
	}
	
	/**
	 * @param $settings
	 * @return mixed
	 */
	private function _modifyOpacity(&$settings)
	{
		foreach ($settings as &$setting)
		{
			data_set(
				$setting, 'backgroundImage.opacity',
				((float)data_get($setting, 'backgroundImage.opacity')) / 100
			);
		}
	}
	
	/**
	 * @param $settings
	 * @return array
	 */
	private function _getIntegrationsServices($settings)
	{
		$result = [];
		
		foreach ($settings as $gameId => $setting)
		{
			$facebookMessengerField = data_get($setting, 'behavior.chooseInputFieldType.facebookMessengerField', false);
			
			if ($facebookMessengerField === '1')
			{
				$chatChamp = $this->_getChatchampByGameId($gameId);
				
				if ($chatChamp->api_key)
				{
					$result[$gameId]['chatchamp'] = $chatChamp->api_key;
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	private function _getChatchampByGameId($gameId)
	{
		return ChatChampApp::where('game_id', $gameId)->first();
	}
}