<?php namespace LuckyCoupon\ChatChampApps;

use Illuminate\Database\Eloquent\Model;

class ChatChampApp extends Model
{
	public $table = 'chatchamp_apps';
	
	public $timestamps = false;
	
	public $fillable = ['active', 'site_id', 'game_id', 'user_id', 'api_key'];
}
