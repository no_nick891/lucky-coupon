<?php namespace LuckyCoupon\Coupons;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Settings\Commands\GetDBSettingsCommand;
use LuckyCoupon\Settings\Commands\GetSafeEmailTextSettingsCommand;
use LuckyCoupon\Settings\EloquentSettingRepository;
use Integration\SendGrid\Commands\SendCodeByEmailCommand;
use LuckyCoupon\Statistics\EloquentStatisticRepository;

class GetCouponCodeCommand extends BaseCommand
{
	private $couponRepo;
	
	private $language;
	
	private $gameRepo;
	
	private $statRepo;
	
	/**
	 * GetCouponCodeCommand constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->couponRepo = new EloquentCouponRepository();
		
		$this->gameRepo = new EloquentGameRepository();
		
		$this->statRepo = new EloquentStatisticRepository();
	}
	
	/**
	 * @return array|bool
	 */
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->response($errs);
		
		$data = $this->getRequestData();
		
		$email = $data['email'];
		
		unset($data['email']);
		
		$coupon = data_get($this->couponRepo->getByConditions($data), 0);
		
		$gameId = data_get($coupon, 'game_id', 0);
		
		$code = data_get($coupon, 'code', null);
		
		$settings = $this->_getSettings($gameId);
		
		$this->language = data_get($settings, 'language', 'en');
		
		$this->_handleEmail($gameId, $email, $code, $settings, $coupon);
		
		return $this->response(['code' => $this->_getCodeText($settings, $code, $gameId)]);
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	private function _getSettings($gameId)
	{
		$settings = dispatch(new GetDBSettingsCommand($gameId));
		
		return data_get($settings, 'behavior', false);
	}
	
	/**
	 * @param $settings
	 * @return mixed
	 */
	private function _getReceivedEmailSettings($settings)
	{
		return data_get($settings, 'howDoYouWantYourCustomerToReceiveTheCoupon', false);
	}
	
	/**
	 * @param $settings
	 * @param $code
	 * @param $gameId
	 * @return array|string
	 */
	private function _getCodeText($settings, $code, $gameId)
	{
		$onlyEmail = false;
		
		$mailSettings = $this->_getReceivedEmailSettings($settings);
		
		if ($mailSettings)
		{
			$onlyEmail = data_get($mailSettings, 'sendTroughEmail') === '1';
		}
		
		$returnText = $this->_getReturnText($settings, $gameId);
		
		$isCodeAvailable = $onlyEmail && !$this->_isEmailFieldDisabled($settings);
		
		return $this->_getResponseData($isCodeAvailable, $returnText, $code);
	}
	
	/**
	 * @param $isCodeAvailable
	 * @param $returnText
	 * @param $code
	 * @return array
	 */
	private function _getResponseData($isCodeAvailable, $returnText, $code)
	{
		return $isCodeAvailable
			? ['text' => $returnText]
			: $this->_getCode($code);
	}
	
	/**
	 * @param $code
	 * @return array
	 */
	private function _getCode($code)
	{
		return $code !== null ? $code : ['text' => 'Something goes wrong on server. Please try get coupon code later.'];
	}
	
	/**
	 * @return string
	 */
	private function _getTextFromDiscount()
	{
		switch ($this->language)
		{
			case 'de': return '';
			case 'fr': return '';
			case 'es': return '';
			case 'he': return '';
			case 'ru': return 'Скидочный код выслан на указанный ящик';
			case 'en': default: return 'Discount code sent to your email';
		}
	}
	
	/**
	 * @param $settings
	 * @return bool
	 */
	private function _isCodeSendByEmail($settings)
	{
		$mailSettings = $this->_getReceivedEmailSettings($settings);
		
		$isSendByEmail = data_get($mailSettings, 'winningGameScreenAndEmail') === '1'
					  || data_get($mailSettings, 'sendTroughEmail') === '1';
		
		return $isSendByEmail && !$this->_isEmailFieldDisabled($settings);
	}
	
	/**
	 * @param $settings
	 * @return bool
	 */
	private function _isEmailFieldDisabled($settings)
	{
		return data_get($settings, 'collectEmailFromUsers.yes', '1') === '0';
	}
	
	/**
	 * @param $settings
	 * @param $gameId
	 * @return mixed
	 */
	private function _getReturnText($settings, $gameId)
	{
		$game = $this->gameRepo->getById($gameId);
		
		$emailText = dispatch(new GetSafeEmailTextSettingsCommand($game->type, $settings));
		
		return data_get($emailText, 'discountCodeSentToYourEmail', $this->_getTextFromDiscount());
	}
	
	/**
	 * @param $email
	 * @return bool
	 */
	private function _isTestEmail($email)
	{
		return $email === 'test@test.com';
	}
	
	/**
	 * @param $gameId
	 * @param $email
	 * @param $code
	 * @param $settings
	 * @param $coupon
	 * @return bool
	 */
	private function _handleEmail($gameId, $email, $code, $settings, $coupon)
	{
		if ($this->_isTestEmail($email) || $email === 'non-collect-option-on') return false;
		
		$this->statRepo->addCodeByGameIdAndEmail($gameId, $email, $code);
		
		if ($this->_isCodeSendByEmail($settings))
		{
			dispatch((new SendCodeByEmailCommand($settings, $coupon, $email))->onQueue('dispatcher'));
		}
		
		return true;
	}
	
}