<?php namespace LuckyCoupon\Coupons;

use App\LuckyCoupon\Coupons\Coupon;

/**
 * Class EloquentCouponRepository
 * @package LuckyCoupon\Coupons
 */
class EloquentCouponRepository implements CouponRepositoryInterface
{
	/**
	 * @var
	 */
	public $model;
	
	private $couponKeys = [
		'code',
		'value',
		'chance',
		'gravity',
		'color'
	];
	
	/**
	 * EloquentCouponRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new Coupon();
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	public function getByGameId($gameId)
	{
		return $this->model->where('game_id', $gameId)->get();
	}
	
	/**
	 * @param $gamesId
	 * @return mixed
	 */
	public function getByGamesId($gamesId)
	{
		$couponsArray = $this->model
			->whereIn('game_id', $gamesId)
			->orderBy('game_id', 'asc')
			->orderBy('id', 'asc')
			->get()->toArray();
		
		return $this->_arrangeByGameId($couponsArray);
	}
	
	/**
	 * @param $conditions
	 * @return array
	 */
	public function getByConditions($conditions)
	{
		$query = $this->model;
		
		foreach ($conditions as $name => $value)
		{
			$query = $query->where($name, $value);
		}
		
		return $query->count() > 0 ? $query->get()->toArray() : [];
	}
	
	/**
	 * @param $coupon
	 * @return mixed
	 */
	public function insert($coupon)
	{
		return $this->model->insertGetId($coupon);
	}
	
	/**
	 * @param $coupon
	 * @return mixed
	 */
	public function update($coupon)
	{
		$id = $coupon['id'];
		
		unset($coupon['id']);
		
		return $this->model->where('id', $id)->update($coupon);
	}
	
	/**
	 * @param $coupons
	 * @return array
	 */
	public function updateBunch($coupons)
	{
		$result = 'empty data';
		
		foreach ($coupons as $coupon)
		{
			$result[$coupon['id']] = $this->update($coupon);
		}
		
		return ['coupons' => $result];
	}
	
	/**
	 * @param $gameId
	 * @param $coupons
	 * @return array
	 */
	public function save($gameId, $coupons)
	{
		$coupons = $this->_prepare($coupons);
		
		$result = [];
		
		foreach ($coupons as $coupon)
		{
			$coupon['game_id'] = $gameId;
			
			if (isset($coupon['id']))
			{
				$result[$coupon['id']] = $this->update($coupon);
			}
			else
			{
				$id = $this->insert($coupon);
				
				$result[$id] = $id ? true : false;
			}
		}
		
		return $result;
	}
	
	/**
	 * @param $coupons
	 * @return array
	 */
	private function _prepare($coupons)
	{
		$result = [];
		
		foreach ($coupons as $coupon)
		{
			unset($coupon['temp_id']);
			
			$coupon = array_filter($coupon);
			
			$coupon = $this->_getSafeCoupon($coupon);
			
			$result[] = $coupon;
		}
		
		return $result;
	}
	
	/**
	 * @param $coupon
	 * @return mixed
	 */
	private function _getSafeCoupon($coupon)
	{
		$strings = ['color', 'code'];
		
		foreach ($this->couponKeys as $couponKey)
		{
			$coupon[$couponKey] = isset($coupon[$couponKey])
				? $coupon[$couponKey]
				: (in_array($couponKey, $strings) ? '' : 0);
		}
		
		return $coupon;
	}
	
	/**
	 * @param $gameId
	 */
	public function deleteGameCoupons($gameId)
	{
		return $this->model->where('game_id', $gameId)->delete();
	}
	
	/**
	 * @param $couponsArray
	 * @return array
	 */
	private function _arrangeByGameId($couponsArray)
	{
		$result = [];
		
		foreach ($couponsArray as $coupon)
		{
			$result[$coupon['game_id']][] = $coupon;
		}
		
		return $result;
	}
}