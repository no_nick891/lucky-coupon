<?php namespace LuckyCoupon\Coupons\Commands;

class GetWheelStructureCommand
{
	private $slices;
	
	/**
	 * GetWheelStructureCommand constructor.
	 * @param $slices
	 */
	public function __construct($slices)
	{
		$this->slices = $slices;
	}
	
	public function handle()
	{
		$result = [];
		
		foreach ($this->slices as $key => $slice)
		{
			$result[] = $this->_getWheel($key, $slice);
		}
		
		return $result;
	}
	
	/**
	 * @param $key
	 * @param $slice
	 * @return array
	 */
	private function _getWheel($key, $slice)
	{
		return [
			'tagName' => 'div',
			'className' => 'slice',
			'textNode' => $slice->value,
			'data' => $this->_getAttributes($key, $slice)
		];
	}
	
	/**
	 * @param $key
	 * @param $slice
	 * @return mixed
	 */
	private function _getAttributes($key, $slice)
	{
		$result = $slice->getAttributes();
		
		unset(
			$result['created_at'],
			$result['updated_at'],
			$result['color'],
			$result['code']
		);
		
		$result['stop'] = abs($key - 5) * 60 + 30;
		
		return $result;
	}
}