<?php namespace LuckyCoupon\Coupons\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Coupons\EloquentCouponRepository;
use Redis;

/**
 * @property  couponRepo
 */
class UpdateCouponsCommand extends BaseCommand
{
	private $couponRepo;
	
	/**
	 * UpdateCouponsCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->couponRepo = new EloquentCouponRepository();
	}
	
	/**
	 * @return array
	 */
	public function handle()
	{
		$update = $this->request->all();
		
		$game = $update['game'];
		
		$coupons = $this->couponRepo->save($game['id'], $update['coupons']);
		
		Redis::del('script:site:' . $game['site_id']);
		
		return ['coupons' => $coupons];
	}
}