<?php namespace LuckyCoupon\Coupons\Commands;

class GetGiftsStructureCommand
{
	private $gifts;
	
	/**
	 * GetGiftsStructureCommand constructor.
	 * @param $gifts
	 */
	public function __construct($gifts)
	{
		$this->gifts = $gifts;
	}
	
	public function handle()
	{
		return $this->_getGifts($this->gifts);
	}
	
	/**
	 * @param $gifts
	 * @return array
	 */
	public function _getGifts($gifts)
	{
		$result = [];
		
		foreach ($gifts as $gift)
		{
			$result[] = $this->_getGift($gift);
		}
		
		return $result;
	}
	
	/**
	 * @param $gift
	 * @return array
	 */
	private function _getGift($gift)
	{
		return [
			'tagName' => 'div',
			'className' => 'gift',
			'meta' => ['id' => $gift->id],
			'style' => ['opacity' => 0],
			'data' => $this->_getAttributes($gift),
			'content' => ''
		];
	}
	
	/**
	 * @param $gift
	 * @return mixed
	 */
	private function _getAttributes($gift)
	{
		$result = $gift->getAttributes();
		
		unset(
			$result['created_at'],
			$result['updated_at'],
			$result['color'],
			$result['code']
		);
		
		return $result;
	}
}