<?php namespace LuckyCoupon\Coupons\Commands;

use LuckyCoupon\Seeds\Helper;

class GetCouponsStructureCommand
{
	private $coupons;
	
	private $language;
	
	private $text;
	
	/**
	 * GetCouponsStructureCommand constructor.
	 * @param $coupons
	 * @param $language
	 * @param $text
	 */
	public function __construct($coupons, $language, $text)
	{
		$this->coupons = $coupons;
		
		$this->language = $language;
		
		$this->text = $text;
		
		$this->translations = $this->_getTranslations();
	}
	
	/**
	 * @return array
	 */
	public function handle()
	{
		return $this->_getCoupons($this->coupons);
	}
	
	/**
	 * @param $coupons
	 * @return array
	 */
	private function _getCoupons($coupons)
	{
		$resultCoupons = [];
		
		foreach ($coupons as $key => $coupon)
		{
			$resultCoupons[] = $this->_getWrappedCoupon($coupon);
			
			if ($key === 3)
			{
				$resultCoupons[] = $this->_getFillerCoupon();
			}
		}
		
		return $resultCoupons;
	}
	
	/**
	 * @return array
	 */
	private function _getFillerCoupon()
	{
		$couponWrapper = $this->_getCouponWrapper(true, new \stdClass());
		
		$couponWrapper['content'] = $this->_getEmptyCouponContent();
		
		return $couponWrapper;
	}
	
	/**
	 * @param $coupon
	 * @return array
	 */
	private function _getWrappedCoupon($coupon)
	{
		return $this->_getCouponWrapper(false, $coupon);
	}
	
	/**
	 * @param bool $filler
	 * @param $coupon
	 * @return array
	 */
	private function _getCouponWrapper($filler = false, $coupon)
	{
		return [
			'tagName' => 'li',
			'meta' => $filler ? [] : ['id' => $coupon->id],
			'className' => $filler ? 'filler' : 'prize-item coupon',
			'data' => $filler ? [] : $this->_getCouponDataAttributes($coupon),
			'content' => $filler ? [] : $this->_getCouponArray($coupon)
		];
	}
	
	/**
	 * @param $coupon
	 * @return array
	 */
	private function _getCouponArray($coupon)
	{
		switch ($coupon->type)
		{
			case 'discount': $result = $this->_getCouponItem(
				(float)$coupon->value . '%',
				$this->_text('startScreen.coupon', 'coupon'),
				'coupon'
			);
				break;
			case 'cash': $result = $this->_getCouponItem(
				'$' . (float)$coupon->value,
				$this->_text('startScreen.cash', 'cash'),
				'cash'
			);
				break;
			case 'free product': $result = $this->_getFreeCoupon('product');
				break;
			default: $result = $this->_getFreeCoupon('shipping');
		}
		
		return $result;
	}
	
	/**
	 * @param $type
	 * @return array
	 */
	private function _getFreeCoupon($type)
	{
		$fullType = $this->_getTranslation('free' . ucfirst($type));
		
		$free = explode(' ', $fullType);
		
		$startScreen = 'startScreen.free' . ucfirst($type);
		
		$upperText = data_get($this->text, $startScreen . '.free', $free[0]);
		
		$lowerText = data_get($this->text, $startScreen . '.' . $type, $free[1]);
		
		return $this->_getCouponItem($upperText, $lowerText, $fullType);
	}
	
	/**
	 * @param $upperText
	 * @param $lowerText
	 * @param $type
	 * @return array
	 */
	private function _getCouponItem($upperText, $lowerText, $type)
	{
		return [
			['tagName' => 'div', 'className' => 'right'],
			['tagName' => 'div', 'className' => 'left'],
			[
				'tagName' => 'div',
				'className' => 'upper-text',
				'textNode' => $upperText,
			    'data' => ['text' => 'startScreen.' . $type . '.0']
			],
			[
				'tagName' => 'div',
				'className' => 'lower-text',
				'textNode' => $lowerText,
				'data' => ['text' => 'startScreen.' . $type . '.1']
			]
		];
	}
	
	/**
	 * @param $coupon
	 * @return array
	 */
	private function _getCouponDataAttributes($coupon)
	{
		$attributes = $coupon->getAttributes();
		
		unset($attributes['created_at'], $attributes['updated_at'], $attributes['code']);
		
		return $attributes;
	}
	
	/**
	 * @return array
	 */
	private function _getEmptyCouponContent()
	{
		return [[
	        'tagName' => 'button',
	        'className' => 'prize-item btn-push red',
	        'title' => 'Check here',
	        'content' => [
		        [
			        'tagName' => 'div',
			        'className' => 'start-line',
			        'data' => [
			        	'text' => 'startScreen.button.0'
			        ],
			        'textNode' => $this->_text('startScreen.button.startLine', 'start')
		        ],
		        [
			        'tagName' => 'div',
			        'className' => 'game',
			        'data' => [
				        'text' => 'startScreen.button.1'
			        ],
			        'textNode' => $this->_text('startScreen.button.endLine', 'game')
		        ]
	        ]
        ]];
	}
	
	/**
	 * @param $textKey
	 * @param $word
	 * @return string
	 */
	private function _text($textKey, $word)
	{
		return data_get($this->text, $textKey, $this->_getTranslation($word));
	}
	
	/**
	 * @param $word
	 * @return string
	 */
	private function _getTranslation($word)
	{
		$result = '';
		
		if (isset($this->translations[$word]) && isset($this->translations[$word]->{$this->language}))
		{
			$result = $this->translations[$word]->{$this->language};
		}
		
		return $result;
	}
	
	/**
	 * @return array
	 */
	private function _getTranslations()
	{
		$helper = new Helper();
		
		$jsonString = $helper->getJsConstants('resources/assets/js/helpers/language/coupons.js');
		
		return (array)json_decode(array_pop($jsonString));
	}
}