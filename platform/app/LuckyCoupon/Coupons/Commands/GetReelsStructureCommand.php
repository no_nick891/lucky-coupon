<?php namespace LuckyCoupon\Coupons\Commands;

class GetReelsStructureCommand
{
	private $coupons;
	
	/**
	 * GetReelsStructureCommand constructor.
	 * @param $coupons
	 */
	public function __construct($coupons)
	{
		$this->coupons = $coupons;
	}
	
	public function handle()
	{
		return $this->_getReels($this->coupons);
	}
	
	/**
	 * @param $coupons
	 * @return array
	 */
	private function _getReels($coupons)
	{
		$reelItems = $this->_getReelItems($coupons);
		
		$object = new \stdClass();
		
		$object->className = 'dotted';
		
		$object->tagName = 'div';
		
		$object->content = $this->_getSlot($reelItems);
		
		return [$object];
	}
	
	/**
	 * @param $coup
	 * @return \StdClass
	 */
	public function _getRelItem($coup)
	{
		switch ($coup->type) {
			case 'free shipping':
				$className = 'shipping';break;
			case 'discount':
				$className = 'coupon'; break;
			case 'cash':
				$className = 'cash'; break;
		}
		return $this->_getItemObject($coup, $className);
	}
	
	/**
	 * @param $coup
	 * @param $type
	 * @return \StdClass
	 */
	public function _getItemObject($coup, $type)
	{
		$coupon = new \StdClass();
		
		$coupon->tagName = "div";
		
		$coupon->className = "reel-item " . $type;
		
		$coupon->data = $this->_getDataObject($coup->toArray());
		
		$coupon->style = new \StdClass();
		
		$coupon->style->{'background-color'} = $coup->color ? $coup->color : null;

		if ($type === 'shipping')
		{
			$coupon->pure = true;
			
			$coupon->content = '<img src=' . $this->_getAppUrl() . '/img/game/truck_slot.svg >';
		}
		else
		{
			$value = $coup->value + 0;
			
			$coupon->textNode = ($type === 'coupon' ? $value . '%' : '$' . ($value));
		}
		
		return $coupon;
	}
	
	/**
	 * @param $coup
	 * @return \StdClass
	 */
	private function _getDataObject($coup)
	{
		$result = new \StdClass();
		
		foreach ($coup as $key => $value)
		{
			if ($this->isExcluded($key))
			{
				$result->{$key} = $value;
			}
		}
		
		return $result;
	}
	
	/**
	 * @param $index
	 * @return bool
	 */
	public function isExcluded($index) {
		return $index !== 'code'
			&& $index !== 'created_at'
			&& $index !== 'updated_at'
			&& $index !== 'gravity';
	}
	
	/**
	 * @return mixed
	 */
	private function _getAppUrl()
	{
		return str_replace(['https:', 'http:'], '', config('app.url'));
	}
	
	/**
	 * @param $coupons
	 * @return array
	 */
	private function _getReelItems($coupons)
	{
		$couponObjects = [];
		
		foreach ($coupons as $coupon)
		{
			$couponObjects[] = $this->_getRelItem($coupon);
		}
		
		$couponObjects[] = $couponObjects[0];
		
		return $couponObjects;
	}
	
	/**
	 * @param $reelItems
	 * @return array
	 */
	private function _getSlot($reelItems)
	{
		return [
			$this->_getWithWrapper($reelItems, 0),
			$this->_getWithWrapper($reelItems, 1),
			$this->_getWithWrapper($reelItems, 2)
		];
	}
	
	/**
	 * @param $reelItems
	 * @param $index
	 * @return \StdClass
	 */
	private function _getWithWrapper($reelItems, $index)
	{
		$wrapper = $this->_getReelWrapperObject($index);
		
		$wrapper->content = $reelItems;
		
		$reelObject = $this->_getReelObject();
		
		array_push($reelObject->content, $wrapper);
		
		return $reelObject;
	}
	
	/**
	 * @param $index
	 * @return \StdClass
	 */
	private function _getReelWrapperObject($index)
	{
		$result = new \StdClass();
		
		$result->tagName = 'div';
		
		$result->className = 'reel-wrapper';
		
		$result->style = new \StdClass();
		
		$result->style->top = (-110 * $index) . 'px';
		
		$result->content = [];
		
		return $result;
	}
	
	private function _getReelObject()
	{
		$result = new \StdClass();
		
		$result->tagName = 'div';
		
		$result->className = 'reel';
		
		$result->content = [];
		
		return $result;
	}
}