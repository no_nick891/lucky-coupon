<?php namespace LuckyCoupon\Coupons;

/**
 * Interface CouponRepositoryInterface
 * @package LuckyCoupon\Coupons
 */
interface CouponRepositoryInterface
{
	/**
	 * @param $gameId
	 * @return mixed
	 */
	public function getByGameId($gameId);
	public function getByGamesId($gamesId);
	public function update($coupon);
	public function updateBunch($coupons);
	public function save($gameId, $coupons);
	public function deleteGameCoupons($gameId);
}