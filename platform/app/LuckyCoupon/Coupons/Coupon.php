<?php namespace App\LuckyCoupon\Coupons;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Coupon
 * @package App\LuckyCoupon\Coupons
 */
class Coupon extends Model
{
	/**
	 * @var string
	 */
	protected $table = 'coupons';
}
