<?php namespace LuckyCoupon\Translations;

use LuckyCoupon\Seeds\Helper;

trait Translation
{
	/**
	 * @var Helper
	 */
	public $helper;
	
	private $language;
	
	private $translations;
	
	/**
	 * @param $language
	 * @return array
	 */
	private function _setTranslations($language)
	{
		$this->language = $language;
		
		$this->helper = new Helper();
		
		$translationsFiles = [
			'coupon' => 'resources/assets/js/helpers/language/coupons.js',
		    'game' => 'resources/assets/js/helpers/language/game/' . $language . '.js',
		    'codeMail' => 'resources/assets/js/helpers/language/email.js'
		];
		
		foreach ($translationsFiles as $keyName => $path)
		{
			$this->_setTranslation($keyName, $path);
		}
	}
	
	/**
	 * @param $path
	 * @param $keyName
	 */
	private function _setTranslation($keyName, $path)
	{
		$translations = $this->helper->getJsConstants($path);
		
		$this->translations[$keyName] = (array)json_decode(array_pop($translations));
	}
	
	/**
	 * @param $string
	 * @return mixed
	 */
	private function _translate($string)
	{
		$word = explode('.', $string);
		
		$translation = data_get($this->translations, data_get($word, '0', ''), '');
		
		$language = in_array($word[0], ['coupon', 'codeMail']) ? '.' . $this->language : '';
		
		$result = data_get($translation, data_get($word, '1', '') . $language, '');
		
		return $result;
	}
}