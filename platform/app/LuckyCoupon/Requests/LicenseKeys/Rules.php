<?php namespace LuckyCoupon\Requests\LicenseKeys;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Coupons
 */
class Rules extends BaseRules
{
	/**
	 * @var array
	 */
	protected $_rules = [
		'user_id' => 'required',
	    'license_key' => 'license_key'
	];
	
	/**
	 * @return array
	 */
	public function addLicenseKey()
	{
		return $this->_getRulesArray(['user_id', 'license_key']);
	}
}