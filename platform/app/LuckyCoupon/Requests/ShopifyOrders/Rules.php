<?php namespace LuckyCoupon\Requests\ShopifyOrders;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Coupons
 */
class Rules extends BaseRules
{
	/**
	 * @var array
	 */
	protected $_rules = [
		'user_id' => '',
	    'shopify_product_id' => 'required',
	    'app_id' => 'required'
	];
	
	/**
	 * @return array
	 */
	public function getShopifyOrders()
	{
		return $this->_getRulesArray(['user_id']);
	}
	
	/**
	 * @return array
	 */
	public function updateShopifyAppModalFlag()
	{
		return $this->_getRulesArray(['id', 'showed_rate_modal']);
	}
	
	/**
	 * @return array
	 */
	public function getShopifyProduct()
	{
		return $this->_getRulesArray(['shopify_product_id']);
	}
	
}