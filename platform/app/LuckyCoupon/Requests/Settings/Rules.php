<?php namespace LuckyCoupon\Requests\Settings;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Coupons
 */
class Rules extends BaseRules
{
	/**
	 * @var array
	 */
	protected $_rules = [
		'id' => 'required',
		'game_id' => 'required',
	    'background_image' => 'required',
	    'wheel_logo_image' => 'required',
	    'logo_image' => 'required'
	];
	
	/**
	 * @return array
	 */
	public function getSettings()
	{
		return $this->_getRulesArray(['game_id']);
	}
	
	/**
	 * @return array
	 */
	public function postSettingsFile()
	{
		return $this->_getRulesArray(['game_id', 'background_image']);
	}
	
	/**
	 * @return array
	 */
	public function postSettingsWheelFileLogo()
	{
		return $this->_getRulesArray(['game_id', 'wheel_logo_image']);
	}
	
	/**
	 * @return array
	 */
	public function deleteSettingsFile()
	{
		return $this->_getRulesArray(['game_id']);
	}
	
	/**
	 * @return array
	 */
	public function updateSettingsLogo()
	{
		return $this->_getRulesArray(['game_id', 'logo_image']);
	}
	
	
	/**
	 * @return array
	 */
	public function deleteSettingsLogo()
	{
		return $this->_getRulesArray(['game_id']);
	}
}