<?php namespace LuckyCoupon\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class PostSettingsWheelFileLogoRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return \App::make(Rules::class)->postSettingsWheelFileLogo();
	}
	
	/**
	 * @param array $errors
	 * @return JsonResponse
	 */
	public function response(array $errors)
	{
		return new JsonResponse(
			$errors,
			200,
			['Access-Control-Allow-Origin' => '*']
		);
	}
}
