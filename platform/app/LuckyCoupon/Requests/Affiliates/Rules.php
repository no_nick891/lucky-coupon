<?php namespace LuckyCoupon\Requests\Affiliates;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Affiliates
 */
class Rules extends BaseRules
{
	/**
	 * @var array
	 */
	protected $_rules = [
		'paypal_email' => 'email'
	];

	/**
	 * @return array
	 */
	public function patchAffiliateOwner()
	{
		return $this->_getRulesArray(['paypal_email']);
	}
}