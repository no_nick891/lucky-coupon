<?php  namespace LuckyCoupon\Requests\Sites;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Sites
 */
class Rules {

	/**
	 * @var array
	 */
	protected $_rules = [
		'id' => 'required',
	    'url' => 'required|url'
	];

	/**
	 * @return array
	 */
	public function getSite()
	{
		return [
			'id' => $this->_rules['id']
		];
	}
	
	/**
	 * @return array
	 */
	public function putSite()
	{
		return [
			'url' => $this->_rules['url']
		];
	}

	/**
	 * @return array
	 */
	public function patchSite()
	{
		return [
			'id' => $this->_rules['id'],
			'url' => $this->_rules['url']
		];
	}
}