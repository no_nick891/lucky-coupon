<?php  namespace LuckyCoupon\Requests\Integrations;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Sites
 */
class Rules extends BaseRules {

	/**
	 * @var array
	 */
	protected $_rules = [
		'service' => 'required',
	    'active' => 'nullable|integer',
	    'site_id' => 'required|integer',
	    'game_id' => 'nullable|integer',
	    'selected_list_id' => 'nullable|string',
	    'private_key' => 'nullable|string',
	    'api_key' => 'nullable|string',
	    'api_url' => 'nullable|string'
	];

	/**
	 * @return array
	 */
	public function getService()
	{
		return $this->_getRulesArray(['service', 'site_id', 'game_id']);
	}
	
	public function postService()
	{
		return $this->_getRulesArray([
			'service', 'site_id', 'game_id',
			'active', 'private_key', 'selected_list_id',
			'api_key', 'api_url'
		]);
	}
	
	/**
	 * @return array
	 */
	public function updateService()
	{
		return $this->_getRulesArray(['site_id', 'game_id', 'service', 'active', 'selected_list_id']);
	}
	
	/**
	 * @return array
	 */
	public function getChatchampConfig()
	{
		return $this->_getRulesArray(['game_id']);
	}
}