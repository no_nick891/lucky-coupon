<?php  namespace LuckyCoupon\Requests\Integrations;

use Illuminate\Foundation\Http\FormRequest;

class PostIntegrationServiceRequest extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * @return mixed
	 */
	public function rules()
	{
		return \App::make(Rules::class)->postService();
	}
}