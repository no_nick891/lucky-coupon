<?php namespace LuckyCoupon\Requests\Coupons;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Coupons
 */
class Rules extends BaseRules
{
	/**
	 * @var array
	 */
	protected $_rules = [
		'id' => 'required',
		'game_id' => 'required',
		'code' => 'min:2',
		'value' => 'numeric',
		'chance' => 'numeric|min:0|max:100',
		'type' => 'in:discount,cash,free shipping',
	    'email' => 'string|nullable'
	];

	/**
	 * @return array
	 */
	public function getCoupons()
	{
		return [
			'game_id' => $this->_rules['game_id']
		];
	}
	
	/**
	 * @return array
	 */
	public function patchCoupon()
	{
		return [
			'id' => $this->_rules['id'],
			'code' => $this->_rules['code'],
			'value' => $this->_rules['value'],
			'chance' => $this->_rules['chance'],
			'type' => $this->_rules['type']
		];
	}
	
	/**
	 * @param $email
	 * @return array
	 */
	public function postGetCode($email)
	{
		if ($email === 'non-collect-option-on')
		{
			return array_merge(
				$this->_getRulesArray(['id', 'game_id']),
				['email' => 'in:non-collect-option-on']
			);
		}
		
		return $this->_getRulesArray(['id', 'game_id', 'email']);
	}
}