<?php  namespace LuckyCoupon\Requests\Users;

use Illuminate\Validation\Rule;
use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Sites
 */
class Rules extends BaseRules
{

	/**
	 * @var array
	 */
	protected $_rules = [
		'id' => 'required',
	    'name' => 'required|string',
	    'email' => ['nullable', 'email'],
	    'password' => 'string|nullable',
	    'is_first_time' => 'nullable'
	];
	
	/**
	 * @param $id
	 * @return array
	 */
	public function updateUserProfile($id)
	{
		$this->_rules['email'][] = Rule::unique('users')->ignore($id, 'id');
		
		return $this->_getRulesArray(['id', 'name', 'email', 'password', 'is_first_time']);
	}
}