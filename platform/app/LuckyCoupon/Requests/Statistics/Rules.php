<?php namespace LuckyCoupon\Requests\Statistics;

use LuckyCoupon\Requests\BaseRules;

class Rules extends BaseRules
{
	/**
	 * @var array
	 */
	protected $_rules = [
		'game_id' => 'required|exists:games,id',
		'site_id' => 'required|exists:sites,id',
	    'subscriber_email' => 'email|email_game:game_id'
	];
	
	/**
	 * @return array
	 */
	public function addEmailAndStatistic()
	{
		return $this->_getRulesArray(['game_id' , 'site_id', 'subscriber_email']);
	}
}