<?php namespace LuckyCoupon\Requests;

class BaseRules
{
	/**
	 * @param $rulesNames
	 * @return array
	 */
	protected function _getRulesArray($rulesNames)
	{
		$result = [];
		
		foreach ($rulesNames as $name)
		{
			$result[$name] = $this->_rules[$name];
		}
		
		return $result;
	}
}