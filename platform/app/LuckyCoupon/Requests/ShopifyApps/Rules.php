<?php namespace LuckyCoupon\Requests\ShopifyApps;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Coupons
 */
class Rules extends BaseRules
{
	/**
	 * @var array
	 */
	protected $_rules = [
		'id' => 'required',
	    'showed_rate_modal' => 'required'
	];
	
	/**
	 * @return array
	 */
	public function getShopifyApp()
	{
		return $this->_getRulesArray(['id']);
	}
	
	/**
	 * @return array
	 */
	public function updateShopifyAppModalFlag()
	{
		return $this->_getRulesArray(['id', 'showed_rate_modal']);
	}
}