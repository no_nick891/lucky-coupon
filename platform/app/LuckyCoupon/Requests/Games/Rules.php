<?php  namespace LuckyCoupon\Requests\Games;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Sites
 */
class Rules extends BaseRules {

	/**
	 * new_game is logic key only.
	 * There is no database column new_game in games table.
	 *
	 * @var array
	 */
	protected $_rules = [
		'id' => 'required|integer|exists:games',
		'site_id' => 'required|integer|exists:sites,id',
	    'type' => 'required|string',
		'name' => 'nullable|string',
		'active' => 'integer',
		'sort' => 'integer',
	    'subscriber_email' => 'email|email_game:game_id',
	    'subscriber_name' => '',
	    'is_created' => 'required',
	    'new_game' => '',
		'url' => 'required|url',
	    'device_type' => 'in:pc,mobile',
	    'error' => 'required'
	];
	
	/**
	 * @return array
	 */
	public function getGames()
	{
		return $this->_getRulesArray(['site_id']);
	}
	
	/**
	 * @return array
	 */
	public function putGame()
	{
		return $this->_getRulesArray(['type', 'site_id', 'name']);
	}
	
	/**
	 * @return array
	 */
	public function putGameWithSite()
	{
		return $this->_getRulesArray([
			'type',
			'name',
			'url',
		    'active'
		]);
	}
	
	/**
	 * @return array
	 */
	public function patchGame()
	{
		return $this->_getRulesArray([
			'id',
			'active',
			'type',
			'site_id',
			'name',
			'sort',
			'is_created',
			'new_game'
		]);
	}
	
	/**
	 * @return array
	 */
	public function deleteGame()
	{
		return  $this->_getRulesArray(['id', 'site_id']);
	}
	
	/**
	 * @return array
	 */
	public function addGameStatistic()
	{
		return  $this->_getRulesArray(['id', 'site_id']);
	}
	
	/**
	 * @param $email
	 * @return array
	 */
	public function addEmailAndStatistic($email)
	{
		if ($email === 'non-collect-option-on')
		{
			return array_merge(
				$this->_getRulesArray(['id', 'site_id', 'device_type']),
				['subscriber_email' => 'in:non-collect-option-on']
			);
		}
		
		return $this->_getRulesArray(['id', 'site_id', 'subscriber_email', 'subscriber_name', 'device_type']);
	}
	
	/**
	 * @return array
	 */
	public function PostGetSubscribers()
	{
		return $this->_getRulesArray(['id']);
	}
	
	/**
	 * @return array
	 */
	public function logFrontendGameErrors()
	{
		return $this->_getRulesArray(['id', 'error']);
	}
}