<?php namespace LuckyCoupon\Requests\Games;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class AddEmailAndStatistic extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return \App::make(Rules::class)->addEmailAndStatistic($this->get('subscriber_email'));
    }
	
	/**
	 * @param array $errors
	 * @return JsonResponse
	 */
	public function response(array $errors)
	{
		return new JsonResponse(
			$errors,
			200,
			['Access-Control-Allow-Origin' => '*']
		);
	}
}
