<?php namespace LuckyCoupon\Plans\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Plans\Plan;
use LuckyCoupon\Plans\PlansEloquentRepository;

class GetShopifyPlansViewCommand extends BaseCommand
{
	private $plansRepo;
	
	/**
	 * GetShopifyPlansViewCommand constructor.
	 */
	public function __construct()
	{
		$this->request = session('request');
		
		$this->plansRepo = new PlansEloquentRepository();
	}
	
	public function handle()
	{
		if(empty($this->request['shop'])) return redirect('/');
		
		$query = json_encode($this->request);
		
		$plans = $this->plansRepo->getShopifyPlans();
		
		$plans = $this->_modify($plans);
		
		return view('shopify.plans', compact('plans', 'request', 'query'));
	}
	
	/**
	 * @param $plans
	 * @return mixed
	 */
	private function _modify($plans)
	{
		foreach ($plans as $plan)
		{
			$plan->condition = $this->_getCondition($plan->name);
			
			$plan->amount_screen = $this->_getAmount($plan);
		}
		
		return $plans;
	}
	
	/**
	 * @param $name
	 * @return string
	 */
	private function _getCondition($name)
	{
		switch ($name)
		{
			case 'Starter': return 'Up to 100 impressions';
			case 'Basic': return 'Up to 1,000 impressions';
			case 'Advanced': return 'Up to 5,000 impressions';
			case 'Pro': return 'Up to 30,000 impression';
			case 'Unlimited': return 'unlimited impressions';
		}
	}
	
	/**
	 * @param $plan
	 * @return string
	 */
	private function _getAmount($plan)
	{
		if ((int)$plan->amount === 0) return 'FREE';
		
		return $this->_getMoneySign($plan) . (float)$plan->amount;
	}
	
	/**
	 * @param $plan
	 * @return string
	 */
	private function _getMoneySign($plan)
	{
		$currency = '';
		
		if ($plan->currency === 'USD')
		{
			$currency = '$';
		}
		
		return $currency;
	}
}