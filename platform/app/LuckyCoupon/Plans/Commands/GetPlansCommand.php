<?php namespace LuckyCoupon\Plans\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Plans\PlansEloquentRepository;

class GetPlansCommand extends BaseCommand
{
	private $plansRepo;
	
	/**
	 * GetPlansCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->plansRepo = new PlansEloquentRepository();
	}
	
	public function handle()
	{
		$planType = (bool)$this->request->get('app_id', false) ? 'shopify' : 'basic';
		
		if ($planType === 'shopify')
		{
			$plans = $this->plansRepo->getShopifyPlans();
		}
		else
		{
			$plans = $this->plansRepo->get($planType);
		}
		
		return ['plans' => $plans];
	}
}