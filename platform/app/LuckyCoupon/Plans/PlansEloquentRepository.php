<?php namespace LuckyCoupon\Plans;

class PlansEloquentRepository implements PlansRepositoryInterface
{
	public $model;
	
	/**
	 * PlansEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new Plan();
	}
	
	/**
	 * @param bool $type
	 * @return mixed
	 */
	public function get($type = null)
	{
		$query = $this->_getQuery($type);
		
		return $query->get();
	}
	
	/**
	 * @param $name
	 * @param $type
	 * @return array
	 */
	public function getByName($name, $type = null)
	{
		$query = $this->_getQuery($type)
					->whereName($name)
					->where('active', 1)
					->get();
		
		return $query->count() > 0 ? $query[0]->toArray() : [];
	}
	
	/**
	 * @param $selected
	 * @return mixed
	 */
	private function _getQuery($selected)
	{
		if (!$selected) return $this->model;
		
		return $this->model->whereType($selected);
	}
	
	/**
	 * @return mixed
	 */
	public function getShopifyPlans()
	{
		return $this->model
				->where('type', 'shopify')
				->where('active', 1)
				->orderByRaw('FIELD(name, "Starter", "Basic", "Advanced", "Pro", "Unlimited") ASC')
				->get();
	}
	
	/**
	 * @param $planName
	 * @param $fields
	 */
	public function update($planName, $fields)
	{
		$this->model
			->where('name', $planName)
			->where('active', 1)
			->update($fields);
	}
}