<?php namespace LuckyCoupon\Plans;

interface PlansRepositoryInterface
{
	public function get();
}