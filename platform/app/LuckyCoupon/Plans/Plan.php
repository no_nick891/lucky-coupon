<?php namespace LuckyCoupon\Plans;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
	protected $table = 'plans';
	
	public $timestamps = false;
}
