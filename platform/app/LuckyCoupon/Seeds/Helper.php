<?php namespace LuckyCoupon\Seeds;


class Helper
{
	public $gameTypes = ['default', 'slot', 'gift', 'wheel', 'scratch'];
	
	/**
	 * @param $path
	 * @return mixed
	 */
	public function getStyle($path)
	{
		$file = $this->getFileContent($path);
		
		return $this->makeOneLine($file);
	}
	
	/**
	 * @param $path
	 * @return array
	 */
	public function getJsConstants($path)
	{
		$file = $this->getFileContent($path);
		
		$templateStrings = $this->_templateStringToArray($file);
		
		$result = $this->_jsConstantsToArray($templateStrings);
		
		return $result;
	}
	
	/**
	 * @param $filePath
	 * @return mixed
	 */
	public function getObjectFromFile($filePath)
	{
		$variable = $this->getJsConstants($filePath);
		
		$variable = array_pop($variable);
		
		return json_decode($variable);
	}
	
	/**
	 * @param $filePath
	 * @return mixed
	 */
	public function getProcessedStyles($filePath)
	{
		return $this->_replaceSelector(
			$this->getStyle($filePath)
		);
	}
	
	/**
	 * @param $styles
	 * @return mixed
	 */
	private function _replaceSelector($styles)
	{
		return strtr($styles, $this->_getSelectorsReplacer());
	}
	
	/**
	 * @return array
	 */
	private function _getSelectorsReplacer()
	{
		return [
			'/*<main-selector>*/' => '#lucky-coupon-%27 + id + %27.game-%27 + this.gameId + %27',
			'/*<small-selector>*/' => '#lucky-coupon-%27 + id + %27.game-%27 + this.gameId + %27 .small-window',
			'/*<wheel-selector>*/' => '#lucky-coupon-%27 + id + %27.game-%27 + this.gameId + %27 .wheel-window',
			'/*<wheel-left>*/' => '#lucky-coupon-%27 + id + %27.game-%27 + this.gameId + %27 .wheel-window.lucky-coupon-left',
			'/*<wheel-right>*/' => '#lucky-coupon-%27 + id + %27.game-%27 + this.gameId + %27 .wheel-window.lucky-coupon-right'
		];
	}
	
	/**
	 * @param $file
	 * @return array
	 */
	private function _templateStringToArray($file)
	{
		return array_values(array_filter(explode('export const ', $this->makeOneLine($file))));
	}
	
	/**
	 * @param $path
	 * @return bool|string
	 */
	public function getFileContent($path)
	{
		$fileName = base_path($path);
		
		return file_get_contents($fileName);
	}
	
	/**
	 * @param $file
	 * @return mixed
	 */
	public function makeOneLine($file)
	{
		return str_replace(["\n", "\t", "\r"], '', $file);
	}
	
	/**
	 * @param $templateStrings
	 * @return array
	 */
	private function _jsConstantsToArray($templateStrings)
	{
		$result = [];
		
		foreach ($templateStrings as $templateString)
		{
			$templateArray = array_map('trim', explode(' = ', $templateString));
			
			$variable = lcfirst(str_replace($this->gameTypes, '', $templateArray[0]));
			
			$result[$variable] = str_replace(';', '', $templateArray[1]);
		}
		
		return $result;
	}
}