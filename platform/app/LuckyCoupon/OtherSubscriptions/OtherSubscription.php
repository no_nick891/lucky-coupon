<?php namespace LuckyCoupon\OtherSubscriptions;

use Illuminate\Database\Eloquent\Model;

class OtherSubscription extends Model
{
    public $table = 'other_subscriptions';
}
