<?php namespace LuckyCoupon\OtherSubscriptions\Commands;

use LuckyCoupon\OtherSubscriptions\OtherSubscriptionsEloquentRepository;
use LuckyCoupon\Plans\PlansEloquentRepository;
use LuckyCoupon\Subscriptions\Commands\AddSubscriptionCommand;

class AddOtherSubscriptionCommand
{
	private $user;
	
	private $selectedPlan;
	
	/**
	 * AddOtherSubscriptionCommand constructor.
	 * @param $user
	 * @param $planName
	 */
	public function __construct($user, $planName)
	{
		$this->user = $user;
		
		$this->selectedPlan = $planName;
	}
	
	public function handle()
	{
		$userId = $this->user->id;
		
		switch ($this->selectedPlan)
		{
			case 'jvzoo-premium': $planName = 'JVZoo Premium'; break;
			case 'jvzoo-premium-plus': $planName = 'JVZoo Premium Plus'; break;
			case 'life-time-access': $planName = 'Life time access'; break;
			default: $planName = ''; break;
		}
		
		if (!$planName) return false;
		
		$planRepo = new PlansEloquentRepository();
		
		$plan = $planRepo->getByName($planName, 'other');
		
		if (!$plan && !$plan['id']) return false;
		
		$otherSubRepo = new OtherSubscriptionsEloquentRepository();
		
		$otherSubscription = $otherSubRepo->insert([
			'user_id' => $userId,
			'active' => 1,
			'plan_id' => $plan['id']
		]);
		
		if (!$otherSubscription->id) return false;
		
		$subscription = [
			'user_id' => $userId,
			'subscription_id' => $otherSubscription->id,
			'subscription_gateway' => 'other'
		];
		
		return dispatch(new AddSubscriptionCommand($this->user, $subscription));
	}
}