<?php namespace LuckyCoupon\OtherSubscriptions;

class OtherSubscriptionsEloquentRepository implements OtherSubscriptionsRepositoryInterface
{
	private $model;
	
	/**
	 * OtherSubscriptionsEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new OtherSubscription();
	}
	
	/**
	 * @param $data
	 * @return OtherSubscription
	 */
	public function insert($data)
	{
		foreach ($data as $column => $value)
		{
			$this->model->{$column} = $value;
		}
		
		$this->model->save();
		
		return $this->model;
	}
}