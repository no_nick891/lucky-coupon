<?php namespace LuckyCoupon\OtherSubscriptions;

interface OtherSubscriptionsRepositoryInterface
{
	public function insert($data);
}