<?php namespace LuckyCoupon\Affiliates;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
    public $table = 'affiliates';
    
    public $timestamps = false;
	
    public $fillable = ['owner_id', 'user_id', 'amount_paid', 'commission', 'is_payed'];
    
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function referral()
	{
		return $this->hasMany('App\LuckyCoupon\Users\User', 'id', 'user_id');
    }
}
