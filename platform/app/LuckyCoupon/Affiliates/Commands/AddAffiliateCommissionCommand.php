<?php namespace LuckyCoupon\Affiliates\Commands;

use App\LuckyCoupon\Users\User;
use LuckyCoupon\BaseCommand;

class AddAffiliateCommissionCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $payment;
	
	/**
	 * AddAffiliateCommissionCommand constructor.
	 * @param $payment
	 */
	public function __construct($payment)
	{
		$this->payment = $payment;
	}
	
	public function handle()
	{
		$affiliateId = $this->payment->user_id;
		
		$amountPayed = (int)$this->payment->amount * 100;
		
		$user = User::whereId($affiliateId)->limit(1)->first();
		
		if ( ! $user) return false;
		
		$affiliate = $user->affiliatesUser->first();
		
		if ( ! $affiliate) return false;
		
		$affiliate->amount_paid = $affiliate->amount_paid + $amountPayed;
		
		return $affiliate->save();
	}
}