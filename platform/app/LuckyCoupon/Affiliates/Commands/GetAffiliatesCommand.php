<?php namespace LuckyCoupon\Affiliates\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class GetAffiliatesCommand extends BaseCommand
{
	private $user;
	
	/**
	 * GetAffiliatesCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->user = $request->user();
	}
	
	public function handle()
	{
		$result = [];
		
		foreach ($this->user->affiliates as $affiliate)
		{
			if (! $affiliate->is_payed)
			{
				$affiliate->referral;
				
				$result[] = $affiliate;
			}
		}
		
		return ['affiliates' => $result];
	}
}