<?php namespace LuckyCoupon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;

class BaseCommand
{
	/**
	 * @var Request $request
	 */
	protected $request;
	
	/**
	 * @param $headers
	 * @return bool|\LuckyCoupon\ShopifyApps\ShopifyApp
	 */
	protected function getWebHookApp($headers)
	{
		$appRepo = new ShopifyAppEloquentRepository();
		
		$app = $appRepo->getApp(data_get($headers, 'x-shopify-shop-domain.0', 0));
		
		if (\App::environment('local'))
		{
			return $app->id ? $app : false;
		}
		
		return !verifyShopifyWebHook($headers) && !$app ? false : $app;
	}
	
	/**
	 * @param Request $request
	 * @return bool
	 */
	public function getErrors($request)
	{
		$validator = Validator::make($request->all(), $request->rules());
		
		if ($validator->fails())
		{
			$errors = $validator->errors();
			
			return $errors;
		}
		
		return false;
	}
	
	/**
	 * @param Request $request
	 * @return array
	 */
	public function getRequestKeys($request)
	{
		return array_keys($request->rules());
	}
	
	/**
	 * @return array
	 */
	public function getRequestData()
	{
		return $this->request->only($this->getRequestKeys($this->request));
	}
	
	/**
	 * @param string $message
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function getSuccessResponse($message = 'success')
	{
		return $this->logResponse(
			['status' => $message],
			200,
			['discount_codes']
		);
	}
	
	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function getErrorResponse()
	{
		return $this->logResponse(
			['status' => '404 Not Found'],
			404,
			['discount_codes']
		);
	}
	
	/**
	 * @param $responseData
	 * @return mixed
	 */
	public function response($responseData)
	{
		return response(json_encode($responseData), 200)
			->header('Access-Control-Allow-Origin', '*');
	}
	
	/**
	 * @param $responseData
	 * @return mixed
	 */
	public function errorResponse($responseData)
	{
		return response(json_encode($responseData), 422)
			->header('Access-Control-Allow-Origin', '*');
	}
	
	/**
	 * @param $view
	 * @return Response
	 */
	public function getResponseWidthAdminExit($view)
	{
		$response = new Response($view);
		
		$exitToAdminValue = $this->getEncodedExitToAdminValue(\Auth::user());
		
		$response->withCookie(cookie('exitToShops', $exitToAdminValue, 3600));
		
		return $response;
	}
	
	/**
	 * @param $user
	 * @return string
	 */
	public function getEncodedExitToAdminValue($user)
	{
		return $this->_getReplacedBase($user->id);
	}
	
	/**
	 * @param $string
	 * @return mixed
	 * @internal param $user
	 */
	private function _getReplacedBase($string)
	{
		return str_replace('=', '.p', base64_encode($string));
	}
	
	/**
	 * @param $array
	 * @param $variables
	 */
	protected function _log($array, $variables = null)
	{
		\Log::error(print_r($array, true));
	}
	
	/**
	 * @param $data
	 * @param $status
	 * @param array $requestKeyData
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function logResponse($data, $status, $requestKeyData = [])
	{
		$request = !$requestKeyData
			? $this->request->all()
			: $this->request->only($requestKeyData);
		
		$this->_log([
			'response_data' => [$status, $data],
			'headers' => $this->request->headers->all(),
			'request' => $request
		]);
		
		return response()->json($data, $status);
	}
}