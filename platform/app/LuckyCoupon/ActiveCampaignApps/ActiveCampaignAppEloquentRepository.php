<?php

namespace LuckyCoupon\ActiveCampaignApps;

/**
 * Class ActiveCampaignAppEloquentRepository
 * @package App\LuckyCoupon\ActiveCampaignApps
 */
class ActiveCampaignAppEloquentRepository implements ActiveCampaignAppEloquentRepositoryInterface
{
    public $model;

    /**
     * KlaviyoAppEloquentRepository constructor.
     */
    public function __construct()
    {
        $this->model = new ActiveCampaignApp();
    }

    /**
     * @param $userId
     * @param $fields
     * @return mixed
     */
    public function updateByUserId($userId, $fields)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('site_id', $fields['site_id'])
            ->update($fields);
    }
}
