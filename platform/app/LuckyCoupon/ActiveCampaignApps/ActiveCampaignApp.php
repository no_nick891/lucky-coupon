<?php

namespace LuckyCoupon\ActiveCampaignApps;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ActiveCampaignApp
 * @package App\LuckyCoupon\ActiveCampaignApps
 */
class ActiveCampaignApp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'active_campaign_apps';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'active',
        'user_id',
        'site_id',
        'private_key',
        'api_url',
        'lists',
        'selected_list_id'
    ];
}
