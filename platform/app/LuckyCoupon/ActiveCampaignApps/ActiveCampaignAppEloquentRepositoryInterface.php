<?php

namespace LuckyCoupon\ActiveCampaignApps;

/**
 * Interface ActiveCampaignAppEloquentRepositoryInterface
 * @package App\LuckyCoupon\ActiveCampaignApps
 */
interface ActiveCampaignAppEloquentRepositoryInterface
{
    /**
     * @param $userId
     * @param $fields
     * @return mixed
     */
    public function updateByUserId($userId, $fields);
}
