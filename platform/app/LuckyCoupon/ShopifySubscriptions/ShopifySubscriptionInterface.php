<?php namespace LuckyCoupon\ShopifySubscriptions;

interface ShopifySubscriptionInterface
{
	public function create($subscription);
	public function getByUserId($userId);
}