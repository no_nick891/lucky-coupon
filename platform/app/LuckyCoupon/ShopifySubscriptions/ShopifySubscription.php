<?php namespace LuckyCoupon\ShopifySubscriptions;

use Illuminate\Database\Eloquent\Model;

class ShopifySubscription extends Model
{
	public $table = 'shopify_subscriptions';
	
    public $timestamps = true;
}
