<?php namespace LuckyCoupon\ShopifySubscriptions\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Plans\PlansEloquentRepository;
use LuckyCoupon\ShopifySubscriptions\ShopifySubscriptionEloquentRepository;
use LuckyCoupon\Subscriptions\SubscriptionEloquentRepository;

class SaveShopifySubscriptionCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $chargeName;
	/**
	 * @var
	 */
	private $userId;
	/**
	 * @var
	 */
	private $app;
	
	private $planRepo;
	
	private $shopifySubRepo;
	
	private $subRepo;
	
	/**
	 * SaveShopifySubscriptionCommand constructor.
	 * @param $chargeName
	 * @param $userId
	 * @param $app
	 */
	public function __construct($chargeName, $userId, $app)
	{
		$this->chargeName = $chargeName;
		
		$this->userId = $userId;
		
		$this->app = $app;
		
		$this->planRepo = new PlansEloquentRepository();
		
		$this->shopifySubRepo = new ShopifySubscriptionEloquentRepository();
		
		$this->subRepo = new SubscriptionEloquentRepository();
	}
	
	public function handle()
	{
		$planName = $this->_getPlanName();
		
		$plan = $this->planRepo->getByName($planName);
		
		if (!$plan) return false;
		
		$shopifySubId = $this->_getShopifySubId($plan);
		
		if (!$shopifySubId) return false;
		
		return $this->_updateSubData($shopifySubId);
	}
	
	/**
	 * @return mixed
	 */
	private function _getPlanName()
	{
		return str_replace([
			'GetWooHoo ',
			' plan for shopify'
		], '', $this->chargeName);
	}
	
	/**
	 * @param $plan
	 * @return mixed
	 */
	private function _getShopifySubId($plan)
	{
		$subscription = $this->shopifySubRepo->getByUserId($this->userId);
		
		if ($subscription)
		{
			$subscription->plan_id = $plan['id'];
			
			$subscription->is_emailed = 0;
			
			$subscription->save();
			
			$shopifySubId = $subscription->id;
		}
		else
		{
			$shopifySubId = $this->shopifySubRepo->create([
				'user_id' => $this->userId,
				'plan_id' => $plan['id'],
				'shop_url' => $this->app->shop,
			    'is_emailed' => 0
			]);
		}
		
		return $shopifySubId;
	}
	
	/**
	 * @param $shopifySubId
	 * @return mixed
	 */
	private function _updateSubData($shopifySubId)
	{
		$data = [
			'user_id' => $this->userId,
			'subscription_id' => $shopifySubId,
			'subscription_gateway' => 'shopify'
		];
		
		$subscription = $this->subRepo->get($this->userId);
		
		if ($subscription)
		{
			$result = $this->subRepo->update($data);
		}
		else
		{
			$result = $this->subRepo->insert($data);
		}
		
		return $result;
	}
}