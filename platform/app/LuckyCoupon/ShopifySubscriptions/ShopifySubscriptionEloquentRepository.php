<?php namespace LuckyCoupon\ShopifySubscriptions;

class ShopifySubscriptionEloquentRepository implements ShopifySubscriptionInterface
{
	public $model;
	
	/**
	 * ShopifySubscriptionEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new ShopifySubscription();
	}
	
	/**
	 * @param $subscription
	 * @return mixed
	 */
	public function create($subscription)
	{
		$this->_assignProperties($subscription);
		
		$this->model->save();
		
		return $this->model->id;
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getByUserId($userId)
	{
		return $this->model->whereUserId($userId)->first();
	}
	
	/**
	 * @param $subscription
	 */
	private function _assignProperties($subscription)
	{
		foreach ($subscription as $key => $item)
		{
			$this->model->{$key} = $item;
		}
	}
	
	/**
	 * @param $data
	 * @return mixed
	 */
	public function update($data)
	{
		$query = $this->model;
		
		if (data_get($data, 'id', false))
		{
			$query = $query->where('id', $data['id']);
		}
		
		if (data_get($data, 'user_id', false))
		{
			$query = $query->where('user_id', $data['user_id']);
		}
		
		return $query->update($data);
	}
}