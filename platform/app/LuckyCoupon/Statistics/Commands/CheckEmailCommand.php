<?php namespace LuckyCoupon\Statistics\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class CheckEmailCommand extends BaseCommand
{
	
	/**
	 * CheckEmailCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		return $this->response(['subscriber_email' => true]);
	}
}