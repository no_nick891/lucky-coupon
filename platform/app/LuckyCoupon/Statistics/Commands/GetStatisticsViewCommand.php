<?php namespace LuckyCoupon\Statistics\Commands;

use App\LuckyCoupon\Users\User;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\Commands\GetGamesByUsersCommand;
use LuckyCoupon\Games\Commands\GetGamesStatisticsCommand;
use ShopifyIntegration\Apps\Commands\GetAppsCommand;

class GetStatisticsViewCommand extends BaseCommand
{
	
	public function handle()
	{
		$users = User::where('app_id', '>', 0)->where('is_admin', 0)->get();
		
		$games = dispatch(new GetGamesByUsersCommand($users));
		
		$apps = dispatch(new GetAppsCommand($users));
		
		$gamesStatistics = dispatch(new GetGamesStatisticsCommand($games));
		
		$totalActive = $this->_getActiveAppsCount($apps);
		
		return view('admin.statistics', compact(['apps', 'totalActive', 'gamesStatistics']));
	}
	
	
	/**
	 * @param $apps
	 * @return int
	 */
	private function _getActiveAppsCount($apps)
	{
		$counter = 0;
		
		foreach ($apps as $app)
		{
			if ($app['installed'] === true)
			{
				$counter++;
			}
		}
		
		return $counter;
	}
}