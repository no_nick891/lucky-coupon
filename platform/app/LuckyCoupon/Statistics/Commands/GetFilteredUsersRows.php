<?php namespace LuckyCoupon\Statistics\Commands;

class GetFilteredUsersRows
{
	/**
	 * @var string $statement
	 * <code>
	 *  $statement = 'app_id > 0'; // shopify users
	 *  $statement = 'app_id = 0'; // non-shopify users
	 * </code>
	 */
	private $statement;
	
	/**
	 * @var string $orderCondition
	 * <code>
	 *  $filter = '';
	 *  $filter = 'all_total_ctr desc';
	 *  $filter = 'all_total_hits desc';
	 *  $filter = 'created_at desc';
	 * </code>
	 */
	private $orderBy;
	
	/**
	 * GetFilteredUsersRows constructor.
	 * @param string $filter
	 * @param string $statement
	 * @param string $filteredValue
	 */
	public function __construct($filter = 'created', $statement = 'app_id > 0', $filteredValue)
	{
		$orderCondition = $this->_getOrderCondition($filter);
		
		$this->where = $this->_getWhere($filter, $filteredValue);
		
		$this->orderBy = $this->_getOrderBy($orderCondition);
		
		$this->statement = $statement;
	}
	
	/**
	 * @param $filter
	 * @return string
	 */
	private function _getOrderCondition($filter)
	{
		switch ($filter)
		{
			case 'created':
				$order = 'created_at desc';
				break;
			case 'best-total-ctr':
				$order = 'all_total_ctr desc';
				break;
			case 'best-total-hits':
				$order = 'all_total_hits desc';
				break;
			case 'shop-url':
				$order = '';
				break;
			default:
				$order = 'created_at desc';
				break;
		}
		
		return $order;
	}
	
	/**
	 * @param $filter
	 * @param $filteredValue
	 * @return string
	 */
	private function _getWhere($filter, $filteredValue)
	{
		$where[] = $filter === 'best-total-ctr'
			? 'user_statistics.all_total_hits > 100'
			: null;
		
		$where[] = ($filter === 'shop-url' && $filteredValue !== '')
			? 'user_statistics.name = \'' . str_replace(['https:', 'http:', '/'], '', $filteredValue) . '\''
			: null;
		
		$where[] = ($filter === 'created' && in_array($filteredValue, ['hide-small', '']))
			? 'user_statistics.all_total_hits > 10'
			: null;
		
		$where[] = ($filter === 'paying')
			? 'user_statistics.is_paying = 1'
			: null;
		
		$where = array_filter($where);
		
		return count($where) > 0 ? 'where ' . trim(implode(' AND ', $where), ' AND ') : '';
	}
	
	/**
	 * @param $orderCondition
	 * @return string
	 */
	private function _getOrderBy($orderCondition)
	{
		return $orderCondition ? 'order by user_statistics.' . $orderCondition : '';
	}
	
	public function handle()
	{
		$query = $this->_getComplexQuery();
		
		$dbConn = config('database.connections.mysql');
		
		try
		{
			$result = [];
			
			$dbh = new \PDO($this->_getConnectionString($dbConn), $dbConn['username'], $dbConn['password']);
			
			$dbh->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));");
			
			$queryResult = $dbh->query($query, \PDO::FETCH_ASSOC);
			
			if (!is_array($queryResult) && !is_object($queryResult)) return [];
			
			foreach ($queryResult as $row)
			{
				$result[] = $row;
			}
			
			return $result;
		}
		catch (\PDOException $e)
		{
			print "Error!: " . $e->getMessage() . "<br/>";
			
			die();
		}
	}
	
	/**
	 * @param $dbConn
	 * @return string
	 */
	private function _getConnectionString($dbConn)
	{
		$ipAddress = \App::environment('local') ? $dbConn['host'] . ':' . $dbConn['port'] : $dbConn['host'];
		
		return $dbConn['driver'] . ':host=' . $ipAddress . ';dbname=' . $dbConn['database'];
	}
	
	/**
	 * @return string
	 */
	private function _getComplexQuery()
	{
		return "
			select *
			from (
				select
					*,
					COUNT(distinct(name)) as all_games,
					SUM(total_hits) as all_total_hits,
					SUM(total_imressions) as all_total_impressions,
					CAST(SUM(total_hits) / SUM(total_imressions) * 100 AS DECIMAL(11,2)) as all_total_ctr
				from (
					select
						users.*,
						c.counter,
						ads.hits as hits, ads.device_hits,
						ads.impressions as impressions, ads.device_impressions as device_impressions,
						(ads.hits + ads.device_hits) as total_hits,
						(ads.impressions + ads.device_impressions) as total_imressions,
						sha.is_paying,
						CAST(ads.hits / ads.impressions * 100 AS DECIMAL(11,2)) as ctr,
						CAST(ads.device_hits / ads.device_impressions * 100 AS DECIMAL(11,2)) as device_ctr,
						CAST((ads.hits + ads.device_hits) / (ads.impressions + ads.device_impressions) * 100 AS DECIMAL(11,2)) as total_ctr
					from users
					left join games on games.user_id = users.id
					left join admin_statistics as ads on games.id = ads.game_id
					left join counters as c on c.column_name = 'user_id' and c.column_value = users.id
					left join shopify_apps as sha on sha.id = users.app_id
					where
						{$this->statement}
						and is_admin = 0
				) as temp
				group by name
			) as user_statistics
			{$this->where}
			{$this->orderBy}";
	}
}