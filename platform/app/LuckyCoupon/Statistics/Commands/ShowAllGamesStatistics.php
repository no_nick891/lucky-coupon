<?php namespace LuckyCoupon\Statistics\Commands;

use LuckyCoupon\Statistics\AdminStatisticRepository;

class ShowAllGamesStatistics
{
	private $request;
	
	/**
	 * ShowAllStoresStatistics constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->adminStatisticsRepo = new AdminStatisticRepository();
	}
	
	public function handle()
	{
		$payingAccounts = getPayingCount();
		
		$statistics = $this->_getStatisticsByGame();
		
		$statistics = $this->_addTotalStoresStatistics($statistics, $payingAccounts);
		
		return view('admin.game_statistics', compact(['statistics', 'payingAccounts']));
	}
	
	/**
	 * @return mixed
	 */
	private function _getStatisticsByGame()
	{
		$rawStatistics = $this->_getRawStatistics();
		
		return $this->_extendStatistics($rawStatistics);
	}
	
	/**
	 * @return array|mixed
	 */
	private function _getRawStatistics()
	{
		$statistics = [];
		
		$statistics = $this->_getFilledVariable(
			$statistics,
			$this->adminStatisticsRepo->getImpressionsByGame(),
			'impressions'
		);
		
		$statistics = $this->_getFilledVariable(
			$statistics,
			$this->adminStatisticsRepo->getHitsByGame(),
			'hits'
		);
		
		return $statistics;
	}
	
	/**
	 * @param $result
	 * @param $statistics
	 * @param $type
	 * @return mixed
	 */
	private function _getFilledVariable($result, $statistics, $type)
	{
		foreach ($statistics as $statistic)
		{
			$result[$statistic->game_type][$statistic->device_type][$type] = $statistic->{$type};
		}
		
		return $result;
	}
	
	/**
	 * @param $statistics
	 * @return mixed
	 */
	private function _extendStatistics($statistics)
	{
		foreach ($statistics as $gameType => $stat)
		{
			$statistics = calculateGroupedStatistic($statistics, $stat, $gameType);
		}
		
		return $statistics;
	}
	
	/**
	 * @param $statistics
	 * @param $payingAccounts
	 * @return mixed
	 */
	private function _addTotalStoresStatistics($statistics, $payingAccounts)
	{
		foreach ($statistics as $gameName => $statistic)
		{
			$statistics[$gameName]['all']['totalNumberOfStores'] = $payingAccounts;
			
			$statistics[$gameName]['all']['totalNumberWithGames'] = getPayingCount($gameName);
		}
		
		return $statistics;
	}
}