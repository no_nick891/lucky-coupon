<?php namespace LuckyCoupon\Statistics\Commands;

use LuckyCoupon\Statistics\AdminStatisticRepository;

class ShowAllStoresStatistics
{
	private $request;
	
	/**
	 * ShowAllStoresStatistics constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->adminStatisticsRepo = new AdminStatisticRepository();
	}
	
	public function handle()
	{
		$statistics = $this->_getStatisticsByDate();
		
		return view('admin.all_statistics', compact('statistics'));
	}
	
	/**
	 * @return array|mixed
	 */
	private function _getStatisticsByDate()
	{
		$statistics = $this->_getStatistics();
		
		return $this->_extendStatistics($statistics);
	}
	
	
	/**
	 * @return array|mixed
	 */
	private function _getStatistics()
	{
		$statistics = [];
		
		$statistics = $this->_getFillStatistics(
			$statistics,
			$this->adminStatisticsRepo->getImpressionsByMonth(),
			'impressions'
		);
		
		$statistics = $this->_getFillStatistics(
			$statistics,
			$this->adminStatisticsRepo->getHitsByMonth(),
			'hits'
		);
		
		return $statistics;
	}
	
	/**
	 * @param $result
	 * @param $data
	 * @param $type
	 * @return mixed
	 */
	private function _getFillStatistics($result, $data, $type)
	{
		foreach ($data as $month)
		{
			$result[$month->month_date][$month->device_type][$type] = $month->{$type};
		}
		
		return $result;
	}
	
	/**
	 * @param $statistics
	 * @return mixed
	 */
	private function _extendStatistics($statistics)
	{
		foreach ($statistics as $date => $stat)
		{
			$statistics = calculateGroupedStatistic($statistics, $stat, $date);
		}
		
		return $statistics;
	}
}