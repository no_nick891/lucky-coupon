<?php namespace LuckyCoupon\Statistics;

class EloquentStatisticRepository implements StatisticRepositoryInterface
{
	public $model;
	
	/**
	 * EloquentStatisticRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new Statistic();
	}
	
	/**
	 * @param $stats
	 * @return mixed
	 */
	public function add($stats)
	{
		return $this->model->insert($stats);
	}
	
	/**
	 * @param $gameId
	 * @param bool $isArray
	 * @param string $orderType
	 * @return array
	 */
	public function getByGameId($gameId, $isArray = true, $orderType = 'none')
	{
		$query = $this->model;
		
		if (!is_array($gameId))
		{
			$query = $query->where('game_id', $gameId);
		}
		else
		{
			$query = $query->where(function($query) use($gameId) {
				foreach ($gameId as $key => $id)
				{
					if ($key === 0)
					{
						$query = $query->where('game_id', $id);
					}
					else
					{
						$query = $query->orWhere('game_id', $id);
					}
				}
			});
		}
		
		if ($orderType !== 'none')
		{
			$query = $query->orderBy($orderType . '_date', 'desc');
		}
		
		$result = $query->whereNotNull('subscriber_email')->get();
		
		return $result->count() > 0 ? ($isArray ? $result->toArray() : $result) : [];
	}
	
	/**
	 * @param $gameId
	 * @param $email
	 * @param $code
	 * @return mixed
	 */
	public function addCodeByGameIdAndEmail($gameId, $email, $code)
	{
		$statistic = $this->model
			->whereGameId($gameId)
			->whereSubscriberEmail($email)
			->first();
		
		return $statistic ? $statistic->update(['coupon_code' => $code]) : false;
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	public function deleteByGameId($gameId)
	{
		return $this->model->where('game_id', $gameId)->delete();
	}
	
	/**
	 * @return mixed
	 */
	public function getHitCountByMonth()
	{
		return \DB::select("
			select count(temp.hit_month) as count, temp.hit_month
			from (
				select statistics.*,
					DATE_FORMAT(statistics.hit_date, \"%Y-%m-01\") as hit_month
				from statistics
				where hit_date is not NULL
				) as temp
			group by temp.hit_month
			order by temp.hit_month desc");
	}
}