<?php namespace LuckyCoupon\Statistics;

class AdminStatisticRepository
{
	/**
	 * @return mixed
	 */
	public function getImpressionsByMonth()
	{
		return $this->_getStatisticsHelper('impression', 'month');
	}
	
	/**
	 * @return mixed
	 */
	public function getHitsByMonth()
	{
		return $this->_getStatisticsHelper('hit', 'month');
	}
	
	/**
	 * @return mixed
	 */
	public function getImpressionsByGame()
	{
		return $this->_getStatisticsHelper('impression', 'game');
	}
	
	/**
	 * @return mixed
	 */
	public function getHitsByGame()
	{
		return $this->_getStatisticsHelper('hit', 'game');
	}
	
	/**
	 * @param $type
	 * @param $field
	 * @return mixed
	 */
	public function _getStatisticsHelper($type, $field)
	{
		return \DB::select(
			\DB::raw($this->_getStatisticsQuery($type, $field))
		);
	}
	
	/**
	 * @param $type
	 * @param string $field
	 * @return string
	 */
	private function _getStatisticsQuery($type, $field = 'month')
	{
		$column = ($field === 'game') ? 'game_type' : 'month_date';
		
		$monthField = ($field === 'month') ? "str_to_date(concat('01-', month(s.{$type}_date), '-', year(s.{$type}_date)),'%d-%m-%Y') as month_date," : '';
		
		$gameField = ($field === 'game') ? ", g.type as game_type" : '';
		
		$gameJoin = ($field === 'game') ? "left join games as g on (g.id = s.game_id and g.active = 1)" : '';
		
		$shopifyAppJoin = ($field === 'game')
			? "left join users as u on g.user_id = u.id
				left join shopify_apps as sa on (u.app_id = sa.id and sa.deleted = 0 and sa.is_paying = 1)"
			: '';
		
		$gameWhere = ($field === 'game') ? "and g.id is not null and g.active = 1" : '';
		
		$innerGroup = ($field === 'month') ? "year(s.{$type}_date), month(s.{$type}_date), s.game_id," : "g.type,";
		
		$outerGameGroupGroup = ($field === 'game') ? "game_type, " : '';
		
		$outerMonthGroupGroup = ($field === 'month') ? ", month_date order by month_date desc" : '';
		
		return "
			select
				temp.{$column},
				cast(sum(temp.{$type}s)as UNSIGNED) as {$type}s,
				temp.device_type
			from (
				select
					{$monthField}
					s.game_id as game_id,
					cast(count(s.game_id) as UNSIGNED) as {$type}s,
					s.device_type{$gameField}
				from statistics as s
					{$gameJoin}
					{$shopifyAppJoin}
				where s.{$type}_date is not null
					{$gameWhere}
				group by {$innerGroup}
					s.device_type
				order by s.{$type}_date desc, s.game_id asc) as temp
			group by {$outerGameGroupGroup}device_type{$outerMonthGroupGroup}";
	}
}