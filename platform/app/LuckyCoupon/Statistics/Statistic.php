<?php namespace LuckyCoupon\Statistics;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Statistic
 * @package LuckyCoupon\Statistics
 */
class Statistic extends Model
{
	/**
	 * @var string
	 */
	protected $table = 'statistics';
	
	public $timestamps = false;
	
	protected $fillable = ['coupon_code'];
}