<?php namespace LuckyCoupon\Statistics;

interface StatisticRepositoryInterface
{
	public function add($stats);
	public function getByGameId($gameId);
	public function deleteByGameId($gameId);
}