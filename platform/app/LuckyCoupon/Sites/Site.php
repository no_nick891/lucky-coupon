<?php namespace LuckyCoupon\Sites;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Site
 * @package LuckyCoupon\Sites
 */
class Site extends Model
{

	/**
	 * @var string
	 */
	protected $table = 'sites';
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function user()
	{
		return $this->hasOne('App\LuckyCoupon\Users\User', 'id', 'user_id');
	}
}
