<?php  namespace LuckyCoupon\Sites;

class EloquentSiteRepository implements SiteRepositoryInterface
{
	public $model;
	
	/**
	 * EloquentSiteRepository constructor.
	 */
	function __construct()
	{
		$this->model = new Site();
	}

	/**
	 * @param $id
	 * @return array
	 */
	public function getById($id)
	{
		$site = $this->model->whereId($id)->get();

		return $site->count() > 0 ? $site[0] : [];
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getByUserId($userId)
	{
		$query = $this->model
			->where('user_id', $userId)
			->orderBy('id');
		
		return $query->get();
	}
	
	/**
	 * @param $site
	 * @return mixed
	 */
	public function insert($site)
	{
		$site['created_at'] = date('Y-m-d h:i:s');
		
		return $this->model->insertGetId($site);
	}

	/**
	 * @param $update
	 * @return mixed
	 */
	public function update($update)
	{
		$id = $update['id'];

		unset($update['id']);

		return $this->model->where('id', $id)->update($update);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function delete($id)
	{
		return $this->model->whereId($id)->delete();
	}
	
	/**
	 * @param $shopUrl
	 * @return bool|mixed
	 */
	public function getUserConnectedSite($shopUrl)
	{
		$sites = $this->model
			->where('url', $shopUrl)
			->get();
		
		foreach ($sites as $site)
		{
			if (!isset($sites[0])) return false;
			
			if ($site->user)
			{
				return $site;
			}
		}
		
		return false;
	}
}