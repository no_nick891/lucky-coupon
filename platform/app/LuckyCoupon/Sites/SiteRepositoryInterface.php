<?php namespace LuckyCoupon\Sites;

interface SiteRepositoryInterface {

	public function getById($id);
	public function getByUserId($userId);
	public function insert($site);
	public function update($site);
	public function delete($id);
}