<?php namespace LuckyCoupon\Sites\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Sites\EloquentSiteRepository;

class AddSiteCommand extends BaseCommand
{
	private $siteRepo;
	
	/**
	 * AddSiteCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->siteRepo = new EloquentSiteRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->response($errs);
		
		$site = $this->request->all();
		
		$user = \Auth::user();
		
		$site['user_id'] = $user->id;
		
		return ['site' => $this->siteRepo->insert($site)];
	}
}