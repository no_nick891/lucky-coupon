<?php namespace LuckyCoupon\Sites\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\Commands\DeleteGameCommand;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Requests\Games\DeleteGameRequest;
use LuckyCoupon\Sites\EloquentSiteRepository;

class DeleteSiteCommand extends BaseCommand
{
	private $siteRepo;
	
	private $gameRepo;
	
	/**
	 * DeleteSiteCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->siteRepo = new EloquentSiteRepository();
		
		$this->gameRepo = new EloquentGameRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $errs;
		
		extract($this->request->only('id'));
		
		$games = $this->gameRepo->getGamesBySiteId($id);
		
		$result = $this->_deleteGames($games, $id);
		
		return ['delete' => $this->siteRepo->delete($id)];
	}
	
	/**
	 * @param $games
	 * @param $id
	 * @return array
	 */
	private function _deleteGames($games, $id)
	{
		$result = [];
		
		foreach ($games as $game)
		{
			$result[$game['id']] = $this->_deleteGame($game['id'], $id);
		}
		
		return $result;
	}
	
	/**
	 * @param $gameId
	 * @param $id
	 * @return mixed
	 */
	private function _deleteGame($gameId, $id)
	{
		$deleteGameRequest = $this->_getDeleteGameRequest($gameId, $id);
		
		$deleteGameRequest['id'] = $gameId;
		
		return dispatch(new DeleteGameCommand($deleteGameRequest));
	}
	
	/**
	 * @param $gameId
	 * @param $siteId
	 * @return DeleteGameRequest
	 */
	private function _getDeleteGameRequest($gameId, $siteId)
	{
		$params = [
			'id' => $gameId,
		    'site_id' => $siteId
		];
		
		return DeleteGameRequest::create($this->_getUri($params), 'DELETE', $params, $_COOKIE, $_FILES, $_SERVER);
	}
	
	/**
	 * @param $params
	 * @return string
	 */
	private function _getUri($params)
	{
		return $_SERVER['HTTP_REFERER'] . $_SERVER['REQUEST_URI'] . '?' . http_build_query($params);
	}
}