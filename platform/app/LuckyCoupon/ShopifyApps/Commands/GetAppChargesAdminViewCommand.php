<?php namespace LuckyCoupon\ShopifyApps\Commands;

use App\LuckyCoupon\Users\User;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use ShopifyIntegration\RecurringCharges\RecurringEloquentRepository;

class GetAppChargesAdminViewCommand
{
	private $userId;
	
	/**
	 * GetAppChargesAdminViewCommand constructor.
	 * @param int $userId
	 */
	public function __construct($userId)
	{
		$this->userId = $userId;
	}
	
	public function handle()
	{
		$recurringRepo = new RecurringEloquentRepository();
		
		$user = User::find($this->userId);
		
		$charges = $recurringRepo->getCharges((int)$user->app_id);
		
		$shop = (new ShopifyAppEloquentRepository())->getAppById((int)$user->app_id);
		
		$name = $shop['shop'];
		
		return view('admin.charges', compact(['charges', 'name']));
	}
}