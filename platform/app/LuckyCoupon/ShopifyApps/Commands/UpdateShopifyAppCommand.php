<?php namespace LuckyCoupon\ShopifyApps\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;

class UpdateShopifyAppCommand extends BaseCommand
{
	private $appRepo;
	
	/**
	 * UpdateShopifyAppCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->appRepo = new ShopifyAppEloquentRepository();
	}
	
	public function handle()
	{
		$request = $this->request->all();
		
		return ['shopify_app' => $this->_updateApp($request)];
	}
	
	/**
	 * @param $request
	 * @return mixed
	 */
	private function _updateApp($request)
	{
		$request['deleted'] = 0;
		
		return $this->appRepo->updateAppByUrl($request);
	}
}