<?php namespace LuckyCoupon\ShopifyApps\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;

class UpdateShopifyAppModalFlagCommand extends BaseCommand
{
	private $shopifyRepo;
	
	/**
	 * UpdateShopifyAppModalFlagCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->shopifyRepo = new ShopifyAppEloquentRepository();
	}
	
	public function handle()
	{
		if ($err = $this->getErrors($this->request)) return $err;
		
		$data = $this->request->only($this->getRequestKeys($this->request));
		
		return ['showed_rate_modal' => $this->shopifyRepo->updateModalFlag($data['id'], $data['showed_rate_modal'])];
	}
}