<?php  namespace LuckyCoupon\ShopifyApps\Commands; 

use LuckyCoupon\BaseCommand;
use ShopifyIntegration\Commands\InitShopifyAccountCommand;

class InitializeShopifyApplicationCommand extends BaseCommand
{
	private $app;
	/**
	 * @var
	 */
	private $chargeName;
	
	/**
	 * InitializeShopifyApplicationCommand constructor.
	 * @param $app
	 * @param $chargeName
	 */
	public function __construct($app, $chargeName)
	{
		$this->app = $app;
		
		$this->chargeName = $chargeName;
	}
	
	public function handle()
	{
		$user = dispatch(new InitShopifyAccountCommand($this->app, $this->chargeName));
		
		$this->_updateShopifyApp();
		
		return $user;
	}
	
	private function _updateShopifyApp()
	{
		$this->app->plan_id = 0;
		
		$this->app->redirect_uri = NULL;
		
		$this->app->trial_days = 0;
		
		$this->app->is_paying = 1;
		
		$this->app->save();
	}
}