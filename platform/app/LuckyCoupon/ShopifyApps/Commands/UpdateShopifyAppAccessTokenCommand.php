<?php  namespace LuckyCoupon\ShopifyApps\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use ShopifyIntegration\Provider;

class UpdateShopifyAppAccessTokenCommand extends BaseCommand {

	protected $request;
	
	private $appRepo;
	
	/**
	 * UpdateShopifyAppAccessTokenCommand constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->appRepo = new ShopifyAppEloquentRepository();
	}
	
	/**
	 * @return bool
	 */
	public function handle()
	{
		$accessToken = dispatch(new GetShopifyAppAccessTokenCommand($this->request));
		
		if ($accessToken)
		{
			if ($this->_updateToken($accessToken, $this->request))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @param $accessToken
	 * @param $request
	 * @return bool|int|mixed
	 */
	private function _updateToken($accessToken, $request)
	{
		$app = $this->appRepo->getApp($request['shop']);
		
		$planId = $app && $app->plan_id ? $app->plan_id : 0;
		
		$request = array_merge(
			$request, ['access_token' => $accessToken, 'plan_id' => $planId]
		);
		
		return $app ? $this->appRepo->updateApp($app, $request) : $this->appRepo->model->insertGetId($request);
	}
}