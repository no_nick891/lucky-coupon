<?php namespace LuckyCoupon\ShopifyApps\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;

class GetShopifyAppCommand extends BaseCommand
{
	private $appRepo;
	
	/**
	 * GetShopifyAppCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->appRepo = new ShopifyAppEloquentRepository();
	}
	
	public function handle()
	{
		if ($err = $this->getErrors($this->request)) return $err;
		
		$appId = $this->request->only('id');
		
		return ['shopify_app' => $this->appRepo->getAppById($appId)];
	}
}