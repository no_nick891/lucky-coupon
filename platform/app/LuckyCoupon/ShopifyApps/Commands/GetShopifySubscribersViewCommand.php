<?php namespace LuckyCoupon\ShopifyApps\Commands;

use LuckyCoupon\BaseCommand;

class GetShopifySubscribersViewCommand extends BaseCommand
{
	public function handle()
	{
		$subscribers = dispatch(new GetShopifySubscribersCommand());
		
		return view('admin.subscribers', compact('subscribers'));
	}
}