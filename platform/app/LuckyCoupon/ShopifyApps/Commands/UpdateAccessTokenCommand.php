<?php  namespace LuckyCoupon\ShopifyApps\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use LuckyCoupon\Users\UserEloquentRepository;

class UpdateAccessTokenCommand extends BaseCommand {

	protected $request;

	/**
	 * UpdateAccessTokenCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$requestArray = $this->request->all();
		
		if (dispatch(new UpdateShopifyAppAccessTokenCommand($requestArray)))
		{
			$appRepo = new ShopifyAppEloquentRepository();
			
			$app = $appRepo->getApp($requestArray['shop']);
			
			$userRepo = new UserEloquentRepository();
			
			$user = $userRepo->getUserByAppId($app->id);
			
			if ($user)
			{
				\Auth::login($user);
				
				return redirect('/');
			}
		}
		
		return redirect('/shopify/auth/install/fail');
	}
}