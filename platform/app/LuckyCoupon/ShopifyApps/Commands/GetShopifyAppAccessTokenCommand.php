<?php  namespace LuckyCoupon\ShopifyApps\Commands; 

use LuckyCoupon\BaseCommand;
use ShopifyIntegration\Provider;

class GetShopifyAppAccessTokenCommand extends BaseCommand {

	protected $request;
	
	/**
	 * GetShopifyAppAccessTokenCommand constructor.
	 * @param array $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$shopifyProvider = new Provider($this->request['shop']);
		
		return $shopifyProvider->getAccessToken($this->request);
	}
}