<?php  namespace LuckyCoupon\ShopifyApps\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Sites\EloquentSiteRepository;
use LuckyCoupon\Users\UserEloquentRepository;
use ShopifyIntegration\Apps\AppEloquentRepository;
use ShopifyIntegration\Apps\Commands\InitializeUserDataCommand;

class DeleteShopifyAppChainedCommand extends BaseCommand {
	
	/**
	 * @var string
	 */
	private $siteUrl;
	
	private $userRepo;
	
	private $siteRepo;
	
	private $appRepo;
	
	/**
	 * DeleteShopifyAppChainedCommand constructor.
	 * @param $siteUrl
	 */
	public function __construct($siteUrl)
	{
		$this->siteUrl = $siteUrl;
		
		$this->appRepo = new AppEloquentRepository();
		
		$this->userRepo = new UserEloquentRepository();
		
		$this->siteRepo = new EloquentSiteRepository();
	}
	
	public function handle()
	{
		$app = $this->appRepo->getApp($this->siteUrl);
		
		$site = $this->siteRepo->getUserConnectedSite($app->shop);
		
		if ($site)
		{
			// remove delete code from InitializeUserDataCommand and place it here
			$initUserData = new InitializeUserDataCommand($app);
			
			$initUserData->clearGames($site->user_id);
			
			$user = $this->userRepo->getUserById($site->user_id);
			
			$user->delete();
			
			$site->delete();
		}
		
		$app->delete();
	}
}