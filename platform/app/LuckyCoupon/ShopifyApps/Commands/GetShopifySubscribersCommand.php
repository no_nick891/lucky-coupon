<?php namespace LuckyCoupon\ShopifyApps\Commands;

use LuckyCoupon\BaseCommand;

class GetShopifySubscribersCommand extends BaseCommand
{
	public function handle()
	{
		$fields = [
			'u.id as user_id',
			'u.name as shop_name',
			'g.type as game_type', 's.*',
			'DATE_ADD(s.hit_date, INTERVAL 3 HOUR) as hit_date'
		];
		
		return \DB::table('statistics as s')
			->select(\DB::raw(implode(', ', $fields)))
			->leftJoin('games as g', 'g.id', '=', 's.game_id')
			->leftJoin('users as u', 'u.id', '=', 'g.user_id')
			->where('u.app_id', '>', 0)
			->whereNull('s.impression_date')
			->orderBy('s.hit_date', 'desc')
			->paginate(200);
	}
}