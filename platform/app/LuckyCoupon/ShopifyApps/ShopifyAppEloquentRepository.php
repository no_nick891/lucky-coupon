<?php namespace LuckyCoupon\ShopifyApps;

class ShopifyAppEloquentRepository implements ShopifyAppRepositoryInterface
{
	public $model;
	
	/**
	 * ShopifyAppEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new ShopifyApp();
	}
	
	/**
	 * @param $appId
	 * @return array
	 */
	public function getAppById($appId)
	{
		$result = $this->model
			->select('id', 'rated_us', 'showed_rate_modal', 'shop')
			->whereId($appId)
			->get();
		
		return $result->count() > 0 ? $result->toArray()[0] : [];
	}
	
	/**
	 * @param $appId
	 * @return mixed
	 */
	public function getAppObjById($appId)
	{
		return $result = $this->model
			->whereId($appId)
			->get();
	}
	
	/**
	 * @param $shopUrl
	 * @return array
	 */
	public function getByShopUrl($shopUrl)
	{
		$result = $this->model
			->where('shop', $shopUrl)
			->get();
		
		return $result->count() > 0 ? $result->toArray()[0] : [];
	}
	
	/**
	 * @param $appId
	 * @param $flag
	 * @return bool
	 */
	public function updateModalFlag($appId, $flag)
	{
		return $this->model->whereId($appId)->update(['showed_rate_modal' => $flag]);
	}
	
	/**
	 * @param $shopUrl
	 * @return ShopifyApp|array mixed
	 */
	public function getApp($shopUrl)
	{
		$app = $this->model->where('shop', $shopUrl)->get();
		
		return isset($app[0]) ? $app[0] : [];
	}
	
	/**
	 * @param $shopUrl
	 * @return bool
	 */
	public function getAppAccessToken($shopUrl)
	{
		$app = $this->getApp($shopUrl);
		
		return isset($app->access_token) ? $app->access_token : false;
	}
	
	/**
	 * @param $data
	 * @return mixed
	 */
	public function updateAppByUrl($data)
	{
		if (!isset($data['shop']) && !$data['shop']) return false;
		
		return $this->updateApp($this->getApp($data['shop']), $data);
	}
	
	/**
	 * @param $app ShopifyApp
	 * @param $request
	 * @return mixed
	 */
	public function updateApp($app, $request)
	{
		foreach ($request as $varName => $value)
		{
			$app->{$varName} = $value;
		}
		
		if ($app->save())
		{
			return isset($app->id) ? $app->id : false;
		}
		
		return false;
	}
}