<?php namespace LuckyCoupon\ShopifyApps;


interface ShopifyAppRepositoryInterface
{
	public function getAppById($appId);
	public function updateModalFlag($appId, $flag);
	public function getApp($shopUrl);
	public function getAppAccessToken($shopUrl);
}