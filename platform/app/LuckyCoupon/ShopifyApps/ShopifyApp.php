<?php namespace LuckyCoupon\ShopifyApps;

use Illuminate\Database\Eloquent\Model;
use ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrder;

class ShopifyApp extends Model
{
    protected $table = 'shopify_apps';
    
    public $timestamps = false;
	
    protected $attributes = ['access_token' => '', 'code' => ''];
    
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function user()
	{
		return $this->hasOne('App\LuckyCoupon\Users\User', 'app_id');
    }
    
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function plan()
	{
		return $this->hasOne('LuckyCoupon\Plans\Plan', 'id', 'plan_id');
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany|\ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrder
	 */
	public function orders()
	{
		return $this->hasMany(ShopifyAppOrder::class, 'app_id');
	}
}
