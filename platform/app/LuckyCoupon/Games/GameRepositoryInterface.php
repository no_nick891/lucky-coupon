<?php namespace LuckyCoupon\Games;

interface GameRepositoryInterface {
	public function getById($id);
	public function getByUserId($userId);
	public function getBySiteId($siteId);
	public function insert($game);
	public function update($site);
	public function delete($id);
	public function deleteByUserId($userId, $gameId);
	public function deleteGame($userId, $game);
}