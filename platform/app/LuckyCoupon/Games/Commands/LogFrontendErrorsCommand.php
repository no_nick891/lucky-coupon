<?php  namespace LuckyCoupon\Games\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Requests\Games\LogFrontendGameErrorsRequest;

class LogFrontendErrorsCommand extends BaseCommand {

	protected $request;

	/**
	 * LogFrontendErrorsCommand constructor.
	 * @param LogFrontendGameErrorsRequest $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->_response($errs);
		
		$data = $this->getRequestData();
		
		$path = storage_path('logs');
		
		if (!is_dir($path)) mkdir($path, 0777);
		
		file_put_contents(
			$path . '/runtime-' . date('Y-m-d') . '.txt',
			date('Y-m-d h:i:s') . ' ' . str_replace('\n', "\r\n", $data['error']) . "\r\n\r\n",
			FILE_APPEND | LOCK_EX
		);
		
		return $this->_response(['error' => 'Error added.']);
	}
	
	/**
	 * @param $responseData
	 * @return mixed
	 */
	private function _response($responseData)
	{
		return response(json_encode($responseData), 200)
			->header('Access-Control-Allow-Origin', '*');
	}
}