<?php namespace LuckyCoupon\Games\Commands;

use LuckyCoupon\AdminStatistics\AdminStatisticEloquentRepository;
use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Statistics\EloquentStatisticRepository;

class DeleteGameCommand extends BaseCommand
{
	private $gameRepo;
	
	/**
	 * DeleteGameCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->gameRepo = new EloquentGameRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $errs;
		
		$game = $this->getRequestData();
		
		$user = \Auth::user();
		
		$userId = $user->id;
		
		return ['game' => $this->gameRepo->deleteGame($userId, $game)];
	}
}