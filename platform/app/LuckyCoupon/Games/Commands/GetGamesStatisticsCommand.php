<?php namespace LuckyCoupon\Games\Commands;

class GetGamesStatisticsCommand
{
	private $games;
	
	/**
	 * GetShopifyGamesStatisticsCommand constructor.
	 * @param $games
	 */
	public function __construct($games)
	{
		$this->games = $games;
	}
	
	public function handle()
	{
		$statistics = $this->_getEmptyStatistics();
		
		$statistics = $this->_getGameStatistics($this->games, $statistics);
		
		$statistics['desktop']['ctr'] = $this->_calculateCtr($statistics['desktop']);
		
		$statistics['mobile']['ctr'] = $this->_calculateCtr($statistics['mobile']);
		
		$statistics['total'] = $this->_calculateTotal($statistics);
		
		$statistics['total']['ctr'] = $this->_calculateCtr($statistics['total']);
		
		return $statistics;
	}
	
	
	/**
	 * @param int $hits
	 * @param int $impressions
	 * @return array
	 */
	private function _getStatistic($hits = 0, $impressions = 0)
	{
		return [
			'impressions' => $impressions,
			'hits' => $hits,
			'ctr' => 0
		];
	}
	
	/**
	 * @param $statistic
	 * @param $game
	 * @return mixed
	 */
	private function _addStatistic($statistic, $game)
	{
		$pc = $statistic['desktop'];
		
		$device = $statistic['mobile'];
		
		$gameStat = $game->adminStatistic;
		
		$statistic['desktop'] = $this->_getStatistic($pc['hits'] + $gameStat->hits,
			$pc['impressions'] + $gameStat->impressions);
		
		$statistic['mobile'] = $this->_getStatistic($device['hits'] + $gameStat->device_hits,
			$device['impressions'] + $gameStat->device_impressions);
		
		return $statistic;
	}
	
	/**
	 * @param $stat
	 * @return string
	 */
	private function _calculateCtr($stat)
	{
		return number_format(
			$stat['hits'] === 0 || ! $stat['hits']
				? 0
				: floatVal($stat['hits']/$stat['impressions']* 100), 2);
	}
	
	/**
	 * @param $games
	 * @param $statistic
	 * @return mixed
	 */
	private function _getGameStatistics($games, $statistic)
	{
		foreach ($games as $gameApp)
		{
			foreach ($gameApp as $game)
			{
				$statistic = $this->_addStatistic($statistic, $game);
			}
		}
		
		return $statistic;
	}
	
	/**
	 * @return array
	 */
	private function _getEmptyStatistics()
	{
		return [
			'desktop' => $this->_getStatistic(0, 0),
			'mobile' => $this->_getStatistic(0, 0),
			'total' => $this->_getStatistic(0, 0)
		];
	}
	
	/**
	 * @param $stat
	 * @return array
	 */
	private function _calculateTotal($stat)
	{
		return $this->_getStatistic(
			$stat['desktop']['hits'] + $stat['mobile']['hits'],
			$stat['desktop']['impressions'] + $stat['mobile']['impressions']
		);
	}
}