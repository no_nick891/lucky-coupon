<?php namespace LuckyCoupon\Games\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Integrations\Commands\AddEmailToServicesCommand;
use LuckyCoupon\Statistics\EloquentStatisticRepository;

/**
 * Class UpdateGameStatisticCommand
 * @package LuckyCoupon\Games\Commands
 */
class UpdateGameStatisticCommand extends BaseCommand
{
	private $field;
	
	private $statRepo;
	
	/**
	 * UpdateStatisticCommand constructor.
	 * @param $field
	 * @param $request
	 */
	public function __construct($field, $request)
	{
		$this->field = $field;
		
		$this->request = $request;
		
		$this->statRepo = new EloquentStatisticRepository();
	}
	
	public function handle()
	{
		try
		{
			return $this->_incrementStatisticField($this->field, $this->request);
		}
		catch (\Exception $exception)
		{
			return $this->_response(['result' => 1, 'text' => $exception->getMessage()]);
		}
	}
	
	/**
	 * @param Request $request
	 * @param $field
	 * @return bool
	 */
	private function _incrementStatisticField($field, $request)
	{
		if ($errs = $this->getErrors($request)) return $this->_response($errs);
		
		$statisticData = $request->only($this->getRequestKeys($request));

		$email = data_get($statisticData, 'subscriber_email', null);
		
		if($this->_isTestEmail($email)) return $this->_response(['result' => 1]);
		
		$name = data_get($statisticData, 'subscriber_name', null);
		
		$deviceType = $request->get('device_type', 'pc');
		
		$gameId = $statisticData['id'];
		
		dispatch((new UpdateGameCountersCommand($gameId, $field, $email, $name, $deviceType))->onQueue('counter'));
		
		$this->_addToService($email, $gameId);
		
		return $this->_response(['result' => 1]);
	}
	
	/**
	 * @param $responseData
	 * @return mixed
	 */
	private function _response($responseData)
	{
		return response(json_encode($responseData), 200)
			->header('Access-Control-Allow-Origin', '*');
	}
	
	/**
	 * @param $email
	 * @param $gameId
	 */
	private function _addToService($email, $gameId)
	{
		if ($email && $email !== 'non-collect-option-on')
		{
			dispatch((new AddEmailToServicesCommand($gameId, $email))->onQueue('dispatcher'));
		}
	}
	
	/**
	 * @param $email
	 * @return bool
	 */
	private function _isTestEmail($email)
	{
		return $email === 'test@test.com';
	}
}