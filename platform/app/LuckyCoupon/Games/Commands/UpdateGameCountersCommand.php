<?php namespace LuckyCoupon\Games\Commands;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use LuckyCoupon\AdminStatistics\Commands\IncrementAdminStatisticCommand;
use LuckyCoupon\Counters\Commands\UpdateGameCounterCommand;
use LuckyCoupon\Games\EloquentGameRepository;

/**
 * Class UpdateGameCountersCommand
 * @package LuckyCoupon\Games\Commands
 */
class UpdateGameCountersCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    private $gameId;
    /**
     * @var
     */
    private $field;
    /**
     * @var
     */
    private $email;
    /**
     * @var
     */
    private $name;

    /**
     * @var
     */
    private $deviceType;

    /**
     * @var EloquentGameRepository
     */
    private $gameRepo;

    /**
     * Create a new job instance.
     *
     * @param $gameId
     * @param $field
     * @param $email
     * @param $name
     * @param $deviceType
     */
    public function __construct($gameId, $field, $email, $name, $deviceType)
    {
        $this->gameId = $gameId;

        $this->field = $field;

        $this->email = $email;

        $this->name = $name;

        $this->deviceType = $deviceType;

        $this->gameRepo = new EloquentGameRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        dispatch((new UpdateGameCounterCommand($this->gameId, $this->field))->onQueue('counter'));

        dispatch((new IncrementAdminStatisticCommand($this->gameId, getDevice($this->deviceType) . $this->field))->onQueue('counter'));

        $this->gameRepo->updateStat($this->gameId, $this->field, $this->email, $this->name, $this->deviceType);
    }
}
