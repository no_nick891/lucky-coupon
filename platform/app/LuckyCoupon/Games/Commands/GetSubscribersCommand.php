<?php namespace LuckyCoupon\Games\Commands;

use Illuminate\Support\Facades\Response;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Statistics\EloquentStatisticRepository;

class GetSubscribersCommand extends BaseCommand
{
    /**
     * @var EloquentStatisticRepository
     */
	private $statisticRepo;
	
	/**
	 * GetSubscribersCommand constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->statisticRepo = new EloquentStatisticRepository();
	}
	
	/**
	 * @return mixed
	 */
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->_response($errs);
		
		$gameData = $this->request->only($this->getRequestKeys($this->request));
		
		$emails = $this->getEmails($gameData['id']);
		
		$out = $this->getCsv($emails);
		
		return $this->response($out);
	}
	
	/**
	 * @param $gameId
	 * @return array
	 */
	public function getEmails($gameId)
	{
		$result = [];
		
		$emails = $this->statisticRepo->getByGameId($gameId, true, 'hit');
		
		foreach ($emails as $email)
		{
			$result[] = [$email['subscriber_email'], $email['subscriber_name'], $email['coupon_code']];
		}
		
		return $result;
	}
	
	/**
	 * @param $result
	 * @return mixed
	 */
	public function response($result)
	{
		$headers = [
			'Content-type' => 'text/csv',
			'Content-Length' => strlen($result)
		];
		
		return Response::make($result, 200, $headers);
	}
	
	/**
	 * @param $emails
	 * @return string
	 */
	private function getCsv($emails)
	{
		$out = '';
		
		foreach ($emails as $email)
		{
			$out .= (is_array($email) ? implode(',', $email) : $email) . "\r\n";
		}
		
		return $out;
	}
}