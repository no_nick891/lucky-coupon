<?php namespace LuckyCoupon\Games\Commands;

use LuckyCoupon\Games\EloquentGameRepository;

class TurnOffGamesCommand
{
	private $game;
	
	/**
	 * TurnOffGamesCommand constructor.
	 * @param $game
	 */
	public function __construct($game)
	{
		$this->game = $game;
		
		$this->gameRepo = new EloquentGameRepository();
	}
	
	public function handle()
	{
		$id = $this->game['id'];
		
		$siteId = $this->game['site_id'];
		
		$userId = $this->game['user_id'];
		
		return $this->gameRepo->model
			->where('user_id', $userId)
			->where('site_id', $siteId)
			->where('active', 1)
			->where('id', '<>', $id)
			->update(['active' => 0]);
	}
}