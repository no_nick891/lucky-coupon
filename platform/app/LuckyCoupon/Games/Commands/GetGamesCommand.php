<?php namespace LuckyCoupon\Games\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\EloquentGameRepository;

class GetGamesCommand extends BaseCommand
{
	private $gameRepo;
	
	/**
	 * GetGameCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->gameRepo = new EloquentGameRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $errs;
		
		$siteId = $this->getRequestData();
		
		$userId = \Auth::user()->id;
		
		$games = $this->gameRepo->getByUserId($userId, 0, $siteId);
		
		return ['games' => $games];
	}
}