<?php namespace LuckyCoupon\Games\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Requests\Games\PutGameRequest;
use LuckyCoupon\Requests\Sites\PutSiteRequest;
use LuckyCoupon\Sites\Commands\AddSiteCommand;

class AddGameWithSiteCommand extends BaseCommand
{
	/**
	 * AddGameWithSiteCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->response($errs);
		
		$requestData = $this->getRequestData();
		
		$siteAddResult = $this->_addSite($requestData);
		
		$siteId = $siteAddResult['site'];
		
		$gameAddResult = (array)json_decode($this->_addGame($requestData, $siteId)->original);
		
		return array_merge(['site' => $siteId], $gameAddResult);
	}
	
	/**
	 * @param $requestData
	 * @return mixed
	 */
	private function _getPutSiteRequest($requestData)
	{
		$params = ['url' => $requestData['url']];
		
		return PutSiteRequest::create($this->_getUri($params), 'PUT', $params, $_COOKIE, $_FILES, $_SERVER);
	}
	
	/**
	 * @param $requestData
	 * @param $siteId
	 * @return PutGameRequest
	 */
	private function _getPutGameRequest($requestData, $siteId)
	{
		$params = [
			'type' => $requestData['type'],
		    'site_id' => $siteId,
		    'name' => $requestData['name'],
		    'is_created' => 1,
		    'new_game' => 1
		];
		
		return PutGameRequest::create($this->_getUri($params), 'PUT', $params, $_COOKIE, $_FILES, $_SERVER);
	}
	
	/**
	 * @param $params
	 * @return string
	 */
	private function _getUri($params)
	{
		return $_SERVER['HTTP_REFERER'] . $_SERVER['REQUEST_URI'] . '?' . http_build_query($params);
	}
	
	/**
	 * @param $requestData
	 * @param $siteId
	 * @return mixed
	 */
	private function _addGame($requestData, $siteId)
	{
		$gameRequest = $this->_getPutGameRequest($requestData, $siteId);
		
		$gameRequest->user = \Auth::user();
		
		unset($gameRequest['user_id']);
		
		unset($gameRequest['url']);
		
		return dispatch(new AddGameCommand($gameRequest));
	}
	
	/**
	 * @param $requestData
	 * @return mixed
	 */
	private function _addSite($requestData)
	{
		$putSiteRequest = $this->_getPutSiteRequest($requestData);
		
		$putSiteRequest->user = \Auth::user();
		
		unset($putSiteRequest['name']);
		
		unset($putSiteRequest['type']);
		
		return dispatch(new AddSiteCommand($putSiteRequest));
	}
}