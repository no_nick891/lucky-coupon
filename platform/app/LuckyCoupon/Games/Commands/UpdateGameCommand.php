<?php namespace LuckyCoupon\Games\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\EloquentGameRepository;
use Redis;

class UpdateGameCommand extends BaseCommand
{
	private $gameRepo;
	
	/**
	 * UpdateGameCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->gameRepo = new EloquentGameRepository();
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $errs;
		
		$game = $this->getRequestData();
		
		if (isset($game['new_game']) && $game['new_game'])
		{
			dispatch(new TurnOffGamesCommand(array_merge($game, ['user_id' => $this->request->user()->id])));
		}
		
		unset($game['new_game']);
		
		$saved = $this->gameRepo->update($game);
		
		if ($saved)
		{
			Redis::del('script:site:' . $game['site_id']);
		}
		
		return ['game' => $saved];
	}
}