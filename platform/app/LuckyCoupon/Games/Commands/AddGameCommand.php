<?php namespace LuckyCoupon\Games\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\AdminStatistics\AdminStatisticEloquentRepository;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Coupons\Commands\UpdateCouponsCommand;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Requests\Games\PutGameRequest;
use LuckyCoupon\Seeds\Helper;
use LuckyCoupon\Settings\Commands\UpdateSettingsCommand;

class AddGameCommand extends BaseCommand
{
	private $gameRepo;
	
	private $helper;
	
	private $adminStatisticRepo;
	
	/**
	 * AddGameCommand constructor.
	 * @param PutGameRequest $request
	 */
	public function __construct(PutGameRequest $request)
	{
		$this->request = $request;
		
		$this->helper = new Helper();
		
		$this->gameRepo = new EloquentGameRepository();
		
		$this->adminStatisticRepo = new AdminStatisticEloquentRepository();
	}
	
	/**
	 * @return array
	 */
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->response($errs);
		
		$gameMeta = $this->getRequestData();
		
		$user = \Auth::user();
		
		$gameMeta['user_id'] = $user->id;
		
		return $this->_createGame($gameMeta);
	}
	
	/**
	 * @param $gameMeta
	 * @return mixed
	 */
	private function _createGame($gameMeta)
	{
		return \DB::transaction(function() use ($gameMeta) {
			
			$game = $this->gameRepo->create($gameMeta);
			
			$settingsRequest = $this->_getCustomRequest($game, 'settings');
			
			$settings = dispatch(new UpdateSettingsCommand($settingsRequest));
			
			$couponsRequest = $this->_getCustomRequest($game, 'coupons');
			
			$coupons = dispatch(new UpdateCouponsCommand($couponsRequest));
			
			$this->adminStatisticRepo->addRow($game['id']);
			
			return $this->response([
				'game' => $game,
				'settings' => $settings,
				'coupons' => $coupons
			]);
		});
	}
	
	/**
	 * @param $game
	 * @param $dataName
	 * @return Request
	 */
	private function _getCustomRequest($game, $dataName)
	{
		$customRequest = new Request();
		
		$customRequest['game'] = $game;
		
		$customRequest[$dataName] = $this->_getData($game['type'], $dataName);
		
		return $customRequest;
	}
	
	/**
	 * @param $type
	 * @param $dataName
	 * @return mixed
	 */
	private function _getData($type, $dataName)
	{
		$type = ($type === 'coupon' ? 'default' : $type);
		
		$templatesPath = 'resources/assets/js/helpers/templates/';
		
		$settings = $this->helper->getJsConstants($templatesPath . $type . '/' . $dataName . '.js');
		
		$array = [];
		
		$result = $this->objToArray(json_decode($settings['game' . ucfirst($dataName)]), $array);
		
		if ($dataName === 'settings')
		{
			$result['text'] = $this->_getTextTabContent($type);
		}
		
		return $result;
	}
	
	/**
	 * @param $type
	 * @return mixed
	 */
	private function _getTextTabContent($type)
	{
		$path = 'resources/assets/js/helpers/templates/' . $type . '/' . 'text.js';
		
		return json_decode($this->helper->getJsConstants($path)['text'], true);
	}
	
	/**
	 * @param $obj
	 * @param $arr
	 * @return mixed
	 */
	private function objToArray($obj, &$arr)
	{
		if ( ! is_object($obj) && ! is_array($obj))
		{
			$arr = $obj;
			
			return $arr;
		}
		
		foreach ($obj as $key => $value)
		{
			if ( ! empty($value))
			{
				$arr[$key] = [];
				
				$this->objToArray($value, $arr[$key]);
			}
			else
			{
				$arr[$key] = $value;
			}
		}
		
		return $arr;
	}
}