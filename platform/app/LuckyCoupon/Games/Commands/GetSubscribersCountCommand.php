<?php namespace LuckyCoupon\Games\Commands;

use Illuminate\Support\Facades\Response;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Statistics\EloquentStatisticRepository;

class GetSubscribersCountCommand extends BaseCommand
{
	private $statisticRepo;
	
	/**
	 * GetSubscribersCommand constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->statisticRepo = new EloquentStatisticRepository();
	}
	
	/**
	 * @return mixed
	 */
	public function handle()
	{
		$gameData = $this->request->only('ids');
		
		$emails = $emails = $this->statisticRepo->getByGameId($gameData['ids']);
		
		return $this->response(json_encode(['count' => count($emails)]));
	}
	
	/**
	 * @param $result
	 * @return mixed
	 */
	public function response($result)
	{
		$headers = [
			'Content-type' => 'text/plain',
			'Content-Length' => strlen($result)
		];
		
		return Response::make($result, 200, $headers);
	}
}