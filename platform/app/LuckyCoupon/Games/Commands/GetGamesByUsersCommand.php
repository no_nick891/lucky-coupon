<?php namespace LuckyCoupon\Games\Commands;

use LuckyCoupon\Games\EloquentGameRepository;

class GetGamesByUsersCommand
{
	private $users;
	
	/**
	 * GetShopifyGamesCommand constructor.
	 * @param array $users
	 */
	public function __construct($users)
	{
		$this->users = $users;
		
		$this->gamesRepo = new EloquentGameRepository();
	}
	
	public function handle()
	{
		return $this->_getGames($this->users);
	}
	
	/**
	 * @param $users
	 * @return array
	 */
	private function _getGames($users)
	{
		$games = [];
		
		if (!is_array($users))
		{
			$users = $this->_getArrayFromObject($users);
		}
		
		$usersId = array_map(function($data) {return $data['id'];}, $users);
		
		$gamesCollection = $this->gamesRepo->getByUserId($usersId);
		
		foreach ($gamesCollection as $game)
		{
			$games[$game->user_id][$game->id] = $game;
		}
		
		return $games;
	}
	
	/**
	 * @param $users
	 * @return mixed
	 */
	private function _getArrayFromObject($users)
	{
		$arrayUsers = $users->toArray();
		
		return isset($arrayUsers['data']) ? $arrayUsers['data'] : $arrayUsers;
	}
}