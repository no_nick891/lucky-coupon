<?php  namespace LuckyCoupon\Games;

use App\LuckyCoupon\Games\Game;
use Illuminate\Database\Eloquent\Collection;
use LuckyCoupon\AdminStatistics\AdminStatisticEloquentRepository;
use LuckyCoupon\Coupons\EloquentCouponRepository;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Statistics\EloquentStatisticRepository;

/**
 * Class EloquentGameRepository
 * @package LuckyCoupon\Games
 */
class EloquentGameRepository implements GameRepositoryInterface
{
	/**
	 * @var Game
	 */
	public $model;
	
	private $settingRepo;
	
	private $couponRepo;
	
	private $statsRepo;
	
	private $adminStatisticRepo;
	
	/**
	 * EloquentGameRepository constructor.
	 */
	function __construct()
	{
		$this->model = new Game();
		
		$this->settingRepo = new EloquentSettingRepository();
		
		$this->couponRepo = new EloquentCouponRepository();
		
		$this->statsRepo = new EloquentStatisticRepository();
		
		$this->adminStatisticRepo = new AdminStatisticEloquentRepository();
	}

	/**
	 * @param $id
	 * @return array
	 */
	public function getById($id)
	{
		$game = $this->model->findOrFail($id);

		return $game ? $game : [];
	}
	
	/**
	 * @param $userId
	 * @param int $gameId
	 * @param int $siteId
	 * @return mixed
	 */
	public function getByUserId($userId, $gameId = 0, $siteId = 0)
	{
		$query = $this->model->orderBy('sort');
		
		if (is_array($userId))
		{
			$query->whereIn('user_id', $userId);
		}
		else if ($userId)
		{
			$query->where('user_id', $userId);
		}
		
		if ($siteId)
		{
			$query->where('site_id', $siteId);
		}
		
		if ($gameId)
		{
			$query->where('id', $gameId);
		}
		
		$games = $query->get();
		
		return $games ? $games : false;
	}
	
	/**
	 * @param $game
	 * @return mixed
	 */
	public function insert($game)
	{
		return $this->model->insertGetId($game);
	}

	/**
	 * @param $update
	 * @return mixed
	 */
	public function update($update)
	{
		$id = $update['id'];

		unset($update['id']);

		return $this->model->where('id', $id)->update($update);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function delete($id)
	{
		return $this->model->whereId($id)->delete();
	}
	
	/**
	 * @param $userId
	 * @param $gameId
	 * @return mixed
	 */
	public function deleteByUserId($userId, $gameId)
	{
		return $this->model->whereId($userId)->whereGameId($gameId)->delete();
	}
	
	/**
	 * @param $insert
	 * @return array|mixed
	 */
	public function create($insert)
	{
		if (!isset($insert['name']) && !$insert['name'])
		{
			$gamesCount = $this->getUserGamesByType($insert)->count();
			
			$insert['name'] = ucfirst($insert['type']) . ' ' . ($gamesCount + 1);
		}
		
		$insert = $this->_getRowBlueprint($insert);
		
		if ($gameId = $this->insert($insert))
		{
			$insert['id'] = $gameId;
			
			return $insert;
		}
		else
		{
			return $gameId;
		}
	}
	
	/**
	 * @param $insert
	 * @return mixed
	 */
	private function getUserGamesByType($insert)
	{
		return $this->model->where('user_id', $insert['user_id'])
			->where('type', $insert['type'])
			->get();
	}
	
	/**
	 * @param $newGame
	 * @return array
	 */
	private function _getRowBlueprint($newGame)
	{
		$insert = [
			'name' => 'Game',
			'type' => 'coupon',
			'active' => 0,
			'sort' => 100,
			'is_created' => 0,
			'user_id' => 0,
			'site_id' => 0,
		    'created_at' => date('Y-m-d h:i:s')
		];
		
		foreach($insert as $field => $value)
		{
			$insert[$field] = isset($newGame[$field]) ? $newGame[$field] : $value;
		}
		
		return $insert;
	}
	
	/**
	 * @param $siteId
	 * @param bool $activeState
	 * @return bool
	 */
	public function getBySiteId($siteId, $activeState = true)
	{
		$games = $this->_getQuery($siteId, $activeState)->get();
		
		return isset($games[0]) ? $games[0] : false;
	}
	
	/**
	 * @param $siteId
	 * @param bool $activeState
	 * @return bool|Collection
	 */
	public function getAllBySiteId($siteId, $activeState = true)
	{
		$games = $this->_getQuery($siteId, $activeState)->get();
		
		return $games->count() > 0 ? $games : false;
	}
	
	/**
	 * @param $siteId
	 * @param $activeState
	 * @return mixed
	 */
	private function _getQuery($siteId, $activeState)
	{
		$query = $this->model->where('site_id', $siteId);
		
		if ($activeState)
		{
			$query = $query->where('active', 1);
		}
		
		return $query;
	}
	
	/**
	 * @param $siteId
	 * @return array
	 */
	public function getGamesBySiteId($siteId)
	{
		$games = $this->model->where('site_id', $siteId)->get();
		
		return $games->count() > 0 ? $games->toArray() : [];
	}
	
	/**
	 * @param $userId
	 * @param $game
	 * @return bool|int
	 */
	public function deleteGame($userId, $game)
	{
		return \DB::transaction(function() use ($game, $userId)
		{
			$games = $this->getByUserId($userId, $game['id'], $game['site_id']);
			
			if (isset($games[0]))
			{
				$this->adminStatisticRepo->deleteByGameId($game['id']);
				
				$this->statsRepo->deleteByGameId($game['id']);
				
				$this->settingRepo->deleteGameSettings($game['id']);
				
				$this->couponRepo->deleteGameCoupons($game['id']);
				
				return (int)$games[0]->delete();
			}
			
			return false;
		});
	}
	
	/**
	 * @param $gameId
	 * @param $fieldName
	 * @param string $email
	 * @param string $name
	 * @param $deviceType
	 * @return int
	 */
	public function updateStat($gameId, $fieldName, $email = '', $name = '', $deviceType)
	{
		$query = $this->model;
		
		$query = $query->where('id', $gameId);
		
		return \DB::transaction(function () use($query, $fieldName, $gameId, $email, $name, $deviceType) {
			
			$stat = [
				'game_id' => $gameId,
				substr($fieldName, 0, -1) . '_date' => date('Y-m-d H:i:s'),
			    'device_type' => $deviceType
			];
			
			if ($email)
			{
				$stat['subscriber_email'] = $email;
			}
			
			if ($name)
			{
				$stat['subscriber_name'] = $name;
			}
			
			if ($email !== 'non-collect-option-on')
			{
				$this->statsRepo->add($stat);
			}
			
			return $query->increment(getDevice($deviceType).$fieldName);
		}, 3);
	}
}