<?php

namespace App\LuckyCoupon\Games;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Game
 * @package App\LuckyCoupon\Games
 */
class Game extends Model
{

	/**
	 * @var string
	 */
	protected $table = 'games';
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function coupons()
	{
		return $this->hasMany('LuckyCoupon\Coupons\Coupon');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function settings()
	{
		return $this->hasMany('LuckyCoupon\Settings\Setting');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function adminStatistic()
	{
		return $this->hasOne('LuckyCoupon\AdminStatistics\AdminStatistic')
			->withDefault([
				'impressions' => 0,
				'hits' => 0,
				'device_impressions' => 0,
				'device_hits' => 0
			]);
	}
	
}
