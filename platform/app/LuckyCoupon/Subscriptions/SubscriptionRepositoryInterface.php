<?php namespace LuckyCoupon\Subscriptions;

interface SubscriptionRepositoryInterface
{
	public function get($userId);
	public function insert($subscription);
}