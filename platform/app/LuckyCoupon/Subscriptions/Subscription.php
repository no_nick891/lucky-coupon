<?php namespace LuckyCoupon\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';
    
    public $timestamps = false;
	
    protected $fillable = [
    	'user_id',
		'subscription_id',
		'subscription_gateway'
	];
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function isracard()
	{
		return $this->hasMany(
			'LuckyCoupon\IsracardSubscriptions\IsracardSubscription',
			'user_id',
			'user_id'
		);
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function shopify()
	{
		return $this->hasMany(
			'LuckyCoupon\ShopifySubscriptions\ShopifySubscription',
			'user_id',
			'user_id'
		);
    }
}
