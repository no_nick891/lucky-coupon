<?php namespace LuckyCoupon\Subscriptions\Commands;

use App\LuckyCoupon\Users\User;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Subscriptions\SubscriptionEloquentRepository;

class AddSubscriptionCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $user;
	
	private $subRepo;
	/**
	 * @var
	 */
	private $subscription;
	
	/**
	 * AddSubscriptionCommand constructor.
	 * @param User $user
	 * @param $subscription
	 */
	public function __construct($user, $subscription)
	{
		$this->user = $user;
		
		$this->subRepo = new SubscriptionEloquentRepository();
		
		$this->subscription = $subscription;
	}
	
	public function handle()
	{
		if ($this->_isUserSubscribed())
		{
			$subscriptionId = $this->subRepo->insert($this->subscription);
		}
		else
		{
			$subscriptionId = $this->subRepo->update($this->subscription);
		}
		
		return $subscriptionId;
	}
	
	/**
	 * @return bool
	 */
	private function _isUserSubscribed()
	{
		return $this->user->subscription && $this->user->subscription->subscription_id === 0;
	}
}