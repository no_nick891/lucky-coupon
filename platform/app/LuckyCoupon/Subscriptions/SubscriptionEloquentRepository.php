<?php namespace LuckyCoupon\Subscriptions;

class SubscriptionEloquentRepository implements SubscriptionRepositoryInterface
{
	public $model;
	
	/**
	 * SubscriptionEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new Subscription();
	}
	
	/**
	 * @param $userId
	 * @return array
	 */
	public function get($userId)
	{
		$result = $this->model->where('user_id', $userId);
		
		return $result->count() > 0 ? $result->get()->toArray()[0] : [];
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getObject($userId)
	{
		return $this->model->where('user_id', $userId)->get();
	}
	
	/**
	 * @param $subscription
	 * @return mixed
	 */
	public function insert($subscription)
	{
		return $this->model->insertGetId($subscription);
	}
	
	/**
	 * @param $data
	 * @return mixed
	 */
	public function update($data)
	{
		return $this->model->whereUserId($data['user_id'])->update($data);
	}
}