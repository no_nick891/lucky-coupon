<?php namespace LuckyCoupon\AffiliateOwners\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class UpdateAffiliateOwnerCommand extends BaseCommand
{
	
	/**
	 * UpdateAffiliateOwnerCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $this->response($errs);
		
		$data = $this->getRequestData();
		
		$user = \Auth::user();
		
		$updated = false;
		
		if ($user->affiliateOwner)
		{
			$user->affiliateOwner->paypal_email = $data['paypal_email'];
			
			$updated = $user->affiliateOwner->save();
		}
		
		return ['affiliate_owner' => $updated];
	}
}