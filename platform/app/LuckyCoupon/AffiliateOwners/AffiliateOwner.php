<?php namespace LuckyCoupon\AffiliateOwners;

use Illuminate\Database\Eloquent\Model;

class AffiliateOwner extends Model
{
    public $table = 'affiliate_owners';
    
    public $timestamps = false;
    
    public $fillable = ['user_id', 'reference', 'paypal_email'];
}
