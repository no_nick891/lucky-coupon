<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\LicenseKeys\Commands\PutLicenseKeysFileCommand;
use LuckyCoupon\LicenseKyes\Commands\GenerateLicenseKeysCommand;

class GenerateKeysAndSave extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'license:generate {keysCount=500}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate license keys, store to database and load not acquired to file in storage.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$keysCount = (int)$this->argument('keysCount');
    	
        $result = dispatch(new GenerateLicenseKeysCommand($keysCount));
	
	    if ($result['saved'] > 0)
	    {
		    $saved = dispatch(new PutLicenseKeysFileCommand());
		
		    if ($saved['status'])
		    {
			    $this->info('Saved to ' . storage_path($saved['file']));
		    }
		    else
		    {
		    	$this->error('Can\' save to file');
		    }
	    }
    }
}
