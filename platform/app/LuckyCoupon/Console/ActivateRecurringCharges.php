<?php namespace LuckyCoupon\Console;

use App\LuckyCoupon\Users\User;
use Illuminate\Console\Command;
use ShopifyIntegration\API\RecurringApplicationCharge;

class ActivateRecurringCharges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify:recurring-activate {userId=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $userModel = new User();
	
	    $userId = $this->argument('userId');
	    
	    $users = $userModel
		    ->where('app_id', '>', 0)
		    ->where('id', '>', $userId)
		    ->orderBy('id', 'asc')
		    ->get();
	
	    $finalUsers = $this->_setShopsToAccepted($users);
	    
	    $this->_showUsers($finalUsers);
    }
	
	/**
	 * @param $users
	 * @return array
	 */
	private function _setShopsToAccepted($users)
	{
		$finalUsers = [];
		
		foreach ($users as $key => $user)
		{
			$app = $user->apps;
			
			if (!$app)
			{
				$this->_loading($key, $user);
				
				continue;
			}
			
			$payment = new RecurringApplicationCharge($app);
			
			$recurringCharges = $payment->getAll();
			
			foreach ($recurringCharges as $charge)
			{
				if ($charge->status === 'accepted')
				{
					$payment->activeAccepted($charge->id);
					
					$finalUsers[] = $user->toArray();
					
					continue;
				}
			}
			
			$this->_loading($key, $user);
		}
		
		return $finalUsers;
	}
	
	/**
	 * @param $finalUsers
	 */
	private function _showUsers($finalUsers)
	{
		foreach ($finalUsers as $finalUser)
		{
			$this->_showUser($finalUser);
		}
	}
	
	/**
	 * @param $activeUser
	 */
	private function _showUser($activeUser)
	{
		$info = '|  ' . $activeUser['id'] . ' | ' . $activeUser['name'] . '  |';
		
		$infoLength = strlen($info);
		
		$this->info(' ' . str_repeat('_', $infoLength - 2) . ' ');
		
		$this->info('|' . str_repeat(' ', $infoLength - 2) . '|');
		
		$this->info($info);
		
		$this->info('|' . str_repeat('_', $infoLength - 2) . '|');
		
		$this->info(str_repeat(' ', $infoLength));
	}
	
	/**
	 * @param $key
	 * @param $user
	 */
	private function _loading($key, $user)
	{
		system('clear');
		
		$this->info('Loading... ' . $key . ' user_id = ' . $user->id);
	}
}
