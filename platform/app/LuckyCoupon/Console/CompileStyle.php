<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\Seeds\Helper;
use LuckyCoupon\Templates\Commands\AssembleGameStylesCommand;

class CompileStyle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compile:style';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile style for game from css into js.';
    
	private $helper;
	
	private $templatesPath;
	
	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
	
	    $this->helper = new Helper();
	
	    $this->templatesPath = 'resources/assets/js/helpers/templates/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $templates = [
		    $this->templatesPath . 'default/',
		    $this->templatesPath . 'slot/',
		    $this->templatesPath . 'gift/',
		    $this->templatesPath . 'wheel/',
		    $this->templatesPath . 'scratch/'
	    ];
	
	    if ($this->_compileTemplates($templates))
	    {
		    $this->info('Styles compiled.');
	    }
	    else
	    {
		    $this->info('Something goes wrong.');
	    }
    }
	
	/**
	 * @param $templates
	 * @return bool
	 */
	private function _compileTemplates($templates)
	{
		foreach ($templates as $template)
		{
			$this->_compileTemplate($template);
		}
		
		return true;
	}
	
	/**
	 * @param $defaultTemplate
	 */
	private function _compileTemplate($defaultTemplate)
	{
		$cssPath = $defaultTemplate . 'style.css';
		
		$styles = dispatch(new AssembleGameStylesCommand($cssPath));
		
		$jsPath = $defaultTemplate . 'style.js';
		
		$currentContent = $this->helper->getFileContent($jsPath);
		
		$this->_compileFile($currentContent, $styles, $jsPath);
	}
	
	/**
	 * @param $jsContent
	 * @param $lineStyle
	 * @param $jsPath
	 */
	private function _compileFile($jsContent, $lineStyle, $jsPath)
	{
		$startLine = 'var styleUnescape = (\'';
		
		$endLine = '\').replace(/%27/g, "\'").replace(/%22/g, \'"\');';
		
		$beforeLine = explode($startLine, $jsContent);
		
		$afterLine = explode($endLine, $beforeLine[1]);
		
		$codeBefore = $beforeLine[0];
		
		$codeAfter = $afterLine[1];
		
		$fileCompiled = $codeBefore . $startLine . $lineStyle . $endLine . $codeAfter;
		
		file_put_contents($jsPath, '');
		
		file_put_contents($jsPath, $fileCompiled);
	}
}
