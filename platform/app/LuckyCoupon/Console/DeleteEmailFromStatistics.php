<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;

class DeleteEmailFromStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistics:delete-email {email=volue.nik@gmail.com}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete email for testing game with valid email.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $email = $this->argument('email');
	    
	    $result = \DB::table('statistics')
            ->where('subscriber_email', $email)
		    ->delete();
	    
	    $text = $result ? ' deleted from' : ' not found in';
	
	    $this->info($email . $text . ' statistics table.');
    }
}
