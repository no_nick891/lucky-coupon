<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\Counters\Commands\SetCounterCommand;

class CountHits extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'counter:hits-calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate all users account emails and save it to user_id hits counter in Counter table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $emailUsersCount = \DB::select(\DB::raw("
			select user_id, sum(count) as sum_count from (
				select g.user_id, count(s.game_id) as count
				from `statistics` as s
				left join games as g on s.game_id = g.id
				where `subscriber_email` is not null
					and user_id is not null
				group by s.game_id
			) as tmp
			group by user_id
			order by user_id
		"));
	
	    foreach ($emailUsersCount as $userEmailCount)
	    {
		    dispatch(new SetCounterCommand(
		    	$userEmailCount->user_id,
			    $userEmailCount->sum_count,
			    'hits'
		    ));
	    }
	    
	    $this->info('Counter write done.');
    }
}
