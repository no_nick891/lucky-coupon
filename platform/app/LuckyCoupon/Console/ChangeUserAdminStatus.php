<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\Users\UserEloquentRepository;

class ChangeUserAdminStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:rights {--user=} {--admin=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change user is_admin flag in user table.';
    
	private $userRepo;
	
	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
	
	    $this->userRepo = new UserEloquentRepository();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $userId = $this->option('user');
	    
	    $isAdmin = (bool)$this->option('admin');
	    
	    $user = $this->userRepo->getUserById((int)$userId);
	
	    if (!$user)
	    {
		    $user = $this->userRepo->getUser('email', $userId);
		
		    if (!$user)
		    {
			    $this->info('Can\'t find this user.');
			
			    return false;
		    }
	    }
	    
	    $user->is_admin = $isAdmin;
	
	    if ($user->save())
	    {
	    	$type = $isAdmin ? 'god-like' : 'mere mortal';
	    	
		    $this->info('User rights has been change to: ' . $type);
	    }
	    else
	    {
		    $this->error('Can\'t save user data.');
	    }
    }
}
