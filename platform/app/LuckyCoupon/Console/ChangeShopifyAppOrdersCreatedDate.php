<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrder;

class ChangeShopifyAppOrdersCreatedDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify:change-orders-date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change created_at field value from current server to created_at value from shopify database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
	    ShopifyAppOrder::chunk(100, function($orders) {
	        foreach ($orders as $order)
	        {
		        $createdAt = getDateFromShopifyFormat(data_get(json_decode($order->payload), 'created_at'));
		        
		        $order->created_at = $createdAt;
		        
		        $order->save();
	        }
        });
    }
}
