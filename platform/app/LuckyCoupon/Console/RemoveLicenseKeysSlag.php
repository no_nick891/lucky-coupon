<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\LicenseKeys\Commands\PutLicenseKeysFileCommand;
use LuckyCoupon\LicenseKeys\Commands\RemoveDashesFromLicenseKeysCommands;

class RemoveLicenseKeysSlag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'license:remove-slag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new RemoveDashesFromLicenseKeysCommands());
        
        $result = dispatch(new PutLicenseKeysFileCommand());
	
	    $this->info('Slag saved finished: ' . json_encode($result));
    }
}
