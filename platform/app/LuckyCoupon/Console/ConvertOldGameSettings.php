<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\Settings\Commands\GetDefaultGamesSettings;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Settings\SettingsEloquentRepository;

class ConvertOldGameSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert all game settings that still have no one line settings to new one.';
	
	private $settingRepo;
    
    private $oldSettingRepo;
    
	private $tmpOldSettings;
	private $tmpDefaultSettings;
	
	
	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
	
        $this->settingRepo = new SettingsEloquentRepository();
        
	    $this->oldSettingRepo = new EloquentSettingRepository();
    }
	
	/**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$defaultGamesSettings = dispatch(new GetDefaultGamesSettings());
    	
    	$games = $this->_getGamesWithoutNewSettings();
	
	    $this->_setNewGamesSettings($games, $defaultGamesSettings);
    }
	
	/**
	 * @return mixed
	 */
	private function _getGamesWithoutNewSettings()
	{
		return \DB::table('games')
			->select(['g.id', 'g.type'])
			->from('games as g')
			->leftJoin('settings as s', 's.game_id', '=', 'g.id')
			->where('s.name', 'not like', 'settings')
			->groupBy('g.id')
			->get();
	}
	
	/**
	 * @param $games
	 * @param $defaultGamesSettings
	 */
	private function _setNewGamesSettings($games, $defaultGamesSettings)
	{
		foreach ($games as $game)
		{
//			if ($game->id !== 1633) continue;
			
			$oldSettings = $this->oldSettingRepo->getByGameId($game->id);
			
			$defaultSettings = $defaultGamesSettings[$game->type];
			
			$newSettings = $this->_getNewSettings($oldSettings, $defaultSettings);
			
			if ($this->settingRepo->save($game->id, $newSettings))
			{
				$this->info('New settings saved for game id = ' . $game->id);
			}
		}
	}
	
	/**
	 * @param $oldSettings
	 * @param $defaultSettings
	 * @return \stdClass
	 */
	private function _getNewSettings($oldSettings, $defaultSettings)
	{
		$this->tmpOldSettings = $oldSettings;
		
		$this->tmpDefaultSettings = $defaultSettings;
		
		$newSettings = new \stdClass();
		
		foreach ($defaultSettings as $key => $settings)
		{
			if (in_array($key, ['text', 'behavior'])) continue;
			
			$newSettings->{$key} = $this->_safeGetter($key);
		}
		
		if (!$newText = data_get($oldSettings, 'text', false))
		{
			$newText = $defaultSettings->text;
			
			if ($title = data_get($oldSettings, 'content.title', false))
			{
				data_set($newText, 'startScreen.title', $title);
			}
			
			if ($email = data_get($oldSettings, 'content.email', false))
			{
				data_set($newText, 'startScreen.email', $email);
			}
			
			if ($note = data_get($oldSettings, 'content.note', false))
			{
				data_set($newText, 'startScreen.note', $note);
			}
			
			if ($gdpr = data_get($oldSettings, 'content.gpdr', false))
			{
				data_set($newText, 'startScreen.gdpr', $gdpr);
			}
			
			data_set($newText, 'trigger', data_get($oldSettings, 'content.trigger', null));
		}
		
		$newSettings->text = $newText;
		
		foreach ($defaultSettings->behavior as $key => $settings)
		{
			data_set($newSettings, 'behavior.' . $key,  $this->_safeGetter('behavior.' . $key));
		}
		
		return $newSettings;
	}
	
	/**
	 * @param $path
	 * @param null $defaultPath
	 * @return mixed
	 */
	private function _safeGetter($path, $defaultPath = null)
	{
		$defaultPath = $defaultPath === null ? $path : $defaultPath;
		
		return data_get($this->tmpOldSettings, $path, data_get($this->tmpDefaultSettings, $defaultPath, ''));
	}
}
