<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\ShopifyApps\ShopifyApp;
use ShopifyIntegration\API\RecurringApplicationCharge;

class SetShopifyAppsPayingStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify:set-paying';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update status for shopify app table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$self = $this;
    	
        ShopifyApp::chunk(200, function ($apps) use ($self) {
	        $self->setPayingStatuses($apps);
        });
    }
    
	/**
	 * @param $apps
	 */
	public function setPayingStatuses($apps)
	{
		foreach ($apps as $app)
		{
			$this->_setPayingStatus($app);
		}
	}
	
	/**
	 * @param $app
	 * @return bool
	 */
	private function _setPayingStatus($app)
	{
		if (!$charges = $this->_getCharges($app)) return false;
		
		if ($isPaying = $this->_isPaying($charges))
		{
			$app->is_paying = true;
			
			if ($app->save())
			{
				$this->info('Application ' . $app->id . ' data update');
			}
		}
		
    }
    
	/**
	 * @param $app
	 * @return array|object
	 */
	private function _getCharges($app)
	{
		$chargeApi = new RecurringApplicationCharge($app);
		
		$chargeApi->provider->log = false;
		
		$allCharges = $chargeApi->getAll();
		
		$message = $allCharges
			? 'Charges get from '
			: 'Cant get charges from ';
		
		$this->info($message . $app->id);
		
		return $allCharges;
	}
	
	/**
	 * @param $charges
	 * @return bool
	 */
	private function _isPaying($charges)
	{
		foreach ($charges as $charge)
		{
			if ($charge->status === 'active' || $charge->status === 'accepted')
			{
				return true;
			}
		}
		
		return false;
	}
}
