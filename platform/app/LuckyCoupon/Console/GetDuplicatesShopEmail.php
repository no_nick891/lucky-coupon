<?php namespace LuckyCoupon\Console;

use App\LuckyCoupon\Users\User;
use Illuminate\Console\Command;
use ShopifyIntegration\API\Shop;

class GetDuplicatesShopEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify:get-duplicates-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$emails = [];
    	
	    $users = $this->_getApps();
	
	    foreach ($users as $app)
	    {
		    $shopifyShop = new Shop($app);
		
		    $shop = $shopifyShop->getShop();
		
		    if (!data_get($emails, $app->shop, null))
		    {
			    $emails[$app->shop] = data_get($shop, 'email', null);
		    }
	    }
	    
	    $this->info(print_r($emails, true));
    }
	
	/**
	 * @return mixed
	 */
	protected function _getApps()
	{
		return \DB::select("
						select u.name as name,
								u.created_at as created_at,
								sa.shop,
								sa.access_token
						from users as u
						left join shopify_apps as sa on u.app_id = sa.id
						left join (
							select
								ut.name as name,
								count(ut.name) as c
							from users as ut
							where app_id > 0
							group by ut.name
							having c > 1
						) as tmp on u.name = tmp.name
						where u.app_id > 0
							and tmp.name is not null
							and sa.access_token is not null
						order by u.name");
	}
}
