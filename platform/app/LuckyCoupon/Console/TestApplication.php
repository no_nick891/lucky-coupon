<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\Console\Commands\TestGameTemplatesCommand;
use Mockery\Exception;

class TestApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make test some part of application.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$games = ['default', 'slot', 'gift', 'wheel', 'scratch'];
    	
	    foreach ($games as $game)
	    {
		    if (!dispatch(new TestGameTemplatesCommand($game)))
		    {
			    throw new Exception($game . ' template has error.');
		    }
	    }
    }
}
