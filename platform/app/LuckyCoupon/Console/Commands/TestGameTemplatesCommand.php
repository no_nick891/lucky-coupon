<?php namespace LuckyCoupon\Console\Commands;


use LuckyCoupon\Seeds\Helper;

class TestGameTemplatesCommand
{
	private $template;
	
	/**
	 * TestGameTemplatesCommand constructor.
	 * @param $template
	 */
	public function __construct($template)
	{
		$this->template = $template;
	}
	
	public function handle()
	{
		$path = 'resources/assets/js/helpers/templates/' . $this->template . '/template.js';
		
		$helper = new Helper();
		
		$variables = $helper->getJsConstants($path);
		
		foreach ($variables as $variable)
		{
			$test = json_decode($variable);
			
			if (json_last_error_msg() !== 'No error')
			{
				return false;
			}
		}
		
		return true;
	}
}