<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\Counters\CounterEloquentRepository;
use LuckyCoupon\ShopifySubscriptions\ShopifySubscriptionEloquentRepository;

class ResetImpressionsCounter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'counter:impressions-reset {--value=0} {--counter=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset all counters in counters table to zero. Should be called at start of every month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $value = $this->option('value');
	    
	    $counter = $this->option('counter');
	
	    $counterRepo = new CounterEloquentRepository();
	    
	    $result = $counterRepo->resetCounter('impressions', $value, $counter);
	    
	    $shopifySub = new ShopifySubscriptionEloquentRepository();
	    
	    $query = $shopifySub->model->where('is_emailed', 1);
	
	    if ($value != '0')
	    {
		    $query->where('user_id', $value);
	    }
	    
		$query->update(['is_emailed' => 0]);
	    
	    if ($result)
	    {
	    	\Log::error('Cron: all counters for accounts dropped to 0.');
	    	
		    $this->info('Reset counter finish.');
	    }
    }
}
