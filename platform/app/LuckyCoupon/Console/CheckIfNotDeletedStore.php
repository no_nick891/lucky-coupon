<?php namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use LuckyCoupon\ShopifyApps\ShopifyApp;
use ShopifyIntegration\Provider;

class CheckIfNotDeletedStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify:check-not-deleted-stores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command restores status if shop was not deleted but have deleted = 1 status in database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$updatedStores = 0;
    	
	    $apps = (new ShopifyApp())->where('deleted', 1)->get();
	
	    foreach ($apps as $app)
	    {
	    	$provider = new Provider($app->shop);
		    
	    	$provider->setToken($app);
		    
		    if ($provider->getAccessScopes())
		    {
			    $app->deleted = 0;
			
			    $app->save();
			
			    $updatedStores++;
			
			    $this->info($app->shop . ' false delete.');
		    }
		    else
		    {
		    	$this->info($app->shop . ' deleted.');
		    }
	    }
        
        $this->info('Stores that have expired tokens and have deleted status: ' . $updatedStores);
    }
}
