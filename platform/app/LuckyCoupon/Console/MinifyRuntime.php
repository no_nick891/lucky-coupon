<?php namespace LuckyCoupon\Console;

use Exception;
use Illuminate\Console\Command;

class MinifyRuntime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minify:runtime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Game core minimisation.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
	
	/**
	 * Execute the console command.
	 * @return mixed
	 * @throws Exception
	 */
    public function handle()
    {
	    $runtimeDevPath = 'js/runtime.dev.js';
	    
	    if ($this->_getFileStatus($runtimeDevPath)) return true;
	
	    $this->_minifyRuntime($runtimeDevPath);
	
    }
	
	/**
	 * @param $minified
	 * @return bool|int
	 */
	private function _savePublicRuntime($minified)
	{
		return file_put_contents(public_path('js/runtime.js'), $minified);
	}
	
	/**
	 * @param $code
	 * @param string $level
	 * @return mixed
	 */
	private function _useClosureCompiler($code, $level = 'SIMPLE_OPTIMIZATIONS')
	{
		try
		{
			$data = 'output_info=compiled_code&output_format=text&compilation_level=' . $level . '&js_code=' . urlencode($code);
			
			$ch = curl_init('https://closure-compiler.appspot.com/compile');
			
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-type: application/x-www-form-urlencoded']);
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			curl_setopt($ch, CURLOPT_POST, 1);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			
			$minified = curl_exec($ch);
			
			curl_close($ch);
		}
		catch (Exception $e)
		{
			$minified = $code;
		}
		
		return $minified;
	}
	
	/**
	 * Change version of the compile attempts for file RuntimeVersion
	 * @return bool|int
	 */
	private function _changeVersion()
	{
		$path = app_path('LuckyCoupon/Templates/Resources/RuntimeVersion');
		
		$version = file_get_contents($path);
		
		$version = $this->_incrementVersion($version);
		
		file_put_contents(resource_path('assets/js/lucky-coupon/runtime-version.txt'), $version);
		
		return file_put_contents($path, $version);
	}
	
	/**
	 * @param $runtimeDevPath
	 * @return bool
	 */
	private function _getFileStatus($runtimeDevPath)
	{
		$changedFilesList = shell_exec('git ls-files -m');
		
		$isFileChanged = strpos($changedFilesList, $runtimeDevPath) === false;
		
		if ($isFileChanged)
		{
			$this->info('Runtime file was not changed.');
		}
		else
		{
			$this->info('Runtime file was changed.');
			
			$this->info('Preparing minification function.');
		}
		
		return $isFileChanged;
	}
	
	/**
	 * @param $minified
	 * @throws Exception
	 */
	private function _saveMinification($minified)
	{
		if ($this->_savePublicRuntime($minified) && $this->_changeVersion())
		{
			$this->info('Runtime file minified.');
		}
		else
		{
			throw new Exception('Write minified file failed.');
		}
	}
	
	/**
	 * @param $runtimeDevPath
	 * @throws Exception
	 */
	private function _minifyRuntime($runtimeDevPath)
	{
		$runtimeDev = file_get_contents(public_path($runtimeDevPath));
		
		if ($minified = $this->_useClosureCompiler($runtimeDev))
		{
			$this->_saveMinification($minified);
		}
		else
		{
			throw new Exception('Can\'t get compiled script from google servers.');
		}
	}
	
	/**
	 * @param $version
	 * @return array
	 */
	private function _incrementVersion($version)
	{
		$version = explode('.', $version);
		
		$lastElement = count($version) - 1;
		
		$version[$lastElement] = ((int)$version[$lastElement]) + 1;
		
		return implode('.', $version);
	}
}
