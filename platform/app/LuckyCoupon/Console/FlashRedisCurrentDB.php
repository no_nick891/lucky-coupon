<?php

namespace LuckyCoupon\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class FlashRedisCurrentDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:flush-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush current redis connection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Redis flush DB status: ' . Redis::flushDB()->getPayload());
    }
}
