<?php namespace LuckyCoupon\KlaviyoApps;

class KlaviyoAppEloquentRepository implements KlaviyoAppRepositoryInterface
{
	public $model;
	
	/**
	 * KlaviyoAppEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new KlaviyoApp();
	}
	
	/**
	 * @param $userId
	 * @param $fields
	 * @return mixed
	 */
	public function updateByUserId($userId, $fields)
	{
		return $this->model
			->where('user_id', $userId)
			->where('site_id', $fields['site_id'])
			->update($fields);
	}
}