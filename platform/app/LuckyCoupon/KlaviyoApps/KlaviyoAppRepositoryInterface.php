<?php namespace LuckyCoupon\KlaviyoApps;

interface KlaviyoAppRepositoryInterface
{
	public function updateByUserId($userId, $fields);
}