<?php namespace LuckyCoupon\KlaviyoApps;

use Illuminate\Database\Eloquent\Model;

class KlaviyoApp extends Model
{
    public $table = 'klaviyo_apps';
    
    public $timestamps = false;
    
    public $fillable = ['site_id', 'user_id', 'active', 'private_key', 'selected_list_id'];
}
