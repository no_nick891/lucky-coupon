<?php namespace LuckyCoupon\AdminStatistics;


interface AdminStatisticRepositoryInterface
{
	public function addRow($gameId);
	
	public function incrementField($gameId, $field);
	
	public function deleteByGameId($gameId);
}