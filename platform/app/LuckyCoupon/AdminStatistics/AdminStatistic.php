<?php namespace LuckyCoupon\AdminStatistics;

use Illuminate\Database\Eloquent\Model;

class AdminStatistic extends Model
{
	protected $table = 'admin_statistics';
	
    public $timestamps = false;
}
