<?php namespace LuckyCoupon\AdminStatistics\Commands;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use LuckyCoupon\AdminStatistics\AdminStatisticEloquentRepository;

class IncrementAdminStatisticCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;
	
	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 10;
	
	private $adminStatisticRepo;
	/**
	 * @var
	 */
	private $gameId;
	/**
	 * @var
	 */
	private $field;
	
	/**
	 * Create a new job instance.
	 *
	 * @param $gameId
	 * @param $field
	 */
    public function __construct($gameId, $field)
    {
	    $this->field = $field;
	    
	    $this->gameId = $gameId;
	    
	    $this->adminStatisticRepo = new AdminStatisticEloquentRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	    $this->adminStatisticRepo->incrementField($this->gameId, $this->field);
    }
}
