<?php namespace LuckyCoupon\AdminStatistics;

class AdminStatisticEloquentRepository implements AdminStatisticRepositoryInterface
{
	private $model;
	
	/**
	 * AdminStatisticEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new AdminStatistic();
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	public function addRow($gameId)
	{
		return $this->model->insert(['game_id' => $gameId]);
	}
	
	
	/**
	 * @param $gameId
	 * @param $field
	 * @return mixed
	 */
	public function incrementField($gameId, $field)
	{
		$query = $this->model->where('game_id', $gameId);
		
		if ($query->count() === 0)
		{
			$this->addRow($gameId);
		}
		
		return $query->increment($field);
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	public function deleteByGameId($gameId)
	{
		return $this->model->where('game_id', $gameId)->delete();
	}
	
}