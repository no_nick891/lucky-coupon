<?php namespace LuckyCoupon\Counters;

interface CounterRepositoryInterface
{
	public function getCounter($key, $value, $counterName);
	public function getByFilter($filter);
	public function resetCounter($name, $value, $counter);
	public function create($data);
}