<?php namespace LuckyCoupon\Counters\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Counters\CounterEloquentRepository;

class SetCounterCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $userId;
	/**
	 * @var int
	 */
	private $counter;
	
	private $counterRepo;
	
	private $counterName;
	
	/**
	 * UpdateImpressionsCounterCommand constructor.
	 * @param $userId
	 * @param int $counter
	 * @param string $counterName
	 */
	public function __construct($userId, $counter = 1, $counterName = 'impressions')
	{
		$this->userId = $userId;
		
		$this->counter = $counter;
		
		$this->counterName = $counterName;
		
		$this->counterRepo = new CounterEloquentRepository();
	}
	
	public function handle()
	{
		$result = $this->counterRepo
			->getOrCreate('user_id', $this->userId, $this->counterName)
			->update(['counter' => $this->counter]);
		
		return $result;
	}
}