<?php namespace LuckyCoupon\Counters\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifySubscriptions\ShopifySubscriptionEloquentRepository;

class SetAdminCounterCommand extends BaseCommand
{
	protected $request;
	
	private $shopifySub;
	
	/**
	 * SetCounterCommand constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->shopifySub = new ShopifySubscriptionEloquentRepository();
	}
	
	public function handle()
	{
		$userId = $this->request->get('user_id', false);
		
		$counter = (int)$this->request->get('counter', 0);
		
		$result = false;
		
		if ($userId)
		{
			$this->shopifySub->update(['user_id' => $userId, 'is_emailed' => 0]);
			
			$result = dispatch(new SetCounterCommand($userId, $counter));
		}
		
		return ['counter' => $result];
	}
}