<?php namespace LuckyCoupon\Counters\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Counters\CounterEloquentRepository;

class UpdateCounterCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $name;
	/**
	 * @var
	 */
	private $data;
	
	private $counterRepo;
	/**
	 * @var
	 */
	private $increment;
	
	/**
	 * UpdateCounterCommand constructor.
	 * @param $counterName
	 * @param $columnData
	 * @param $increment
	 */
	public function __construct($counterName, $columnData, $increment = 1)
	{
		$this->name = $counterName;
		
		$this->data = $columnData;
		
		$this->increment = $increment;
		
		$this->counterRepo = new CounterEloquentRepository();
	}
	
	public function handle()
	{
		reset($this->data);
		
		$key = key($this->data);
		
		$value = $this->data[$key];
		
		$counter = $this->counterRepo->getOrCreate($key, $value, $this->name);
		
		$counter->increment('counter', $this->increment);
		
		return $counter;
	}
}