<?php namespace LuckyCoupon\Counters\Commands;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Integration\SendGrid\Commands\SendEmailCommand;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\ShopifySubscriptions\ShopifySubscriptionEloquentRepository;

class UpdateGameCounterCommand extends BaseCommand implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;
	
	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;
	
	/**
	 * @var
	 */
	private $gameId;
	
	/**
	 * @var
	 */
	private $field;
	
	private $gameRepo;
	
	private $shopifySubscriptions;
	
	/**
	 * UpdateGameCounterCommand constructor.
	 * @param $gameId
	 * @param $field
	 */
	public function __construct($gameId, $field)
	{
		$this->gameId = $gameId;
		
		$this->field = $field;
		
		$this->gameRepo = new EloquentGameRepository();
		
		$this->shopifySubscriptions = new ShopifySubscriptionEloquentRepository();
	}
	
	public function handle()
	{
		$data = $this->_getUserData();
		
		if ($data)
		{
			$counter = dispatch(new UpdateCounterCommand(
				$this->field,
				['user_id' => $data->user_id]
			));
			
			if (!$this->_shouldMail($data, $counter)) return false;
			
			if ($this->_updateShopifySubscription($data->ss_id))
			{
				try
				{
					dispatch((new SendEmailCommand((array)$data, [
						'from' => 'team@getwoohoo.com',
						'subject' => 'Your WooHoo account is out of impressions',
						'template_id' => 'c799cb3c-8dd2-4b62-ae10-c1ee8b30d833'
					]))->onQueue('email'));
				}
				catch (\Exception $e)
				{
					\Log::error(print_r(['Exception before added to queue' => $e->getMessage()], true));
				}
			}
			
		}
	}
	
	/**
	 * @return mixed
	 */
	private function _getUserData()
	{
		return \DB::table('games')
			->select(
				'games.*', 'ss.*', 'p.*', 'u.*',
				'ss.id as ss_id',
				'u.name as name',
				'games.user_id as user_id',
				'p.name as plan_name'
			)
			->where('games.id', $this->gameId)
			->leftJoin('users as u', 'u.id', '=', 'games.user_id')
			->leftJoin('shopify_subscriptions as ss', 'ss.user_id', '=', 'games.user_id')
			->leftJoin('plans as p', 'p.id', '=', 'ss.plan_id')
			->first();
	}
	
	/**
	 * @param $data
	 * @param $counter
	 * @return bool
	 */
	private function _shouldMail($data, $counter)
	{
		return $this->field === 'impressions'
			&& $this->_isShopifyMailable($data, $counter)
			&& $this->_isConditionsAvailable($data);
	}
	
	/**
	 * @param $data
	 * @param $counterRow
	 * @return bool
	 */
	private function _isShopifyMailable($data, $counterRow)
	{
		$counter = data_get($counterRow, 'counter', false);
		
		return data_get($data, 'app_id', 0) > 0
			&& !$data->is_emailed
			&& $counter !== false
			&& $counter >= (int)$data->conditions;
	}
	
	/**
	 * @param $data
	 * @return bool
	 */
	private function _isConditionsAvailable($data)
	{
		$ssId = $data->ss_id;
		
		$conditions = $data->conditions;
		
		$isConditionAvailable = (int)$conditions > 0;
		
		if (
			!$isConditionAvailable
			&& $data->app_id > 0
			&& !$data->is_emailed
		)
		{
			$this->_updateShopifySubscription($ssId);
		}
		
		return $isConditionAvailable;
	}
	
	/**
	 * @param $ss_id
	 * @return mixed
	 * @internal param $data
	 */
	private function _updateShopifySubscription($ss_id)
	{
		$update = [
			'id' => $ss_id,
			'is_emailed' => 1
		];
		
		return $this->shopifySubscriptions->update($update);
	}
}