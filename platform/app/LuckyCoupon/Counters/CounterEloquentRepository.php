<?php namespace LuckyCoupon\Counters;

class CounterEloquentRepository implements CounterRepositoryInterface
{
	public $model;
	
	/**
	 * CounterEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new Counter();
	}
	
	/**
	 * @param $key
	 * @param $value
	 * @param $counterName
	 * @return Counter|null
	 */
	public function getCounter($key, $value, $counterName)
	{
		return $this->model
			->whereColumnName($key)
			->whereColumnValue($value)
			->whereCounterName($counterName)
			->first();
	}
	
	/**
	 * @param $filter
	 * @return mixed
	 */
	public function getByFilter($filter)
	{
		$query = $this->_getQuery($filter);
		
		$result = $query->first();
		
		return $result ? $result->counter : 0;
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getUserImpressions($userId)
	{
		return $this->getByFilter([
			'column_name' => 'user_id',
			'column_value' => $userId,
			'counter_name' => 'impressions'
		]);
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getUserHits($userId)
	{
		return $this->getByFilter([
			'column_name' => 'user_id',
			'column_value' => $userId,
			'counter_name' => 'hits'
		]);
	}
	
	/**
	 * @param $userId
	 */
	public function deleteImpressionsCounter($userId)
	{
		$this->_getQuery([
			'column_name' => 'user_id',
			'column_value' => $userId,
			'counter_name' => 'impressions'
		])->delete();
	}
	
	/**
	 * @param $data
	 * @return Counter
	 */
	public function create($data)
	{
		foreach ($data as $column => $value)
		{
			$this->model->{$column} = $value;
		}
		
		$this->model->save();
		
		return $this->model;
	}
	
	/**
	 * @param $key
	 * @param $value
	 * @param $name
	 * @return Counter
	 */
	public function getOrCreate($key, $value, $name)
	{
		$counter = $this->getCounter($key, $value, $name);
		
		if (!$counter)
		{
			$counter = $this->create([
				'column_name' => $key,
				'column_value' => $value,
				'counter_name' => $name
			]);
		}
		
		return $counter;
	}
	
	/**
	 * @param $name
	 * @param $value
	 * @param $counter
	 * @return mixed
	 */
	public function resetCounter($name, $value, $counter)
	{
		$query = \DB::table('counters')->whereCounterName($name);
		
		if ($value)
		{
			$query = $query->whereColumnValue($value);
		}
		
		return $query->update(['counter' => $counter]);
	}
	
	/**
	 * @param $filter
	 * @return mixed
	 */
	private function _getQuery($filter)
	{
		$query = $this->model->select('counter');
		
		foreach ($filter as $column => $value)
		{
			$query = $query->where($column, $value);
		}
		
		return $query;
	}
}