<?php namespace LuckyCoupon\Counters;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    public $table = 'counters';
    
    public $timestamps = false;
    
    public $fillable = ['counter'];
}
