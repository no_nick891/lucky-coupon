<?php

namespace App\Console;

use App\Console\Commands\ChangeShopifyAppOrdersCreatedDate;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use LuckyCoupon\Console\ActivateRecurringCharges;
use LuckyCoupon\Console\ChangeUserAdminStatus;
use LuckyCoupon\Console\CheckIfNotDeletedStore;
use LuckyCoupon\Console\CompileStyle;
use LuckyCoupon\Console\ConvertOldGameSettings;
use LuckyCoupon\Console\CountHits;
use LuckyCoupon\Console\DeleteEmailFromStatistics;
use LuckyCoupon\Console\FlashRedisCurrentDB;
use LuckyCoupon\Console\GenerateKeysAndSave;
use LuckyCoupon\Console\GetDuplicatesShopEmail;
use LuckyCoupon\Console\MinifyRuntime;
use LuckyCoupon\Console\RemoveLicenseKeysSlag;
use LuckyCoupon\Console\ResetImpressionsCounter;
use LuckyCoupon\Console\SetShopifyAppsPayingStatus;
use LuckyCoupon\Console\TestApplication;
use ShopifyIntegration\Console\FreeCharge;
use ShopifyIntegration\Console\ShowAppsStatistics;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
	    CompileStyle::class,
	    FreeCharge::class,
        TestApplication::class,
        MinifyRuntime::class,
	    ShowAppsStatistics::class,
        ResetImpressionsCounter::class,
        ChangeUserAdminStatus::class,
        DeleteEmailFromStatistics::class,
        ActivateRecurringCharges::class,
        CountHits::class,
        GenerateKeysAndSave::class,
        RemoveLicenseKeysSlag::class,
	    CheckIfNotDeletedStore::class,
        ChangeShopifyAppOrdersCreatedDate::class,
        SetShopifyAppsPayingStatus::class,
        FlashRedisCurrentDB::class,
	    ConvertOldGameSettings::class,
	    GetDuplicatesShopEmail::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
