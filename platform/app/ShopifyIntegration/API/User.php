<?php namespace ShopifyIntegration\API;

class User extends CommonApi
{
	/**
	 * WebHooks constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		parent::__construct($app);
	}
	
	/**
	 * @return bool|mixed
	 */
	public function getUsers()
	{
		$object = $this->provider->call([
			'URL' => '/admin/users.json',
		    'METHOD' => 'GET'
		]);
		
		return $this->getValue($object);
	}
	
}