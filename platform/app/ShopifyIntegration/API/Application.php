<?php namespace ShopifyIntegration\API;

class Application extends CommonApi
{
	
	/**
	 * Application constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		parent::__construct($app);
	}
	
	/**
	 * @return array|object
	 */
	public function delete()
	{
		$object = $this->provider->call([
			'URL' => '/admin/api_permissions/current.json',
		    'METHOD' => 'DELETE'
		]);
		
		return $this->getValue($object);
	}
}