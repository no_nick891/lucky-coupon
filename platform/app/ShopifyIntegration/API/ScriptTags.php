<?php namespace ShopifyIntegration\API;


class ScriptTags extends CommonApi
{
	/**
	 * WebHooks constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		parent::__construct($app);
	}
	
	/**
	 * @return bool|array|object
	 */
	public function getAll()
	{
		$object = $this->provider->call([
			'URL' => '/admin/script_tags.json',
		    'METHOD' => 'GET'
		]);
		
		return isset($object) && $object ? $object->script_tags : false;
	}
	
	/**
	 * @param $scriptUrl
	 * @return bool
	 */
	public function add($scriptUrl)
	{
		$object = $this->provider->call([
			'URL' => '/admin/script_tags.json',
		    'DATA' => ['script_tag' => [
		    	'event' => 'onload',
		        'src' => $scriptUrl
		    ]],
		    'METHOD' => 'POST'
		]);
		
		return isset($object) && $object ? $object->script_tag : false;
	}
	
	/**
	 * @param $scriptId
	 * @return array|object
	 */
	public function delete($scriptId)
	{
		$object = $this->provider->call([
			'URL' => "/admin/script_tags/{$scriptId}.json",
		    'METHOD' => 'DELETE'
		]);
		
		return $this->getValue($object);
	}
	
}