<?php namespace ShopifyIntegration\API;

class Order extends CommonApi
{
	/**
	 * WebHooks constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		parent::__construct($app);
	}
	
	/**
	 * @param array $data
	 * @return array|object
	 */
	public function getOrders($data = [])
	{
		$object = $this->provider->call([
			'URL' => '/admin/orders.json' . '?' . http_build_query($data),
			'METHOD' => 'GET',
		]);
		
		return $this->getValue($object);
	}
	
	/**
	 * @return array|object
	 */
	public function getOrdersCount()
	{
		$object = $this->provider->call([
			'URL' => '/admin/orders/count.json',
		    'METHOD' => 'GET',
		]);
		
		return $this->getValue($object);
	}
}