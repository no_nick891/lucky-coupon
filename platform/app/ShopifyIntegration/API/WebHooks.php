<?php namespace ShopifyIntegration\API;

class WebHooks extends CommonApi
{
	public $provider;
	
	public $webHookEvents = [
		'orders/cancelled',
		'orders/create',
		'orders/delete',
		'orders/fulfilled',
		'orders/paid',
		'orders/partially_fulfilled',
		'orders/updated',
		'app/uninstalled',
		'shop/update'
	];
	
	/**
	 * WebHooks constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		parent::__construct($app);
	}
	
	/**
	 * @return bool|array
	 */
	public function getAll()
	{
		$object = $this->provider->call([
			'URL' => '/admin/webhooks.json',
		    'METHOD' => 'GET'
		]);
		
		return isset($object->webhooks) ? $object->webhooks : false;
	}
	
	/**
	 * @param $id
	 * @return bool
	 */
	public function getById($id)
	{
		$object = $this->provider->call([
			'URL' => '/admin/webhooks/'.$id.'.json',
		    'METHOD' => 'GET'
		]);
		
		return isset($object->webhook) ? $object->webhook : false;
	}
	
	/**
	 * @param $topic
	 * @return bool
	 */
	public function post($topic)
	{
		$object = $this->provider->call([
			'URL' => '/admin/webhooks.json',
		    'DATA' => [
		    	'webhook' => [
				    'topic' => $topic,
				    'address' => $this->_getUrlWebHook($topic),
				    'format' => 'json'
			    ]
		    ],
		    'METHOD' => 'POST'
		]);
		
		return isset($object) && $object ? $object->webhook : false;
	}
	
	/**
	 * @param $topic
	 * @return string
	 */
	private function _getUrlWebHook($topic)
	{
		$listen = explode('/', $topic);
		
		$entities = [
			'orders' => 'orders',
		    'app' => 'setup',
		    'shop' => 'setup'
		];
		
		$urlEnd = $entities[$listen[0]];
		
		return config('app.url') . 'webhooks/shopify/' . $urlEnd;
	}
	
	/**
	 * @param $id
	 */
	public function delete($id)
	{
		 $this->provider->call([
			'URL' => '/admin/webhooks/'.$id.'.json',
		    'METHOD' => 'DELETE'
		]);
	}
}