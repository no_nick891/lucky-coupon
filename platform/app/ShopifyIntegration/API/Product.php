<?php namespace ShopifyIntegration\API;

class Product extends CommonApi
{
	/**
	 * WebHooks constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		parent::__construct($app);
	}
	
	/**
	 * @param $productId
	 * @return array|object
	 */
	public function getImages($productId)
	{
		$object = $this->provider->call([
			'URL' => '/admin/products/' . $productId . '/images.json',
		    'METHOD' => 'GET'
		]);
		
		return $this->getValue($object);
	}
}