<?php namespace ShopifyIntegration\API;

use ShopifyIntegration\Provider;

class Assets
{
	private $provider;
	
	/**
	 * WebHooks constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		if ( ! isset($app->access_token))
		{
			return 'Can\'t find application.';
		}
		
		$this->provider = new Provider($app->shop, $app->access_token);
	}
	
	/**
	 * @return int
	 */
	public function getThemes()
	{
		$object = $this->provider->call([
			'URL'    => '/admin/themes.json',
			'METHOD' => 'GET'
		]);
		
		return isset($object) && $object ? $object->themes : false;
	}
	
	/**
	 * @param $themeId
	 * @param $assetName
	 * @return mixed
	 */
	public function getAsset($themeId, $assetName)
	{
		$objAsset = $this->provider->call([
			'URL' => '/admin/themes/'.$themeId.'/assets.json',
			'DATA' => [
				'asset' => [
					'key' => $assetName
				],
				'theme_id' => $themeId
			],
			'METHOD' => 'GET'
		]);
		
		return isset($objAsset->asset) ? $objAsset->asset : false;
	}
	
	/**
	 * @param $themeId
	 * @param $assetName
	 * @param $value
	 * @return bool
	 */
	public function createAsset($themeId, $assetName, $value)
	{
		$objAsset = $this->provider->call([
			'URL' => '/admin/themes/'.$themeId.'/assets.json',
			'DATA' => [
				'asset' => [
					'key' => $assetName,
					'attachment' => base64_encode($value)
				]
			],
			'METHOD' => 'PUT'
		]);
		
		return isset($objAsset->asset->key) ? true : false;
	}
	
	public function delete($id, $name)
	{
		$this->provider->call([
			'URL' => '/admin/themes/'.$id.'/assets.json',
		    'DATA' => ['asset' => [
		    	'key' => $name
		    ]],
		    'METHOD' => 'DELETE'
		]);
	}
	
	/**
	 * @param $themeId
	 * @param $assetName
	 * @param $value
	 * @return bool
	 */
	public function updateAsset($themeId, $assetName, $value)
	{
		$objAsset = $this->provider->call([
			'URL' => '/admin/themes/'.$themeId.'/assets.json',
			'DATA' => [
				'asset' => [
					'key' => $assetName,
					'value' => $value
				]
			],
			'METHOD' => 'PUT'
		]);
		
		return isset($objAsset->asset->key) ? true : false;
	}
	
	/**
	 * @return int
	 */
	public function getCurrentThemeId()
	{
		$themes = $this->getThemes();
		
		return $this->findCurrentThemeId($themes);
	}
	
	/**
	 * @param $themes
	 * @return int theme id
	 */
	public function findCurrentThemeId($themes)
	{
		$id = 0;
		
		foreach ($themes as $theme)
		{
			if ($theme->role == 'main')
			{
				$id = $theme->id;
			}
		}
		
		return $id;
	}
	
	/**
	 * @param $themeId
	 * @return mixed
	 */
	public function getAssets($themeId)
	{
		$objAssets = $this->provider->call([
			'URL' => '/admin/themes/'.$themeId.'/assets.json',
			'METHOD' => 'GET'
		]);
		
		return $objAssets->assets;
	}
}