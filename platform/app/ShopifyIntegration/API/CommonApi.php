<?php namespace ShopifyIntegration\API;

use LuckyCoupon\ShopifyApps\ShopifyApp;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use ShopifyIntegration\Provider;

class CommonApi
{
	public $provider;
	
	/**
	 * WebHooks constructor.
	 * @param ShopifyApp|int $app
	 */
	public function __construct($app)
	{
		$appId = 0;
		
		if (is_int($app))
		{
			$appId = $app;
			
			$app = data_get((new ShopifyAppEloquentRepository())->getAppObjById($app), '0', false);
		}
		
		if ( ! isset($app->access_token) && ! $app)
		{
			\Log::error(print_r([
				'Shopify CommonApi' => 'Can\'t find application.',
				'app id' => $appId,
				'app' => $app
			], true));
			
			die();
		}
		
		if ( ! $this->provider = new Provider($app->shop, $app->access_token))
		{
			\Log::error(print_r([
				'Shopify CommonApi' => 'Can\'t get access to shop.',
				'app' => $app
			], true));
			
			die();
		}
		
	}
	
	/**
	 * @param $object
	 * @return array|object
	 */
	protected function getValue($object)
	{
		if ( ! $object) return [];
		
		foreach ($object as $key => $value)
		{
			if ($object->{$key})
			{
				return $object->{$key};
			}
		}
		
		return [];
	}
}