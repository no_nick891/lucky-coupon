<?php namespace ShopifyIntegration\API;


class RecurringApplicationCharge extends CommonApi
{
	/**
	 * RecurringApplicationCharge constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		parent::__construct($app);
	}
	
	/**
	 * @param $data
	 * @param null $testMode
	 * @return array|object
	 */
	public function add($data, $testMode = null)
	{
		$object = $this->provider->call([
			'URL' => '/admin/recurring_application_charges.json',
			'DATA' => [
				'recurring_application_charge' => $this->_getAddData($data, $testMode)
			],
			'METHOD' => 'POST'
		]);
		
		return $this->getValue($object);
	}
	
	/**
	 * @param $data
	 * @param $test
	 * @return array
	 */
	private function _getAddData($data, $test)
	{
		$testKey = [];
		
		if ($test)
		{
			$testKey = [
				'test' => $this->_getTrueOrNull($test)
			];
		}
		
		return array_merge($data, $testKey);
	}
	
	/**
	 * @param $test
	 * @return mixed|null
	 */
	private function _getTrueOrNull($test)
	{
		return (is_null($test) || $test === true)
				? strtolower(var_export($test, true))
				: 'null';
	}
	
	/**
	 * @param $id
	 * @return array|object
	 */
	public function get($id)
	{
		$object = $this->provider->call([
			'URL' => '/admin/recurring_application_charges/' . $id . '.json',
		    'METHOD' => 'GET'
		]);
		
		return $this->getValue($object);
	}
	
	/**
	 * @return array|object
	 */
	public function getAll()
	{
		$object = $this->provider->call([
			'URL' => '/admin/recurring_application_charges.json',
		    'METHOD' => 'GET'
		]);
		
		return $this->getValue($object);
	}
	
	/**
	 * @param $id
	 * @return array|object
	 */
	public function activeAccepted($id)
	{
		$object = $this->provider->call([
			'URL' => '/admin/recurring_application_charges/'.$id.'/activate.json',
		    'METHOD' => 'POST'
		]);
		
		return $this->getValue($object);
	}
	
	/**
	 * @param $id
	 * @return mixed
	 */
	public function delete($id)
	{
		return $this->provider->call([
			'URL' => '/admin/recurring_application_charges/'.$id.'.json',
		    'METHOD' => 'DELETE'
		]);
	}
}