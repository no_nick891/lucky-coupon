<?php namespace ShopifyIntegration\API;

class Shop extends CommonApi
{
	
	/**
	 * Shop constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		parent::__construct($app);
	}
	
	/**
	 * @return array|object
	 */
	public function getShop()
	{
		$object = $this->provider->call([
			'URL' => '/admin/shop.json',
		    'METHOD' => 'GET'
		]);
		
		return $this->getValue($object);
	}
}