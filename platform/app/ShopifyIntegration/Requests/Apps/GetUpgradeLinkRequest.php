<?php  namespace ShopifyIntegration\Requests\Apps;

use Illuminate\Foundation\Http\FormRequest;

class GetUpgradeLinkRequest extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * @return mixed
	 */
	public function rules()
	{
		return \App::make(Rules::class)->getUpgradeLink();
	}
}