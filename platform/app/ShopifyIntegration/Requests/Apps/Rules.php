<?php  namespace ShopifyIntegration\Requests\Apps;

use LuckyCoupon\Requests\BaseRules;

/**
 * Class Rules
 * @package LuckyCoupon\Requests\Sites
 */
class Rules extends BaseRules {

	/**
	 * @var array
	 */
	protected $_rules = [
		'shop_url' => 'url|shopify_url',
	    'app_id' => 'required|numeric',
	    'plan_id' => 'required|numeric'
	];
	
	/**
	 * @return array
	 */
	public function getInstallApp()
	{
		return $this->_getRulesArray(['shop_url']);
	}
	
	/**
	 * @return array
	 */
	public function getUpgradeLink()
	{
		return $this->_getRulesArray(['app_id', 'plan_id']);
	}
}