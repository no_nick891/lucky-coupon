<?php namespace ShopifyIntegration\Apps;

interface AppRepositoryInterface
{
	public function getApp($shopUrl);
	public function getAppAccessToken($shopUrl);
}