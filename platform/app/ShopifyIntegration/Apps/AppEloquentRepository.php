<?php namespace ShopifyIntegration\Apps;

/**
 * @TODO: remove using of this class in ShopifyIntegration/Commands and use instead ShopifyAppEloquentRepository
 *
 * Class AppEloquentRepository
 * @package ShopifyIntegration\Apps
 */
class AppEloquentRepository
{
	/**
	 * AppEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new App();
	}
	
	/**
	 * @param $shopUrl
	 * @return mixed
	 */
	public function getApp($shopUrl)
	{
		$app = $this->model->where('shop', $shopUrl)->get();
		
		return isset($app[0]) ? $app[0] : [];
	}
	
	/**
	 * @param $shopUrl
	 * @return bool
	 */
	public function getAppAccessToken($shopUrl)
	{
		$app = $this->getApp($shopUrl);
		
		return isset($app->access_token) ? $app->access_token : false;
	}
}