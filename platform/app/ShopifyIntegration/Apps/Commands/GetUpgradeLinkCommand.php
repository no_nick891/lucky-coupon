<?php namespace ShopifyIntegration\Apps\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Plans\PlansEloquentRepository;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use LuckyCoupon\ShopifySubscriptions\Commands\SaveShopifySubscriptionCommand;

class GetUpgradeLinkCommand extends BaseCommand
{
	protected $request;
	
	private $appRepo;
	
	private $planRepo;
	
	/**
	 * GetUpgradeLinkCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->appRepo = new ShopifyAppEloquentRepository();
		
		$this->planRepo = new PlansEloquentRepository();
	}
	
	/**
	 * todo: refactor this method into smaller if possible
	 * @return array|bool
	 */
	public function handle()
	{
		if ($errs = $this->getErrors($this->request)) return $errs;
		
		$data = $this->getRequestData();
		
		$result = ['plans' => false];
		
		if (!$data['app_id'] || !$data['plan_id']) return $result;
		
		$app = $this->appRepo->getAppObjById($data['app_id'])[0];
		
		$plan = $this->_getPlan((int)$data['plan_id']);
		
		if (!$plan) return $result;
		
		if ((int)$plan['amount'] !== 0)
		{
			$app->is_upgrade = 1;
			
			$app->save();
			
			$payment = dispatch(new CreateShopifyPaymentCommand($plan, $app));
			
			$url = $payment->confirmation_url;
		}
		else
		{
			$url = 'free';
			
			dispatch(new SaveShopifySubscriptionCommand($plan['name'], $app->user->id, $app));
			
			dispatch(new DeleteRecurringPaymentsCommand($app));
		}
		
		return ['plans' => ['url' => $url, 'id' => $plan['id']]];
	}
	
	/**
	 * @param $planId
	 * @return mixed
	 */
	private function _getPlan($planId)
	{
		$result = $this->planRepo->model->whereId($planId)->first();
		
		return $result ? $result->toArray() : [];
	}
}