<?php namespace ShopifyIntegration\Apps\Commands;

use LuckyCoupon\BaseCommand;
use ShopifyIntegration\API\WebHooks;

class AddWebHooksCommand extends BaseCommand
{
	private $webHooks;
	
	/**
	 * @var bool|array
	 */
	private $shopWebHooks;
	
	/**
	 * AddWebHooksCommand constructor.
	 * @param $app
	 * @param bool $webHooks
	 */
	public function __construct($app, $webHooks = false)
	{
		$this->shopWebHooks = $webHooks;
		
		$this->webHooks = new WebHooks($app);
	}
	
	public function handle()
	{
		$filteredHooks = $this->webHooks->webHookEvents;
		
		$existedWebHooks = $this->shopWebHooks && count($this->shopWebHooks) > 0 ? $this->shopWebHooks : $this->webHooks->getAll();
		
		array_map([&$this, 'filterWebHooks'], $existedWebHooks, $filteredHooks);
		
		array_map([&$this->webHooks, 'post'], $filteredHooks);
	}
	
	/**
	 * @param $el
	 * @return bool
	 */
	protected function filterWebHooks($el)
	{
		$topic = data_get($el, 'topic', false);
		
		$index = array_search($topic, $this->webHooks->webHookEvents);
		
		if ($index === false) return $topic;
		
		return null;
	}
}