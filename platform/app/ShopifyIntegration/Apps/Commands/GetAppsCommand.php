<?php namespace ShopifyIntegration\Apps\Commands;

use Carbon\Carbon;
use ShopifyIntegration\API\RecurringApplicationCharge;
use ShopifyIntegration\Apps\AppEloquentRepository;

class GetAppsCommand
{
	private $users;
	
	/**
	 * GetAppsCommand constructor.
	 * @param $users
	 */
	public function __construct($users)
	{
		$this->users = $users;
		
		$this->appsRepo = new AppEloquentRepository();
	}
	
	public function handle()
	{
		$apps = [];
		
		foreach ($this->users as $user)
		{
			$apps[$user['id']]['id'] = $user['id'];
			
			$apps[$user['id']]['name'] = $user['name'];
			
			$apps[$user['id']]['counter'] = data_get($user, 'counter', null);
			
			$apps[$user['id']]['last_login'] = $user['last_login'];
			
			$apps[$user['id']]['date_create'] = $user['created_at'];
			
			$apps[$user['id']]['installed'] = ((int)data_get($user, 'is_paying', 0)) === 1;
		}
		
		return $apps;
	}
}