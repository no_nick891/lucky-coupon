<?php namespace ShopifyIntegration\Apps\Commands;

use Carbon\Carbon;
use LuckyCoupon\BaseCommand;
use ShopifyIntegration\API\RecurringApplicationCharge;
use ShopifyIntegration\API\Shop;

class CreateShopifyPaymentCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $plan;
	/**
	 * @var
	 */
	private $app;
	
	private $trialDays;
	
	/**
	 * CreateShopifyPaymentCommand constructor.
	 * @param $plan
	 * @param $app
	 */
	public function __construct($plan, $app)
	{
		$this->plan = $plan;
		
		$this->app = $app;
		
		$this->trialDays = 7;
	}
	
	public function handle()
	{
		$shop = new Shop($this->app);
		
		$shopData = $shop->getShop();
		
		$planName = data_get($shopData, 'plan_name', false);
		
		$shopUrl = $this->app->shop;
		
		$testMode = $this->_getTestMode($shopUrl);
		
		if ($testMode || !in_array($planName, ['affiliate', 'trial', 'singtel_trial', 'fraudulent']))
		{
			$payment = new RecurringApplicationCharge($this->app);
			
			return $payment->add([
				'name' => 'GetWooHoo ' . $this->plan['name'] . ' plan for shopify',
				'price' => (float)$this->plan['amount'],
				'return_url' => config('app.url') . '/shopify/charge/complete?shop_url=' . $shopUrl . '&',
				'trial_days' => $this->_getTrialDays($this->app)
			], $testMode);
		}
		
		return false;
	}
	
	/**
	 * @param $shopUrl
	 * @return bool
	 */
	private function _getTestMode($shopUrl)
	{
		if (config('services.shopify.test.mode'))
		{
			return true;
		}
		
		$storageList = explode(', ', config('services.shopify.test.storage.list'));
		
		return in_array($shopUrl, $storageList);
	}
	
	/**
	 * @param $app
	 * @return int
	 */
	public function _getTrialDays($app)
	{
		$trialDays = $this->trialDays;
		
		$subscribedAt = $this->_getSubscribedAt();
		
		if ($subscribedAt)
		{
			$trialDays = $this->_getTrialDaysLeft($subscribedAt);
		}
		
		return $app->trial_days != 0 ? $app->trial_days : $trialDays;
	}
	
	/**
	 * @return mixed
	 */
	private function _getSubscribedAt()
	{
		$result = \DB::table('shopify_apps as sa')
			->leftJoin('shopify_subscriptions as sf', 'sf.shop_url', 'sa.shop')
			->where('sa.shop', $this->app->shop)
			->orderBy('created_at', 'desc')
			->first();
		
		return $result && $result->created_at ? $result->created_at : false;
	}
	
	/**
	 * @param $subscribedAt
	 * @return int
	 */
	private function _getTrialDaysLeft($subscribedAt)
	{
		$trialDays = $this->trialDays;
		
		$subscribed = new Carbon($subscribedAt);
		
		$now = new Carbon();
		
		$diffInDays = $subscribed->diffInDays($now);
		
		if ($diffInDays < $this->trialDays)
		{
			$trialDays = $this->trialDays - $diffInDays;
		}
		elseif ($diffInDays >= $this->trialDays)
		{
			$trialDays = 0;
		}
		
		return $trialDays;
	}
}