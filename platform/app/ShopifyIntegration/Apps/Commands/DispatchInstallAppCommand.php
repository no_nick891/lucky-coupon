<?php namespace ShopifyIntegration\Apps\Commands;

use Integration\SendGrid\Commands\SendEmailCommand;
use LuckyCoupon\BaseCommand;
use Illuminate\Http\Request;
use LuckyCoupon\Plans\PlansEloquentRepository;
use LuckyCoupon\ShopifyApps\Commands\GetShopifyAppAccessTokenCommand;
use LuckyCoupon\ShopifyApps\Commands\InitializeShopifyApplicationCommand;
use LuckyCoupon\ShopifyApps\Commands\UpdateShopifyAppAccessTokenCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use LuckyCoupon\Users\UserEloquentRepository;
use ShopifyIntegration\API\Shop;
use ShopifyIntegration\API\WebHooks;
use ShopifyIntegration\Commands\InitShopifyAccountCommand;
use ShopifyIntegration\Provider;

class DispatchInstallAppCommand  extends BaseCommand
{
	private $app;
	
	private $appRepo;
	
	private $userRepo;
	
	private $planRepo;
	
	/**
	 * @var Provider
	 */
	private $shopifyProvider;
	
	/**
	 * @var WebHooks
	 */
	private $webHooks;
	
	private $shopName;
	
	private $requestArray;
	
	/**
	 * InstallAppCommand constructor.
	 * @param $request Request
	 */
	function __construct($request)
	{
		$this->shopName = '';
		
		$this->request = $request;
		
		$this->userRepo = new UserEloquentRepository();
		
		$this->planRepo = new PlansEloquentRepository();
		
		$this->appRepo = new ShopifyAppEloquentRepository();
		
		$this->app = $this->appRepo->model;
		
		$this->requestArray = $this->_getRequestValues();
		
		$this->shopifyProvider = new Provider($this->requestArray['shop']);
	}
	
	/**
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function handle()
	{
		return $this->_getPageRedirect();
	}
	
	/**
	 * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function _getPageRedirect()
	{
		return $this->requestArray['shop']
			? $this->_getPage($this->requestArray)
			: redirect('/shopify/auth');
	}
	
	/**
	 * @return mixed
	 */
	private function _getRequestValues()
	{
		$requestArray = $this->request->all();
		
		$requestArray['shop'] = $this->_getShopUrl();
		
		$this->shopName = $requestArray['shop'];
		
		unset($requestArray['_token']);
		
		unset($requestArray['shop_url']);
		
		return $requestArray;
	}
	
	
	/**
	 * @return mixed
	 */
	private function _getShopUrl()
	{
		return $this->request->get(
			'shop',
			data_get(parse_url($this->request->get('shop_url')), 'host')
		);
	}
	
	/**
	 * @param $requestArray
	 * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function _getPage($requestArray)
	{
		$code = data_get($requestArray, 'code', null);
		
		$app = $this->_getApp($requestArray['shop']);
		
		if ($app)
		{
			$this->webHooks = new WebHooks($app);
			
			return $this->_selectPage($app, $code, $requestArray);
		}
		else if ($code)
		{
			$app = $this->_saveShopifyApp($requestArray);
			
			$shop = $this->_getShopifyShopData($app);
			
			if (data_get($shop, 'plan_name', false) === 'affiliate')
			{
				return $this->_getAffiliateUserRedirect($app);
			}
			
			return $this->_getPlansPage($requestArray);
		}
		else
		{
			return $this->_installApp($requestArray['shop']);
		}
	}
	
	/**
	 * @param $app
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function _getAffiliateUserRedirect($app)
	{
		$user = $this->_initApplicationWithoutCharge('Unlimited', $app);
		
		return $this->_getLoginRedirector($user);
	}
	
	/**
	 * @param $shopUrl
	 * @return array|\LuckyCoupon\ShopifyApps\ShopifyApp
	 */
	private function _getApp($shopUrl)
	{
		$app = $this->appRepo->getApp($shopUrl);
		
		if (!$app)
		{
			$app = $this->appRepo->getApp($shopUrl);
		}
		
		$this->_log(__METHOD__, [
			'before_all_selectors' => 'true',
			'searched_shop' => $shopUrl,
			'app' => $app
		]);
		
		return $app;
	}
	
	/**
	 * @param $app
	 * @param $code
	 * @param $requestArray
	 * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function _selectPage($app, $code, $requestArray)
	{
		$isAccessible = !is_bool($this->_checkAccessToken($app));
		
		switch (true)
		{
			case !is_null($app->redirect_uri):
				return redirect($app->redirect_uri);
			case ($isAccessible || $code) && $app->plan_id > 0:
				dispatch(new UpdateShopifyAppAccessTokenCommand($requestArray));
				return $this->_getChargePage($app, $requestArray['shop']);
			case $isAccessible && $app->plan_id === 0:
				return $this->_loginUser($app, $requestArray);
			case $isAccessible === false && $app->access_token && $app->deleted === 0:
				return $this->_redirectToUpdateAccessToken();
			case $this->_isUserHaveAccount($app, $isAccessible):
				return $this->_getPlansPage($requestArray);
			case ($isAccessible === false && $app->access_token === '0' && is_null($app->user)):
				$app->delete();
				return redirect('/shopify/auth/install?' . http_build_query($requestArray));
			case ($isAccessible === false && $app->access_token === '0' && !is_null($app->user)):
				return $this->_redirectToUpdateAccessToken();
			case $isAccessible === false && !$code:
				return $this->_installApp($app->shop);
		}
	}
	
	/**
	 * @param $app
	 * @param $isAccessible
	 * @return bool
	 */
	private function _isUserHaveAccount($app, $isAccessible)
	{
		return $isAccessible === false
			&& ($this->_isDeletedUser($app)
			|| $this->_isModifiedApp($app));
	}
	
	/**
	 * @param $app
	 * @return bool
	 */
	private function _isDeletedUser($app)
	{
		return $app->deleted === 1
			&& data_get($app, 'user.id', false);
	}
	
	/**
	 * @param $app
	 * @return bool
	 */
	private function _isModifiedApp($app)
	{
		return $app->code === ''
			&& $app->plan_id === 0;
	}
	
	/**
	 * @param $app
	 * @param bool|array $requestArray
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function _loginUser($app, $requestArray = false)
	{
		if ($this->_checkScopes($app) === 0)
		{
			return redirect($this->shopifyProvider->getAuthUrl());
		}
		
		$this->_installWebHooks($app);
		
		$user = $this->userRepo->getUserByAppId($app->id);
		
		if (!$user)
		{
			$this->_sendErrorEmail($app, __METHOD__);
			
			$user = $this->userRepo->getUserByAppId($app->id);
		}
		
		if ($user)
		{
			$params = dispatch(new InjectScriptCommand($app, $user)) ? '?signup=1' : '';
			
			return $this->_getLoginRedirector($user, $params);
		}
		else if ($requestArray)
		{
			return $this->_getPlansPage($requestArray);
		}
	}
	
	/**
	 * @param $planName
	 * @param $app
	 * @return mixed
	 */
	private function _initApplicationWithoutCharge($planName, $app)
	{
		$app->plan_id = data_get($this->planRepo->getByName($planName), 'id', 0);
		
		$app->save();
		
		$user = dispatch(new InitializeShopifyApplicationCommand($app, $planName));
		
		return $user;
	}
	
	/**
	 * @param $user
	 * @param string $params
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function _getLoginRedirector($user, $params = '')
	{
		$this->_log(__METHOD__, ['userLogin' => 'true']);
		
		\Auth::login($user);
		
		return redirect('/' . $params);
	}
	
	/**
	 * @param $app
	 */
	private function _installWebHooks($app)
	{
		$webHooks = $this->webHooks->getAll();
		
		if ($this->_isWebHooksNotInstalled($webHooks))
		{
			$this->_log(__METHOD__, [
				'app' => $app,
			    'webHooks' => $webHooks
			]);
			
			dispatch(new AddWebHooksCommand($app, $webHooks));
		}
	}
	
	/**
	 * @param $webHooks
	 * @return bool
	 */
	private function _isWebHooksNotInstalled($webHooks)
	{
		return is_array($webHooks) && count($webHooks) < count($this->webHooks->webHookEvents);
	}
	
	/**
	 * @return mixed
	 */
	private function _redirectToUpdateAccessToken()
	{
		$this->_log(__METHOD__, [
			'redirected' => 'true'
		]);
		
		return redirect($this->shopifyProvider->getAuthUrl('shopify/auth/update-token'));
	}
	
	/**
	 * @param $app
	 * @return array|object
	 */
	private function _checkAccessToken($app)
	{
		if (!\App::environment('local'))
		{
			$this->shopifyProvider->log = false;
		}
		
		$accessScopes = $this->_getAccessScopes($app);
		
		if ($accessScopes === false)
		{
			$accessScopes = $this->_getAccessScopes($app);
			
			if ($accessScopes === false)
			{
				$accessScopes = $this->_getAccessScopes($app);
			}
		}
		
		return $accessScopes;
	}
	
	/**
	 * @param $app
	 * @return array|object
	 */
	private function _getAccessScopes($app)
	{
		$this->shopifyProvider->setToken($app);
		
		return $this->shopifyProvider->getAccessScopes();
	}
	
	/**
	 * @param $app
	 * @return bool
	 */
	private function _checkScopes($app)
	{
		$scopes = $this->_checkAccessToken($app);
		
		if (!$scopes) return false;
		
		$result = array_map([&$this, '_checkScope'], $scopes);
		
		return $this->_comparePermissions($result) ? 1 : 0;
	}
	
	/**
	 * @param $result
	 * @return bool
	 */
	private function _comparePermissions($result)
	{
		return count($result) === count($this->shopifyProvider->permissions)
			&& !in_array(false, $result);
	}
	
	/**
	 * @param $el
	 * @return bool
	 */
	private function _checkScope($el)
	{
		return in_array($el->handle, $this->shopifyProvider->permissions);
	}
	
	/**
	 * @param $shopUrl
	 * @return bool|\Illuminate\Http\RedirectResponse
	 */
	private function _installApp($shopUrl)
	{
		$this->_log(__METHOD__, [
			'shopUrl' => $shopUrl
		]);
		
		return dispatch(new InstallAppCommand($shopUrl));
	}
	
	/**
	 * @param $app
	 * @param $shopUrl
	 * @return bool|\Illuminate\Http\RedirectResponse
	 */
	private function _getChargePage($app, $shopUrl)
	{
		$plan = $app->plan;
		
		$amount = (int)data_get($plan, 'amount', 0);
		
		if ($amount === 0)
		{
			return $this->_initUserData($app, $plan['name']);
		}
		
		return redirect('/shopify/charge')
			->with('shop_url', $shopUrl);
	}
	
	/**
	 * @param $app
	 * @param $planName
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function _initUserData($app, $planName)
	{
		$this->_log(__METHOD__, [
			'app' => $app,
			'planName' => $planName
		]);
		
		dispatch(new InitShopifyAccountCommand($app, $planName));
		
		return $this->_loginUser($app, false);
	}
	
	/**
	 * @param $requestArray
	 * @return \Illuminate\Http\RedirectResponse
	 */
	private function _getPlansPage($requestArray)
	{
		return redirect('/shopify/plans')->with('request', $requestArray);
	}
	
	/**
	 * @param $requestArray
	 * @return int|\LuckyCoupon\ShopifyApps\ShopifyApp
	 */
	private function _addShopifyApp($requestArray)
	{
		$requestArray['plan_id'] = 0;
		
		$app = $this->appRepo->getApp($requestArray['shop']);
		
		if ($app) return $app;
		
		return data_get(
			$this->appRepo->getAppObjById(
				$this->app->insertGetId($requestArray)
			),0
		);
	}
	
	/**
	 * @param $app
	 * @param $requestArray
	 * @return mixed
	 */
	private function _saveAccessToken($app, $requestArray)
	{
		$token = dispatch(new GetShopifyAppAccessTokenCommand($requestArray));
		
		$app->access_token = $token;
		
		$app->save();
		
		return $app;
	}
	
	/**
	 * @param $methodName
	 * @param $variables
	 */
	protected function _log($methodName, $variables = null)
	{
		$newVariables = [];
		
		foreach ($variables as $key => $variable)
		{
			$newVariables[$key] = $this->_getObjectToArray($variable);
		}
		
		logLine([
			'location' => $methodName,
			'shop_name' => $this->shopName,
			'variables' => $newVariables
		]);
	}
	
	/**
	 * @param $app
	 * @param $method
	 * @param string $subject
	 */
	private function _sendErrorEmail($app, $method, $subject = 'Can\'t find user')
	{
		$this->_log($method, [
			'attempt to get user' => 'failed',
			'app' => $app
		]);
		
		$sendData = array_merge(
			['method' => $method],
			$this->_getObjectToArray($app)
		);
		
		dispatch((new SendEmailCommand(['email' => 'volue.nik@gmail.com'], [
			'from' => 'error@getwoohoo.com',
			'subject' => $subject,
			'text' => print_r($sendData, true),
			'type' => 'text/plain'
		]))->onQueue('email'));
	}
	
	/**
	 * @param $variable
	 * @return array
	 */
	protected function _getObjectToArray($variable)
	{
		return method_exists($variable, 'toArray')
			? array_merge(['object' => get_class($variable)], $variable->toArray())
			: $variable;
	}
	
	/**
	 * @param $requestArray
	 * @return int|\LuckyCoupon\ShopifyApps\ShopifyApp|mixed
	 */
	private function _saveShopifyApp($requestArray)
	{
		$app = $this->_addShopifyApp($requestArray);
		
		return $this->_saveAccessToken($app, $requestArray);
	}
	
	/**
	 * @param $app
	 * @return array|object
	 */
	private function _getShopifyShopData($app)
	{
		return (new Shop($app))->getShop();
	}
}