<?php namespace ShopifyIntegration\Apps\Commands;

use LuckyCoupon\Counters\CounterEloquentRepository;
use LuckyCoupon\Coupons\EloquentCouponRepository;
use LuckyCoupon\Games\EloquentGameRepository;
use LuckyCoupon\Settings\EloquentSettingRepository;
use LuckyCoupon\Sites\EloquentSiteRepository;
use LuckyCoupon\Statistics\EloquentStatisticRepository;
use LuckyCoupon\Users\UserEloquentRepository;

class InitializeUserDataCommand
{
	private $shopUrl;
	
	private $appId;
	
	private $couponRepo;
	
	private $settingRepo;
	
	private $appObj;
	
	/**
	 * InitializeUserDataCommand constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		$this->appObj = $app;
		
		$this->appId = $app->id;
		
		$this->shopUrl = $app->shop;
		
		$this->userRepo = new UserEloquentRepository();
		
		$this->siteRepo = new EloquentSiteRepository();
		
		$this->gamesRepo = new EloquentGameRepository();
		
		$this->statisticRepo = new EloquentStatisticRepository();
		
		$this->couponRepo = new EloquentCouponRepository();
		
		$this->settingRepo = new EloquentSettingRepository();
		
		$this->counterRepo = new CounterEloquentRepository();
	}
	
	/**
	 * Init user data
	 */
	public function handle()
	{
		$user = $this->_checkUser($this->appId);
		
		if ($user)
		{
			if ($this->appObj->is_upgrade)
			{
				$this->appObj->is_upgrade = 0;
				
				$this->appObj->save();
				
				return $user;
			}
			
			$user->is_first_time = 1;
			
			$user->save();
		}
		else
		{
			$user = $this->_addUser($this->shopUrl, $this->appId);
			
			$this->_addSite($this->shopUrl, $user->id);
			
			$this->appObj->plan_id = 0;
			
			$this->appObj->save();
		}
		
		return $user;
	}
	
	/**
	 * @param $shopUrl
	 * @param $appId
	 * @return mixed
	 */
	private function _addUser($shopUrl, $appId)
	{
		$userData = [
			'name' => $shopUrl,
			'app_id' => $appId,
			'email' => null, 'password' => '',
			'is_first_time' => 1,
			'remember-token' => null
		];
		
		return $this->userRepo->model->create($userData);
	}
	
	
	/**
	 * @param $shopUrl
	 * @param $userId
	 */
	private function _addSite($shopUrl, $userId)
	{
		$siteData = [
			'url' => $shopUrl,
			'user_id' => $userId,
			'active' => true
		];
		
		$this->siteRepo->model->insert($siteData);
	}
	
	/**
	 * @param $appId
	 * @return array
	 */
	private function _checkUser($appId)
	{
		$user = $this->userRepo->getUserByAppId($appId);
		
		return $user ? $user : false;
	}
	
	/**
	 * @todo: add new command with that logic
	 * @param $userId
	 */
	public function clearGames($userId)
	{
		$games = $this->gamesRepo->getByUserId($userId);
		
		$this->counterRepo->deleteImpressionsCounter($userId);
		
		foreach ($games as $game)
		{
			$id = $game->id;
			
			$this->_clearSettings($id);
			
			$this->_clearStatistics($id);
			
			$this->_clearCoupons($id);
			
			$game->delete();
		}
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	private function _clearStatistics($gameId)
	{
		return $this->statisticRepo->deleteByGameId($gameId);
	}
	
	/**
	 * @param $gameId
	 */
	private function _clearCoupons($gameId)
	{
		$coupons = $this->couponRepo->getByGameId($gameId);
		
		foreach ($coupons as $coupon)
		{
			$coupon->delete();
		}
	}
	
	/**
	 * @param $gameId
	 * @return mixed
	 */
	private function _clearSettings($gameId)
	{
		return $this->settingRepo->deleteByGameId($gameId);
	}
}