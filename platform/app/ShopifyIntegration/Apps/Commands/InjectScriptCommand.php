<?php namespace ShopifyIntegration\Apps\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Sites\Site;
use ShopifyIntegration\API\ScriptTags;

class InjectScriptCommand extends BaseCommand
{
	private $site;
	
	private $scriptTags;
	
	/**
	 * InjectScriptCommand constructor.
	 * @param $app
	 * @param $user
	 */
	public function __construct($app, $user)
	{
		$this->site = Site::where('user_id', $user->id)->first()->toArray();
		
		$this->scriptTags = new ScriptTags($app);
	}
	
	/**
	 * Inject script into shopify template
	 */
	public function handle()
	{
		$scriptTags = $this->scriptTags->getAll();
		
		if (count($scriptTags) === 0)
		{
			$scriptUrl = str_replace(['https:', 'http:'], '', config('app.url')) . '/' . $this->site['id'] . '/script.js';
			
			return $this->scriptTags->add($scriptUrl);
		}
		else
		{
			foreach ($scriptTags as $key => $scriptTag)
			{
				if ($key === 0) continue;
				
				$this->scriptTags->delete($scriptTag->id);
			}
		}
		
		return false;
	}
}