<?php namespace ShopifyIntegration\Apps\Commands;

use LuckyCoupon\BaseCommand;
use ShopifyIntegration\Provider;

class InstallAppCommand extends BaseCommand
{
	/**
	 * @var
	 */
	private $shopUrl;
	
	/**
	 * InstallAppCommand constructor.
	 * @param $shopUrl
	 */
	public function __construct($shopUrl)
	{
		$this->shopUrl = $shopUrl;
	}
	
	public function handle()
	{
		$shopifyProvider = new Provider($this->shopUrl);
		
		$url = $shopifyProvider->getAuthUrl();
		
		return redirect()->away($url);
	}
}