<?php namespace ShopifyIntegration\Apps\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use ShopifyIntegration\API\RecurringApplicationCharge;
use ShopifyIntegration\API\Shop;

class ChargeCommand extends BaseCommand
{
	private $appObj;
	
	/**
	 * ChargeCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$data = $request->all();
		
		$shopUrl = isset($data['shop_url']) ? $data['shop_url'] : session('shop_url');
		
		if ( ! $shopUrl) die('Shop url is not provided.');
		
		$appRepo = new ShopifyAppEloquentRepository();
		
		$this->appObj = $appRepo->getApp($shopUrl);
	}
	
	public function handle()
	{
		if (!$this->appObj->plan) return $this->_getFailInstallPage();
		
		$plan = $this->appObj->plan;
		
		if ($plan['id'] && (int)$plan['amount'] !== 0)
		{
			$paymentData = dispatch(new CreateShopifyPaymentCommand($plan, $this->appObj));
			
			$confirmationUrl = data_get($paymentData, 'confirmation_url', false);
			
			if ($confirmationUrl)
			{
				return redirect()->away($confirmationUrl);
			}
			else
			{
				$this->appObj->plan_id = 0;
				
				$this->appObj->save();
				
				return $this->_getFailInstallPage('Can\'t process with current plan on shopify account. Please upgrade to shopify paid plan or choose free plan for GetWooHoo application.');
			}
		}
		
		return $this->_getFailInstallPage();
	}
	
	/**
	 * @param string $message
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function _getFailInstallPage($message = '')
	{
		return redirect('/shopify/auth/install/fail')->with(['message' => $message]);
	}
}