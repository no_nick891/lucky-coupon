<?php namespace ShopifyIntegration\Apps\Commands;

use LuckyCoupon\ShopifyApps\Commands\InitializeShopifyApplicationCommand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LuckyCoupon\BaseCommand;
use ShopifyIntegration\API\Application;
use ShopifyIntegration\API\RecurringApplicationCharge;
use ShopifyIntegration\Apps\AppEloquentRepository;

class ChargeCompleteCommand extends BaseCommand
{
	private $shopUrl;
	
	private $chargeId;
	
	/**
	 * ChargeCompleteCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$data = $request->all();
		
		$this->shopUrl = isset($data['shop_url']) ? $data['shop_url'] : session('shop_url');
		
		$this->chargeId = isset($data['charge_id']) ? $data['charge_id'] : '';
		
		if (!$this->shopUrl && !$this->chargeId)
		{
			$this->_log(['Shop url or charge is not provided.']);
			
			die();
		}
	}
	
	public function handle()
	{
		$appRepo = new AppEloquentRepository();
		
		$app = $appRepo->getApp($this->shopUrl);
		
		if (!$app && !isset($app->id))
		{
			$this->_log(['Can\'t find application.']);
			
			die();
		}
		
		$payment = new RecurringApplicationCharge($app);
		
		$charge = $payment->get($this->chargeId);
		
		if ($charge)
		{
			if ($charge->status === 'accepted')
			{
				$payment->activeAccepted($this->chargeId);
				
				$user = dispatch(new InitializeShopifyApplicationCommand($app, $charge->name));
				
				Auth::login($user);
				
				return redirect($this->_getRedirectUrl() . '?paidsignup=1');
			}
			else
			{
				$application = new Application($app);
				
				$application->delete();
				
				$app->delete();
				
				return redirect('/shopify/auth/install/fail');
			}
		}
	}
	
	/**
	 * @return string
	 */
	private function _getRedirectUrl()
	{
		$redirectUrl = '/';
		
		if (isUpgradeCharge())
		{
			$redirectUrl = $_COOKIE['redirectUrl'];
			
			unset($_COOKIE['redirectUrl']);
			
			setcookie('redirectUrl', null, -1, '/');
		}
		
		return $redirectUrl;
	}
}