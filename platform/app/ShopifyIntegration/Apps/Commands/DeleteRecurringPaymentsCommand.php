<?php namespace ShopifyIntegration\Apps\Commands;

use LuckyCoupon\BaseCommand;
use ShopifyIntegration\API\RecurringApplicationCharge;

class DeleteRecurringPaymentsCommand extends BaseCommand
{
	private $chargeApi;
	
	/**
	 * DeleteRecurringPaymentsCommand constructor.
	 * @param $app
	 */
	public function __construct($app)
	{
		$this->chargeApi = new RecurringApplicationCharge($app);
	}
	
	public function handle()
	{
		$charges = $this->chargeApi->getAll();
		
		return $this->_deleteCharges($charges);
	}
	
	/**
	 * @param $charges
	 * @return bool
	 */
	private function _deleteCharges($charges)
	{
		$activeExists = false;
		
		foreach ($charges as $charge)
		{
			if ($charge->status === 'active')
			{
				$this->chargeApi->delete($charge->id);
				
				$activeExists = true;
			}
		}
		
		return $activeExists;
	}
}