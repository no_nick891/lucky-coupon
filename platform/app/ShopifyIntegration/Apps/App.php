<?php namespace ShopifyIntegration\Apps;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
	protected $table = 'shopify_apps';
	
	public $timestamps = false;
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function plan()
	{
		return $this->hasOne('LuckyCoupon\Plans\Plan', 'id', 'plan_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function orders()
	{
		return $this->hasMany('ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrder', 'id', 'app_id');
	}
}
