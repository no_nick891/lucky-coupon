<?php namespace ShopifyIntegration\Console;

use Illuminate\Console\Command;
use ShopifyIntegration\Apps\AppEloquentRepository;
use ShopifyIntegration\Apps\Commands\DeleteRecurringPaymentsCommand;

class FreeCharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify:free {url} {--file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make application free of charge.';
    
	private $appRepo;
	
	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->appRepo = new AppEloquentRepository();
    }
	
	/**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $isFile = $this->option('file');
	    
	    if ( ! $isFile)
	    {
		    $this->_freeOfChargeForShop($this->argument('url'), true);
	    }
	    else
	    { // make file reader for array of urls
		    $shopUrls = [];
	    	
		    $this->_freeOfChargeForShops($shopUrls);
	    }
    }
	
	/**
	 * @param $shopUrls
	 */
	private function _freeOfChargeForShops($shopUrls)
	{
		$bar = $this->output->createProgressBar(count($shopUrls));
		
		foreach ($shopUrls as $shopUrl)
		{
			$this->_freeOfChargeForShop($shopUrl);
			
			$bar->advance();
		}
		
		$bar->finish();
	}
	
	/**
	 * @param $shopUrl
	 * @param bool $message
	 */
	private function _freeOfChargeForShop($shopUrl, $message = false)
	{
		$shopUrl = removeProtocol($shopUrl);
		
		$app = $this->appRepo->getApp($shopUrl);
		
		if ($app->id)
		{
			$deleteCharges = dispatch(new DeleteRecurringPaymentsCommand($app));
			
			if ($message)
			{
				if ($deleteCharges)
				{
					$this->info('Charge deleted.');
				}
				elseif ( ! $deleteCharges)
				{
					$this->info('Active charges not detected.');
				}
			}
		}
		else
		{
			$this->info('Can not find current shop.');
		}
	}
}
