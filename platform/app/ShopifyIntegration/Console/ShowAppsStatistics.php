<?php namespace ShopifyIntegration\Console;

use App\LuckyCoupon\Users\User;
use Illuminate\Console\Command;
use LuckyCoupon\Games\Commands\GetGamesByUsersCommand;
use LuckyCoupon\Games\Commands\GetGamesStatisticsCommand;
use ShopifyIntegration\Apps\Commands\GetAppsCommand;

class ShowAppsStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopify:statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all statistics from shopify apps.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $shopifyUsers = User::where('app_id', '>', 0)->get();
	
	    $games = dispatch(new GetGamesByUsersCommand($shopifyUsers));
	
	    $apps = dispatch(new GetAppsCommand($shopifyUsers));
	
	    $gamesStatistics = dispatch(new GetGamesStatisticsCommand($games, $apps));
	
	    $totalActive = $this->_getActiveAppsCount($apps);
	    
	    $this->info(print_r(['TotalActive' => $totalActive, 'Statistics' => $gamesStatistics], true));
    }
	
	/**
	 * @param $apps
	 * @return int
	 */
	private function _getActiveAppsCount($apps)
	{
		$counter = 0;
		
		foreach ($apps as $app)
		{
			if ($app['installed'] === true)
			{
				$counter++;
			}
		}
		
		return $counter;
	}
}
