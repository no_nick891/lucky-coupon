<?php namespace ShopifyIntegration;

use GuzzleHttp\Client;
use RocketCode\Shopify\API;

class Provider
{
	public $log = true;
	
	/**
	 * @var array of credentials access for shopify API application
	 */
	public $credentials;
	
	/**
	 * @var array of permissions for shopify application
	 */
	public $permissions = [
		'read_products',
		'read_script_tags',
		'write_script_tags',
	    'read_orders'
	];
	
	private $shopUrl;
	
	function __construct($shopName, $token = null)
	{
		$this->credentials = [
			'API_KEY' => config('services.shopify.app.key'),
			'API_SECRET' => config('services.shopify.app.secret'),
			'SHOP_DOMAIN' => $shopName
		];
		
		if ($token)
		{
			$this->credentials['ACCESS_TOKEN'] = $token;
		}
		
		$this->shopUrl = 'https://' . $this->credentials['SHOP_DOMAIN'] . '/admin/';
	}
	
	/**
	 * @param $app
	 */
	public function setToken($app)
	{
		$this->credentials['ACCESS_TOKEN'] = is_string($app) ? $app : data_get($app, 'access_token', '');
	}
	
	/**
	 * @return array|object
	 */
	public function getAccessScopes()
	{
		$result = $this->call([
			'URL' => '/admin/oauth/access_scopes.json',
			'METHOD' => 'GET'
		]);
		
		return isset($result->access_scopes) ? $result->access_scopes : false;
	}
	
	/**
	 * @return bool
	 */
	public function revokeAccess()
	{
		$client = new Client(['base_uri' => $this->shopUrl]);
		
		$response = $client->request(
			'DELETE',
			'api_permissions/current.json',
			[
				'headers' => [
					'X-Shopify-Access-Token' => $this->credentials['ACCESS_TOKEN'],
				    'Content-Length' => '0',
				    'Accept' => 'application/json',
				    'Content-Type' => 'application/json'
				]
			]
		);
		
		return $response->getStatusCode() === 200;
	}
	
	/**
	 * Get install url
	 * @param string $url
	 * @return mixed
	 */
	public function getAuthUrl($url = 'shopify/auth/install')
	{
		$sh = new API($this->credentials);
		
		$url = $sh->installURL([
			'permissions' => $this->permissions,
			'redirect' => config('app.url') . $url
		]);
		
		return $url;
	}
	
	/**
	 * @param $request
	 * @return bool
	 */
	public function getAccessToken(array $request)
	{
		return $this->errorsHandler(function(API $sh, array $request) {
			$verify = $this->verifyRequest($request);
			
			if ($verify)
			{
				return $sh->getAccessToken($request['code']);
			}
			
			return false;
		}, $request);
	}
	
	/**
	 * @param $request
	 * @return mixed
	 */
	public function verifyRequest($request)
	{
		return $this->errorsHandler(function(API $sh, $request){
			return $sh->verifyRequest($request);
		}, $request);
	}
	
	/**
	 * @param $handler
	 * @param null $params
	 * @return bool|mixed
	 */
	public function errorsHandler($handler, $params = null)
	{
		$sh = new API($this->credentials);
		
		try
		{
			$mergedParams = is_array($params) ? array_merge([$sh], [$params]) : [$sh];
			
			return call_user_func_array($handler, $mergedParams);
		}
		catch (\Exception $e)
		{
			try
			{
				$message = $e->getMessage();
				
				if (strpos($message, '401') === false && $this->log)
				{
					\Log::error(
						print_r(['message' => $message, 'data' => $params], true),
						[__CLASS__, __METHOD__]
					);
				}
			}
			catch (\Exception $exception)
			{
				echo $exception->getMessage() . ' ' . $exception->getFile();
				
				return false;
			}
			
			return false;
		}
	}
	
	/**
	 * @param $params
	 * @return mixed
	 */
	public function call($params)
	{
		return $this->errorsHandler(function(API $sh, array $params) {
			return $sh->call($params);
		}, $params);
	}
	
}