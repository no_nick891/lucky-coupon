<?php namespace ShopifyIntegration\ShopifyAppOrders;

interface ShopifyAppOrderRepositoryInterface
{
	public function getAllPaidOrdersQuery();
	public function getAllOrders();
}