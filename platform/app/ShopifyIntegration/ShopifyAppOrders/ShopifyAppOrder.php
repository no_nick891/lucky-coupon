<?php namespace ShopifyIntegration\ShopifyAppOrders;

use Illuminate\Database\Eloquent\Model;

class ShopifyAppOrder extends Model
{
    protected $fillable = ['app_id', 'coupon_id',  'action', 'order_id', 'order_total_price', 'order_status', 'coupon_code', 'payload'];
	
	public $timestamps = true;
}
