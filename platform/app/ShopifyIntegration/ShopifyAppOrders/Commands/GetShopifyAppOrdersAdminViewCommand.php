<?php  namespace ShopifyIntegration\ShopifyAppOrders\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use LuckyCoupon\Users\UserEloquentRepository;

class GetShopifyAppOrdersAdminViewCommand extends BaseCommand {

	protected $userId;
	
	/**
	 * @var ShopifyAppEloquentRepository
	 */
	private $shopifyApp;
	
	/**
	 * @var UserEloquentRepository
	 */
	private $userRepo;
	
	/**
	 * GetShopifyAppOrdersViewCommand constructor.
	 * @param Request $userId
	 */
	public function __construct($userId)
	{
		$this->userId = (int)$userId;
		
		$this->shopifyApp = new ShopifyAppEloquentRepository();
		
		$this->userRepo = new UserEloquentRepository();
	}
	
	public function handle()
	{
		$userId = $this->userId;
		
		$appId = data_get($this->userRepo->getUserById($userId), 'app_id', false);
		
		$app = data_get($this->shopifyApp->getAppObjById($appId), '0', false);
		
		$orders = [];
		
		if ($app)
		{
			$ordersObject = $app->orders();
			
			if ($ordersObject)
			{
				$orders = $ordersObject->paginate(20);
			}
		}
		
		return view('admin.shopify.orders', compact('orders', 'userId'));
	}
}