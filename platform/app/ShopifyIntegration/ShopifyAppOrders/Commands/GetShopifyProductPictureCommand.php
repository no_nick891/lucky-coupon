<?php  namespace ShopifyIntegration\ShopifyAppOrders\Commands; 

use App\Helpers\FileStorageHelper;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Users\UserEloquentRepository;
use ShopifyIntegration\API\Product;

class GetShopifyProductPictureCommand extends BaseCommand {

	protected $productId;
	/**
	 * @var
	 */
	private $userId;
	
	private $userRepo;
	
	/**
	 * GetShopifyProductPictureCommand constructor.
	 * @param Request $productId
	 * @param $userId
	 */
	public function __construct($productId, $userId = false)
	{
		$this->productId = $productId;
		
		$this->userId = $userId;
		
		$this->userRepo = new UserEloquentRepository();
	}
	
	/**
	 * @return string
	 */
	public function handle()
	{
		$appId = \Auth::user()->app_id;
		
		$userId = \Auth::user()->id;
		
		if (!$appId && !$this->userId)
		{
			return '';
		}
		else if ($this->userId)
		{
			$user = $this->userRepo->getUserById($this->userId);
			
			$userId = $user->id;
			
			$appId = $user->app_id;
		}
		
		$file = 'order-pictures/' . $this->productId . '.jpg';
		
		$fileStorage = new FileStorageHelper($userId, 'jpg');
		
		$storageFilePath = $fileStorage->getImageDirectory(false) . $file;
		
		\Storage::makeDirectory($fileStorage->getImageDirectory(true) . 'order-pictures/');
		
		if (is_file(public_path($storageFilePath))) return Image::make(public_path($storageFilePath))->response();
		
		$products = new Product($appId);
		
		$images = $products->getImages($this->productId);
		
		$imageUrl = data_get($images, '0.src', '');
		
		if ($imageUrl === '') return '';
		
		$storageDir = $fileStorage->getImageDirectory(true) . $file;
		
		$protocol =  \Request::secure() ? 'https:' : 'http:';
		
		$imageContent = Image::make(file_get_contents(
			str_replace(['https:', 'http:'], $protocol, $imageUrl)
		))
			->resize(42, 42)
			->encode('jpg', 90)
			->fill('#fff', 0, 0);
		
		$imageContent->save(storage_path('app/' . $storageDir));
		
		return $imageContent->response();
	}
}