<?php  namespace ShopifyIntegration\ShopifyAppOrders\Commands;

use LuckyCoupon\BaseCommand;
use LuckyCoupon\Statistics\EloquentStatisticRepository;
use ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrderEloquentRepository;

class GetShopifyOrdersStatisticsViewCommand extends BaseCommand {
	
	private $shopifyAppOrderRepo;
	
	private $statisticsRepo;
	
	/**
	 * GetShopifyOrdersStatisticsViewCommand constructor.
	 */
	public function __construct()
	{
		$this->shopifyAppOrderRepo = new ShopifyAppOrderEloquentRepository();
		
		$this->statisticsRepo = new EloquentStatisticRepository();
	}
	
	public function handle()
	{
		$ordersHitsStatistics = $this->_getOrdersHitsStatistics();
		
		$orders = $this->shopifyAppOrderRepo->getAllOrders()->paginate(25);
		
		return view('admin.shopify.statistic_orders', compact('orders', 'ordersHitsStatistics'));
	}
	
	/**
	 * @return mixed
	 */
	private function _getOrdersHitsStatistics()
	{
		$ordersHitsStatistics = [];
		
		$hitStatistics = $this->statisticsRepo->getHitCountByMonth();
		
		$ordersStatistics = $this->shopifyAppOrderRepo->getCountByDate();
		
		foreach ($hitStatistics as $hitStatistic)
		{
			$ordersHitsStatistics[$hitStatistic->hit_month] = ['hit_count' => $hitStatistic->count];
		}
		
		foreach ($ordersStatistics as $ordersStatistic)
		{
			$ordersHitsStatistics[$ordersStatistic->orders_date]['orders_count'] = data_get($ordersStatistic,
				'orders_count',
				0);
			
			$ordersHitsStatistics[$ordersStatistic->orders_date]['orders_sum'] = data_get($ordersStatistic,
				'orders_sum',
				0);
		}
		
		return $ordersHitsStatistics;
	}
}