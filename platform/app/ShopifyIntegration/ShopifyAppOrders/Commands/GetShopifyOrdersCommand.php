<?php  namespace ShopifyIntegration\ShopifyAppOrders\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrder;

class GetShopifyOrdersCommand extends BaseCommand {

	protected $request;

	/**
	 * GetShopifyOrdersCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$user = \Auth::user();
		
		$appId = $user->app_id;
		
		$shopifyUserOrders = new ShopifyAppOrder();
		
		$orders = $shopifyUserOrders
			->from('shopify_app_orders as sao')
			->leftJoin('coupons as c', 'c.id', '=', 'sao.coupon_id')
			->where('sao.app_id', $appId)
			->where('sao.coupon_id', '>', 0)
			->where('sao.order_status', 'paid')
			->orderBy('sao.created_at', 'desc')
			->get();
		
		return ['orders' => $orders];
	}
}