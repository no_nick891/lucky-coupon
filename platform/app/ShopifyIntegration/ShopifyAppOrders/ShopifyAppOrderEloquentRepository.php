<?php namespace ShopifyIntegration\ShopifyAppOrders;

class ShopifyAppOrderEloquentRepository implements ShopifyAppOrderRepositoryInterface
{
	public $model;
	
	/**
	 * ShopifyAppOrderEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->model = new ShopifyAppOrder();
	}
	
	/**
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function getAllPaidOrdersQuery()
	{
		return $this->model
			->from('shopify_app_orders as sao')
			->leftJoin('users as u', 'u.app_id', '=', 'sao.app_id')
			->where('coupon_id', '>', 0)
			->where('order_status', 'paid')
			->where('u.app_id', '>', 0);
	}
	
	/**
	 * @return mixed
	 */
	public function getCountByDate()
	{
		return \DB::select("
			select
				temp.orders_date,
				sum(temp.user_count) as orders_count,
				sum(temp.sum_prices) as orders_sum
			from (
				select
					sum(orders_total_price) as sum_prices, count(userTmp.user_id) as user_count,
					userTmp.user_id, userTmp.orders_date
				from (
					select
						sum(sao.order_total_price)/100 as orders_total_price,
						DATE_FORMAT(sao.created_at, \"%Y-%m-01\") as orders_date,
						u.id as user_id
					from `shopify_app_orders` as `sao`
					left join `users` as `u` on `u`.`app_id` = `sao`.`app_id`
					where `coupon_id` > 0
						and `order_status` = 'paid'
						and `u`.`app_id` > 0
					group by sao.created_at, u.id
					order by sao.created_at desc
				) as userTmp
				group by userTmp.user_id, userTmp.orders_date
			) as temp
			group by temp.orders_date
			order by temp.orders_date desc;
		");
	}
	
	/**
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function getAllOrders()
	{
		return $this->getAllPaidOrdersQuery()
			->select([
				'sao.id',
				'sao.created_at',
				'u.name',
				'u.id as user_id',
				'g.type',
				'c.type as coupon_type',
				'c.value',
				'sao.coupon_code',
				'sao.order_total_price',
			    'sao.payload'
			])
			->leftJoin('coupons as c', 'c.id', '=', 'sao.coupon_id')
			->leftJoin('games as g', 'g.id', '=', 'c.game_id')
			->orderBy('created_at', 'desc');
	}
}