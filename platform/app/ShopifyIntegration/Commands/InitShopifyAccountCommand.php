<?php  namespace ShopifyIntegration\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifySubscriptions\Commands\SaveShopifySubscriptionCommand;
use LuckyCoupon\Users\Commands\SaveShopUserDataCommand;
use ShopifyIntegration\Apps\Commands\AddWebHooksCommand;
use ShopifyIntegration\Apps\Commands\InitializeUserDataCommand;
use ShopifyIntegration\Apps\Commands\InjectScriptCommand;

class InitShopifyAccountCommand extends BaseCommand {

	protected $app;
	/**
	 * @var
	 */
	private $planName;
	
	/**
	 * InitShopifyAccountCommand constructor.
	 * @param $app
	 * @param $planName
	 */
	public function __construct($app, $planName)
	{
		$this->app = $app;
		
		$this->planName = $planName;
	}
	
	public function handle()
	{
		$user = dispatch(new InitializeUserDataCommand($this->app));
		
		dispatch(new InjectScriptCommand($this->app, $user));
		
		dispatch(new AddWebHooksCommand($this->app));
		
		dispatch(new SaveShopifySubscriptionCommand($this->planName, $user->id, $this->app));
		
		dispatch(new SaveShopUserDataCommand(data_get($this->app, 'shop', '')));
		
		return $user;
	}
}