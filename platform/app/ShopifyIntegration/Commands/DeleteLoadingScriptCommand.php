<?php  namespace ShopifyIntegration\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use ShopifyIntegration\API\ScriptTags;

class DeleteLoadingScriptCommand extends BaseCommand {

	private $scriptId;
	
	private $scriptTag;
	
	/**
	 * DeleteLoadingScriptCommand constructor.
	 * @param $scriptId
	 * @param Request $request
	 */
	public function __construct($scriptId, $request)
	{
		$this->scriptId = $scriptId;
		
		$this->scriptTag = new ScriptTags((int)$request->get('app_id'));
	}
	
	public function handle()
	{
		$this->scriptTag->delete($this->scriptId);
		
		return redirect()->back();
	}
}