<?php  namespace ShopifyIntegration\Commands; 

use App\LuckyCoupon\Users\User;
use LuckyCoupon\BaseCommand;
use ShopifyIntegration\API\ScriptTags;

class GetLoadingScriptsViewCommand extends BaseCommand {
	
	private $user;
	
	private $scriptTags;
	
	/**
	 * GetLoadingScriptsViewCommand constructor.
	 * @param $userId
	 */
	public function __construct($userId)
	{
		$this->user = User::find($userId);
		
		$this->scriptTags = new ScriptTags((int)$this->user->app_id);
	}
	
	public function handle()
	{
		$userId = $this->user->id;
		
		$appId = $this->user->app_id;
		
		$name = $this->user->name;
		
		$scripts = $this->scriptTags->getAll();
		
		return view('admin.scripts', compact('name', 'scripts', 'userId', 'appId'));
	}
}