<?php  namespace ShopifyIntegration\ShopifyAppOrders\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

class GetSetupTestDataWebHooksCommand extends BaseCommand {
	
	public function handle()
	{
		return [
			0 => array (
				'id' => 2354544705,
				'name' => 'olegaffiliate',
				'email' => 'lirantadmor@gmail.com',
				'domain' => 'olegaffiliate.myshopify.com',
				'province' => NULL,
				'country' => 'IL',
				'address1' => 'Ben Ezra',
				'zip' => '6424617',
				'city' => 'Tel Aviv',
				'source' => NULL,
				'phone' => NULL,
				'latitude' => NULL,
				'longitude' => NULL,
				'primary_locale' => 'en',
				'address2' => NULL,
				'created_at' => '2018-04-19T13:24:53-04:00',
				'updated_at' => '2018-05-16T12:29:20-04:00',
				'country_code' => 'IL',
				'country_name' => 'Israel',
				'currency' => 'ILS',
				'customer_email' => 'lirantadmor@gmail.com',
				'timezone' => '(GMT-05:00) Eastern Time (US & Canada)',
				'iana_timezone' => 'America/New_York',
				'shop_owner' => 'olegaffiliate Admin',
				'money_format' => '{{amount}} NIS',
				'money_with_currency_format' => '{{amount}} NIS',
				'weight_unit' => 'kg',
				'province_code' => NULL,
				'taxes_included' => false,
				'tax_shipping' => NULL,
				'county_taxes' => true,
				'plan_display_name' => 'unlimited',
				'plan_name' => 'unlimited',
				'has_discounts' => true,
				'has_gift_cards' => false,
				'myshopify_domain' => 'olegaffiliate.myshopify.com',
				'google_apps_domain' => NULL,
				'google_apps_login_enabled' => NULL,
				'money_in_emails_format' => '{{amount}} NIS',
				'money_with_currency_in_emails_format' => '{{amount}} NIS',
				'eligible_for_payments' => false,
				'requires_extra_payments_agreement' => false,
				'password_enabled' => false,
				'has_storefront' => true,
				'eligible_for_card_reader_giveaway' => false,
				'finances' => true,
				'primary_location_id' => 6311510081,
				'checkout_api_supported' => false,
				'multi_location_enabled' => false,
				'setup_required' => false,
				'force_ssl' => true,
				'pre_launch_enabled' => false,
			),
			1 => array (
				'content-type' =>
					array (
						0 => 'application/json',
					),
				'content-length' =>
					array (
						0 => '1442',
					),
				'x-shopify-topic' =>
					array (
						0 => 'shop/update',
					),
				'x-shopify-shop-domain' =>
					array (
						0 => 'olegaffiliate.myshopify.com',
					),
				'x-shopify-hmac-sha256' =>
					array (
						0 => 'oLFjyc+ife8zXYZLONS5YyLWO1MmmhEI3f2AX6coLdA=',
					),
				'accept-encoding' =>
					array (
						0 => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
					),
				'accept' =>
					array (
						0 => '*/*',
					),
				'user-agent' =>
					array (
						0 => 'Ruby',
					),
				'x-newrelic-id' =>
					array (
						0 => 'VQQUUFNS',
					),
				'x-newrelic-transaction' =>
					array (
						0 => 'PxRTAF9QDFBTAARaVlABAwABFB8EBw8RVU4aBlkBBwZQBl0ECQBSVAFSB0NKQQkKVVEAWgFQFTs=',
					),
				'connection' =>
					array (
						0 => 'close',
					),
				'host' =>
					array (
						0 => 'dev-app.getwoohoo.com',
					),
			)
		];
	}
}