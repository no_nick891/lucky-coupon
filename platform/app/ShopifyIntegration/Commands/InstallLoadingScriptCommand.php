<?php  namespace ShopifyIntegration\Commands; 

use App\LuckyCoupon\Users\User;
use LuckyCoupon\BaseCommand;
use ShopifyIntegration\Apps\Commands\InjectScriptCommand;

class InstallLoadingScriptCommand extends BaseCommand {

	protected $userId;
	
	/**
	 * InstallLoadingScriptCommand constructor.
	 * @param $userId
	 */
	public function __construct($userId)
	{
		$this->userId = $userId;
	}
	
	public function handle()
	{
		$user = User::find($this->userId);
		
		dispatch(new InjectScriptCommand($user->app_id, $user));
		
		return redirect('/admin/shopify/scripts/' . $this->userId);
	}
}