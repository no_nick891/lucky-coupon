<?php  namespace ShopifyIntegration\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use ShopifyIntegration\API\Shop;

class GetShopDataCommand extends BaseCommand {

	protected $shopUrl;
	
	private $shopifyRepo;
	
	/**
	 * GetShopDataCommand constructor.
	 * @param Request $shopUrl
	 */
	public function __construct($shopUrl)
	{
		$this->shopUrl = $shopUrl;
		
		$this->shopifyRepo = new ShopifyAppEloquentRepository();
	}
	
	public function handle()
	{
		$shopifyShop = new Shop($this->shopifyRepo->getApp($this->shopUrl));
		
		$shop = $shopifyShop->getShop();
		
		if (!$shop)
		{
			\Log::error(print_r(['Shop getter failed' => $this->shopUrl]));
		}
		
		return $shop;
	}
}