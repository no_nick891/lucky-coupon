<?php  namespace ShopifyIntegration\Commands; 

use LuckyCoupon\BaseCommand;
use ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrder;

class SaveOrUpdateOrderCommand extends BaseCommand {

	protected $request;
	/**
	 * @var
	 */
	private $variables;
	/**
	 * @var
	 */
	private $topic;
	/**
	 * @var
	 */
	private $app;
	
	/**
	 * SaveOrderCommand constructor.
	 * @param $variables
	 * @param $topic
	 * @param $app
	 */
	public function __construct($variables, $topic, $app)
	{
		$this->variables = $variables;
		
		$this->topic = $topic;
		
		$this->app = $app;
	}
	
	public function handle()
	{
		if (!$userId = data_get($this->app, 'user.id', 0))
		{
			\Log::error(print_r([
				'Cant find user for application' => $this->app,
				'Variables' => $this->variables,
			    'topic' => $this->topic
			], true));
			
			return false;
		}
		
		$discountCode = $this->_getCommonDiscount($this->variables, $userId);
		
		$orderId = data_get($this->variables, 'id', 0);
		
		$appOrderData = [
			'app_id' => $this->app->id,
			'coupon_id' => data_get($discountCode, 'id', 0),
			'action' => $this->topic,
			'order_id' => $orderId,
			'order_total_price' => (float)data_get($this->variables, 'total_price', 0) * 100,
			'order_status' => data_get($this->variables, 'financial_status', ''),
			'coupon_code' => data_get($discountCode, 'code', ''),
			'payload' => json_encode($this->variables),
		    'created_at' => getDateFromShopifyFormat(data_get($this->variables, 'created_at'))
		];
		
		$this->_saveOrUpdateOrder($orderId, $appOrderData, $this->app);
	}
	
	
	/**
	 * @param $variables
	 * @param $id
	 * @return array
	 */
	private function _getCommonDiscount($variables, $id)
	{
		$discountCodes = data_get($variables, 'discount_codes', []);
		
		$orderCodes = $this->_getOrderCodes($discountCodes);
		
		$commonCodes = $this->_getDBCodes($id, $orderCodes);
		
		return data_get($commonCodes, '0', false);
	}
	
	/**
	 * @param $discountCodes
	 * @return array
	 */
	private function _getOrderCodes($discountCodes)
	{
		return array_map(function($el) {
			return trim(data_get($el, 'code'));
		}, $discountCodes);
	}
	
	/**
	 * @param $id
	 * @param $orderCodes
	 * @return array
	 */
	private function _getDBCodes($id, $orderCodes)
	{
		$query = \DB::table('users as u')
			->select(['c.id', 'c.code'])
			->leftJoin('games as g', 'g.user_id', '=', 'u.id')
			->leftJoin('coupons as c', 'c.game_id', '=', 'g.id')
			->where('u.id', $id)
			->whereIn('c.code', $orderCodes);
		
		return $query->get()->toArray();
	}
	
	/**
	 * @param $orderId
	 * @param $appOrderData
	 * @param $app
	 */
	private function _saveOrUpdateOrder($orderId, $appOrderData, $app)
	{
		$createdAt = data_get($appOrderData, 'created_at', false);
		
		$order = $this->_getOrCreateOrder($orderId, $app);
		
		$order->fill($appOrderData);
		
		if ($createdAt)
		{
			$order->created_at = $createdAt;
		}
		
		$order->save();
	}
	
	/**
	 * @param $orderId
	 * @param $app
	 * @return mixed
	 */
	private function _getOrder($orderId, $app)
	{
		return $app->orders()
			->where('order_id', $orderId)
			->where('app_id', $app->id)
			->first();
	}
	
	/**
	 * @param $orderId
	 * @param $app
	 * @return mixed
	 */
	private function _getOrCreateOrder($orderId, $app)
	{
		$order = $this->_getOrder($orderId, $app);
		
		return $order ? $order : new ShopifyAppOrder();
	}
}