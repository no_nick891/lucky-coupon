<?php  namespace ShopifyIntegration\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;

/**
 * Class GetOrdersTestWebHookData
 * @package ShopifyIntegration\Commands
 * <code>
		// for local tests
		$testData = dispatch(new GetOrdersTestWebHookData());
		$variables = $testData0];
		$headers = $testData1];
 * </code>
 */
class GetOrdersTestWebHookData extends BaseCommand {

	public function handle()
	{
		return array (
			0 => array(
				'id' => '533982642241',
				'email' => '',
				'closed_at' => '2018-07-16T05:22:49-04:00',
				'created_at' => '2018-07-16T04:53:59-04:00',
				'updated_at' => '2018-07-16T05:22:49-04:00',
				'number' => '16',
				'note' => '',
				'token' => 'a0265b81744859b023d742e37143780f',
				'gateway' => 'manual',
				'test' => '',
				'total_price' => '68.80',
				'subtotal_price' => '58.80',
				'total_weight' => '0',
				'total_tax' => '10.00',
				'taxes_included' => '',
				'currency' => 'ILS',
				'financial_status' => 'paid',
				'confirmed' => '1',
				'total_discounts' => '1.20',
				'total_line_items_price' => '60.00',
				'cart_token' => '',
				'buyer_accepts_marketing' => '',
				'name' => '#1016',
				'referring_site' => '',
				'landing_site' => '',
				'cancelled_at' => '',
				'cancel_reason' => '',
				'total_price_usd' => '18.91',
				'checkout_token' => '',
				'reference' => '',
				'user_id' => '10530226241',
				'location_id' => '6311510081',
				'source_identifier' => '',
				'source_url' => '',
				'processed_at' => '2018-07-16T04:53:59-04:00',
				'device_id' => '',
				'phone' => '',
				'customer_locale' => '',
				'app_id' => '1354745',
				'browser_ip' => '',
				'landing_site_ref' => '',
				'order_number' => '1016',
				'discount_applications' => Array(
					0 => Array(
						'type' => 'manual',
						'value' => '2.0',
						'value_type' => 'percentage',
						'allocation_method' => 'across',
						'target_selection' => 'all',
						'target_type' => 'line_item',
						'description' => 'Custom discount',
						'title' => 'Custom discount',
					)
				),
				'discount_codes' => Array(
					0 => Array(
						'code' => '10CASH',
						'amount' => '1.20',
						'type' => 'percentage',
					)
				),
				'note_attributes' => Array(),
				'payment_gateway_names' => Array(
					0 => 'manual'
				),
				'processing_method' => 'manual',
				'checkout_id' => '',
				'source_name' => 'shopify_draft_order',
				'fulfillment_status' => 'fulfilled',
				'tax_lines' => Array(
					0 => Array(
						'price' => '10.00',
						'rate' => '0.17',
						'title' => 'VAT',
					)
				),
				'tags' => '',
				'contact_email' => '',
				'order_status_url' => 'https://checkout.shopify.com/2354544705/orders/a0265b81744859b023d742e37143780f/authenticate?key=de6070c6bae30e31935aa8b382b947db',
				'line_items' => Array(
					0 => Array(
						'id' => '1340485533761',
						'variant_id' => '12524660916289',
						'title' => 'coat',
						'quantity' => '4',
						'price' => '15.00',
						'sku' => '',
						'variant_title' => '',
						'vendor' => 'olegfeedbacktest',
						'fulfillment_service' => 'manual',
						'product_id' => '1375480578113',
						'requires_shipping' => '1',
						'taxable' => '1',
						'gift_card' => '',
						'name' => 'coat',
						'variant_inventory_management' => '',
						'properties' => Array(),
						'product_exists' => '1',
						'fulfillable_quantity' => '0',
						'grams' => '0',
						'total_discount' => '0.00',
						'fulfillment_status' => 'fulfilled',
						'discount_allocations' => Array(
							0 => Array(
								'amount' => '1.20',
								'discount_application_index' => '0',
							)
						),
						'tax_lines' => Array(
							0 => Array(
								'title' => 'VAT',
								'price' => '10.00',
								'rate' => '0.17',
							)
						)
					)
				),
				'shipping_lines' => Array(),
				'fulfillments' => Array(
					0 => Array(
						'id' => '513862565953',
						'order_id' => '533982642241',
						'status' => 'success',
						'created_at' => '2018-07-16T05:22:49-04:00',
						'service' => 'manual',
						'updated_at' => '2018-07-16T05:22:49-04:00',
						'tracking_company' => '',
						'shipment_status' => '',
						'location_id' => '6311510081',
						'tracking_number' => 'asdf',
						'tracking_numbers' => Array(
							0 => 'asdf',
						),
						'tracking_url' => '',
						'tracking_urls' => Array(),
						'receipt' => Array(),
						'name' => '#1016.1',
						'line_items' => Array(
							0 => Array(
								'id' => '1340485533761',
								'variant_id' => '12524660916289',
								'title' => 'coat',
								'quantity' => '4',
								'price' => '15.00',
								'sku' => '',
								'variant_title' => '',
								'vendor' => 'olegfeedbacktest',
								'fulfillment_service' => 'manual',
								'product_id' => '1375480578113',
								'requires_shipping' => '1',
								'taxable' => '1',
								'gift_card' => '',
								'name' => 'coat',
								'variant_inventory_management' => '',
								'properties' => Array(),
								'product_exists' => '1',
								'fulfillable_quantity' => '0',
								'grams' => '0',
								'total_discount' => '0.00',
								'fulfillment_status' => 'fulfilled',
								'discount_allocations' => Array(
									0 => Array(
										'amount' => '1.20',
										'discount_application_index' => '0',
									)
								),
								'tax_lines' => Array(
									0 => Array(
										'title' => 'VAT',
										'price' => '10.00',
										'rate' => '0.17',
									)
								)
							)
						)
					)
				),
				'refunds' => Array()
			),
			1 => array (
				'content-type' =>
					array (
						0 => 'application/json',
					),
				'content-length' =>
					array (
						0 => '3666',
					),
				'x-shopify-topic' =>
					array (
						0 => 'orders/paid',
					),
				'x-shopify-shop-domain' =>
					array (
						0 => 'olegfeedbacktest.myshopify.com',
					),
				'x-shopify-order-id' =>
					array (
						0 => '516375511105',
					),
				'x-shopify-test' =>
					array (
						0 => 'false',
					),
				'x-shopify-hmac-sha256' =>
					array (
						0 => 'fMsFsh+PwIG3zBX/EBjcPK5OmX71oX1ihzPHQCO1HhE=',
					),
				'accept-encoding' =>
					array (
						0 => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
					),
				'accept' =>
					array (
						0 => '*/*',
					),
				'user-agent' =>
					array (
						0 => 'Ruby',
					),
				'connection' =>
					array (
						0 => 'close',
					),
				'host' =>
					array (
						0 => 'dev-app.getwoohoo.com',
					),
			)
		);
	}
}