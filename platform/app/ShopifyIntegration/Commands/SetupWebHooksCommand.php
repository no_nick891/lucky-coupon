<?php  namespace ShopifyIntegration\Commands; 

use App\LuckyCoupon\Users\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Integration\SendGrid\Commands\SendEmailCommand;
use LuckyCoupon\BaseCommand;
use ShopifyIntegration\ShopifyAppOrders\Commands\GetSetupTestDataWebHooksCommand;

class SetupWebHooksCommand extends BaseCommand {

	protected $request;

	/**
	 * SetupWebHooksCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$headers = $this->request->headers->all();
		
		$variables = $this->request->all();
		
		if (\App::environment('local'))
		{
			$testData = dispatch(new GetSetupTestDataWebHooksCommand());
			
			$variables = $testData[0];

			$headers = $testData[1];
		}
		
		if (!($app = $this->getWebHookApp($headers))) return $this->getErrorResponse();
		
		if ($app && data_get($headers, 'x-shopify-topic.0', '') === 'shop/update')
		{
			if ($app->shopify_plan_name === 'affiliate' && $variables['plan_name'] !== 'affiliate')
			{
				$this->_updateShopifyPlan($variables, $app);
			}
		}
		
		if ($app && data_get($headers, 'x-shopify-topic.0', '') === 'app/uninstalled')
		{
			$app->plan_id = 0;
			
			$app->deleted = 1;
			
			$app->is_paying = 0;
			
			$app->save();
			
			dispatch(
				(new SendEmailCommand(User::whereAppId($app->id)->first(), [
					'from' => 'team@getwoohoo.com',
					'subject' => 'Uninstall feedback',
					'template_id' => 'e8048017-a52b-4b5d-8746-779db8d9563f'
				]))
				->delay(Carbon::now()->addMinutes(10))
				->onQueue('email')
			);
		}
		
		return $this->getSuccessResponse();
	}
	
	/**
	 * @param $variables
	 * @param $app
	 */
	private function _updateShopifyPlan($variables, $app)
	{
		$app->email = $variables['customer_email'];
		
		$app->shopify_plan_name = $variables['plan_name'];
		
		$app->access_token = '';
		
		$app->code = '';
		
		$app->hmac = '';
		
		$app->is_paying = 0;
		
		$app->deleted = 1;
		
		$app->plan_id = 0;
		
		return $app->save();
	}
}