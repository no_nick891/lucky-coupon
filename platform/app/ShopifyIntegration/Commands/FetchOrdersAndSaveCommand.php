<?php  namespace ShopifyIntegration\Commands; 

use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use ShopifyIntegration\API\Order;

class FetchOrdersAndSaveCommand extends BaseCommand {
	
	private $appId;
	
	/**
	 * FetchOrdersAndSaveCommand constructor.
	 * @param $appId
	 */
	public function __construct($appId)
	{
		$this->appId = $appId;
	}
	
	public function handle()
	{
		$app = data_get((new ShopifyAppEloquentRepository())->getAppObjById($this->appId), '0', false);
		
		if (!$app) return ['fetch' => false, 'reason' => 'Can\'t find app.'];
		
		$order = new Order($app);
		
		$count = $order->getOrdersCount();
		
		if (!$count) return ['fetch' => false, 'reason' => 'Have no access or empty orders list .'];
		
		$page = 1;
		
		$limit = 250;
		
		while ($count > 0)
		{
			$ordersList = $order->getOrders([
				'limit' => $limit,
				'page' => $page
			]);
			
			foreach ($ordersList as $orderItem)
			{
				dispatch(new SaveOrUpdateOrderCommand($orderItem, 'orders/fetch', $app));
			}
			
			$page++;
			
			$count -= count($ordersList);
		}
		
		return ['fetch' => true];
	}
}