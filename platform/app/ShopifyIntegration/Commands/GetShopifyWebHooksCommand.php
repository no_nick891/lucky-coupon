<?php  namespace ShopifyIntegration\Commands; 

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use LuckyCoupon\Users\UserEloquentRepository;
use ShopifyIntegration\API\WebHooks;

class GetShopifyWebHooksCommand extends BaseCommand {

	protected $userId;

	/**
	 * GetShopifyWebHooksCommand constructor.
	 * @param $userId
	 */
	public function __construct($userId)
	{
		$this->userId = $userId;
	}
	
	public function handle()
	{
		$user = (new UserEloquentRepository())
				->getUserById($this->userId);
		
		$app = (new ShopifyAppEloquentRepository())
				->getAppObjById(data_get($user, 'app_id', 0))[0];
		
		$webHooks = new WebHooks($app);
		
		$shopWebHooks = $webHooks->getAll();
		
		return view('admin.shopify.webhooks', compact('shopWebHooks'));
	}
}