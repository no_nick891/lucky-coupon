<?php namespace ShopifyIntegration\Commands;

use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use ShopifyIntegration\ShopifyAppOrders\ShopifyAppOrder;

class OrdersWebHooksCommand extends BaseCommand
{
	protected $request;
	
	/**
	 * OrdersWebHooksCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$headers = $this->request->headers->all();
		
		$variables = $this->request->all();
		
		if (\App::environment('local'))
		{
			$testData = dispatch(new GetOrdersTestWebHookData());
			
			$variables = $testData[0];
			
			$headers = $testData[1];
		}
		
		if (!($app = $this->getWebHookApp($headers))) return $this->getErrorResponse();
		
		$topic = data_get($headers, 'x-shopify-topic.0', '');
		
		dispatch(new SaveOrUpdateOrderCommand($variables, $topic, $app));
		
		return $this->getSuccessResponse();
	}
	
}