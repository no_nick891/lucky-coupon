<?php  namespace ShopifyIntegration\RecurringCharges\Commands;

use App\LuckyCoupon\Users\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LuckyCoupon\BaseCommand;
use LuckyCoupon\Plans\PlansEloquentRepository;
use ShopifyIntegration\API\RecurringApplicationCharge;
use ShopifyIntegration\Apps\Commands\CreateShopifyPaymentCommand;

class ExtendShopifyTrialCommand extends BaseCommand {

	protected $request;
	
	private $activeCharge;
	
	private $plansRepo;
	
	/**
	 * ExtendShopifyTrialCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->plansRepo = new PlansEloquentRepository();
	}
	
	public function handle()
	{
		$result = ['trial' => 0];
		
		$userId = $this->request->get('user_id');
		
		$trialDays = (int)$this->request->get('trial');
		
		$user = User::find($userId);
		
		if (!data_get($user, 'app_id', false)) return $result;
		
		$recurringCharges = new RecurringApplicationCharge($user->app_id);
		
		$allRecurringCharges = $recurringCharges->getAll();
		
		$trialDaysLeft = $this->_getTrialDaysLeft($allRecurringCharges);
		
		if ($trialDaysLeft > 0)
		{
			$subscription = $user->shopifySubscription;
			
			$planId = $subscription[0]->plan_id;
			
			$plan = $this->plansRepo->model->find($planId)->toArray();
			
			$user->apps->trial_days = $trialDays + $trialDaysLeft;
			
			$paymentData = dispatch(new CreateShopifyPaymentCommand($plan, $user->apps));
			
			$confirmUrl = data_get($paymentData, 'confirmation_url', null);
			
			$user->apps->redirect_uri = $confirmUrl;
			
			$result = ['trial' => $user->apps->save()];
		}
		
		return $result;
	}
	
	/**
	 * @param $allRecurringCharges
	 * @return int
	 */
	private function _getTrialDaysLeft($allRecurringCharges)
	{
		$trialDaysLeft = 0;
		
		foreach ($allRecurringCharges as $recurringCharge)
		{
			if ($recurringCharge->status === 'active')
			{
				$this->activeCharge = $recurringCharge;
				
				$ends = new Carbon($recurringCharge->trial_ends_on);
				
				$now = new Carbon();
				
				if ($ends->gte($now))
				{
					$trialDaysLeft = $now->diffInDays($ends) + 1;
				}
			}
		}
		
		return $trialDaysLeft;
	}
}