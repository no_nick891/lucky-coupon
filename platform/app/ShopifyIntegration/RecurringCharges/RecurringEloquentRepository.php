<?php namespace ShopifyIntegration\RecurringCharges;

use LuckyCoupon\ShopifyApps\ShopifyAppEloquentRepository;
use ShopifyIntegration\API\RecurringApplicationCharge;

class RecurringEloquentRepository implements RecurringRepositoryInterface
{
	private $appRepo;
	
	/**
	 * RecurringEloquentRepository constructor.
	 */
	public function __construct()
	{
		$this->appRepo =  new ShopifyAppEloquentRepository();
	}
	
	/**
	 * @param $shopId
	 * @return array|object
	 */
	public function getCharges($shopId)
	{
		$app = $this->appRepo->getAppObjById($shopId);
		
		$chargeApi = new RecurringApplicationCharge(data_get($app, '0', false));
		
		return $chargeApi->getAll();
	}
}