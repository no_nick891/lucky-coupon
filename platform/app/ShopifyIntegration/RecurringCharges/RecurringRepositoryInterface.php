<?php namespace ShopifyIntegration\RecurringCharges;

interface RecurringRepositoryInterface
{
	public function getCharges($shopId);
}