<?php namespace App\Http\Controllers\Webhooks;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ShopifyIntegration\Commands\OrdersWebHooksCommand;
use ShopifyIntegration\Commands\SetupWebHooksCommand;

class ShopifyController extends Controller
{
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function orders(Request $request)
	{
		return dispatch(new OrdersWebHooksCommand($request));
    }
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function setup(Request $request)
	{
		return dispatch(new SetupWebHooksCommand($request));
    }
}
