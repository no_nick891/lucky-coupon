<?php namespace App\Http\Controllers\Integrations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Integration\Mailchimp\Commands\GetMailChimpAuthorizeUrlCommand;
use Integration\Mailchimp\Commands\InstallMailChimpAppCommand;

class InstallController extends Controller
{
	/**
	 * @param Request $request
	 * @return string
	 */
	public function getAuthUrl(Request $request)
	{
		$service = $request->get('service');
		
		$siteId = $request->get('site_id');
		
		switch (strtolower($service))
		{
			case 'mailchimp': return redirect(dispatch(new GetMailChimpAuthorizeUrlCommand($siteId)));
			default: return '<script>!function(){ window.close(); }()</script>';
		}
    }
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function mailChimp(Request $request)
	{
		return dispatch(new InstallMailChimpAppCommand($request));
    }
}
