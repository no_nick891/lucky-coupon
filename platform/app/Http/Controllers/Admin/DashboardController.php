<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use LuckyCoupon\Counters\Commands\SetAdminCounterCommand;
use LuckyCoupon\ShopifyApps\Commands\GetShopifySubscribersViewCommand;
use LuckyCoupon\ShopifyApps\Commands\GetAppChargesAdminViewCommand;
use LuckyCoupon\Statistics\Commands\GetStatisticsViewCommand;
use LuckyCoupon\Statistics\Commands\ShowAllGamesStatistics;
use LuckyCoupon\Statistics\Commands\ShowAllStoresStatistics;
use LuckyCoupon\Users\Commands\AuthUserCommand;
use LuckyCoupon\Users\Commands\GetAdminDataCommand;
use LuckyCoupon\Users\Commands\GetDuplicateUserStoresViewCommand;
use LuckyCoupon\Users\Commands\GetRegularUsersAdminViewCommand;
use LuckyCoupon\Users\Commands\GetShopifyUsersAdminViewCommand;
use LuckyCoupon\Users\UserEloquentRepository;
use ShopifyIntegration\Commands\DeleteLoadingScriptCommand;
use ShopifyIntegration\Commands\FetchOrdersAndSaveCommand;
use ShopifyIntegration\Commands\GetLoadingScriptsViewCommand;
use ShopifyIntegration\Commands\GetShopifyWebHooksCommand;
use ShopifyIntegration\Commands\InstallLoadingScriptCommand;
use ShopifyIntegration\RecurringCharges\Commands\ExtendShopifyTrialCommand;
use ShopifyIntegration\ShopifyAppOrders\Commands\GetShopifyAppOrdersAdminViewCommand;
use ShopifyIntegration\ShopifyAppOrders\Commands\GetShopifyOrdersStatisticsViewCommand;

class DashboardController extends Controller
{
	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request)
	{
		return dispatch(new GetShopifyUsersAdminViewCommand($request));
	}
	
	/**
	 * @param $userId
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function charges($userId)
	{
		return dispatch(new GetAppChargesAdminViewCommand($userId));
	}
	
	/**
	 * @param $userId
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function appAuth($userId)
	{
		return dispatch(new AuthUserCommand($userId));
	}
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function regular(Request $request)
	{
		return dispatch(new GetRegularUsersAdminViewCommand($request));
	}
	
	/**
	 * @return mixed
	 */
	public function allShopifySubscribers()
	{
		return dispatch(new GetShopifySubscribersViewCommand());
	}
	
	/**
	 * @return mixed
	 */
	public function shopifyStatistics()
	{
		return dispatch(new GetStatisticsViewCommand());
	}
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function exitToAdmin(Request $request)
	{
		$userId = dispatch(new GetAdminDataCommand($request));
		
		return dispatch(new AuthUserCommand($userId, '/admin/shopify'));
	}
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function setCounter(Request $request)
	{
		return dispatch(new SetAdminCounterCommand($request));
	}
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function allStoresStatistics(Request $request)
	{
		return dispatch(new ShowAllStoresStatistics($request));
	}
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function perGameStatistics(Request $request)
	{
		return dispatch(new ShowAllGamesStatistics($request));
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getWebHooks($userId)
	{
		return dispatch(new GetShopifyWebHooksCommand($userId));
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getShopifyOrders($userId)
	{
		return dispatch(new GetShopifyAppOrdersAdminViewCommand($userId));
	}
	
	/**
	 * @return mixed
	 */
	public function getShopifyOrdersStatistics()
	{
		return dispatch(new GetShopifyOrdersStatisticsViewCommand());
	}
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function fetchShopifyOrders(Request $request)
	{
		return dispatch(new FetchOrdersAndSaveCommand(data_get(
			(new UserEloquentRepository())->getUserById($request->get('user_id')),
		'app_id', 0)));
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function getShopifyScript($userId)
	{
		return dispatch(new GetLoadingScriptsViewCommand($userId));
	}
	
	/**
	 * @param $userId
	 * @return mixed
	 */
	public function installShopifyScript($userId)
	{
		return dispatch(new InstallLoadingScriptCommand($userId));
	}
	
	/**
	 * @param $scriptId
	 * @param Request $request
	 * @return mixed
	 * @internal param $scriptId
	 */
	public function deleteShopifyScript($scriptId, Request $request)
	{
		return dispatch(new DeleteLoadingScriptCommand($scriptId, $request));
	}
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function extendShopifyTrial(Request $request)
	{
		return dispatch(new ExtendShopifyTrialCommand($request));
	}
	
	/**
	 * @return mixed
	 */
	public function duplicateStores()
	{
		return dispatch(new GetDuplicateUserStoresViewCommand());
	}
}
