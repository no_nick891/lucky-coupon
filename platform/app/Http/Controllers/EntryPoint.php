<?php namespace App\Http\Controllers;

class EntryPoint extends Controller
{
	
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		return view('layouts.app');
    }
}
