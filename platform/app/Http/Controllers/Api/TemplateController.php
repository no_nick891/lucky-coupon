<?php namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Response;
use LuckyCoupon\Templates\Commands\GenerateScriptCommand;
use LuckyCoupon\Templates\Commands\GenerateSvgCommand;
use LuckyCoupon\Templates\Commands\GetGameBackgroundCommand;

/**
 * Class TemplateController
 * @package App\Http\Controllers\Api
 */
class TemplateController extends ValidateController
{
	/**
	 * @param $id
	 * @return mixed
	 */
	public function getScript($id)
	{
		$script = dispatch(new GenerateScriptCommand($id));
		
		return $this->_response($script);
	}
	
	/**
	 * @param $gameId
	 * @return \Illuminate\Http\Response
	 */
	public function getWheel($gameId)
	{
		$svg = dispatch(new GenerateSvgCommand($gameId));
		
		return $this->_response($svg, 'image/svg+xml')
					->header('Access-Control-Allow-Origin', '*');
	}
	
	/**
	 * @param $userId
	 * @param $gameId
	 * @param $fileName
	 * @return \Illuminate\Http\Response
	 */
	public function getBackgroundImage($userId, $gameId, $fileName = null)
	{
		return dispatch(new GetGameBackgroundCommand($userId, $gameId, $fileName));
	}
	
	/**
	 * @param $content
	 * @param string $type
	 * @return \Illuminate\Http\Response
	 */
	private function _response($content, $type = 'text/plain')
	{
		$headers = [
			'Content-type' => $type,
			'Content-Length' => strlen($content)
		];
		
		return Response::make($content, 200, $headers);
	}
}
