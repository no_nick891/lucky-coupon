<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use LuckyCoupon\Requests\Settings\DeleteSettingsEmailLogoRequest;
use LuckyCoupon\Requests\Settings\DeleteSettingsFileRequest;
use LuckyCoupon\Requests\Settings\GetSettingsRequest;
use LuckyCoupon\Requests\Settings\PostSettingsWheelFileLogoRequest;
use LuckyCoupon\Requests\Settings\PostSettingsFileRequest;
use LuckyCoupon\Requests\Settings\UpdateSettingsEmailLogoRequest;
use LuckyCoupon\Settings\Commands\DeleteSettingFileCommand;
use LuckyCoupon\Settings\Commands\GetGameSettingsCommand;
use LuckyCoupon\Settings\Commands\GetSettingsAndCouponsCommand;
use LuckyCoupon\Settings\Commands\UpdateSettingEmailLogoCommand;
use LuckyCoupon\Settings\Commands\UpdateSettingFileCommand;
use LuckyCoupon\Settings\Commands\UpdateSettingWheelFileLogoCommand;
use LuckyCoupon\Settings\Commands\UpdateSettingsCommand;

/**
 * Class SettingController
 * @package App\Http\Controllers\Api
 */
class SettingController extends ValidateController
{
	/**
	 * @param Request|GetSettingsRequest $request
	 * @return array|bool
	 */
	public function index(GetSettingsRequest $request)
	{
		return dispatch(new GetGameSettingsCommand($request));
    }
	
	/**
	 * @param GetSettingsRequest $request
	 * @return mixed
	 */
	public function get(GetSettingsRequest $request)
	{
		return dispatch(new GetSettingsAndCouponsCommand($request));
    }
    
	/**
	 * @param Request $request
	 * @return array
	 */
	public function update(Request $request)
	{
		return dispatch(new UpdateSettingsCommand($request));
    }
	
	/**
	 * @param PostSettingsFileRequest $request
	 * @return mixed
	 */
	public function updateFile(PostSettingsFileRequest $request)
	{
		return dispatch(new UpdateSettingFileCommand($request));
	}
	
	/**
	 * @param DeleteSettingsFileRequest $request
	 * @return mixed
	 */
	public function deleteFile(DeleteSettingsFileRequest $request)
	{
		return dispatch(new DeleteSettingFileCommand($request, 'image'));
	}
	
	/**
	 * @param PostSettingsWheelFileLogoRequest $request
	 * @return mixed
	 */
	public function updateWheelFileLogo(PostSettingsWheelFileLogoRequest $request)
	{
		return dispatch(new UpdateSettingWheelFileLogoCommand($request));
	}
	
	/**
	 * @param DeleteSettingsFileRequest $request
	 * @return mixed
	 */
	public function deleteWheelFileLogo(DeleteSettingsFileRequest $request)
	{
		return dispatch(new DeleteSettingFileCommand($request, 'wheelLogo'));
	}
	
	/**
	 * @param UpdateSettingsEmailLogoRequest $request
	 * @return mixed
	 */
	public function updateEmailLogo(UpdateSettingsEmailLogoRequest $request)
	{
		return dispatch(new UpdateSettingEmailLogoCommand($request));
	}
	
	/**
	 * @param DeleteSettingsEmailLogoRequest $request
	 * @return mixed
	 */
	public function deleteEmailLogo(DeleteSettingsEmailLogoRequest $request)
	{
		return dispatch(new DeleteSettingFileCommand($request, 'receiveEmailLogoImage'));
	}
}
