<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use LuckyCoupon\Requests\Games\AddEmailAndStatistic;
use LuckyCoupon\Statistics\Commands\CheckEmailCommand;

class ValidationController extends Controller
{
	/**
	 * @param AddEmailAndStatistic $request
	 * @return bool
	 */
	public function checkEmail(AddEmailAndStatistic $request)
	{
		return dispatch(new CheckEmailCommand($request));
    }
}
