<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LuckyCoupon\Requests\Users\UpdateUserProfileRequest;
use LuckyCoupon\Users\Commands\GetAuthUserDataCommand;
use LuckyCoupon\Users\Commands\UpdateUserProfileCommand;

/**
 * Class UserController
 * @package App\Http\Controllers\Api
 */
class UserController extends Controller
{

	/**
	 * @param Request $request
	 * @return array
	 */
	public function index(Request $request)
	{
		return dispatch(new GetAuthUserDataCommand($request));
    }
	
	/**
	 * @param UpdateUserProfileRequest $request
	 * @return mixed
	 */
	public function updateProfile(UpdateUserProfileRequest $request)
	{
		return dispatch(new UpdateUserProfileCommand($request));
    }
}
