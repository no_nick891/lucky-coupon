<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use LuckyCoupon\Coupons\Commands\UpdateCouponsCommand;
use LuckyCoupon\Coupons\CouponRepositoryInterface;
use LuckyCoupon\Coupons\EloquentCouponRepository;
use LuckyCoupon\Coupons\GetCouponCodeCommand;
use LuckyCoupon\Requests\Coupons\GetCouponsRequest;
use LuckyCoupon\Requests\Coupons\PatchCouponsRequest;
use LuckyCoupon\Requests\Coupons\PostGetCodeRequest;

/**
 * Class CouponController
 * @package App\Http\Controllers\Api
 */
class CouponController extends ValidateController
{
	/**
	 * @var CouponRepositoryInterface
	 */
	private $couponRepo;
	
	/**
	 * CouponController constructor.
	 * @param CouponRepositoryInterface|EloquentCouponRepository $couponRepo
	 */
	public function __construct(CouponRepositoryInterface $couponRepo)
	{
		$this->couponRepo = $couponRepo;
	}
	
	/**
	 * @param GetCouponsRequest $request
	 * @return array
	 */
	public function index(GetCouponsRequest $request)
	{
		if ($errs = $this->_getErrors($request)) return $errs;
		
		$gameId = $request->only('game_id');
		
		return ['coupons' => $this->couponRepo->getByGameId($gameId)];
    }
	
	/**
	 * @param Request|PatchCouponsRequest $request
	 * @return array|bool
	 */
	public function update(Request $request)
	{
		return dispatch(new UpdateCouponsCommand($request));
    }
	
	/**
	 * @param Request $request
	 * @return array
	 */
	public function updateBunch(Request $request)
	{
		extract($request->only('coupons'));
		
		return $this->couponRepo->updateBunch($coupons);
    }
	
	/**
	 * @param PostGetCodeRequest $request
	 * @return mixed
	 */
	public function getCode(PostGetCodeRequest $request)
	{
		return dispatch(new GetCouponCodeCommand($request));
    }
}
