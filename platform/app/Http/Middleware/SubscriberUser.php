<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class SubscriberUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $user = $request->user();
	
	    $url = parse_url($request->url());
	    
	    $userCreated = new Carbon($user->created_at);
	    
	    $now = new Carbon();
	    
	    if ((!$user->is_admin && !$user->app_id)
		    && $url['path'] !== '/api/v1/user'
		    && $userCreated->diffInDays($now) > 6)
	    {
		    if (!$this->_isUserSubscriptionActive($user))
		    {
			    if ($request->ajax())
			    {
				    return response(json_encode('Subscription expired.'), 422);
			    }
		    }
	    }
		
	    return $next($request);
    }
	
	/**
	 * @param $user
	 * @return bool
	 */
	private function _isUserSubscriptionActive($user)
	{
		$otherSubscription = $user->otherSubscription ? $user->otherSubscription : null;
		
		if ($otherSubscription)
		{
			$subscription = $otherSubscription->toArray();
			
			return $subscription['active'] === 1;
		}
		
		$isracardSubObj = $user->isracardSubscription
			->where('isracard_status', 2)
			->first();
		
		if ($isracardSubObj)
		{
			$isracardSubArray = $isracardSubObj->toArray();
			
			return $isracardSubArray['isracard_status'] === 2;
		}
		
		return false;
    }
}