<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('plans')->insert([
		    'id' => 1,
		    'name' => 'Premium',
		    'amount' => 10,
	        'currency' => 'USD',
	        'period' => 'month'
	    ]);
	
	    DB::table('plans')->insert([
		    'id' => 2,
		    'name' => 'Premium Plus',
		    'amount' => 20,
		    'currency' => 'USD',
		    'period' => 'month'
	    ]);
	
	    if (\App::environment(['local', 'dev']))
	    {
		    DB::table('plans')->insert([
			    'id' => 3,
			    'name' => 'Test subscription',
			    'amount' => 5,
			    'currency' => 'USD',
			    'period' => 'month'
		    ]);
	    }
    }
}
