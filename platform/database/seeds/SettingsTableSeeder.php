<?php

use Illuminate\Database\Seeder;
use LuckyCoupon\Seeds\Helper;

/**
 * Class SettingsTableSeeder
 */
class SettingsTableSeeder extends Seeder
{
    private $helper;

    /**
     * TemplatesTableSeeder constructor.
     */
    public function __construct()
    {
        $this->helper = new Helper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $templatesPath = 'resources/assets/js/helpers/templates/';

        $settings = $this->helper->getJsConstants($templatesPath . 'default/settings.js');

        $settingsArray = json_decode($settings['gameSettings']);

        $this->_seed(0, $settingsArray, 1);

        $this->_seed(0, $settingsArray, 2);

        $this->_seed(0, $settingsArray, 3);
    }

    /**
     * @param int $id
     * @param $settings
     * @param $gameId
     */
    private function _seed($id = 0, $settings, $gameId)
    {
        foreach ($settings as $name => $setting) {
            $isValue = !is_object($setting) && !is_array($setting);

            $parentId = DB::table('settings')->insertGetId([
                'game_id' => $gameId,
                'parent_id' => $id,
                'name' => $name,
                'value' => $isValue ? $setting : null
            ]);

            if (!$isValue) {
                $this->_seed($parentId, $setting, $gameId);
            }
        }
    }
}
