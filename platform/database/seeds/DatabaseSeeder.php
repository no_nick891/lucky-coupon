<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
        	'id' => 1,
            'name' => 'admin',
	        'is_admin' => 1,
	        'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'created_at' => date('Y-m-d h:i:s')
        ]);
	
	    DB::table('users')->insert([
		    'id' => 2,
		    'name' => 'Liran Tadmor',
		    'is_admin' => 0,
		    'email' => 'lirantadmor@gmail.com',
		    'password' => bcrypt('liran123'),
		    'created_at' => date('Y-m-d h:i:s')
	    ]);
	
	    DB::table('users')->insert([
		    'id' => 3,
		    'name' => 'dev',
		    'is_admin' => 0,
		    'email' => 'test@test.com',
		    'password' => bcrypt('test12'),
		    'created_at' => date('Y-m-d h:i:s')
	    ]);

	    DB::table('sites')->insert([
			'user_id' => 1,
			'url' => 'http://with-game.com',
			'active' => 1
		]);
	
	    DB::table('sites')->insert([
		    'user_id' => 2,
		    'url' => 'https://luckyshopify.com/',
		    'active' => 1
	    ]);

	    for($i = 0; $i < 2; $i++)
	    {
		    DB::table('games')->insert([
				'name' => 'coupon ' . ($i + 1),
				'active' => 1,
				'user_id' => 1,
				'site_id' => 1,
		        'type' => 'coupon',
		        'is_created' => 1,
		        'created_at' => date('Y-m-d h:i:s')
			]);
	    }
	
	    DB::table('games')->insert([
		    'name' => 'coupon ' . ($i + 1),
		    'active' => 1,
		    'user_id' => 2,
		    'site_id' => 2,
		    'type' => 'coupon',
		    'is_created' => 1,
		    'created_at' => date('Y-m-d h:i:s')
	    ]);
	
	    $this->call(CouponsTableSeeder::class);
	
	    $this->call(SettingsTableSeeder::class);
	    
	    $this->call(TemplatesTableSeeder::class);
	    
	    $this->call(PlansTableSeeder::class);
    }
}
