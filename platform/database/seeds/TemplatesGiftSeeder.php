<?php use Illuminate\Database\Seeder;

class TemplatesGiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $templatesSeeder = new TemplatesTableSeeder();
        
        $templatesSeeder->i = 7;
        
        $templatesSeeder->addTemplate('gift');
    }
}
