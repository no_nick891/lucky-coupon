<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use LuckyCoupon\Seeds\Helper;

class TemplatesTableSeeder extends Seeder
{
	private $helper;
	
	public $i;
	
	/**
	 * TemplatesTableSeeder constructor.
	 */
	public function __construct()
	{
		$this->helper = new Helper();
	}
	
	/**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $this->i = 1;
	
	    $this->addTemplate('default');
	    
	    $this->addTemplate('slot');
    }
	
	/**
	 * @param $templateName
	 */
	public function addTemplate($templateName)
	{
		$templatesPath = 'resources/assets/js/helpers/templates/';
		
		$template = array_merge(
			['modalContent' => $templatesPath . $templateName . '/template.js'],
			['modalWrapper' => $templatesPath . $templateName . '/template.js'],
			['modalStyle' => $templatesPath . $templateName . '/style.css']
		);
		
		$this->_insertTemplate($template, 'coupon', $templateName);
	}
	
	/**
	 * @param $templateResources
	 * @param $type
	 * @param $name
	 */
	private function _insertTemplate($templateResources, $type, $name)
	{
		foreach ($templateResources as $partName => $value)
		{
			DB::table('templates')->insert([
				'id' => $this->i,
				'type' => $type,
				'name' => $name,
				'part' => $partName,
				'content' => $value
			]);
			
			$this->i = $this->i + 1;
		}
	}
}