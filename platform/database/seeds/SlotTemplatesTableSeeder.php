<?php

use Illuminate\Database\Seeder;

class SlotTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $templates = new TemplatesTableSeeder();
		
        $templates->i = 4;
        
	    $templates->addTemplate('slot');
    }
}
