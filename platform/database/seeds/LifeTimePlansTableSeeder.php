<?php

use Illuminate\Database\Seeder;

class LifeTimePlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('plans')->insert([
		    'name' => 'Life time access',
		    'amount' => 49,
		    'currency' => 'USD',
		    'period' => 'century',
		    'type' => 'other',
		    'conditions' => ''
	    ]);
    }
}
