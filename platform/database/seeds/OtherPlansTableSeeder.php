<?php

use Illuminate\Database\Seeder;

class OtherPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('plans')->insert([
		    'name' => 'JVZoo Premium',
		    'amount' => 37,
		    'currency' => 'USD',
		    'period' => 'century',
		    'type' => 'other',
		    'conditions' => ''
	    ]);
	
	    DB::table('plans')->insert([
		    'name' => 'JVZoo Premium Plus',
		    'amount' => 67,
		    'currency' => 'USD',
		    'period' => 'century',
		    'type' => 'other',
		    'conditions' => ''
	    ]);
    }
}
