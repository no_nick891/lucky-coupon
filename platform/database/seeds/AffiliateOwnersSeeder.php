<?php

use App\LuckyCoupon\Users\User;
use Illuminate\Database\Seeder;
use LuckyCoupon\AffiliateOwners\AffiliateOwner;

class AffiliateOwnersSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$registeredUsers = User::where('app_id', 0)->get();
		
		foreach ($registeredUsers as $registeredUser)
		{
			$affiliateOwner = new AffiliateOwner();
			
			$affiliateOwner->user_id = $registeredUser->id;
			
			$affiliateOwner->reference = 350 + $registeredUser->id;
			
			$affiliateOwner->paypal_email = '';
			
			$affiliateOwner->save();
		}
	}
}
