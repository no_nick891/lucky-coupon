<?php

use Illuminate\Database\Seeder;

class AddBasicPlanWithNoIntegrationsAccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('plans')
		    ->where('name', 'Basic')
		    ->update(['active' => 0]);
	
	    DB::table('plans')->insert([
		    'name' => 'Basic',
		    'amount' => 4.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '1000',
	        'description' => 'no integrations|powered by on'
	    ]);
    }
}
