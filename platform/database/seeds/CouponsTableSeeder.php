<?php use Illuminate\Database\Seeder;

class CouponsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @param int $gameId
	 * @param int $number
	 * @return void
	 */
    public function run($gameId = 1, $number = 8)
    {
	    for($i = 0; $i < 8; $i++)
	    {
		    DB::table('coupons')->insert([
			    'type' => $i > 1 ? 'discount' : ($i === 0 ? 'cash' : 'free shipping'),
			    'value' => 5,
			    'code' => str_random(6),
			    'game_id' => $gameId,
			    'chance' => 100/$number,
		        'color' => ''
		    ]);
		
		    DB::table('coupons')->insert([
			    'type' => $i > 1 ? 'discount' : ($i === 0 ? 'cash' : 'free shipping'),
			    'value' => 5,
			    'code' => str_random(6),
			    'game_id' => 2,
			    'chance' => 100/$number,
		        'color' => ''
		    ]);
		    
		    DB::table('coupons')->insert([
			    'type' => $i > 1 ? 'discount' : ($i === 0 ? 'cash' : 'free shipping'),
			    'value' => 5,
			    'code' => str_random(6),
			    'game_id' => 3,
			    'chance' => 100/$number,
		        'color' => ''
		    ]);
	    }
    }
}
