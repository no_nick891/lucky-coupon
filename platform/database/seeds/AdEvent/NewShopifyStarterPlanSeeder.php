<?php

use Illuminate\Database\Seeder;

class NewShopifyStarterPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('plans')
		    ->where('name', 'Starter')
		    ->update(['active' => 0]);
    	
	    DB::table('plans')->insert([
		    'name' => 'Starter',
		    'amount' => 0,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '300'
	    ]);
    }
}
