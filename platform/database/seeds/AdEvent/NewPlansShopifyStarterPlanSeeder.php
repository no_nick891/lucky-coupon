<?php

use Illuminate\Database\Seeder;

class NewPlansShopifyStarterPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('plans')
		    ->where('name', 'Starter')
		    ->where('active', 1)
		    ->update(['active' => 0]);
	
	    DB::table('plans')
		    ->where('name', 'Basic')
		    ->where('active', 1)
		    ->update(['active' => 0]);
    	
	    DB::table('plans')->insert([
		    'name' => 'Starter',
		    'amount' => 0,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '100'
	    ]);
	
	    DB::table('plans')->insert([
		    'name' => 'Basic',
		    'amount' => 4.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '1000'
	    ]);
    }
}
