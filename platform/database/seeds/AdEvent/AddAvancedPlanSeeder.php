<?php

use Illuminate\Database\Seeder;

class AddAvancedPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('plans')
		    ->where('name', 'Pro')
		    ->update(['active' => 0]);
	
	    DB::table('plans')
		    ->where('name', 'Professional')
		    ->update(['active' => 0]);
	
	    DB::table('plans')->insert([
		    'name' => 'Advanced',
		    'amount' => 9.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '10000'
	    ]);
	
	    DB::table('plans')->insert([
		    'name' => 'Pro',
		    'amount' => 29.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '50000'
	    ]);
    }
}
