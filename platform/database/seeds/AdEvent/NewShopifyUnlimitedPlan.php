<?php

use Illuminate\Database\Seeder;

class NewShopifyUnlimitedPlan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('plans')
		    ->where('name', 'Professional')
		    ->update(['active' => 0]);
    	
	    DB::table('plans')
		    ->where('name', 'Unlimited')
		    ->update(['active' => 0]);
	
	    DB::table('plans')->insert([
		    'name' => 'Professional',
		    'amount' => 29.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '50000'
	    ]);
	    
	    DB::table('plans')->insert([
		    'name' => 'Unlimited',
		    'amount' => 99.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => ''
	    ]);
    }
}
