<?php

use Illuminate\Database\Seeder;

class PlanUpdate8Jan2019Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $plansList = [
	    	[
			    'name' => 'Basic',
			    'amount' => 9.95,
			    'description' => 'no integrations|powered by on'
		    ],
		    [
			    'name' => 'Advanced',
			    'amount' => 14.95,
			    'conditions' => '5000'
		    ]
	    ];
	
	    createPlans($plansList);
    }
}
