<?php

use Illuminate\Database\Seeder;

class PlanUpdate13Dec2018Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('plans')
		    ->where('name', 'Basic')
		    ->where('active', 1)
		    ->update(['active' => 0]);
    	
	    DB::table('plans')
		    ->where('name', 'Advanced')
		    ->where('active', 1)
		    ->update(['active' => 0]);
	    
	    DB::table('plans')->insert([
	    	'active' => 1,
		    'name' => 'Basic',
		    'amount' => 4.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '1000',
	        'description' => 'no integrations|powered by on'
	    ]);
	    
	    DB::table('plans')->insert([
		    'active' => 1,
		    'name' => 'Advanced',
		    'amount' => 9.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '5000'
	    ]);
    }
}
