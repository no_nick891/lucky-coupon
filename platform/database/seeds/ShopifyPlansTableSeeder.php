<?php

use Illuminate\Database\Seeder;

class ShopifyPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('plans')->insert([
		    'name' => 'Starter',
		    'amount' => 0,
	        'currency' => 'USD',
	        'period' => 'month',
	        'type' => 'shopify',
	        'conditions' => '1000'
	    ]);
	
	    DB::table('plans')->insert([
		    'name' => 'Basic',
		    'amount' => 4.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '3000'
	    ]);
	
	    DB::table('plans')->insert([
		    'name' => 'Pro',
		    'amount' => 9.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => '10000'
	    ]);
	
	    DB::table('plans')->insert([
		    'name' => 'Unlimited',
		    'amount' => 29.95,
		    'currency' => 'USD',
		    'period' => 'month',
		    'type' => 'shopify',
		    'conditions' => ''
	    ]);
    }
}
