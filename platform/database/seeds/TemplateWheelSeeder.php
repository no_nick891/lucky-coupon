<?php

use Illuminate\Database\Seeder;

class TemplateWheelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $templatesSeeder = new TemplatesTableSeeder();
	
	    $templatesSeeder->i = 10;
	
	    $templatesSeeder->addTemplate('wheel');
    }
}
