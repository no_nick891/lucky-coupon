<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOmnisendAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('omnisend_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('active');
	        $table->integer('user_id');
	        $table->integer('site_id');
	        $table->string('private_key');
	        $table->string('selected_list_id')->nullable()->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('omnisend_apps');
    }
}
