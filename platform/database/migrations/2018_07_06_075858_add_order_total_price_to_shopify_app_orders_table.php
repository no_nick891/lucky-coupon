<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderTotalPriceToShopifyAppOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_app_orders', function (Blueprint $table) {
        	$table->integer('coupon_id')->after('app_id');
            $table->integer('order_total_price')->after('order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_app_orders', function (Blueprint $table) {
	        $table->dropColumn('coupon_id');
        	$table->dropColumn('order_total_price');
        });
    }
}
