<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsUpgradeColumnSholifyAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('shopify_apps', function(Blueprint $table) {
		    $table->tinyInteger('is_upgrade')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('shopify_apps', function(Blueprint $table) {
		    $table->dropColumn('is_upgrade');
	    });
    }
}
