<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopifyAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('shopify_apps', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('code');
		    $table->string('shop');
		    $table->string('access_token');
		    $table->string('hmac');
		    $table->integer('timestamp');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('shopify_apps');
    }
}
