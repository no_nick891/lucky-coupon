<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailchimpAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('mailchimp_apps', function (Blueprint $table) {
		    $table->increments('id');
		    $table->smallInteger('active');
		    $table->integer('user_id');
		    $table->integer('site_id');
		    $table->string('dc');
		    $table->string('role');
		    $table->string('accountname');
		    $table->string('mailchimp_id');
		    $table->string('login_url');
		    $table->string('api_endpoint');
		    $table->string('access_token');
		    $table->text('lists')->nullable(true)->default(NULL);
		    $table->string('selected_list_id')->nullable(true)->default(NULL);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('mailchimp_apps');
    }
}
