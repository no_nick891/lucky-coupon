<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSubscriptionGatewayColumnSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE subscriptions CHANGE COLUMN subscription_gateway subscription_gateway ENUM('isracard', 'paypal', 'shopify', 'other') NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::statement("ALTER TABLE subscriptions CHANGE COLUMN subscription_gateway subscription_gateway ENUM('isracard', 'paypal', 'shopify') NOT NULL");
    }
}
