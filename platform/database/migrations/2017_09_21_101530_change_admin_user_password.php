<?php

use Illuminate\Database\Migrations\Migration;

class ChangeAdminUserPassword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    if (env('APP_ENV') !== 'local')
	    {
		    DB::table('users')->where('id', 1)
			    ->update(['password' => bcrypt('111@admin@222')]);
	    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
