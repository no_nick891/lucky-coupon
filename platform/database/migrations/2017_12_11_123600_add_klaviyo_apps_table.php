<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKlaviyoAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('klaviyo_apps', function (Blueprint $table) {
		    $table->increments('id');
		    $table->smallInteger('active');
		    $table->integer('user_id');
		    $table->integer('site_id');
		    $table->string('private_key');
		    $table->string('lists');
		    $table->string('selected_list_id')->nullable()->default('');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('klaviyo_apps');
    }
}
