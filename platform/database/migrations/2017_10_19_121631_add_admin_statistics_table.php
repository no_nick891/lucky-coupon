<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('admin_statistics', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('game_id');
		    $table->integer('impressions')->default(0);
		    $table->integer('hits')->default(0);
		    $table->integer('device_impressions')->default(0);
		    $table->integer('device_hits')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('admin_statistics');
    }
}
