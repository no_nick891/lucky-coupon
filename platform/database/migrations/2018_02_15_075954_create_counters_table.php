<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('counters', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('column_name')->default('user_id');
		    $table->integer('column_value')->default(0);
		    $table->string('counter_name')->default('impressions');
		    $table->integer('counter')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('counters');
    }
}
