<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLanguageValueSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    $languages = [];
	
	    $behaviours = \DB::table('settings')->where('name', 'behavior')->get()->toArray();
	
	    foreach ($behaviours as $behaviour)
	    {
		    $languages[] = [
			    'game_id' => $behaviour->game_id,
			    'parent_id' => $behaviour->id,
			    'name' => 'language',
			    'value' => 'en'
		    ];
	    }
	
	    \DB::table('settings')->insert($languages);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
