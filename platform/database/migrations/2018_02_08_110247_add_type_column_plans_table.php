<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeColumnPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('plans', function(Blueprint $table) {
		    $table->enum('type', ['shopify', 'basic', 'all'])->default('basic');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('plans', function(Blueprint $table) {
		    $table->dropColumn('type');
	    });
    }
}
