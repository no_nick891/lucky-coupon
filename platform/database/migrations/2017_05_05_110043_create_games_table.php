<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('name');
	        $table->smallInteger('active');
	        $table->enum('type', ['coupon']);
	        $table->integer('user_id');
	        $table->integer('site_id');
	        $table->integer('sort')->default(100);
	        $table->integer('impressions')->default(0);
	        $table->integer('hits')->default(0);
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('games');
    }
}
