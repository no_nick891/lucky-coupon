<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('other_subscriptions', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('user_id');
		    $table->tinyInteger('active')->default(0);
		    $table->integer('plan_id')->default(0);
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('other_subscriptions');
    }
}
