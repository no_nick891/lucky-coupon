<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubscriptionsSubscriptionGatewayFieldAddEnumValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE subscriptions CHANGE COLUMN subscription_gateway subscription_gateway ENUM('isracard', 'paypal', 'shopify')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::statement("ALTER TABLE subscriptions CHANGE COLUMN subscription_gateway subscription_gateway ENUM('isracard', 'paypal')");
    }
}
