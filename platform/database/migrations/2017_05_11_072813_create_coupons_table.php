<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('game_id');
	        $table->enum('type', ['discount', 'cash', 'free shipping']);
	        $table->decimal('value');
			$table->string('code');
			$table->decimal('chance', 6, 3);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('coupons');
    }
}
