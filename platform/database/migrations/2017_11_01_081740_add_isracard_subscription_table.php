<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsracardSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('isracard_subscriptions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->char('payme_id', 40);
			$table->integer('payme_code');
			$table->char('payme_status', 10);
			$table->integer('isracard_status');
			$table->enum('currency', ['USD', 'ILS', 'EUR']);
			$table->decimal('amount', 5, 2);
			$table->enum('period', ['day', 'week', 'month', 'year']);
			$table->text('description');
			$table->text('sub_url');
			$table->dateTime('sub_payment_date')->nullable()->default(null);
			$table->timestamps();
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('isracard_subscriptions');
	}
}
