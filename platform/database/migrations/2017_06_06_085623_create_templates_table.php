<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('templates', function (Blueprint $table) {
		    $table->increments('id');
		    $table->enum('type', ['coupon']);
		    $table->string('name');
		    $table->string('part');
		    $table->text('content');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('templates');
    }
}
