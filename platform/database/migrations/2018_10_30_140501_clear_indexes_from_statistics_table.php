<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClearIndexesFromStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statistics', function (Blueprint $table) {
            $tableIndexesCollection = collect(DB::select("SHOW INDEXES FROM statistics"))->pluck('Key_name');
            $indexesToDrop = [
                'game_id',
                'impression_date',
                'hit_date',
                'subscriber_email',
                'device_type',
                'game_id_subscriber_email',
            ];

            foreach($indexesToDrop as $indexName) {
                if ($tableIndexesCollection->contains($indexName)) {
                    $table->dropIndex($indexName);
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statistics', function (Blueprint $table) {
            $table->index('game_id', 'game_id');
            $table->index('impression_date', 'impression_date');
            $table->index('hit_date', 'hit_date');
            $table->index('subscriber_email', 'subscriber_email');
            $table->index('device_type', 'device_type');
            $table->index(['game_id', 'subscriber_email'], 'game_id_subscriber_email');
        });
    }
}
