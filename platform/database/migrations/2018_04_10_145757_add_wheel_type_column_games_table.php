<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWheelTypeColumnGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE games CHANGE COLUMN type type ENUM('coupon', 'slot', 'gift', 'wheel') NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::statement("ALTER TABLE games CHANGE COLUMN type type ENUM('coupon', 'slot', 'gift') NOT NULL");
    }
}
