<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChatchampAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('chatchamp_apps', function (Blueprint $table) {
		    $table->increments('id');
		    $table->smallInteger('active');
		    $table->integer('user_id');
		    $table->integer('site_id');
		    $table->integer('game_id');
		    $table->string('api_key')->default('');
		    $table->string('facebook_app_id')->default('');
		    $table->string('facebook_page_id')->default('');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('chatchamp_apps');
    }
}
