<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMobileCountersGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('games', function($table) {
		    $table->integer('device_impressions')->after('hits')->default(0);
		    $table->integer('device_hits')->after('device_impressions')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('games', function($table) {
		    $table->dropColumn('device_impressions');
		    $table->dropColumn('device_hits');
	    });
    }
}
