<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFreeProductTypeCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE coupons CHANGE COLUMN type type ENUM('discount', 'cash', 'free shipping', 'type', 'free product') NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::statement("ALTER TABLE coupons CHANGE COLUMN type type ENUM('discount', 'cash', 'free shipping', 'type') NOT NULL");
    }
}
