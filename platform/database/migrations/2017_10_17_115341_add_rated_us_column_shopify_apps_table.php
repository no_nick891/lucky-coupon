<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatedUsColumnShopifyAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('shopify_apps', function($table) {
		    $table->smallInteger('rated_us')->after('timestamp')->default(0);
		    $table->smallInteger('showed_rate_modal')->after('rated_us')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('shopify_apps', function($table) {
		    $table->dropColumn('rated_us');
		    $table->dropColumn('showed_rate_modal');
	    });
    }
}
