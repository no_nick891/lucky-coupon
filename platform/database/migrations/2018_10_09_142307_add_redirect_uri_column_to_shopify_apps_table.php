<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRedirectUriColumnToShopifyAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_apps', function (Blueprint $table) {
            $table->string('redirect_uri')->nullable()->default(NULL);
            $table->string('trial_days')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_apps', function (Blueprint $table) {
            $table->dropColumn('redirect_uri');
            $table->dropColumn('trial_days');
        });
    }
}
