<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopifyAppOrdersCompoundIndexGetShopifyOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_app_orders', function (Blueprint $table) {
	        $table->index(['app_id', 'order_id'], 'get_shopify_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_app_orders', function (Blueprint $table) {
            $table->dropIndex('get_shopify_orders');
        });
    }
}
