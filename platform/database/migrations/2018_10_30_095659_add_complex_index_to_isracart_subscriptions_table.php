<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComplexIndexToIsracartSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('isracard_subscriptions', function (Blueprint $table) {
            $table->index(['user_id', 'isracard_status'], 'user_id_isracard_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('isracard_subscriptions', function (Blueprint $table) {
            $table->dropIndex('user_id_isracard_status');
        });
    }
}
