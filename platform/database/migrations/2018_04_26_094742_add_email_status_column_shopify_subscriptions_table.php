<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailStatusColumnShopifySubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('shopify_subscriptions', function(Blueprint $table) {
		    $table->tinyInteger('is_emailed')->default(0)->after('shop_url');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('shopify_subscriptions', function(Blueprint $table) {
		    $table->dropColumn('is_emailed')->default(0);
	    });
    }
}
