<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('affiliates', function (Blueprint $table) {
		    $table->increments('id');
		    // get values from users table id column
		    $table->integer('owner_id');
		    $table->integer('user_id');
		    $table->integer('amount_paid');
		    $table->integer('commission');
		    $table->tinyInteger('is_payed');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('affiliates');
    }
}
