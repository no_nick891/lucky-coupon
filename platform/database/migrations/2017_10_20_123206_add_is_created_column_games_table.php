<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCreatedColumnGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('games', function($table) {
		    $table->smallInteger('is_created')->after('active')->default(0);
	    });
	    
	    \DB::table('games')->where('id', '>', 0)->update(['is_created' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('games', function($table) {
		    $table->dropColumn('is_created');
	    });
    }
}
