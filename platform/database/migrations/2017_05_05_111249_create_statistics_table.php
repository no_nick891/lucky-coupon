<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistics', function (Blueprint $table) {
	        $table->increments('id');
	        $table->integer('game_id');
	        $table->dateTime('impression_date')->nullable()->default(null);
	        $table->dateTime('hit_date')->nullable()->default(null);
	        $table->char('subscriber_email')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('statistics');
    }
}
