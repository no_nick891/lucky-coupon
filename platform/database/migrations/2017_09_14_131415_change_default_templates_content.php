<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultTemplatesContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('templates')
	        ->where('part', 'modalWrapper')
            ->where('name', 'default')
	        ->update(['content' => 'resources/assets/js/helpers/templates/default/template.js']);
        
        DB::table('templates')
	        ->where('part', 'modalContent')
	        ->where('name', 'default')
	        ->update(['content' => 'resources/assets/js/helpers/templates/default/template.js']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
