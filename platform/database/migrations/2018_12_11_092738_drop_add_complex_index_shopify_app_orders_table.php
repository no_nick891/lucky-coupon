<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAddComplexIndexShopifyAppOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_app_orders', function (Blueprint $table) {
	        $table->dropIndex('get_shopify_orders');
	        $table->index(['app_id', 'order_id', 'order_status', 'coupon_id'], 'complex_shopify_orders_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_app_orders', function (Blueprint $table) {
	        $table->dropIndex('complex_shopify_orders_index');
	        $table->index(['app_id', 'order_id'], 'get_shopify_orders');
        });
    }
}
