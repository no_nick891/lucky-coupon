<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComplexIndexToShopifySubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_subscriptions', function (Blueprint $table) {
            $table->index(['user_id', 'plan_id', 'created_at'], 'complex_shopify_subscriptions_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_subscriptions', function (Blueprint $table) {
            $table->dropIndex('complex_shopify_subscriptions_index');
        });
    }
}
