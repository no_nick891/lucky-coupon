<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('plans', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('name');
		    $table->decimal('amount', 5, 2);
		    $table->enum('currency', ['ILS', 'USD', 'EUR']);
		    $table->enum('period', ['day', 'week', 'month', 'year']);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('plans');
    }
}
