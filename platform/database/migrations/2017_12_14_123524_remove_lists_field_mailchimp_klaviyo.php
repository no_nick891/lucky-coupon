<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveListsFieldMailchimpKlaviyo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('mailchimp_apps', function($table) {
		    $table->dropColumn('lists');
	    });
	    
	    Schema::table('klaviyo_apps', function($table) {
		    $table->dropColumn('lists');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('mailchimp_apps', function($table) {
		    $table->string('lists');
	    });
	    
	    Schema::table('klaviyo_apps', function($table) {
		    $table->string('lists');
	    });
    }
}
