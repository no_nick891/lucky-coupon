<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IsracardSubColumnRenameSubStartDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	
	    if ($this->_changeColumn())
	    {
		    Schema::table('users', function(Blueprint $table) {
			    $sql = 'alter table `isracard_subscriptions` change column `sub_start_date` `sub_payment_date` datetime';
			    \DB::connection()->getPdo()->exec($sql);
		    });
	    }
	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	   
    }
	
	/**
	 * @return bool
	 */
	private function _changeColumn()
	{
		$changeColumn = true;
		
		$databaseName = config('database.connections.mysql.database');
		
		$query = "SELECT COLUMN_NAME FROM
					INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_SCHEMA = '{$databaseName}'
						AND TABLE_NAME = 'isracard_subscriptions'";
		
		$test = \DB::select($query);
		
		foreach ($test as $item)
		{
			if ($item->COLUMN_NAME === 'sub_payment_date')
			{
				$changeColumn = false;
			}
		}
		
		return $changeColumn;
	}
}
