<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeviceTypeColumnStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('statistics', function($table) {
		    $table->enum('device_type', ['pc', 'mobile'])
			    ->after('subscriber_email')
			    ->default('pc');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('statistics', function($table) {
		    $table->dropColumn('device_type');
	    });
    }
}
