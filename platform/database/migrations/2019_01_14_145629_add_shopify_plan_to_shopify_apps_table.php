<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopifyPlanToShopifyAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopify_apps', function (Blueprint $table) {
            $table->string('shopify_plan_name')->default('')->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopify_apps', function (Blueprint $table) {
            $table->dropColumn('shopify_plan_name');
        });
    }
}
