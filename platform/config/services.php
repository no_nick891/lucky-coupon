<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'shopify' => [
    	'app' => [
    		'key' => env('SHOPIFY_APP_KEY'),
    		'secret' => env('SHOPIFY_APP_SECRET')
	    ],
        'test' => [
	        'storage' => [
		        'list' => env('SHOPIFY_TEST_STORAGE_LIST')
	        ],
        	'mode' => env('SHOPIFY_CHARGE_TEST_MODE')
        ]
    ],
    
    'mailchimp' => [
    	'app' => [
		    'client_id' => env('MAILCHIMP_CLIENT_ID'),
		    'client_secret' => env('MAILCHIMP_CLIENT_SECRET')
	    ]
    ],
    
    'isracard' => [
    	'app' => [
    		'key' => env('ISRACARD_APP_KEY'),
	        'test_mode' => env('ISRACARD_APP_TEST_MODE')
	    ]
    ],
    
    'sendgrid' => [
    	'api' => [
    		'key' => env('SENDGRID_API_KEY')
	    ]
    ]
    
];
